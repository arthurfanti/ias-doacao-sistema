<?php

//Import Braspag library
require_once 'Braspag.php';

class TransacaoBraspag
{
	public $Braspag;

	public $deviceFingerPrint;

	public $Customer;
	public $CreditCard;

	public $cartao;
	public $ambiente;

	public $customer_Name;
	public $customer_ID;
	public $customer_Email;
	public $customer_cpf;
	public $customer_tempo_cliente_em_dias;

	public $order_ID;
	public $payment_CardNumber;
	public $payment_CardHolder;
	public $payment_CardMonthExpirationDate;
	public $payment_CardYearExpirationDate;
	public $payment_CardSecurityCode;
	public $payment_Currency;
	public $payment_Country;
	public $payment_Amount;
	public $payment_creditCardToken;
	public $payment_justClickAlias;

	public $billtodata_city;
	public $billtodata_country;
	public $billtodata_firstname;
	public $billtodata_lastname;
	public $billtodata_state;
	public $billtodata_street1;
	public $billtodata_street2;
	public $billtodata_postalcode;
	public $billtodata_phonenumber;
	public $billtodata_ipaddress;

	public $braspag_OrderId;
	public $recorrencia = false;

	public $braspag_transactionId;

	private $forma_pagamento;
	
	function __construct() {
		$ambiente = 'production';
		if (!PRODUCTION) {
			$ambiente = 'homologation';
		}
		$this->ambiente = $ambiente;

		$this->Braspag = new Braspag($ambiente);
		$this->Customer = new BraspagCustomerData();
		$this->CreditCard = new BraspagCreditCardModel();
	}

	public function efetuar_transacao()
	{
		$this->setarDados();

		//Execute transaction
		// $response = $this->Braspag->authorizeCreditCardTransaction($this->CreditCard, $this->Customer);
		$response = $this->Braspag->captureCreditCardTransaction($this->braspag_transactionId, $this->payment_Amount);

		if ($response->Success) {
			$this->braspag_OrderId = $response->OrderData->BraspagOrderId;
		}

		return $response;
	}

	public function salvarCartao()
	{
		$this->setarDados();
		
		$response = $this->Braspag->saveCreditCard($this->CreditCard, $this->Customer);
		return $response;
	}

	public function setarDados()
	{
		$this->Customer->setName($this->customer_Name);
		$this->Customer->setID($this->customer_ID);
		$this->Customer->setEmail($this->customer_Email);

		$this->CreditCard->setOrderId($this->order_ID);
		$this->CreditCard->setCardNumber($this->payment_CardNumber);
		$this->CreditCard->setCardHolder($this->payment_CardHolder);
		$this->CreditCard->setCardExpirationDate($this->payment_CardMonthExpirationDate, $this->payment_CardYearExpirationDate);
		$this->CreditCard->setCardSecurityCode($this->payment_CardSecurityCode);
		$this->CreditCard->setCurrency($this->payment_Currency);
		$this->CreditCard->setCountry($this->payment_Country);
		$this->CreditCard->setAmount($this->payment_Amount);
		$this->CreditCard->setPaymentPlan(BraspagCreditCardModel::PAYMENT_PLAN_FULL);
		$this->CreditCard->setNumberOfPayments(1);//3

		$this->CreditCard->setCreditCardToken($this->payment_creditCardToken);
		$this->CreditCard->setJustClickAlias($this->payment_justClickAlias);

		if ($this->recorrencia === true) {
			$this->CreditCard->setTransactionType(BraspagCreditCardModel::TRANSACTION_TYPE_RECURRENT);
		} else {
			$this->CreditCard->setTransactionType(BraspagCreditCardModel::TRANSACTION_TYPE_AUTOCAPTURE);
		}

		if (PRODUCTION) {
			if ($this->cartao == 4) {
				$this->CreditCard->setMethod(BraspagCreditCardModel::METHOD_CIELO_VISA);
			} else if ($this->cartao == 5) {
				$this->CreditCard->setMethod(BraspagCreditCardModel::METHOD_CIELO_MASTERCARD);
			} else if ($this->cartao == 6) {
				$this->CreditCard->setMethod(BraspagCreditCardModel::METHOD_CIELO_DINNERS);
			} else if ($this->cartao == 12) {
				$this->CreditCard->setMethod(BraspagCreditCardModel::METHOD_CIELO_ELO);
			} else if ($this->cartao == 13) {
				$this->CreditCard->setMethod(BraspagCreditCardModel::METHOD_CIELO_AMEX);
			}
		} else {
			$this->CreditCard->setMethod(BraspagCreditCardModel::METHOD_HOMOLOGATION);
			$this->cartao = 997;
			$this->billtodata_ipaddress = '200.155.2.221';
		}
		$this->forma_pagamento = $this->CreditCard->getMethod();
	}
	public function fraudAnalysis()
	{
		$this->setarDados();

		$requestId = $this->Braspag->generateGuid();

		if (PRODUCTION) {
			$url        = 'https://antifraude.braspag.com.br/AntiFraudeWS/AntiFraud.asmx?op=FraudAnalysis';//PRODUCAO
			// $url        = 'https://antifraude.braspag.com.br/AntiFraudeWS/AntiFraud.asmx?WSDL';//PRODUCAO
			$merchant_id = BraspagSetup::MERCHANT_ID;
		} else {
			$url        = 'https://homologacao.braspag.com.br/AntiFraudews/antifraud.asmx?op=FraudAnalysis';//HOMOLOGACAO
			$merchant_id = BraspagSetup::MERCHANT_ID_HOMOLOGATION;
		}

    $soapAction = 'http://www.braspag.com.br/antifraud/FraudAnalysis';

        // xml post structure
		$xml_post_string = '<soapenv:Envelope 	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
												xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
												xmlns:ant="http://www.braspag.com.br/antifraud/" 
												xmlns:pag="https://www.pagador.com.br/webservice/pagador">
								<soapenv:Header/> 
								<soapenv:Body>
									<ant:FraudAnalysis>
								        <ant:request>
								            <ant:RequestId>'.$requestId.'</ant:RequestId>
								            <ant:Version>1.1</ant:Version>
								            <ant:MerchantId>'.$merchant_id.'</ant:MerchantId>
								            <ant:AntiFraudSequenceType>AuthorizeAndAnalyseOnSuccess</ant:AntiFraudSequenceType>
								            <ant:DocumentData>
								            	<ant:Cpf>'.$this->customer_cpf.'</ant:Cpf>
								            </ant:DocumentData>
								            <ant:AntiFraudRequest>
								               	<ant:BillToData>
								               		<ant:CustomerId>'.$this->customer_cpf.'</ant:CustomerId> 
													<ant:City>'.$this->billtodata_city.'</ant:City> 
													<ant:Country>'.$this->billtodata_country.'</ant:Country>
													<ant:Email>'.$this->customer_Email.'</ant:Email>
													<ant:FirstName>'.$this->billtodata_firstname.'</ant:FirstName>
													<ant:LastName>'.$this->billtodata_lastname.'</ant:LastName>
													<ant:State>'.$this->billtodata_state.'</ant:State>
													<ant:Street1>'.$this->billtodata_street1.'</ant:Street1>
													<ant:Street2>'.$this->billtodata_street2.'</ant:Street2>
													<ant:PostalCode>'.$this->billtodata_postalcode.'</ant:PostalCode>
													<ant:PhoneNumber>'.$this->billtodata_phonenumber.'</ant:PhoneNumber>
													<ant:IpAddress>'.$this->billtodata_ipaddress.'</ant:IpAddress>
								               	</ant:BillToData>
								               	<ant:Comments></ant:Comments>
								               	<ant:ShipToData>
							               			<ant:City>'.$this->billtodata_city.'</ant:City> 
													<ant:Country>'.$this->billtodata_country.'</ant:Country>
													<ant:FirstName>'.$this->billtodata_firstname.'</ant:FirstName>
													<ant:LastName>'.$this->billtodata_lastname.'</ant:LastName>
													<ant:State>'.$this->billtodata_state.'</ant:State>
													<ant:Street1>'.$this->billtodata_street1.'</ant:Street1>
													<ant:Street2>'.$this->billtodata_street2.'</ant:Street2>
													<ant:PostalCode>'.$this->billtodata_postalcode.'</ant:PostalCode>
													<ant:PhoneNumber>'.$this->billtodata_phonenumber.'</ant:PhoneNumber>
													<ant:ShippingMethod>Other</ant:ShippingMethod>
								               	</ant:ShipToData>
								               	<ant:DeviceFingerPrintId>'.$this->deviceFingerPrint.'</ant:DeviceFingerPrintId>
								               	<ant:AdditionalDataCollection>
													<ant:AdditionalData>
														<ant:Id>1</ant:Id>
														<ant:Value>Sim</ant:Value>
													</ant:AdditionalData>
													<ant:AdditionalData>
														<ant:Id>2</ant:Id>
														<ant:Value>'.$this->customer_tempo_cliente_em_dias.'</ant:Value>
													</ant:AdditionalData>
													<ant:AdditionalData>
														<ant:Id>3</ant:Id>
														<ant:Value>1</ant:Value>
													</ant:AdditionalData>
													<ant:AdditionalData>
														<ant:Id>4</ant:Id>
														<ant:Value>Web</ant:Value>
													</ant:AdditionalData>
												</ant:AdditionalDataCollection>
								               	<ant:CardData>
													<ant:AccountNumber>'.$this->payment_CardNumber.'</ant:AccountNumber>
													<ant:Card>Visa</ant:Card>
													<ant:ExpirationMonth>'.$this->payment_CardMonthExpirationDate.'</ant:ExpirationMonth>
													<ant:ExpirationYear>'.$this->payment_CardYearExpirationDate.'</ant:ExpirationYear>
												</ant:CardData> 
								               	<ant:ItemDataCollection>
								                  <ant:ItemData>
								                     <ant:ProductData>
								                     	<ant:Name>Doação - Instituto Ayrton Senna</ant:Name>
								                        <ant:Sku>1</ant:Sku>
								                        <ant:Quantity>1</ant:Quantity>
								                        <ant:UnitPrice>'.$this->payment_AmountFormatado.'</ant:UnitPrice>
								                     </ant:ProductData>
								                  </ant:ItemData>
								                </ant:ItemDataCollection>
								                <ant:PurchaseTotalsData>
								            		<ant:Currency>BRL</ant:Currency>
								            		<ant:GrandTotalAmount>'.$this->payment_AmountFormatado.'</ant:GrandTotalAmount>
								         		</ant:PurchaseTotalsData>
								                <ant:MerchantReferenceCode>'.$this->order_ID.'</ant:MerchantReferenceCode>
								            </ant:AntiFraudRequest>
								            <ant:AuthorizeTransactionRequest xmlns="https://www.pagador.com.br/webservice/pagador">
												<pag:RequestId>'.$requestId.'</pag:RequestId>
												<pag:Version>1.0</pag:Version>
												<pag:OrderData>
													<pag:MerchantId>'.$merchant_id.'</pag:MerchantId>
													<pag:OrderId>'.$this->order_ID.'</pag:OrderId>
												</pag:OrderData>
												<pag:CustomerData>
													<pag:CustomerIdentity>'.$this->customer_ID.'</pag:CustomerIdentity>
													<pag:CustomerName>'.$this->customer_Name.'</pag:CustomerName>
												</pag:CustomerData>
												<pag:PaymentDataCollection>
													<pag:PaymentDataRequest xsi:type="CreditCardDataRequest">                    
														<pag:PaymentMethod>'.$this->forma_pagamento.'</pag:PaymentMethod>
														<pag:Amount>'.$this->payment_Amount.'</pag:Amount>
														<pag:Currency>BRL</pag:Currency>
														<pag:Country>BRA</pag:Country>
														<pag:ServiceTaxAmount>0</pag:ServiceTaxAmount>
														<pag:NumberOfPayments>1</pag:NumberOfPayments>	
														<pag:PaymentPlan>0</pag:PaymentPlan>	
														<pag:TransactionType>1</pag:TransactionType>	
														<pag:CardHolder>'.$this->payment_CardHolder.'</pag:CardHolder>	
														<pag:CardNumber>'.$this->payment_CardNumber.'</pag:CardNumber>
														<pag:CardSecurityCode>'.$this->payment_CardSecurityCode.'</pag:CardSecurityCode>	
														<pag:CardExpirationDate>'.$this->payment_CardMonthExpirationDate.'/'.$this->payment_CardYearExpirationDate.'</pag:CardExpirationDate>
													</pag:PaymentDataRequest>
												</pag:PaymentDataCollection>
											</ant:AuthorizeTransactionRequest>
								        </ant:request>
								    </ant:FraudAnalysis>
								</soapenv:Body>
							</soapenv:Envelope>';
		$retorno = $this->enviar($xml_post_string, $url, $soapAction);
		
		return $retorno;
	}

	private function enviar($data_string, $url, $soapAction)
	{
		$headers = array(
			"Content-type: text/xml;charset=\"utf-8\"",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: " . $soapAction,
			"Content-length: ".strlen($data_string),
		);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // converting
    $response = curl_exec($ch); 
    curl_close($ch);

    // converting
    $response2 = str_replace(array("<soap:Body>", "</soap:Body>"), array("", ""), $response);

    // convertingc to XML
    $parser = simplexml_load_string($response2);

    return $parser;
	}
}