<?php

/**
 * BraspagSetup
 * Contains settings to Braspag class
 *
 * @author Robson Morais (r.morais@isharelife.com.br)
 * @link http://www.isharelife.com.br/
 * @version $ID$
 * @package opensource
 * @copyright Copyright © 2010-2012 iShareLife
 * @license Apache License, Version 2.0
 *
 */

class BraspagSetup {

	const URL_TRANSACTION_HOMOLOGATION = "https://homologacao.pagador.com.br/webservice/pagadorTransaction.asmx?wsdl";
	const URL_ORDER_HOMOLOGATION = "https://homologacao.pagador.com.br/pagador/webservice/pedido.asmx?wsdl";
	const URL_JUSTCLICK_HOMOLOGATION = "https://homologacao.braspag.com.br/services/testenvironment/cartaoprotegido.asmx?wsdl";

	const URL_TRANSACTION = "https://www.pagador.com.br/webservice/pagadorTransaction.asmx?wsdl";
	const URL_ORDER = "https://query.pagador.com.br/webservices/pagador/pedido.asmx?wsdl";
	const URL_JUSTCLICK = "https://cartaoprotegido.braspag.com.br/services/v2/cartaoprotegido.asmx?wsdl";

	const MERCHANT_ID_HOMOLOGATION = '{9082D514-1CA3-E411-941D-0050569343A3}';//HOMOLOGACAO
	const MERCHANT_KEY_HOMOLOGATION = '{4F1E8CDC-C590-4913-BB7B-0E34C1E9C759}';//HOMOLOGACAO
	const MERCHANT_ID = '{E7931DD2-F9B8-E411-93FC-005056932B77}';//PRODUCAO
	const MERCHANT_KEY = '{D147D5BE-8C62-44EC-A230-1CD8FBE403D2}';//PRODUCAO

	const VERSION = '1.0';
}
