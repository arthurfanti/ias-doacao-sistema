<?php
class DoadorEndereco extends AppModel
{
	public $belongsTo = ['Doador', 'Endereco'];

	public $validate = array(
		'cep' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe o CEP.'
			),
		),
		'logradouro' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe o nome da rua.'
			),
		),
		'numero' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe o nome da rua.'
			),
		),
	);
}