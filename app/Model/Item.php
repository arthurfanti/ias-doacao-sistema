<?php
class Item extends AppModel
{
	public $belongsTo = ['Promocao'];
	public $hasMany = ['Atributo', 'Brinde'];

	public $actsAs = array(
		'Upload.Upload' => array('imagem')
  );
}