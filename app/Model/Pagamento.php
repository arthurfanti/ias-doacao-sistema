<?php
class Pagamento extends AppModel
{
	public $belongsTo = ['Banco', 'FormasPagamento', 'DiaDebito'];
	public $hasOne = ['Doacao'];

	public $actsAs = array(
		'LogAction.LogAction' => array(
      'fields' => array('banco_id', 'formas_pagamento_id', 'agencia', 'conta', 'dia_debito_id', 'cartao_validade', 'cartao_final', 'cvv'),
      'trackDelete' => false
    )
	);
}