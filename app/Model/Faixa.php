<?php
App::uses('AppModel', 'Model');
class Faixa extends AppModel {
	public $hasMany = ['DpjComplemento'];

	public $displayField = 'faixa';

	// public $virtualFields = array(
 //    'faixa' => 'CONCAT("Entre ", Faixa.faixa_inicial, " e ", Faixa.faixa_final)'
	// );
}