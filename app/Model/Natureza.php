<?php
App::uses('AppModel', 'Model');
class Natureza extends AppModel {
	public $hasMany = ['DpjComplemento'];

	public $displayField = 'codigo_descricao';

	public $virtualFields = array(
    	'codigo_descricao' => 'CONCAT(Natureza.codigo, " - ", Natureza.descricao)'
	);
}