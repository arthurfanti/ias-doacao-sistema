<?php
class Assunto extends AppModel
{
	public $hasMany = ['FaleConosco'];

	public $actsAs = array(
        'Translate' => array(
        	'titulo'
        )
    );
	
	public $translateModel = 'AssuntoI18n';
	public $translateTable = 'assuntos_i18n';
}