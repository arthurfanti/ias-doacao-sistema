<?php
class Doador extends AppModel
{
	public $belongsTo = ['Conheceu', 'Contato', 'Pessoa', 'User'];
	public $hasOne = ['DpjComplemento'];
	public $hasMany = ['Doacao', 'DoadorEmail', 'DoadorEndereco', 'DoadorTelefone', 'Indicacao', 'FaleConosco'];

	public $validate = array(
		'nome' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe o nome.'
			),
		),
		'sobrenome' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe o sobrenome.'
			),
		),
		'nascimento' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe a data de nascimento'
			),
			'date' => array(
				'rule' => array('date', 'ymd'),
				'message' => 'Informe a data de nascimento corretamente'
			)
		),
		'cpf_cnpj' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Este CPF/CNPJ já está cadastrado'
			)
		),
	);

	public $validateDpj = array(
		'sobrenome' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe a Razão Social'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Razão Social já cadastrada'
			),
		),
		'cpf_cnpj' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Este CNPJ já está cadastrado'
			)
		),
	);

	public $actsAs = array(
		'CakePtbr.AjusteData' => 'nascimento',
		'LogAction.LogAction' => array(
	        'fields' => array('nome', 'sobrenome', 'cpf_cnpj', 'rg', 'nascimento'),
	        'trackDelete' => false
	    )
	);
}