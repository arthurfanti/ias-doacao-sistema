<?php
class Aceite extends AppModel
{
	public $belongsTo = ['FormasPagamento', 'Pessoa'];
	public $hasMany = ['AceiteDoacao'];

	public $actsAs = array(
        'Translate' => array(
        	'titulo', 'termo'
        )
    );
	
	public $translateModel = 'AceiteI18n';
	public $translateTable = 'aceites_i18n';
}