<?php
class Configuracao extends AppModel
{
	public $actsAs = array(
		'Upload.Upload' => array(
			'background_site_dpf' => [
				'thumbnailSizes' => [
					'big'    => '1920x1080',
					'medium' => '1440x900',
					'tablet' => '1240x768',
					'mobile' => '380x752'
				],
				'thumbnailMethod'  => 'php',
				'extensions' => ['jpeg', 'jpg', 'png']
			],
			'background_site_dpj' => [
				'thumbnailSizes' => [
					'big'    => '1920x1080',
					'medium' => '1440x900',
					'tablet' => '1240x768',
					'mobile' => '380x752'
				],
				'thumbnailMethod'  => 'php',
				'extensions' => ['jpeg', 'jpg', 'png']
			]
		)
	);

	// public $validate = [
	// 	'background_site_dpf' => [
	// 		'required' => [
	// 			'rule' => ['notEmpty'],
	// 			'message' => 'Selecione uma Imagem'
	// 		]
	// 	],
	// 	'background_site_dpj' => [
	// 		'required' => [
	// 			'rule' => ['notEmpty'],
	// 			'message' => 'Selecione uma Imagem'
	// 		]
	// 	],
	// ];
}