<?php
class FormasPagamento extends AppModel
{
	public $belongsTo = ['Banco', 'Mensagem'];
	public $hasMany = ['Pagamento', 'Transacao'];
	public $hasOne = ['Aceite', 'Parcela'];

	public $actsAs = array(
		'Upload.Upload' => array('logo')
	);
}