<?php
class Parcela extends AppModel
{
	public $belongsTo = ['Doacao', 'FormasPagamento', 'Status'];
	public $hasMany = ['Tentativa', 'Transacao'];

	public $actsAs = array(
		'CakePtbr.AjusteData' => 'data_pagamento',
		'CakePtbr.AjusteFloat'
	);
}