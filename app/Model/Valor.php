<?php
class Valor extends AppModel
{
	public $belongsTo = ['Pessoa'];
	
	public $displayField = 'valor';
	
	public $actsAs = array(
		'CakePtbr.AjusteFloat',
		'Translate' => array(
        	'texto_antes', 'texto_depois'
        )
	);
	
	public $translateModel = 'ValorI18n';
	public $translateTable = 'valores_i18n';
}