<?php
class Campanha extends AppModel
{
	public $hasMany = ['Doacao'];
	
	public $actsAs = array(
		'CakePtbr.AjusteData' => ['data_inicial', 'data_final'],
		'Upload.Upload' => ['imagem']
	);
}