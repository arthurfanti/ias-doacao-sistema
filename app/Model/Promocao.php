<?php
class Promocao extends AppModel
{
	public $hasMany = ['Item'];

	public $displayField = 'titulo';

	public $actsAs = array(
		'CakePtbr.AjusteData' => ['data_inicial', 'data_final']
	);	
}