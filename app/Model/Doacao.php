<?php
class Doacao extends AppModel
{
	public $belongsTo = ['Campanha', 'Doador', 'Pagamento', 'Periodo'];
	public $hasMany = ['Parcela'];
	public $hasOne = ['AceiteDoacao', 'Brinde', 'CancelamentoDoacao'];

	public $actsAs = array(
		'CakePtbr.AjusteFloat',
		'LogAction.LogAction' => array(
	        'fields' => array('valor', 'status'),
	        'trackDelete' => false
	    )
	);
}