<?php
App::uses('AppModel', 'Model');

class DpjComplemento extends AppModel {
	public $belongsTo = ['Doador', 'Faixa', 'Natureza'];
}