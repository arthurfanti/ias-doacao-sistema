<?php
class Mensagem extends AppModel
{
	public $hasMany = array('FormasPagamento');

	public $displayField = 'mensagem';
	
	public $actsAs = array(
        'Translate' => array(
        	'mensagem', 'descricao'
        )
    );
	
	public $translateModel = 'MensagemI18n';
	public $translateTable = 'mensagens_i18n';
}