<?php
class DoadorEmail extends AppModel
{
	public $belongsTo = ['Doador', 'Email'];

	public $validate = array(
		'email' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Este email já está cadastrado',
				'on' => 'create'
			)
		),
	);

	public $actsAs = array(
		'LogAction.LogAction' => array(
	        'fields' => array('email'),
	        'trackDelete' => false
	    )
	);
}