<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'doacoes', 'action' => 'inicio'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	// Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

// App::uses('I18nRoute', 'I18n.Routing/Route');
// Router::connect('/',
//     array('controller' => 'pages', 'action' => 'display', 'home'),
//     array('routeClass' => 'I18nRoute')
// );
// Router::connect('/pages/*',
//     array('controller' => 'pages', 'action' => 'display'),
//     array('routeClass' => 'I18nRoute')
// );
Router::parseExtensions('xml');

Router::connect('/soap/*', array('controller' => 'paginas', 'action' => 'xmlgen'), array('pass' => array('soapAction', 'full')));

Router::connect('/users', array('plugin' => 'users', 'controller' => 'users'));
Router::connect('/users/index/*', array('plugin' => 'users', 'controller' => 'users'));
Router::connect('/users/:action/*', array('plugin' => 'users', 'controller' => 'users'));
Router::connect('/users/users/:action/*', array('plugin' => 'users', 'controller' => 'users'));
Router::connect('/login', array('plugin' => 'users', 'controller' => 'users', 'action' => 'login'));
Router::connect('/logout', array('plugin' => 'users', 'controller' => 'users', 'action' => 'logout'));
Router::connect('/nova-senha', array('plugin' => 'users', 'controller' => 'users', 'action' => 'reset_password'));
Router::connect('/nova-senha/:token', array('plugin' => 'users', 'controller' => 'users', 'action' => 'reset_password'));
// Router::connect('/register', array('plugin' => 'users', 'controller' => 'users', 'action' => 'add'));

Router::connect('/admin', array('plugin' => 'users', 'controller' => 'users', 'action' => 'login'));

Router::connect('/valores', array('controller' => 'doacoes', 'action' => 'valores'));
Router::connect('/doacao', array('controller' => 'doacoes', 'action' => 'doacao'));
Router::connect('/complemento', array('controller' => 'doacoes', 'action' => 'complemento'));
Router::connect('/agradecimento', array('controller' => 'doacoes', 'action' => 'agradecimento'));
Router::connect('/conta', array('controller' => 'doadores', 'action' => 'conta'));
Router::connect('/doacoes/:doacao_id', array('controller' => 'doacoes', 'action' => 'doacoes'));
Router::connect('/dados', array('controller' => 'doadores', 'action' => 'dados'));
Router::connect('/idioma/:lang', array('controller' => 'paginas', 'action' => 'mudar_idioma'), array('lang'));
Router::connect('/boleto/:parcela_id', array('controller' => 'paginas', 'action' => 'boleto'));
Router::connect('/alterar_boletos', array('controller' => 'doacoes', 'action' => 'alterar_boletos'));
Router::connect('/manutencao', array('controller' => 'paginas', 'action' => 'manutencao'));

Router::connect('/admin/logs', array('plugin' => 'LogAction', 'controller' => 'logs', 'admin' => true));

Router::connect('/relatorios', array('controller' => 'relatorios', 'action' => 'index'));
Router::connect('/relatorios/excel', array('controller' => 'relatorios', 'action' => 'excel'));
// Router::connect('/doacao', array('controller' => 'paginas', 'action' => 'doacao'));
// Router::connect('/salvarvalores', array('controller' => 'paginas', 'action' => 'salvarvalores'));
// Router::connect('/efetuardoacao', array('controller' => 'paginas', 'action' => 'efetuardoacao'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
