<section role="forms">
	<div id="login">
		<div class="row">
			<div class="small-8 small-centered columns">
				<h2><?= __('Não recebeu o nosso e-mail de confirmação?') ?></h2>
				<p><?= __('Insira seu e-mail abaixo para reenviar') ?>.</p>
			</div>
		</div>
		<div class="row">
			<div class="small-8 small-centered columns">
				<?php echo $this->Form->create($model, array( 'url' => array('admin' => false, 'action' => 'resend_verification'), 'data-abide' => 'data-abide'));?>
					<fieldset>
						<div class="row">
							<div class="small-12 columns">
								<?php echo $this->Form->input('email', array('label' => __('Insira seu e-mail cadastrado', 'div' => false))); ?>
								<small class="error"><?= __('Preencha seu email de cadastro') ?>.</small>
						</div>
						</div>
													
						<div class="clearfix">&nbsp;</div>

						<div class="row">
							<div class="small-12 columns text-center">
								<?php	echo $this->Form->submit( __('Enviar'), array('class' => 'button success small')); ?>
							</div>
						</div>
					</fieldset>
				<?php	echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</section>