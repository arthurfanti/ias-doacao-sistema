<section role="forms">
	<div id="login">
		<div class="row">
			<div class="small-8 small-centered columns">
				<h2><?= __('Redefinição de senha') ?></h2>
				<p><?= __('Digite abaixo a nova senha de sua escolha') ?>.</p>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<div class="row">
			<div class="small-8 small-centered columns">
				<?php	echo $this->Form->create($model, array( 'url' => array('action' => 'reset_password', $token), 'data-abide' => 'data-abide'));	?>
					<?php	echo $this->Form->hidden('token', array('value' => $token)); ?>
					<fieldset>
						<div class="row">
							<div class="small-12 columns">
								<?php	echo $this->Form->input('new_password', array('label' => __('Nova senha'), 'type' => 'password', 'required', 'div' => false));?>
								<small class="error"><?= __('Insira uma nova senha') ?>.</small>
							</div>

							<div class="small-12 columns">
								<?php echo $this->Form->input('confirm_password', array('label' => __('Confirme a nova senha'), 'type' => 'password', 'required', 'div' => false));?>
								<small class="error"><?= __('Repita a senha') ?>.</small>
							</div>
						</div>
													
						<div class="clearfix">&nbsp;</div>

						<div class="row">
							<div class="small-12 columns text-center">
								<?php	echo $this->Form->submit( __('Enviar'), array('class' => 'button success small')); ?>
							</div>
						</div>
					</fieldset>
				<?php	echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</section>
