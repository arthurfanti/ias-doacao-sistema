<div class="users form">
	<?php echo $this->Form->create($model); ?>
		<fieldset>
			<legend>Editar Usuário</legend>
			<?php
				echo $this->Form->input('id');
				echo $this->Form->input('username', array('label' => 'Nome', 'readonly'));
				echo $this->Form->input('email', array('label' => 'Email', 'readonly'));
				if (!empty($roles)) {
					echo $this->Form->input('role', array(
						'label' => __d('users', 'Role'), 'values' => $roles));
				}
				// echo $this->Form->input('is_admin', array('label' => 'Perfil Administrativo'));
				echo $this->Form->input('active', array('label' => 'Ativo'));
			?>
		</fieldset>
		<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class' => 'right')) ?>
	<?php echo $this->Form->end(); ?>
</div>