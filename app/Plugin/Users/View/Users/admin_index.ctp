<article id="list">
	<div class="row">
		<div class="small-12 column">
			<table class="display responsive nowrap">
				<thead>
					<tr>
						<th></th>
						<th>Data</th>
						<th>Nome</th>
						<th>Email</th>
						<th>Status</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th>Data</th>
						<th>Nome</th>
						<th>Email</th>
						<th>Status</th>
						<th>Ações</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($users as $user): ?>
					<tr>
						<td></td>
							<td><?php echo date('d-m-Y', strtotime($user[$model]['created'])); ?></td>
							<td><?php echo $user[$model]['username']; ?></td>
							<td><?php echo $user[$model]['email']; ?></td>
							<td><?php echo $user[$model]['active'] == 1 ? 'Ativo' : ''; ?></td>
							<td>
								<?php echo $this->Html->link(
									$this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o']) . ' Editar',
									array('action' => 'edit', $user[$model]['id']),
									array('class' => 'button radius tiny info', 'style' => 'margin-bottom:0', 'escape' => false)
								); ?>
							</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</article>

<?php
	echo $this->Html->scriptBlock(
		"$('table.responsive.nowrap').DataTable({
			responsive: {
				details: {
					type: 'inline',
					target: 'tr'
				}
			},
			columnDefs: [{
				className: 'control',
				orderable: false,
				targets: 0
			}],
			order: [1, 'asc'],
			pageLength: 50,
			language: {
				processing:     'processando dados...',
				search:         '',
				lengthMenu:     'Itens por p&aacute;gina: _MENU_',
				info:           'Exibindo itens de _START_ a _END_, num total de _TOTAL_ itens',
				infoEmpty:      'Não há itens para exibir',
				infoFiltered:   '(filtrado de _MAX_ itens no total)',
				infoPostFix:    '',
				loadingRecords: 'carregando dados...',
				zeroRecords:    'Não há itens para exibir',
				paginate: {
					first:      'Primeira',
					previous:   'Anterior',
					next:       'Seguinte',
					last:       '&uacute;ltima'
				},
				aria: {
					sortAscending:  ': habilite para classificar a coluna em ordem crescente',
					sortDescending: ': habilite para classificar a coluna em ordem decrescente'
				}
			}
		});

		$('.dataTables_filter input').attr('placeholder', 'Buscar');",
		array('inline' => false));
?>