<?php
/**
 * Copyright 2010 - 2014, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2014, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="users form">
	<h2><?php echo __d('users', 'Add User'); ?></h2>
	<fieldset>
		<?php
			echo $this->Form->create($model);

			echo $this->Form->input('Doador.nacionalidade', array('options' => $nacionalidades, 'selected' => 19));
			echo $this->Form->input('Doador.conta_brasil', array('type' => 'checkbox', 'checked' => true, 'legend' => 'Tem conta em algum banco no Brasil?'));

			echo $this->Form->input('Doador.nome');
			echo $this->Form->input('Doador.sobrenome');
			echo $this->Form->input('Doador.email');
			echo $this->Form->input('Doador.tempemail', array('label' => 'Confirme seu email'));
			echo $this->Form->input('User.password', array('type' => 'password'));
			echo $this->Form->input('User.temppassword', array('type' => 'password', 'label' => 'Confirme sua senha'));
			echo $this->Form->hidden('DoadorTelefone.0.telefone_id', array('value' => $telefone_id));
			echo $this->Form->input('DoadorTelefone.0.numero');
			echo $this->Form->input('Doador.cpf_cnpj');


			// echo $this->Form->input('username', array(
			// 	'label' => __d('users', 'Username')));
			// echo $this->Form->input('email', array(
			// 	'label' => __d('users', 'E-mail (used as login)'),
			// 	'error' => array('isValid' => __d('users', 'Must be a valid email address'),
			// 	'isUnique' => __d('users', 'An account with that email already exists'))));
			// echo $this->Form->input('password', array(
			// 	'label' => __d('users', 'Password'),
			// 	'type' => 'password'));
			// echo $this->Form->input('temppassword', array(
			// 	'label' => __d('users', 'Password (confirm)'),
			// 	'type' => 'password'));
			$tosLink = $this->Html->link(__d('users', 'Terms of Service'), array('controller' => 'pages', 'action' => 'tos', 'plugin' => null));
			// echo $this->Form->input('tos', array(
				// 'label' => __d('users', 'I have read and agreed to ') . $tosLink));
			echo $this->Form->end(__d('users', 'Submit'));
		?>
	</fieldset>
</div>
<?php echo $this->element('Users.Users/sidebar'); ?>
