<section role="forms">
	<div id="login">
		<div class="row">
			<div class="small-12 text-center">
				<h2><?= __('Esqueceu sua senha?') ?></h2>
				<p><?= __('Digite abaixo seu e-mail cadastrado para receber o link de redefinição de senha') ?>.</p>
				<?php echo $this->Session->flash();?>
			</div>
		</div>
		<div class="row">
			<div class="small-8 small-centered columns">
				<?php	echo $this->Form->create($model, array('url' => array('admin' => false, 'action' => 'reset_password'), 'data-abide' => 'data-abide')); ?>
					<fieldset>
						<div class="row">
							<div class="small-12 columns">
								<?php echo $this->Form->input('email', array('label' => __('E-mail cadastrado'), 'required', 'div' => false)); ?>
								<small class="error"><?= __('Preencha com seu email de cadastro') ?>.</small>
							</div>
						</div>
													
						<div class="clearfix">&nbsp;</div>

						<div class="row">
							<div class="small-12 columns text-center">
								<?php	echo $this->Form->submit( __('Redefinir senha'), array('class' => 'button success small')); ?>
							</div>
						</div>
					</fieldset>
				<?php	echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</section>