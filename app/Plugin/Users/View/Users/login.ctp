<section role="forms">
	<div id="login">
		<?php if ($this->params->url !== 'admin'): ?>
			<div class="row">
				<div class="small-7 medium-3 small-centered columns">
					<center>
						<ul style="display:inline-block" class="sub-nav donation-steps">
							<li><a disabled href="#" class="button round active">1</a></li>
							<li><a disabled href="#" class="button round">2</a></li>
						</ul>
					</center>
				</div>
			</div>
			<div class="row">
				<div class="small-12 text-center">
					<p><?= __('Para prosseguir, insira seu cadastro') ?></p>
					<?php echo $this->Session->flash();?>
				</div>
			</div>
		<?php else: ?>
			<div class="row">
				<div class="small-12 text-center">
					<?php echo $this->Html->image('/images/icone_adm_login.png'); ?>
					<p><?= __('Acesso a Área Administrativa, insira seus dados') ?></p>
					<?php echo $this->Session->flash();?>
				</div>
			</div>
		<?php endif ?>

		<div class="row small-collapse">
			<div class="small-12 medium-8 medium-centered columns">
				<?php echo $this->Form->create('User', array('id' => 'formLogin', 'data-abide' => 'data-abide')) ?>
					<fieldset>
						<div class="row">
							<div class="small-12 columns">
								<label><?= __('Email') ?></label>
								<?php echo $this->Form->input('User.email', array('placeholder' => __('Email cadastrado'), 'type' => 'email', 'required' => true, 'label' => false, 'div' => false)) ?>
								<small class="error"><?= __('Preencha seu email de cadastro') ?>.</small>
							</div>
						</div>

						<div class="row">
							<div class="small-12 columns">
								<label><?= __('Senha') ?></label>
								<?php echo $this->Form->input('User.password', array('placeholder' => __('Senha cadastrado'), 'type' => 'password', 'required' => true, 'label' => false, 'div' => false)) ?>
								<small class="error"><?= __('Senha é obrigatória!') ?></small>
							</div>

							<div class="small-12 columns">
								<?php echo $this->Html->link( __('Esqueci minha senha'), '/nova-senha') ?>
								<br><?= __('Ainda não é cadastrado?') ?> <?php echo $this->Html->link( __('Clique aqui'), '/doacao') ?> <?= __('para se cadastrar como doador.') ?>
							</div>
						</div>

						<div class="clearfix">&nbsp;</div>

						<div class="row">
							<div class="small-12 columns text-center">
								<button type="submit" class="button alert">
									<?= __('Continuar') ?>&nbsp;<i class="sprite-check"></i>
								</button>
							</div>
						</div>
					</fieldset>
				<?php echo $this->Form->end() ?>
			</div>
		</div>
	</div>
</section>