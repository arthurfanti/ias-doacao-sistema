<?php echo __('Recebemos uma solicitação para redefinir sua senha de acesso à sua conta. Para alterá-la, clique no link abaixo.'); ?>
<br />
<?php echo Router::url('/nova-senha/'.$token, true); ?>
<br />
<?php echo __('Caso não tenha feito esta solicitação, ignore esta mensagem.'); ?>
<br />
<?php echo __('Um grande abraço,'); ?>
<br />
<?php echo __('Equipe Instituto Ayrton Senna.'); ?>


