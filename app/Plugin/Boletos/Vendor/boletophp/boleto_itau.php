<?php
// +----------------------------------------------------------------------+
// | BoletoPhp - Vers?o Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo est? dispon?vel sob a Licen?a GPL dispon?vel pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Voc? deve ter recebido uma c?pia da GNU Public License junto com     |
// | esse pacote; se n?o, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colabora??es de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de Jo?o Prado Maia e Pablo Martins F. Costa				        |
// | 														                                   			  |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordena??o Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto Ita?: Glauber Portella                        |
// +----------------------------------------------------------------------+


// ------------------------- DADOS DIN?MICOS DO SEU CLIENTE PARA A GERA??O DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formul?rio c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 5;
// $taxa_boleto = 2.95;
$taxa_boleto = 0;
// $data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006"; 
// $valor_cobrado = "2950,00"; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = $dadosboleto['valor_cobrado'];
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

// $dadosboleto["nosso_numero"] = '12345678';  // Nosso numero - REGRA: M?ximo de 8 caracteres!
// $dadosboleto["numero_documento"] = '0123';	// Num do pedido ou nosso numero
// $dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emiss?o do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com v?rgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
// $dadosboleto["sacado"] = "Nome do seu Cliente";
// $dadosboleto["endereco1"] = "Endereço do seu Cliente";
// $dadosboleto["endereco2"] = "Cidade - Estado -  CEP: 00000-000";

// INFORMACOES PARA O CLIENTE
// $dadosboleto["demonstrativo1"] = "";
// $dadosboleto["demonstrativo2"] = "";
// $dadosboleto["demonstrativo3"] = "";
$dadosboleto["instrucoes1"] = "Obrigado pela sua doação. <br>";
$dadosboleto["instrucoes2"] = "Pagável em qualquer banco, mesmo após o vencimento.<br>";
$dadosboleto["instrucoes3"] = "Este boleto não é registrado, o não pagamento não acarreta nenhum tipo de multa, juros, protesto, cobranças judiciais ou inclusão em cadastros de restrição ao crédito.<br>";
$dadosboleto["instrucoes4"] = "Para mais informações, entre em contato conosco via e-mail: doacao@ias.org.br";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "N";
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "OU";


// ---------------------- DADOS FIXOS DE CONFIGURA??O DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - ITA?
$dadosboleto["agencia"] = "8462"; // Num da agencia, sem digito
$dadosboleto["conta"] = "21004";	// Num da conta, sem digito
$dadosboleto["conta_dv"] = "1"; 	// Digito do Num da conta

// DADOS PERSONALIZADOS - ITA?
$dadosboleto["carteira"] = "175";  // C?digo da Carteira: pode ser 175, 174, 104, 109, 178, ou 157

// SEUS DADOS
$dadosboleto["identificacao"] = "IAS";
$dadosboleto["cpf_cnpj"] = "00.328.072/0001-62";
$dadosboleto["endereco"] = "Coloque o endereço da sua empresa aqui";
$dadosboleto["cidade_uf"] = "Cidade / Estado";
$dadosboleto["cedente"] = "Instituto Ayrton Senna - 00.328.072/0001-62";

// N?O ALTERAR!
include("include/funcoes_itau.php"); 
include("include/layout_itau.php");
?>
