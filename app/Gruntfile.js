'use strict';

module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		app: 'dev',
		dist: 'dist',
		webroot: 'webroot',

		sass: {
			options: {
				includePaths: ['<%= app %>/bower_components/foundation/scss'],
				sourcemap: true
			},
			dist: {
				options: {
					outputStyle: 'extended'
				},
				files: {
					'<%= app %>/css/app.css': '<%= app %>/scss/app.scss'
				}
			}
		},

		clean: {
			dist: {
				src: ['<%= webroot %>/css/app.min.css', '<%= webroot %>/css/app.css']
			},
		},

    cssmin: {
        mini: {
            src: '<%= app %>/css/app.css',
            dest: '<%= webroot %>/css/app.min.css'
        }
    },

		watch: {
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['sass']
			},
			sass: {
				files: '<%= app %>/scss/**/*.scss',
				tasks: ['sass', 'bake-my-cake']
			}
		},

	});
	
	grunt.registerTask('bake-my-cake', ['sass', 'clean', 'cssmin']);

	grunt.registerTask('compile-sass', ['sass']);
	grunt.registerTask('default', ['watch']);

};
