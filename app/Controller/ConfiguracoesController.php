<?php

class ConfiguracoesController extends AppController {

	public $model = 'Configuracao';
	public $title = 'Configurações';

	public function admin_index()
	{
		// $this->set(compact('data'));
		if ($this->request->is(['post', 'put'])) {
			// debug($this->request->data);die;
			if($this->Configuracao->save($this->request->data)){
				$this->Geral->setFlash('Configurações alteradas com sucesso', true);
				$this->redirect(['action' => 'index']);
			}
		} else {
			$data = $this->Configuracao->find('first');
			$this->data = $data;
		}
	}
}