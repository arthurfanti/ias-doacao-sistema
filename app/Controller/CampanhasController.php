<?php
class CampanhasController extends AppController
{
	public $title = "Campanhas"	;

	public function admin_index()
	{
		$this->set('campanhas', $this->Campanha->find('all', ['recursive' => -1]));
	}

	public function admin_adicionar()
	{
		if (!empty($this->data)) {
			if ($this->Campanha->save($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}
	}

	public function admin_alterar($campanha_id = null)
	{
		if (!$campanha_id)
			$this->redirect(array('action' => 'index'));

		if (empty($this->data)) {
			$this->data = $this->Campanha->find('first', [
				'conditions' => ['Campanha.id' => $campanha_id],
				'recursive' => -1
			]);
		} else {
			if ($this->Campanha->save($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}
	}
}