<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	protected $_tagSession = 'Doacao';

	public $components = array(
		'Auth',
		'Geral',
		'Session'
	);

	public $helpers = array(
		'Form' => array('className' =>'CustomForm'),
		'Html',
		'Session'
	);

	public function beforeFilter()
	{
		parent::beforeFilter();

		// print_r($this->request);die;
		// if ($this->request->params['controller'] == 'paginas') {
		// 	// echo 'ok';
		// } else {
		// 	$this->redirect('/manutencao');die;
		// }

		if ($this->Session->check('Config.language')) {
			Configure::write('Config.language', $this->Session->read('Config.language'));
		} else {
			Configure::write('Config.language', 'por');
			$this->Session->write('Config.language', 'por');
		}

		if ($this->isPrefix('admin')) {
			// se o usuário não for admin redireciona para a página inicial
			if (!$this->isAdmin()) {
				$this->redirect('/');
			}

			//verifica se o controller tem a action admin_adicionar, se tiver seta a variável has_add para exibir no layouy do admin
			if ($this->Geral->check( $this->request->params['controller'], 'admin_adicionar' )) {
				$this->set( 'has_add', true );
			}

			$this->layout = 'admin';
			$this->set('title', $this->title);
		}

		$this->Auth->loginAction = '/login';

		// $this->Auth->allow();
	}

	protected function isPrefix( $prefix )
	{
		return isset( $this->request->params['prefix'] ) &&
			$this->request->params['prefix'] === $prefix;
	}

	/**
	 * Retorna se o usuário logado é Admin
	 *
	 * @return bool
	 */
	public function isAdmin()
	{
		return $this->Session->read('Auth.User.is_admin');
	}

	/**
	 * Verifica se o usuário é Admin, caso não seja ele é redirecionado para a página inicial
	 */
	public function apenasAdmin()
	{
		if (!$this->isAdmin()) {
			$this->redirect('/');
		}
	}

	public function del($id)
	{
		$this->autoRender = false;
		if ($this->{$this->model}->delete($id))
			$this->Session->setFlash('Registro excluido com sucesso!');
		else $this->Session->setFlash('Ocorreu um erro ao excluir o registro.');

		$this->redirect(array('action' => 'index'));
	}
}
