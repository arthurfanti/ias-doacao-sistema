<?php
App::uses('PaginasController', 'Controller');
class CancelamentoDoacoesController extends AppController
{
	
	public function admin_cancelar()
	{
		$this->layout = false;
		$this->autoRender = false;

		if ($this->CancelamentoDoacao->save($this->data)) {
			$this->Geral->setFlash('Cancelamento efetuado com sucesso.', true);

			$this->CancelamentoDoacao->Doacao->Behaviors->load('Containable');
			$doacao = $this->CancelamentoDoacao->Doacao->find('first', [
				'conditions' => ['Doacao.id' => $this->data['CancelamentoDoacao']['doacao_id']],
				'contain' => ['Doador.User', 'Pagamento.FormasPagamento']
			]);
			$doacao['Doacao']['status'] = 'C';
			$this->CancelamentoDoacao->Doacao->save($doacao);

			$pagina = new PaginasController();

			//se for doação mensal, PJ e boleto efetua o cancelamento das próximas parcelas
			if (($doacao['Doacao']['periodo_id'] == 1) && 
					($doacao['Doador']['pessoa_id'] == 2) && 
					($doacao['Pagamento']['FormasPagamento']['tipo'] == 'B')) {
				$parcelaAtual = $this->CancelamentoDoacao->Doacao->Parcela->find('first', [
					'conditions' => [
						'Parcela.doacao_id' => $this->data['CancelamentoDoacao']['doacao_id'],
						'Parcela.data_pagamento LIKE' => date('Y-m%')
					],
					'recursive' => -1
				]);

				$this->CancelamentoDoacao->Doacao->Parcela->updateAll(
					['Parcela.status_id' => 3],
					[
						'Parcela.doacao_id' => $this->data['CancelamentoDoacao']['doacao_id'],
						'Parcela.parcela >' => $parcelaAtual['Parcela']['parcela']
					]
				);

				$pagina->enviar_proximas_parcelas_boleto($this->data['CancelamentoDoacao']['doacao_id'], $parcelaAtual['Parcela']['parcela']);
			}

			if ($this->data['CancelamentoDoacao']['cancelamento_id'] == 1) {//cancelamento voluntário
				$tipo_mensagem = ($doacao['Doador']['pessoa_id'] == 1) ? 7 : 16;
			} else {//cancelamento involuntário
				$tipo_mensagem = ($doacao['Doador']['pessoa_id'] == 1) ? 17 : 18;
			}

			$EnviarXMLDoacoesParaCRM = $pagina->xmlgen('EnviarXMLDoacoesParaCRM', null, $this->data['CancelamentoDoacao']['doacao_id']);

			$doador['DoadorEmail']['email'] = $doacao['Doador']['User']['email'];
			$doador['Doador']['nome'] = $doacao['Doador']['nome'];
			$doador['Doador']['sobrenome'] = $doacao['Doador']['sobrenome'];
			$mandrill = $pagina->mandrill($tipo_mensagem, $doador);
		} else {
			$this->Geral->setFlash('Erro ao tentar efetuar o cancelamento.', false);
		}

		$this->redirect(['controller' => 'doacoes', 'action' => 'painel', $this->data['CancelamentoDoacao']['doacao_id']]);
	}
}