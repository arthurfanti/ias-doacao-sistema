<?php
class ItensController extends AppController
{
	public $title = "Brindes";

	public function admin_index()
	{
		$this->set('itens', $this->Item->find('all'));
	}

	public function admin_adicionar()
	{
		$this->loadModel('Conteudo');

		if (!empty($this->data)) {
			$dataSource = $this->Item->getDataSource();
			$dataSource->begin();

			$this->Item->save($this->data);

			$item_id = $this->Item->getLastInsertID();

			foreach ($this->request->data['Atributo'] as $atributo) {
				if (!empty($atributo['atributo'])) {
					$atributo_db = $this->Item->Atributo->create([
						'atributo' => $atributo['atributo'],
						'item_id' => $item_id
					]);

					$this->Item->Atributo->save($atributo_db);

					$atributo_id = $this->Item->Atributo->getLastInsertID();

					foreach ($atributo['Conteudo'] as $conteudo) {
						if (!empty($conteudo['conteudo'])) {
							$conteudo_db = $this->Item->Atributo->Conteudo->create([
								'conteudo' => $conteudo['conteudo'],
								'atributo_id' => $atributo_id
							]);

							$this->Item->Atributo->Conteudo->save($conteudo);
						}
					}
				}
			}

			if ($dataSource->commit()) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$promocoes = $this->Item->Promocao->find('list', [
			'conditions' => [
				'Promocao.status' => true
			]
		]);

		$this->set('promocoes', $promocoes);
	}

	public function admin_alterar($item_id = null)
	{
		if (!$item_id)
			$this->redirect(array('action' => 'index'));

		$this->loadModel('Conteudo');

		$this->Item->Atributo->Behaviors->load('Containable');

		$atributos = $this->Item->Atributo->find('all', [
			'conditions' => ['Atributo.item_id' => $item_id],
			'contain' => 'Conteudo',
			'recursive' => -1
		]);

		if (empty($this->data)) {
			$this->data = $this->Item->find('first', [
				'conditions' => ['Item.id' => $item_id],
				'recursive' => -1
			]);
		} else {
			// print_r($this->data);die;
			$dataSource = $this->Item->getDataSource();
			$dataSource->begin();

			foreach ($this->request->data['Atributo'] as $atributo) {
				if (!empty($atributo['atributo'])) {
					if (isset($atributo['id'])) {
						$this->Item->Atributo->save($atributo);

						$atributo_id = $atributo['id'];
					} else {
						$atributo_db = $this->Item->Atributo->create([
							'atributo' => $atributo['atributo'],
							'item_id' => $item_id
						]);

						$this->Item->Atributo->save($atributo_db);

						$atributo_id = $this->Item->Atributo->getLastInsertID();
					}

					foreach ($atributo['Conteudo'] as $conteudo) {
						if (!empty($conteudo['conteudo'])) {
							if (isset($conteudo['id'])) {
								$this->Item->Atributo->Conteudo->save($conteudo);
							} else {
								$conteudo_db = $this->Item->Atributo->Conteudo->create([
									'conteudo' => $conteudo['conteudo'],
									'atributo_id' => $atributo_id
								]);

								$this->Item->Atributo->Conteudo->save($conteudo);
							}
						} else if (isset($conteudo['id'])) {
							$this->Item->Atributo->Conteudo->delete($conteudo['id']);
						}
					}
				} else if (isset($atributo['id'])) {
					$this->Item->Atributo->Conteudo->deleteAll(['Conteudo.atributo_id' => $atributo['id']]);
					$this->Item->Atributo->delete($atributo['id']);
				}
			}

			if ($dataSource->commit()) {
			// if ($this->Item->save($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$promocoes = $this->Item->Promocao->find('list', [
			'conditions' => [
				'Promocao.status' => true
			]
		]);

		$this->set('promocoes', $promocoes);
		$this->set('atributos', $atributos);
	}
}