<?php
class MensagensController extends AppController
{

	public $model = "Mensagem";
	public $title = "Mensagens";

	public function admin_index()
	{
		$this->Mensagem->locale = 'por';
		$this->set('mensagens', $this->Mensagem->find('all', array(
			'recursive' => -1
		)));
	}

	public function admin_adicionar()
	{
		if (!empty($this->data)) {
			if ($this->Mensagem->saveMany($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->set('idiomas', $this->Geral->getIdiomas());
		$this->set('tipos', $this->Geral->getTiposMensagens());
	}

	public function admin_alterar($mensagem_id = null)
	{
		if (!$mensagem_id)
			$this->redirect(array('action' => 'index'));

		$idiomas = $this->Geral->getIdiomas();

		if (empty($this->data)) {
			$this->Mensagem->locale = $idiomas;
			$this->data = $this->Mensagem->find('first', array(
				'conditions' => array('Mensagem.id' => $mensagem_id),
				'recursive' => -1
			));
		} else {
			if ($this->Mensagem->saveMany($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->set('idiomas', $idiomas);
		$this->set('tipos', $this->Geral->getTiposMensagens());
	}

	public function admin_del($id = null)
	{
		parent::del($id);
	}
}
