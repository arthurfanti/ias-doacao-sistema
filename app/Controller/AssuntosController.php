<?php
class AssuntosController extends AppController
{
	
	public $model = "Assunto";
	public $title = "Assuntos";

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow('renderSubjects');
	}

	public function renderSubjects()
	{
		$this->layout = false;
		$this->autoRender = false;

		return $this->Assunto->find('all', array(
			'conditions' => array('Assunto.status' => 1)
		));
	}

	public function admin_index()
	{
		$this->Assunto->locale = 'por';
		$this->set('assuntos', $this->Assunto->find('all', array(
			'recursive' => -1
		)));
	}

	public function admin_adicionar()
	{
		if (!empty($this->data)) {
			if ($this->Assunto->saveMany($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->set('idiomas', $this->Geral->getIdiomas());
	}

	public function admin_alterar($assunto_id = null)
	{
		if (!$assunto_id)
			$this->redirect(array('action' => 'index'));

		$idiomas = $this->Geral->getIdiomas();

		if (empty($this->data)) {
			$this->Assunto->locale = $idiomas;
			$this->data = $this->Assunto->find('first', array(
				'conditions' => array('Assunto.id' => $assunto_id),
				'recursive' => -1
			));
		} else {
			if ($this->Assunto->saveMany($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->set('idiomas', $idiomas);
	}
}