<?php
App::import('Vendor', 'Braspag', array('file' => 'Braspag/TransacaoBraspag.php'));
App::uses('CakeEmail', 'Network/Email', 'Xml', 'Utility');
class PaginasController extends AppController
{
	public $uses = false;


	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow(
			'mudar_idioma', 'fale_conosco', 'gerar_parcelas', 'transacao_parcela', 'retentativa_cartao', 'manutencao', 
			'consultar_parcelas', 'xmlgen', 'enviar_parcelas_debito', 'gerar_parcelas_boleto'
		);
	}

	public function admin_index()
	{
		# code...
	}

	public function mudar_idioma()
	{
		$this->layout = false;
		$this->autoRender = false;
		$this->request->allowMethod('ajax');
		$this->Session->write('Config.language', $this->request->params['lang']);
		Configure::write('Config.language', $this->request->params['lang']);

		return $this->Session->read('Config.language');
		//redirecionar para a página anterior
	}

	public function boleto()
	{
		$parcela_id = $this->request->params['parcela_id'];

		if (!$parcela_id)
			$this->redirect('/');

		$this->loadModel('Parcela');

		if ($this->Session->read('Auth.User.is_admin')) {
			$conditions = [
				'Parcela.id' => $parcela_id
			];
		} else {
			$conditions = [
				'Parcela.id' => $parcela_id,
				'Doacao.doador_id' => $this->Session->read('Auth.User.Doador.id')
			];
		}

		$parcela = $this->Parcela->find('first', [
			'conditions' => $conditions,
			// 'recursive' => -1
		]);

		if (!$parcela)
			$this->redirect('/');

		$doador = $this->Parcela->Doacao->Doador->find('first', [
			'fields' => [
				'Doador.id', 'Doador.nome', 'Doador.sobrenome', 'Doador.cpf_cnpj', 'Doador.pessoa_id',
				'DoadorEndereco.id', 'DoadorEndereco.logradouro', 'DoadorEndereco.numero', 'DoadorEndereco.cep', 'DoadorEndereco.cidade', 'DoadorEndereco.uf'
			],
			'joins' => array(
				array(
					'table'      => 'doador_enderecos',
					'alias'      => 'DoadorEndereco',
					'type'       => 'LEFT',
					'conditions' => array('DoadorEndereco.doador_id = Doador.id')
				),
			),
			'conditions' => ['Doador.id' => $parcela['Doacao']['doador_id']],
			'recursive' => -1
		]);

		$this->autoRender = false;

		$data_pagamento = new DateTime($parcela['Parcela']['data_pagamento']);

		$this->BoletoItau = $this->Components->load('Boletos.BoletoItau');

		$endereco1        = '';
		$endereco2        = '';
		$tipo_pessoa      = $doador['Doador']['pessoa_id'] == 1 ? 'CPF' : 'CNPJ';

		$nome             = $doador['Doador']['nome'] . ' ' . $doador['Doador']['sobrenome'];
		$cpf              = $doador['Doador']['cpf_cnpj'];
		$nossonumero      = $parcela['Parcela']['nossonumero'];
		$numero_doc       = $parcela['Parcela']['numero_documento'];
		$sacado 		  = $nome . ' - ' . $tipo_pessoa . ': ' . $cpf;
		if (!empty($doador['DoadorEndereco']['logradouro'])) {
			$endereco1        = $doador['DoadorEndereco']['logradouro'] . ', ' . $doador['DoadorEndereco']['numero'] . ' - CEP:' . $doador['DoadorEndereco']['cep'];
			$endereco2        = $doador['DoadorEndereco']['cidade'] . '/'. $doador['DoadorEndereco']['uf'];
		}

		$dados = array(
			'sacado'           => $sacado,
			'endereco1'        => $endereco1,
			'endereco2'        => $endereco2,
			'demonstrativo1'   => $sacado,
			'demonstrativo2'   => $endereco1,
			'demonstrativo3'   => $endereco2,
			'valor_cobrado'    => $parcela['Parcela']['valor'],
			'data_vencimento'  => $data_pagamento->format('d/m/Y'),
			'numero_documento' => $numero_doc,
			'nosso_numero'     => $nossonumero,
			'pedido'           => 5 // Usado para gerar o número do documento e o nosso número.
		);
		$this->BoletoItau->render($dados);die;
	}

	public function mandrill( $mensagemId = null, $data = null )
	{
		$this->layout = false;
		$this->autoRender = false;
		$this->loadModel('Mensagem');

		$mensagens = $this->Mensagem->find('first', array(
			'conditions' => array(
				'Mensagem.status' => true,
				'Mensagem.tipo' => $mensagemId
			)
		));

		$headers = array(
			'global_merge_vars' => array(
				array(
					'name' => 'date',
					'content' => date('d-m-Y h:i:s A')
				)
			)
		);

		if (is_null($mensagemId)) {
			$email = new CakeEmail(array(
				'transport'   => 'Mandrill.Mandrill',
				'from'        => $this->Session->read('Auth.User.email'),
				'fromName'    => $this->Session->read('Auth.User.Doador.nome') . ' ' . $this->Session->read('Auth.User.Doador.sobrenome'),
				'api_key'     => 'JKYHImGxNNZZxBnrCBjyXw',
				'timeout'     => 30,
				'emailFormat' => 'html',
			));

			$email->addHeaders($headers);

			$to = array(
				// 'gabriel.medeiros@dominidesign.com.br',
				// 'arthur@dominidesign.com.br',
				$this->request->data['share-email'],
			);

			$viewVars = array(
				'nome'          => $this->Session->read('Auth.User.Doador.nome') . ' ' . $this->Session->read('Auth.User.Doador.sobrenome'),
				'user_email'    => $this->Session->read('Auth.User.email'),
				'share_email'   => $this->request->data['share-email'],
				'share_message' => $this->request->data['share-message']
			);

			$email->template('mandrill');
			$email->subject( __('Apoie você também o Instituto Ayrton Senna') );
			$email->viewVars($viewVars);
			$email->to($to);

			if ($email->send()) {
				$this->WebService = $this->Components->load('WebService');
				$this->WebService->integracaoCompartilhamentoMail2Easy($this->request->data['share-email']);

				$this->Geral->setFlash(__('Sua mensagem foi enviada!'), true);
				$this->redirect('/conta');
			}

		} else {
			$email = new CakeEmail('default');

			$email->addHeaders($headers);

			$to = array(
				$data['DoadorEmail']['email']
			);

			$body = $this->Mensagem->find('first', array(
				'conditions' => array(
					'Mensagem.status' => true,
					'Mensagem.tipo' => $mensagemId
				)
			));

			$viewVars = array(
				'nome' => $data['Doador']['nome'] . ' ' . $data['Doador']['sobrenome'],
				'body' => $body['Mensagem']['descricao']
			);

			$email->template('transactional');
			$email->subject($body['Mensagem']['mensagem']);
			$email->viewVars($viewVars);
			$email->to($to);

			if ($email->send()) {
				return;
			} else {
				throw new InternalErrorException('Envio de email transacional falhou!');
			}
		}
	}

	public function xmlgen( $soapAction = null, $full = null, $id = null)
	{

		$this->layout = false;

		$this->Session = $this->Components->load('Session');

		$view = new View($this, false);

		switch ($soapAction) {
			case 'EnviarXMLDoadorParaCRM':
				$this->loadModel('Doador');

				$doador_id = isset($id) ? $id : $this->Session->read('Auth.User.Doador.id');

				$this->Doador->Behaviors->load('Containable');
				$data = $this->Doador->find('first', array(
					'conditions' => array('Doador.id' => $doador_id),
					'contain'    => ['DoadorTelefone', 'DoadorEmail', 'DoadorEndereco', 'Conheceu', 'Pessoa', 'DpjComplemento', 'DpjComplemento.Faixa', 'DpjComplemento.Natureza'],
					'recursive'  => -1
				));

				$view->set(compact('data'));
				$html = $view->render('xml/doadores');
				// debug($html);
				// die();
				break;

			case 'EnviarXMLDoacoesParaCRM':
				$this->loadModel('Doacao');
				
				if (isset($id)) {
					$conditions = array(
						'Doacao.id' => $id
					);
				} else {
					$id = ($this->Session->check($this->_tagSession . '.doador_id')) ? $this->Session->read($this->_tagSession . '.doador_id') : $this->Session->read('Auth.User.Doador.id');
					$conditions = array(
						'Doacao.doador_id' => $id,
						'Doacao.status' => 'A'
					);
				}

				$this->Doacao->Behaviors->load('Containable');

				$data = $this->Doacao->find('first', array(
					'conditions' => $conditions,
					'limit' => 1,
					'order' => 'Doacao.id DESC',
					'contain' => [
						'Doador', 'Campanha', 'Pagamento', 'Pagamento.FormasPagamento.Banco', 'Pagamento.DiaDebito', 'CancelamentoDoacao',
						'AceiteDoacao', 'Brinde', 'Brinde.Item', 'Brinde.Item.Promocao'
					],
					'recursive' => -1
				));

				$view->set(compact('data'));
				$html = $view->render('xml/doacoes');
				// debug($html);
				// die();
				break;

			case 'EnviarXMLParcelaParaCRM':
				$this->loadModel('Doacao');

				//xml das parcelas recorrentes geradas no inicio do mês
				if (isset($full) && !isset($id)) {
					// ini_set('memory_limit', '1024M');
					$data = $this->Doacao->find('all', [
						'fields' => [
							'Doacao.*', 'Parcela.*'
						],
						'joins' => [
							[
								'table'      => 'parcelas',
								'alias'      => 'Parcela',
								'type'       => 'INNER',
								'conditions' => [
									'Parcela.doacao_id = Doacao.id',
									'Parcela.parcela >' => 1,
									'Parcela.created >=' => date('Y-m-01'),
									'Parcela.formas_pagamento_id' => [2,3,7,10,11,15]
								]
							],
						],
						'conditions' => [
							'Doacao.status' => 'A',
							'Doacao.periodo_id' => 1
						],
						'recursive' => -1,
					]);

					$view->set(compact('data'));
					$html = $view->render('xml/parcelas');
					// debug($html);
					// die;
					// ini_set('memory_limit', '128M');

				//xml das parcelas do boleto
				} else if (isset($full) && isset($id)) {
					// ini_set('memory_limit', '1024M');
					$conditionsParcela = [
						'Parcela.doacao_id = Doacao.id'
					];
					if ($full === false) {
						$conditionsParcela['Parcela.status_id'] = 3;//parcelas canceladas
					}

					$data = $this->Doacao->find('all', [
						'fields' => [
							'Doacao.*', 'Parcela.*'
						],
						'joins' => [
							[
								'table'      => 'parcelas',
								'alias'      => 'Parcela',
								'type'       => 'INNER',
								'conditions' => $conditionsParcela
							],
						],
						'conditions' => [
							'Doacao.id' => $id
						],
						'recursive' => -1,
					]);

					$view->set(compact('data'));
					$html = $view->render('xml/parcelas');
					// debug($html);
					// die;
					// ini_set('memory_limit', '128M');

				//xml da doação que está na sessão
				} else {
					$doacaoSession = $this->Session->read($this->_tagSession);

					$data = $this->Doacao->Parcela->find('first', array(
						'conditions' => array(
							'Parcela.doacao_id' => $doacaoSession['doacao_id']
						),
						'order' => array('Parcela.id DESC'),
						'recursive' => -1,
					));

					$data = array(0 => $data);

					$view->set(compact('data'));
					$html = $view->render('xml/parcelas');

					// debug($html);
					// die();
				}
				break;

			default:
				throw new NotFoundException();
				break;
		}

		$this->WebService = $this->Components->load('WebService');
		return $this->WebService->xml_post_test($html, $soapAction);
	}

	public function receive_xml( $html = null ){
		$this->layout = false;
		echo "string";
	}

	public function fale_conosco()
	{
		$this->layout = false;
		$this->autoRender = false;

		$nome       = $this->request->data['contato-nome'];
		$email_from = $this->request->data['contato-email'];
		$assunto    = $this->request->data['contato-assunto'];
		$mensagem   = $this->request->data['contato-mensagem'];

		$email = new CakeEmail('mandrill');

		$email->addHeaders(array(
			'global_merge_vars' => array(
				array(
					'name' => 'date',
					'content' => date('d-m-Y h:i:s A')
				)
			)
		));

		$viewVars = array(
			'nome'          => $nome,
			'email'         => $email_from,
			// 'share_email'   => $email,
			'mensagem' => $mensagem
		);

		$to = array(
			$this->request->data['recipient'],
		);

		$email->template('fale_conosco');
		$email->viewVars($viewVars);
		$email->to($to);
		$email->from($email_from);
		$email->subject($assunto);

		$retorno = false;
		if ($email->send()) {
			$retorno = true;
		}

		return json_encode(array('retorno' => $retorno));
	}

	public function gerar_parcelas($tipo_pagamento = null)
	{
		$this->layout = false;
		$this->autoRender = false;

		$this->verifica_acesso();

		if (!$tipo_pagamento) {
			die;
		}

		$this->loadModel('Doacao');

		if (date('m') == 1) {
			$mes_anterior = 12;
			$ano_anterior = date('Y') - 1;
		} else {
			$mes_anterior = date('m') - 1;
			$ano_anterior = date('Y');
		}
		// $mes_anterior = 3;//AQUI
		// $ano_anterior = 2015;//AQUI
		// $tipo_pagamento = 'D';//AQUI
		$qtd_meses = 1;

		$doacoes = $this->Doacao->find('all', [
			'fields' => [
				'Doacao.id', 'Doacao.valor', 'Doacao.created',
				'Pagamento.agencia', 'Pagamento.conta', 'Pagamento.dia_debito_id', 'Pagamento.braspag_justclickkey', 'Pagamento.formas_pagamento_id', 'Pagamento.cartao_final',
				'Parcela.parcela', 'Parcela.data_geracao'
			],
			'joins' => [
				[
					'table'      => 'parcelas',
					'alias'      => 'Parcela',
					'type'       => 'INNER',
					'conditions' => [
						'Parcela.doacao_id = Doacao.id',
						'MONTH(Parcela.data_geracao)' => $mes_anterior,
	  				'YEAR(Parcela.data_geracao)' => $ano_anterior,
	  				// 'Parcela.parcela' => 1//AQUI
					]
				],
				[
					'table'      => 'pagamentos',
					'alias'      => 'Pagamento',
					'type'       => 'INNER',
					'conditions' => [
						'Pagamento.id = Doacao.pagamento_id',
					]
				],
				[
					'table'      => 'formas_pagamentos',
					'alias'      => 'FormasPagamento',
					'type'       => 'INNER',
					'conditions' => [
						'FormasPagamento.id = Pagamento.formas_pagamento_id',
						'FormasPagamento.tipo' => $tipo_pagamento
					]
				],
			],
			'conditions' => [
				'Doacao.status' => 'A',
				'Doacao.periodo_id' => 1,
			],
			'recursive' => -1,
		]);

		$formas = $this->Doacao->Parcela->FormasPagamento->find('all', [
			'recursive' => -1
		]);

		$diasDebito = $this->Doacao->Pagamento->DiaDebito->find('list', [
			'conditions' => [
				'DiaDebito.status' => true
			]
		]);

		$qtdNao = 0;
		$qtdSim = 0;
		foreach ($doacoes as $doacao) {
			$parcela = $this->Doacao->Parcela->find('all', [
				'conditions' => [
					'Parcela.doacao_id' => $doacao['Doacao']['id'],
					// 'Parcela.data_geracao LIKE' => date('Y-m%')
					'Parcela.data_pagamento LIKE' => date('Y-m%')
				],
				'recursive' => -1
			]);
			if (count($parcela) > 0) {
				// echo 'Não: ' . $doacao['Doacao']['id'] . '<br>';
				$qtdNao++;
				continue;
			}

			$status_id = '';
			// $nossonumero = '';
			// $numero_doc = '';
			$data_pagamento = '';
			$parcela = $doacao['Parcela']['parcela'];

			foreach ($formas as $forma) {
				if ($forma['FormasPagamento']['id'] == $doacao['Pagamento']['formas_pagamento_id']) {
					if ($forma['FormasPagamento']['tipo'] == 'D') {
						if (array_key_exists($doacao['Pagamento']['dia_debito_id'], $diasDebito) !== false) {
							$data_pagamento = date('Y-m-'.$diasDebito[$doacao['Pagamento']['dia_debito_id']]);
						} else {
							$data_pagamento = date('Y-m-20');
						}

						break;
					} else if ($forma['FormasPagamento']['tipo'] == 'C') {
						$meses_com_30_dias = $this->Geral->mesesCom30Dias();
						$mes_atual = date('m');
						$dia_aniversario = substr($doacao['Doacao']['created'], 8, 2);

						if (($mes_atual == '02') && ($dia_aniversario > '28')) {
							$data_pagamento = date('Y-m-28');
						} else if ((in_array($mes_atual, $meses_com_30_dias)) && ($dia_aniversario == '31')) {
							$data_pagamento = date('Y-m-30');
						} else {
							$data_pagamento = date('Y-m-'.$dia_aniversario);
						}

						break;
					}
				}
			}

			$status_id = 6;

			$parcela = $this->Doacao->Parcela->create([
				'doacao_id' => $doacao['Doacao']['id'],
				'data_geracao' => date('Y-m-d H:i:s'),
				'parcela' => $parcela + $qtd_meses,
				'valor' => $doacao['Doacao']['valor'],
				'formas_pagamento_id' => $doacao['Pagamento']['formas_pagamento_id'],
				'cartao_final' => $doacao['Pagamento']['cartao_final'],
				'status_id' => $status_id,
				// 'nossonumero' => $nossonumero,
				// 'numero_documento' => $numero_doc,
				'data_pagamento' => $data_pagamento
			]);

			$this->Doacao->Parcela->save($parcela);
			$qtdSim++;
		}
		// echo 'Sim: ' . $qtdSim . ' Não: ' . $qtdNao;

		// $this->xmlgen('EnviarXMLParcelaParaCRM', true);
	}

	public function gerar_parcelas_boleto()
	{
		$this->layout = false;
		$this->autoRender = false;

		$this->verifica_acesso();

		$this->WebService = $this->Components->load('WebService');

		$this->loadModel('Doacao');

		if (date('m') == 1) {
			$mes_anterior = 12;
			$ano_anterior = date('Y') - 1;
		} else {
			$mes_anterior = date('m') - 1;
			$ano_anterior = date('Y');
		}
		// $mes_anterior = 3;//AQUI
		// $ano_anterior = 2015;//AQUI
		// $tipo_pagamento = 'D';//AQUI

		$doacoes = $this->Doacao->find('all', [
			'fields' => [
				'Doacao.id', 'Doacao.valor', 'Doacao.created',
				'Pagamento.agencia', 'Pagamento.conta', 'Pagamento.dia_debito_id', 'Pagamento.braspag_justclickkey', 'Pagamento.formas_pagamento_id', 'Pagamento.cartao_final',
				'Parcela.parcela', 'Parcela.data_geracao', 'Parcela.data_pagamento'
			],
			'joins' => [
				[
					'table'      => 'parcelas',
					'alias'      => 'Parcela',
					'type'       => 'INNER',
					'conditions' => [
						'Parcela.doacao_id = Doacao.id',
						'MONTH(Parcela.data_pagamento)' => $mes_anterior,
	  				'YEAR(Parcela.data_pagamento)' => $ano_anterior,
					]
				],
				[
					'table'      => 'pagamentos',
					'alias'      => 'Pagamento',
					'type'       => 'INNER',
					'conditions' => [
						'Pagamento.id = Doacao.pagamento_id',
					]
				],
				[
					'table'      => 'formas_pagamentos',
					'alias'      => 'FormasPagamento',
					'type'       => 'INNER',
					'conditions' => [
						'FormasPagamento.id = Pagamento.formas_pagamento_id',
						'FormasPagamento.tipo' => 'B'
					]
				],
			],
			'conditions' => [
				'Doacao.status' => 'A',
				'Doacao.periodo_id' => 1,
			],
			'recursive' => -1,
		]);

		$qtdNao = 0;
		$qtdSim = 0;
		set_time_limit(300);
		foreach ($doacoes as $doacao) {
			//verifica se a parcela é divisível por 12. se for é a última parcela anual das parcelas geradas
			if (($doacao['Parcela']['parcela'] % 12) != 0) {
				continue;
			}

			//verifica se tem parcela para o mês atual
			$parcela = $this->Doacao->Parcela->find('all', [
				'conditions' => [
					'Parcela.doacao_id' => $doacao['Doacao']['id'],
					// 'Parcela.data_geracao LIKE' => date('Y-m%')
					'Parcela.data_pagamento LIKE' => date('Y-m%')
				],
				'recursive' => -1
			]);
			if (count($parcela) > 0) {
				// echo 'Não: ' . $doacao['Doacao']['id'] . '<br>';
				$qtdNao++;
				continue;
			}

			// $qtdParcela = $doacao['Parcela']['parcela'];
			$meses_com_30_dias = $this->Geral->mesesCom30Dias();
			$mes = date('m');
			$ano = date('Y');
			$data_geracao = $doacao['Parcela']['data_pagamento'];
			for ($i=1; $i <= 12; $i++) { 
				$novaParcela = $doacao['Parcela']['parcela'] + $i;

				$nossonumero = $doacao['Doacao']['id'] . str_pad($novaParcela, 2, "0", STR_PAD_LEFT);
				$numero_doc  = $doacao['Doacao']['id'] . '0' . str_pad($novaParcela, 2, "0", STR_PAD_LEFT);

				$dt_antiga = new DateTime($data_geracao);
				$dt_nova   = new DateTime($data_geracao);
				$dt_nova->add(new DateInterval('P'.$i.'M'));
				$mes_novo = $dt_nova->format('m');
				$mes_antigo = $dt_antiga->format('m');
				if (($mes_novo != ($mes_antigo + ($i % 12))) && ($mes_novo != '01')) {
					$dt_nova->sub(new DateInterval('P'.$dt_nova->format('d').'D'));
				}
				$data_pagamento = $dt_nova->format('Y-m-d');

				$parcela = $this->Doacao->Parcela->create([
					'doacao_id' => $doacao['Doacao']['id'],
					'data_geracao' => date('Y-m-d H:i:s'),
					'parcela' => $novaParcela,
					'valor' => $doacao['Doacao']['valor'],
					'formas_pagamento_id' => $doacao['Pagamento']['formas_pagamento_id'],
					'status_id' => 6,
					'nossonumero' => $nossonumero,
					'numero_documento' => $numero_doc,
					'data_pagamento' => $data_pagamento
				]);

				$dataSource = $this->Doacao->getDataSource();
				$dataSource->begin();

				$this->Doacao->Parcela->save($parcela);

				$parcela_id = $this->Doacao->Parcela->getLastInsertID();

				$dataSource->commit();

				$data = $this->Doacao->find('all', [
					'fields' => [
						'Doacao.*', 'Parcela.*'
					],
					'joins' => [
						[
							'table'      => 'parcelas',
							'alias'      => 'Parcela',
							'type'       => 'INNER',
							'conditions' => [
								'Parcela.doacao_id = Doacao.id',
								'Parcela.parcela >' => 1,
								'Parcela.data_geracao >=' => date('Y-m-01'),
								'Parcela.id' => $parcela_id
							]
						],
						[
							'table'      => 'formas_pagamentos',
							'alias'      => 'FormasPagamento',
							'type'       => 'INNER',
							'conditions' => [
								'Parcela.formas_pagamento_id = FormasPagamento.id',
								'FormasPagamento.tipo' => 'B'
							]
						],
					],
					'conditions' => [
						'Doacao.status' => 'A',
						'Doacao.periodo_id' => 1,
					],
					'recursive' => -1,
				]);

				$view = new View($this, false);
				$view->set(compact('data'));
				$html = $view->render('xml/parcelas');
				// debug($html);
				// die;
				$this->WebService->xml_post_test($html, 'EnviarXMLParcelaParaCRM');
			}
			
			$qtdSim++;
		}
		// echo 'Sim: ' . $qtdSim . ' Não: ' . $qtdNao;
		set_time_limit(30);
	}

	public function enviar_parcelas_debito()
	{
		$this->layout = false;
		$this->autoRender = false;

		$this->verifica_acesso();

		$this->WebService = $this->Components->load('WebService');

		$this->loadModel('Doacao');

		$doacoes = $this->Doacao->find('all', [
			'fields' => [
				'Doacao.*', 'Parcela.*'
			],
			'joins' => [
				[
					'table'      => 'parcelas',
					'alias'      => 'Parcela',
					'type'       => 'INNER',
					'conditions' => [
						'Parcela.doacao_id = Doacao.id',
						'Parcela.parcela >' => 1,
						'Parcela.created >=' => date('Y-m-01'),
					]
				],
				[
					'table'      => 'formas_pagamentos',
					'alias'      => 'FormasPagamento',
					'type'       => 'INNER',
					'conditions' => [
						'Parcela.formas_pagamento_id = FormasPagamento.id',
						'FormasPagamento.tipo' => 'D'
					]
				],
			],
			'conditions' => [
				'Doacao.status' => 'A',
				'Doacao.periodo_id' => 1
			],
			'recursive' => -1,
		]);

		set_time_limit(300);
		for ($i=0; $i < count($doacoes); $i++) { 
			$data = [];
			$data[] = $doacoes[$i];
			$view = new View($this, false);
			$view->set(compact('data'));
			$html = $view->render('xml/parcelas');
			// debug($html);
			// die;
			$this->WebService->xml_post_test($html, 'EnviarXMLParcelaParaCRM');
		}
		set_time_limit(30);
	}

	public function transacao_parcela()
	{
		$this->layout = false;
		$this->autoRender = false;

		$this->verifica_acesso();

		$this->loadModel('Doacao');

		$parcelas = $this->Doacao->Parcela->find('all', [
			'fields' => [
				'Doacao.id',
				'Parcela.id', 'Parcela.valor', 'Parcela.formas_pagamento_id',
				'Pagamento.braspag_justclickkey',
				'Doador.id', 'Doador.nome', 'Doador.sobrenome',
				'User.email'
			],
			'joins' => [
				[
					'table'      => 'formas_pagamentos',
					'alias'      => 'FormasPagamento',
					'type'       => 'INNER',
					'conditions' => [
						'FormasPagamento.id = Parcela.formas_pagamento_id',
						'FormasPagamento.tipo' => 'C'
					]
				],
				[
					'table'      => 'doacoes',
					'alias'      => 'Doacao',
					'type'       => 'INNER',
					'conditions' => [
						'Doacao.id = Parcela.doacao_id'
					]
				],
				[
					'table'      => 'pagamentos',
					'alias'      => 'Pagamento',
					'type'       => 'INNER',
					'conditions' => [
						'Pagamento.id = Doacao.pagamento_id'
					]
				],
				[
					'table'      => 'doadores',
					'alias'      => 'Doador',
					'type'       => 'INNER',
					'conditions' => [
						'Doador.id = Doacao.doador_id'
					]
				],
				[
					'table'      => 'users',
					'alias'      => 'User',
					'type'       => 'INNER',
					'conditions' => [
						'User.id = Doador.user_id'
					]
				],
			],
			'conditions' => [
				'Parcela.status_id' => 6,
				'Parcela.data_pagamento' => date('Y-m-d'),
				'Parcela.parcela' > 1
			],
			'recursive' => -1,
			// 'limit' => 2
		]);

		$this->Session->delete($this->_tagSession);

		$braspag = new TransacaoBraspag();
		foreach ($parcelas as $parcela) {
			$order_id = md5($parcela['Doacao']['id'] . date('Ymd H:m:s'));
			$nova_tentativa = true;

			$braspag->recorrencia = true;

			$braspag->cartao = $parcela['Parcela']['formas_pagamento_id'];

			$braspag->order_ID = substr($order_id, 0, 36);

			$braspag->customer_Name  = $parcela['Doador']['nome'] . ' ' . $parcela['Doador']['sobrenome'];
			$braspag->customer_ID    = $parcela['Doador']['id'];
			$braspag->customer_Email = $parcela['User']['email'];

			$braspag->payment_Amount   = str_replace(array('.', ','), array('', ''), $parcela['Parcela']['valor']);
			$braspag->payment_Currency = 'BRL';
			$braspag->payment_Country  = 'BRA';

			// $braspag->payment_CardSecurityCode = $parcela['Pagamento']['cvv'];
			$braspag->payment_creditCardToken  = $parcela['Pagamento']['braspag_justclickkey'];
			// $braspag->payment_creditCardToken = '5868f907-381d-42f6-a751-3a0ce255ee7d';

			$retorno = $braspag->efetuar_transacao();

			CakeLog::write('recorrencia_braspag', json_encode($retorno));

			if ($retorno->PaymentDataCollection->PaymentDataResponse->Status == 0) {
				$nova_tentativa = false;
			}

			$mensagem_retorno = $retorno->PaymentDataCollection->PaymentDataResponse->ReturnMessage;
			$status_id        = (int)$retorno->PaymentDataCollection->PaymentDataResponse->Status + 12;
			$autorizacao      = $retorno->PaymentDataCollection->PaymentDataResponse->AuthorizationCode;
			$nsu              = $retorno->PaymentDataCollection->PaymentDataResponse->ProofOfSale;

			$parcela['Parcela']['mensagem_retorno'] = $mensagem_retorno;
			$parcela['Parcela']['status_id']        = $status_id;
			$parcela['Parcela']['autorizacao']      = $autorizacao;
			$parcela['Parcela']['nsu']              = $nsu;
			$parcela['Parcela']['order_id']         = $order_id;
			$parcela['Parcela']['nova_tentativa']   = $nova_tentativa;

			$transacao = $this->Doacao->Parcela->Transacao->create([
				'parcela_id'          => $parcela['Parcela']['id'],
				'formas_pagamento_id' => $parcela['Parcela']['formas_pagamento_id'],
				'nsu'                 => $nsu,
				'autorizacao'         => $autorizacao,
				'mensagem_retorno'    => $mensagem_retorno,
				'status_id'           => $status_id
			]);

			$tentativa = $this->Doacao->Parcela->Tentativa->create([
				'parcela_id' => $parcela['Parcela']['id'],
				'tentativa'  => 1
			]);

			$dataSource = $this->Doacao->getDataSource();
			$dataSource->begin();

			$this->Doacao->Parcela->Tentativa->save($parcela);

			$tentativa_id = $this->Doacao->Parcela->Tentativa->getLastInsertID();

			$transacao['Transacao']['tentativa_id'] = $tentativa_id;

			$this->Doacao->Parcela->Transacao->save($transacao);
			$this->Doacao->Parcela->save($parcela);

			$dataSource->commit();

			$doacaoSession['doacao_id'] = $parcela['Doacao']['id'];
			$this->Session->write($this->_tagSession, $doacaoSession);
			$this->xmlgen('EnviarXMLParcelaParaCRM');
		}

		$this->Session->delete($this->_tagSession);
	}

	public function retentativa_cartao()
	{
		$this->layout = false;
		$this->autoRender = false;

		$this->verifica_acesso();

		$this->loadModel('Doacao');

		$parcelas = $this->Doacao->Parcela->find('all', [
			'fields' => [
				'Doacao.id',
				'Parcela.id', 'Parcela.valor', 'Parcela.formas_pagamento_id',
				'Pagamento.braspag_justclickkey',
				'Doador.id', 'Doador.nome', 'Doador.sobrenome',
				'User.email'
			],
			'joins' => [
				[
					'table'      => 'formas_pagamentos',
					'alias'      => 'FormasPagamento',
					'type'       => 'INNER',
					'conditions' => [
						'FormasPagamento.id = Parcela.formas_pagamento_id',
						'FormasPagamento.tipo' => 'C'
					]
				],
				[
					'table'      => 'doacoes',
					'alias'      => 'Doacao',
					'type'       => 'INNER',
					'conditions' => [
						'Doacao.id = Parcela.doacao_id'
					]
				],
				[
					'table'      => 'pagamentos',
					'alias'      => 'Pagamento',
					'type'       => 'INNER',
					'conditions' => [
						'Pagamento.id = Doacao.pagamento_id'
					]
				],
				[
					'table'      => 'doadores',
					'alias'      => 'Doador',
					'type'       => 'INNER',
					'conditions' => [
						'Doador.id = Doacao.doador_id'
					]
				],
				[
					'table'      => 'users',
					'alias'      => 'User',
					'type'       => 'INNER',
					'conditions' => [
						'User.id = Doador.user_id'
					]
				],
			],
			'conditions' => [
				'Parcela.nova_tentativa' => true
			],
			'recursive' => -1,
		]);

		$braspag = new TransacaoBraspag();
		foreach ($parcelas as $parcela) {
			$t = $this->Doacao->Parcela->Tentativa->find('first', [
				'conditions' => [
					'Tentativa.parcela_id' => $parcela['Parcela']['id']
				],
				'order' => 'Tentativa.tentativa DESC',
				'recursive' => -1
			]);
			$qtdTentativa = $t['Tentativa']['tentativa'] + 1;
			$nova_tentativa = false;


			$order_id = md5($parcela['Doacao']['id'] . date('Ymd H:m:s'));

			$braspag->order_ID = substr($order_id, 0, 36);

			$braspag->customer_Name  = $parcela['Doador']['nome'] . ' ' . $parcela['Doador']['sobrenome'];
			$braspag->customer_ID    = $parcela['Doador']['id'];
			$braspag->customer_Email = $parcela['User']['email'];

			$braspag->payment_Amount   = str_replace(array('.', ','), array('', ''), $parcela['Parcela']['valor']);
			$braspag->payment_Currency = 'BRL';
			$braspag->payment_Country  = 'BRA';

			$braspag->payment_creditCardToken = $parcela['Pagamento']['braspag_justclickkey'];

			$retorno = $braspag->efetuar_transacao();

			CakeLog::write('retentativa_braspag', json_encode($retorno));

			$mensagem_retorno = $retorno->PaymentDataCollection->PaymentDataResponse->ReturnMessage;
			$status_id        = (int)$retorno->PaymentDataCollection->PaymentDataResponse->Status + 12;
			$autorizacao      = $retorno->PaymentDataCollection->PaymentDataResponse->AuthorizationCode;
			$nsu              = $retorno->PaymentDataCollection->PaymentDataResponse->ProofOfSale;

			$dataSource = $this->Doacao->getDataSource();
			$dataSource->begin();

			if (isset($retorno->ErrorReportDataCollection->ErrorReportDataResponse->ErrorCode)) {
				$mensagem_retorno = $retorno->ErrorReportDataCollection->ErrorReportDataResponse->ErrorMessage;
				$status_id = 15;
				$nova_tentativa = true;

				$parcela['Parcela']['nova_tentativa']   = false;
				$this->Doacao->Parcela->save($parcela);
			} else if ($retorno->PaymentDataCollection->PaymentDataResponse->Status == '0') {
				$nova_tentativa = false;

				$parcela['Parcela']['mensagem_retorno'] = $mensagem_retorno;
				$parcela['Parcela']['status_id']        = $status_id;
				$parcela['Parcela']['autorizacao']      = $autorizacao;
				$parcela['Parcela']['nsu']              = $nsu;
				$parcela['Parcela']['order_id']         = $order_id;
				$parcela['Parcela']['nova_tentativa']   = false;
				$this->Doacao->Parcela->save($parcela);
			} else if ($qtdTentativa >= 5) {
				$nova_tentativa = false;

				$parcela['Parcela']['nova_tentativa']   = false;
				$this->Doacao->Parcela->save($parcela);
			}

			$tentativa_id = '';

			if ($nova_tentativa === false) {
				$tentativa = $this->Doacao->Parcela->Tentativa->create([
					'parcela_id' => $parcela['Parcela']['id'],
					'tentativa'  => $qtdTentativa
				]);
				$this->Doacao->Parcela->Tentativa->save($tentativa);

				$tentativa_id = $this->Doacao->Parcela->Tentativa->getLastInsertID();
			}

			$transacao = $this->Doacao->Parcela->Transacao->create([
				'parcela_id'          => $parcela['Parcela']['id'],
				'formas_pagamento_id' => $parcela['Parcela']['formas_pagamento_id'],
				'nsu'                 => $nsu,
				'autorizacao'         => $autorizacao,
				'mensagem_retorno'    => $mensagem_retorno,
				'status_id'           => $status_id,
				'tentativa_id'        => $tentativa_id
			]);

			$this->Doacao->Parcela->Transacao->save($transacao);

			$dataSource->commit();
		}
	}

	public function consultar_parcelas()
	{
		$this->layout = false;
		$this->autoRender = false;
		
		$this->verifica_acesso();

		$data = date('Y-m-d');
		// $data = '2015-03-19';

		$this->WebService = $this->Components->load('WebService');
		$retorno = $this->WebService->consultar_parcelas($data);

		if (is_array($retorno) && !empty($retorno)) {
			$this->loadModel('Parcela');

			$formasPagamento = $this->Parcela->FormasPagamento->find('all', [
				'conditions' => [
					'FormasPagamento.tipo' => ['D', 'B']
				],
				'recursive' => -1
			]);
			$formasPagamento = Hash::extract($formasPagamento, '{n}.FormasPagamento.id');

			foreach ($retorno['Parcelas']['Parcela'] as $p) {
				$parcela = $this->Parcela->find('first', [
					'conditions' => [
						'Parcela.doacao_id' => $p['id_doacao'],
						'Parcela.parcela' => $p['parcela']
					],
					'recursive' => -1
				]);

				if ((!empty($parcela)) && (array_search($parcela['Parcela']['formas_pagamento_id'], $formasPagamento) !== false)) {
					$parcela['Parcela']['status_id']      = $p['situacao'];
					$parcela['Parcela']['data_pagamento'] = $p['data_pagamento'];
					$this->Parcela->save($parcela);
				}
			}
		}
	}

	public function enviar_proximas_parcelas_boleto($doacao_id, $parcela)
	{
		$this->layout = false;
		
		$this->loadModel('Parcela');

		$data = $this->Parcela->find('all', [
			'conditions' => [
				'Parcela.doacao_id' => $doacao_id,
				'Parcela.parcela >' => $parcela
			]
		]);

		$this->WebService = $this->Components->load('WebService');
		$view = new View($this, false);
		$view->set(compact('data'));
		$html = $view->render('xml/parcelas');
		// debug($html);
		// die;
		return $this->WebService->xml_post_test($html, 'EnviarXMLParcelaParaCRM');
	}

	public function manutencao()
	{
		$this->layout = false;
	}

	private function verifica_acesso()
	{
		//valida se está em produção e está sendo acessado pelo dominio
		if (!PRODUCTION || ($_SERVER['REMOTE_ADDR'] != IP_SERVER)) {
			// die;
		}
	}
}