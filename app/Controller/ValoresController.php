<?php
class ValoresController extends AppController
{
	public $model = 'Valor';
	public $title = 'Valores';

	public function admin_index()
	{
		$this->Valor->locale = 'por';
		$this->set('tiposPagamentos', $this->Geral->getTiposPagamentos());
		$this->set('valores', $this->Valor->find('all', array(
			'recursive' => -1
		)));
	}

	public function admin_adicionar()
	{
		$pessoas = $this->Valor->Pessoa->find('list', [
			'conditions' => ['Pessoa.status' => true]
		]);

		if (!empty($this->data)) {
			if ($this->Valor->saveMany($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->set('idiomas', $this->Geral->getIdiomas());
		$this->set('tiposPagamentos', $this->Geral->getTiposPagamentos());
		$this->set('pessoas', $pessoas);
	}

	public function admin_alterar($valor_id = null)
	{
		if (!$valor_id)
			$this->redirect(array('action' => 'index'));

		$idiomas = $this->Geral->getIdiomas();

		$pessoas = $this->Valor->Pessoa->find('list', [
			'conditions' => ['Pessoa.status' => true]
		]);

		if (empty($this->data)) {
			$this->Valor->locale = $idiomas;
			$this->data = $this->Valor->find('first', array(
				'conditions' => array('Valor.id' => $valor_id),
				'recursive' => -1
			));
		} else {
			if ($this->Valor->saveMany($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->set('idiomas', $idiomas);
		$this->set('tiposPagamentos', $this->Geral->getTiposPagamentos());
		$this->set('pessoas', $pessoas);
	}

	// public function admin_del($id = null)
	// {
	// 	parent::del($id);
	// }

	// public function getValores($tipo = null)
	// {
	// 	$this->autoRender = false;

	// 	$valores = $this->Valor->find('all', [
	// 		'recursive' => -1
	// 	]);
	// 	return json_encode(['valores' => $valores]);
	// 	// return json_encode(['tipo' => $tipo]);
	// }
}
