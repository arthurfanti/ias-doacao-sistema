<?php
App::uses('PaginasController', 'Controller');

class DoadoresController extends AppController
{
	public $title = 'Doadores';
	public $uses = array('Doador', 'Estado');

	var $components = array('RequestHandler');

	public function admin_index()
	{
		$data = new DateTime(date('Y-m-d'));
		$data->sub(new DateInterval('P10D'));

		if (isset($this->data['Doador']['valor']) && isset($this->data['Doador']['filtro'])) {
			$filtro = $this->data['Doador']['filtro'];
			$valor  = $this->data['Doador']['valor'];

			if ($filtro == 0) {
				$conditions = ['Doador.nome like' => '%'.$valor.'%'];
			} else if ($filtro == 1) {
				$conditions = ['Doador.cpf like' => '%'.$valor.'%'];
			}
		} else {
			$conditions = [
				'Doador.created >=' => $data->format('Y-m-d')
			];
		}

		$data = $this->Doador->find('all', [
			'joins' => [
				[
					'table'      => 'pessoas',
					'alias'      => 'Pessoa',
					'type'       => 'INNER',
					'conditions' => ['Doador.pessoa_id = Pessoa.id']]
			],
			'fields' => ['Doador.*', 'Pessoa.tipo'],
			'conditions' => $conditions,
			'recursive' => -1
		]);
		$this->set(compact('data'));
	}

	public function admin_adicionar()
	{
		$this->helpers[] = 'CakePtbr.Formatacao';

		$this->set('estados', $this->Estado->find('list'));

		$estadosArray = $this->Estado->find('all', array('recursive' => -1 ));
		foreach ($estadosArray as $index => $estado) {
			$options[$estado['Estado']['id']] = array(
				'name'    => $estado['Estado']['nome'],
				'value'   => $estado['Estado']['id'],
				'data-uf' => $estado['Estado']['uf']
			);
		}

		$this->set(compact('options'));

		$conheceu = $this->Doador->Conheceu->find('list', [
			'conditions' => ['Conheceu.status' => true],
			'order' => 'Conheceu.tipo'
		]);

		$this->set(compact('conheceu'));

		$faixas = $this->Doador->DpjComplemento->Faixa->find('list', [
			'conditions' => ['Faixa.status' => true]
		]);

		$naturezas = $this->Doador->DpjComplemento->Natureza->find('list', [
			'conditions' => ['Natureza.status' => true]
		]);

		$this->set(compact('faixas'));
		$this->set(compact('naturezas'));

		if ($this->request->is('post') && !empty($this->request->data)) {
			if ( isset($this->data['DoadorEndereco'][0]['cidade_id']) && !empty($this->data['DoadorEndereco'][0]['cidade_id']) ) {
				$cidade = $this->Estado->Cidade->find('first', [
					'conditions' => ['Cidade.id' => $this->data['DoadorEndereco'][0]['cidade_id']],
				]);

				$this->request->data['DoadorEndereco'][0]['cidade'] = $cidade['Cidade']['nome'];
				$this->request->data['DoadorEndereco'][0]['estado'] = $cidade['Estado']['nome'];
				$this->request->data['DoadorEndereco'][0]['uf'] = $cidade['Estado']['uf'];
			}

			$this->request->data['DoadorEndereco'][0]['pais'] = $this->request->data['Doador']['nacionalidade'];
			$this->request->data['Doador']['modificado']      = $this->Session->read('Auth.User.Doador.nome');

			$this->request->data['User']['username']       = $this->request->data['DoadorEmail'][0]['email'];
			$this->request->data['User']['email']          = $this->request->data['DoadorEmail'][0]['email'];
			$this->request->data['User']['password']       = $hash = hash('adler32', uniqid(rand(), true));
			$this->request->data['User']['temppassword']   = $hash = hash('adler32', uniqid(rand(), true));
			$this->request->data['User']['active']         = '1';
			$this->request->data['User']['email_verified'] = '1';

			$dataSource = $this->Doador->getDataSource();
			$dataSource->begin();

			if ($this->Doador->saveAll($this->request->data, ['atomic', true])) {
				$doador_id = $this->Doador->getLastInsertID();

				$dataSource->commit();

				$pagina  = new PaginasController();
				$EnviarXMLDoadorParaCRM  = $pagina->xmlgen( 'EnviarXMLDoadorParaCRM', null, $doador_id);

				$this->Geral->setFlash('Dados salvos com sucesso. Navegue para "Esqueci Minha Senha" para que uma nova senha seja enviada ao email do doador.', true);
				$this->redirect(['action' => 'index']);
			} else {
				// print_r($this->Doador->validationErrors);die;
				$this->Geral->setFlash('Não foi possível salvas as alterações', false);
			}
		}
	}

	public function admin_alterar($id = null)
	{
		$this->helpers[] = 'CakePtbr.Formatacao';

		if ($this->request->is(['post', 'put']) && !empty($this->request->data)) {
			if ( isset($this->data['DoadorEndereco'][0]['cidade_id']) && !empty($this->data['DoadorEndereco'][0]['cidade_id']) ) {
				$cidade = $this->Estado->Cidade->find('first', [
					'conditions' => ['Cidade.id' => $this->data['DoadorEndereco'][0]['cidade_id']],
				]);

				$this->request->data['DoadorEndereco'][0]['cidade'] = $cidade['Cidade']['nome'];
				$this->request->data['DoadorEndereco'][0]['estado'] = $cidade['Estado']['nome'];
				$this->request->data['DoadorEndereco'][0]['uf'] = $cidade['Estado']['uf'];
			}

			$this->request->data['DoadorEndereco'][0]['pais'] = $this->request->data['Doador']['nacionalidade'];
			$this->request->data['Doador']['modificado']      = $this->Session->read('Auth.User.Doador.nome');
			$this->request->data['User']['username']          = $this->request->data['DoadorEmail'][0]['email'];
			$this->request->data['User']['email']             = $this->request->data['DoadorEmail'][0]['email'];
			$cpf_cnpj                                         = $this->request->data['Doador']['cpf_cnpj'];

			unset($this->request->data['Doador']['cpf_cnpj']);

			if ($this->Doador->saveAll($this->request->data)) {
				$pagina  = new PaginasController();
				$EnviarXMLDoadorParaCRM  = $pagina->xmlgen( 'EnviarXMLDoadorParaCRM', null, $id);

				$this->Geral->setFlash('Dados salvos com sucesso', true);
				$this->redirect(['action' => 'index']);
			} else {
				$this->Geral->setFlash('Não foi possível salvas as alterações', false);
				$this->request->data['Doador']['cpf_cnpj'] = $cpf_cnpj;
			}
		} else {
			$this->set('estados', $this->Estado->find('list'));

			$estadosArray = $this->Estado->find('all', array('recursive' => -1 ));
			foreach ($estadosArray as $index => $estado) {
				$options[$estado['Estado']['id']] = array(
					'name'    => $estado['Estado']['nome'],
					'value'   => $estado['Estado']['id'],
					'data-uf' => $estado['Estado']['uf']
				);
			}

			$this->set(compact('options'));

			$conheceu = $this->Doador->Conheceu->find('list', [
				'conditions' => ['Conheceu.status' => true],
				'order' => 'Conheceu.tipo'
			]);

			$this->set(compact('conheceu'));

			$this->request->data = $this->Doador->findById($id);
		}

		$this->render('admin_adicionar');
	}

	public function admin_painel($doador_id = null)
	{
		if (!$doador_id) {
			$this->redirect(['action' => 'index']);
		}

		$this->helpers[] = 'CakePtbr.Formatacao';

		$this->Doador->Doacao->Behaviors->load('Containable');

		$doacoes = $this->Doador->Doacao->find('all', [
			'conditions' => [
				'Doacao.doador_id' => $doador_id,
				'Doacao.status is not null'
			],
			'contain' => ['Pagamento.FormasPagamento', 'Periodo'],
			'recursive' => -1
		]);

		$this->set('doacoes', $doacoes);

		$emails = $this->Doador->DoadorEmail->find('all', [
			'conditions' => [
				'DoadorEmail.doador_id' => $doador_id
			],
			'recursive' => -1
		]);
		$emails_ids = Hash::extract($emails, '{n}.DoadorEmail.id');

		$this->loadModel('LogAction');
		$logsDoador = $this->LogAction->find('all', [
			'conditions' =>[
				'LogAction.model' => 'Doador',
				'LogAction.row' => $doador_id,
				'LogAction.before <>' => ''
			],
			'recursive' => -1
		]);

		$logsEmails = $this->LogAction->find('all', [
			'conditions' =>[
				'LogAction.model' => 'DoadorEmail',
				'LogAction.row' => $emails_ids,
				'LogAction.before <>' => ''
			],
			'recursive' => -1
		]);

		$logsTmp = array_merge($logsDoador, $logsEmails);

		$this->Doador->Behaviors->load('Containable');

		$logs = [];
		foreach ($logsTmp as $log) {
			$doador = $this->Doador->find('first', [
				'conditions' => [
					'Doador.user_id' => $log['LogAction']['user_id']
				],
				'contain' => 'User',
				'recursive' => -1
			]);

			$log = array_merge($log, $doador);
			array_push($logs, $log);
		}

		$this->set('logs', $logs);

		$this->set('doador_id', $doador_id);
	}

	public function conta()
	{
		$this->helpers[] = 'CakePtbr.Formatacao';
		$this->helpers[] = 'Geral';

		$this->Session->delete($this->_tagSession);

		$doacoes = $this->Doador->Doacao->find('all', [
			'conditions' => [
				'Doacao.doador_id' => $this->Session->read('Auth.User.Doador.id'),
				'Doacao.status is not null'
			],
		]);

		for ($i=0; $i < count($doacoes); $i++) {
			unset($doacoes[$i]['Parcela']);

			$parcela = $this->Doador->Doacao->Parcela->find('first', [
				'conditions' => [
					'Parcela.doacao_id' => $doacoes[$i]['Doacao']['id']
				],
				'order' => 'Parcela.parcela DESC'
			]);
			$doacoes[$i]['Parcela'] = $parcela['Parcela'];
			$doacoes[$i]['FormasPagamento'] = $parcela['FormasPagamento'];
		}

		$this->set('doacoes', $doacoes);
	}

	public function dados()
	{

		$this->helpers[] = 'CakePtbr.Formatacao';

		if (empty($this->data)) {
			$this->data = $this->Doador->findById($this->Session->read('Auth.User.Doador.id'));
		} else {
			if ( isset($this->data['DoadorEndereco'][0]['cidade_id']) && !empty($this->data['DoadorEndereco'][0]['cidade_id']) ) {
				$cidade = $this->Estado->Cidade->find('first', [
					'conditions' => ['Cidade.id' => $this->data['DoadorEndereco'][0]['cidade_id']],
				]);

				$this->request->data['DoadorEndereco'][0]['cidade'] = $cidade['Cidade']['nome'];
				$this->request->data['DoadorEndereco'][0]['estado'] = $cidade['Estado']['nome'];
				$this->request->data['DoadorEndereco'][0]['uf'] = $cidade['Estado']['uf'];

				if ($this->request->data['Doacao']['filial']) {
					for ($i=0; $i < count($this->request->data['DoadorEndereco']); $i++) {
						$cidade = $this->Cidade->find('first', [
							'conditions' => ['Cidade.id' => $this->data['DoadorEndereco'][$i]['cidade_id']],
						]);

						$this->request->data['DoadorEndereco'][$i]['cidade'] = $cidade['Cidade']['nome'];
						$this->request->data['DoadorEndereco'][$i]['estado'] = $cidade['Estado']['nome'];
						$this->request->data['DoadorEndereco'][$i]['uf']     = $cidade['Estado']['uf'];
						// $this->request->data['DoadorEndereco'][$i]['pais']   = ( isset($doador['Doador']['nacionalidade']) ) ? $doador['Doador']['nacionalidade'] : 'BR';
					}
				}
			}

			// $this->request->data['Doador']['pessoa_id'] = 2;
			$this->request->data['User']['email']       = $this->data['DoadorEmail'][0]['email'];
			$this->request->data['User']['username']    = $this->data['DoadorEmail'][0]['email'];

			$cpf_cnpj = $this->request->data['Doador']['cpf_cnpj'];
			unset($this->request->data['Doador']['cpf_cnpj']);

			if ($this->Doador->saveAll($this->data)) {
				$this->Session->write('Auth.User.email', $this->data['DoadorEmail'][0]['email']);
				$this->Session->write('Auth.User.Doador.nome', $this->data['Doador']['nome']);
				$this->Session->write('Auth.User.Doador.sobrenome', $this->data['Doador']['sobrenome']);

				$pagina  = new PaginasController();
				$EnviarXMLDoadorParaCRM  = $pagina->xmlgen( 'EnviarXMLDoadorParaCRM', null );

				if ($EnviarXMLDoadorParaCRM) {
					$this->Geral->setFlash(__('Seus dados foram alterados com sucesso.'), true);
					$this->redirect('/conta');
				}
			} else {
				$this->request->data['Doador']['cpf_cnpj'] = $cpf_cnpj;
				debug($this->Doador->invalidFields());
				$this->Geral->setFlash(__('Não foi possível atualizar os dados.'), false);
			}
		}

		$this->set('estados', $this->Estado->find('list'));

		$estadosArray = $this->Estado->find('all', array('recursive' => -1 ));
		foreach ($estadosArray as $index => $estado) {
			$options[$estado['Estado']['id']] = array(
				'name'    => $estado['Estado']['nome'],
				'value'   => $estado['Estado']['id'],
				'data-uf' => $estado['Estado']['uf']
			);
		}

		$this->set(compact('options'));

		$faixa = $this->Doador->DpjComplemento->Faixa->find('list', [
			'conditions' => [
				'Faixa.status' => true
			]
		]);

		$natureza = $this->Doador->DpjComplemento->Natureza->find('list', [
			'conditions' => [
				'Natureza.status' => true
			]
		]);

		$this->set(compact('faixa'));
		$this->set(compact('natureza'));
	}

	public function nova_filial()
	{
		$this->autoRender = false;

		if ($this->request->is('ajax')) {
			$this->set('index', $this->request->data('index'));
			$this->viewPath = 'Elements';
			$this->render('form-filial');
		}
	}
}