<?php
class AceitesController extends AppController
{

	public $model = "Aceite";
	public $title = "Aceites";

	public function admin_index()
	{
		$this->Aceite->locale = 'por';
		$this->set('aceites', $this->Aceite->find('all', array(
			'recursive' => -1
		)));
	}

	public function admin_adicionar()
	{
		$pessoas = $this->Aceite->Pessoa->find('list', [
			'conditions' => ['Pessoa.status' => true]
		]);

		if (!empty($this->data)) {
			if ($this->Aceite->saveMany($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->set('idiomas', $this->Geral->getIdiomas());
		$this->set('pessoas', $pessoas);
	}

	public function admin_alterar($aceite_id = null)
	{
		if (!$aceite_id)
			$this->redirect(array('action' => 'index'));

		$idiomas = $this->Geral->getIdiomas();

		$pessoas = $this->Aceite->Pessoa->find('list', [
			'conditions' => ['Pessoa.status' => true]
		]);

		if (empty($this->data)) {
			$this->Aceite->locale = $idiomas;
			$this->data = $this->Aceite->find('first', array(
				'conditions' => array('Aceite.id' => $aceite_id),
				'recursive' => -1
			));
		} else {
			if ($this->Aceite->saveMany($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->set('idiomas', $idiomas);
		$this->set('pessoas', $pessoas);
	}
}