<?php
App::import('Vendor', 'Braspag', array('file' => 'Braspag/TransacaoBraspag.php'));
App::uses('PaginasController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class RelatoriosController extends AppController {

	public $title = 'Relatórios';
	public $uses = false;

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow('inicio', 'valores', 'doacao');
	}

	public function admin_index()
	{
		$this->helpers[] = 'CakePtbr.Formatacao';

		$this->loadModel('Parcela');
		$this->loadModel('Doacao');
		$this->loadModel('Doador');
		$this->loadModel('FormasPagamento');
		$this->loadModel('Periodo');
		$this->loadModel('Pessoa');
		$this->loadModel('Status');

		$this->Parcela->Behaviors->load('Containable');

		if (isset($this->data['Parcela']['data_inicial']) && isset($this->data['Parcela']['data_final']) && 
			!empty($this->data['Parcela']['data_inicial']) && !empty($this->data['Parcela']['data_final'])) {

			$conditions[] = [
				'Parcela.data_pagamento >= ' => $this->data['Parcela']['data_inicial'] . ' 00:00:00',
				'Parcela.data_pagamento <= ' => $this->data['Parcela']['data_final'] . ' 23:59:59'
			];

		} 	

		if(isset($this->data['tipo_doacao']) && !empty($this->data['tipo_doacao']))
		{
			$conditions[] = [
				'Doacao.periodo_id =' => $this->data['tipo_doacao']				
			];

			array_merge($conditions);
		}

		if(isset($this->data['formas_pagamento']) && !empty($this->data['formas_pagamento']))
		{
			$conditions[] = [
				'Parcela.formas_pagamento_id = ' => $this->data['formas_pagamento']
			];

			array_merge($conditions);
		}

		if(isset($this->data['valor']) && !empty($this->data['valor']))
		{
			$conditions[] = [
				'Parcela.valor =' => $this->data['valor']
			];

			array_merge($conditions);
		}

		if(isset($this->data['tipo_pessoa']) && !empty($this->data['tipo_pessoa']))
		{
			$conditions[] = [
				'Doador.pessoa_id =' => $this->data['tipo_pessoa']
			];

			array_merge($conditions);
		}

		if(isset($this->data['Parcela']['nome']) && !empty($this->data['Parcela']['nome']))
		{
			$conditions[] = [
				'OR' => [
					['Doador.nome LIKE ' => "%".$this->data['Parcela']['nome']."%"],
					['Doador.sobrenome LIKE ' => "%".$this->data['Parcela']['nome']."%"]
				]
				
			];

			array_merge($conditions);
		}

		if(isset($this->data['status']) && !empty($this->data['status']))
		{
			$conditions[] = [
				'Parcela.status_id =' => $this->data['status']
			];

			array_merge($conditions);
		}

		if(!isset($conditions))
		{
			$conditions = [
				'Doacao.created >=' => date('Y-m-d')
			];
		}

		$data = $this->Parcela->find('all', array(
			'fields' => array(
				'Parcela.id', 'Parcela.doacao_id' ,'Parcela.data_pagamento', 'Parcela.valor', 'Parcela.status_id','Parcela.formas_pagamento_id',
				'Doacao.id','Doacao.periodo_id','Doacao.doador_id','Doador.id','Doador.nome',
				'Doador.sobrenome','Doador.pessoa_id','Pessoa.id','Pessoa.tipo','FormasPagamento.id','FormasPagamento.nome',
				'Status.id','Status.situacao'
				),
			'joins' => array(
				array(
					'table' => 'doacoes',
			        'alias' => 'Doacao',
			        'type' => 'LEFT',
			        'conditions' => array('Doacao.id = Parcela.doacao_id')
		        ),
		        array(
					'table' => 'doadores',
			        'alias' => 'Doador',
			        'type' => 'LEFT',
			        'conditions' => array('Doador.id = Doacao.doador_id')
		        ),
		        array(
					'table' => 'pessoas',
			        'alias' => 'Pessoa',
			        'type' => 'LEFT',
			        'conditions' => array('Pessoa.id = Doador.pessoa_id')
		        ),
		        array(
					'table' => 'formas_pagamentos',
			        'alias' => 'FormasPagamento',
			        'type' => 'LEFT',
			        'conditions' => array('FormasPagamento.id = Parcela.formas_pagamento_id')
		        ),
		        array(
					'table' => 'status',
			        'alias' => 'Status',
			        'type' => 'LEFT',
			        'conditions' => array('Status.id = Parcela.status_id')
		        )
			),
			'conditions' => array(
				'OR' => array($conditions)
			),
			'recursive' => -1
			
		));

		//Resultados Totais
		$valores = $this->sumValues($data);

		//Guarda na sessao pra não fazer outra chamada no banco
		$this->Session->write('Relatorio', $data);
		$this->Session->write('Valores', $valores);

		$this->set(compact('data','valores'));

		$this->set('formas', $this->FormasPagamento->find('all', array(
			'recursive' => -1
		)));
		$this->set('periodos', $this->Periodo->find('all', array(
			'recursive' => -1
		)));
		$this->set('pessoas', $this->Pessoa->find('all', array(
			'recursive' => -1
		)));
		$this->set('status', $this->Status->find('all', array(
			'recursive' => -1
		)));
	}

	public function admin_excel()
	{
		$this->helpers[] = 'CakePtbr.Formatacao';

		$data = $this->Session->read('Relatorio');
		$valores = $this->Session->read('Valores');

		$date = date('d-m-Y');

		$this->response->header(array(
		    "Content-Type: application/vnd.ms-excel",
		    "Content-Disposition: attachment; filename=relatorio_doacoes_$date.xls"
		));

		$this->set(compact('data','valores'));	

		$this->layout = false;
	}

	protected function sumValues($dados)
	{
		$data = array();
		$data['total_doacoes'] = 0.00;
		$data['doacoes_concluidas'] = 0.00;
		$data['doacoes_pendentes'] = 0.00;

		foreach ($dados as $item) 
		{
			$data['total_doacoes'] += $item['Parcela']['valor'];	

			if($item['Status']['situacao'] == 'Pago')
			{
				$data['doacoes_concluidas']	+= $item['Parcela']['valor'];	
			}
			else
			{
				$data['doacoes_pendentes'] += $item['Parcela']['valor'];		
			}
		}


		$data['ticket'] = (count($dados) > 0) ? round( $data['total_doacoes'] / (count($dados)) ) : 0.00;
		
		return $data;

	}

}