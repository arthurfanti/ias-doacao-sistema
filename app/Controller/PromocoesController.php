<?php
class PromocoesController extends AppController
{

	public function admin_index()
	{
		$this->set('promocoes', $this->Promocao->find('all'));
	}

	public function admin_adicionar()
	{
		if (!empty($this->data)) {
			if ($this->Promocao->save($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}
	}

	public function admin_alterar($promocao_id = null)
	{
		if (!$promocao_id)
			$this->redirect(array('action' => 'index'));

		if (empty($this->data)) {
			$this->data = $this->Promocao->find('first', [
				'conditions' => ['Promocao.id' => $promocao_id],
				'recursive' => -1
			]);
		} else {
			if ($this->Promocao->save($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}
	}
}
