<?php
App::import('Vendor', 'Braspag', array('file' => 'Braspag/TransacaoBraspag.php'));
App::uses('PaginasController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class DoacoesController extends AppController {

	public $model = 'Doacao';
	public $title = 'Doações';

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow('inicio', 'valores', 'doacao');
	}

	public function admin_index()
	{
		$this->helpers[] = 'CakePtbr.Formatacao';

		$this->Doacao->Behaviors->load('Containable');

		if (isset($this->data['Doacao']['data_inicial']) && isset($this->data['Doacao']['data_final'])) {
			$conditions = [
				'Doacao.created >= ' => $this->data['Doacao']['data_inicial'] . ' 00:00:00',
				'Doacao.created <= ' => $this->data['Doacao']['data_final'] . ' 23:59:00'
			];
		} else {
			$conditions = [
				'Doacao.created >=' => date('Y-m-d')
			];
		}

		$data = $this->Doacao->find('all', [
			'contain' => [
				'Doador' => [
					'fields' => ['nome', 'sobrenome']
				],
				'Pagamento' => [
					'fields' => ['id', 'formas_pagamento_id']
				],
				'Pagamento.FormasPagamento' => [
					'fields' => 'nome'
				]
			],
			'fields' => [
				'Doacao.id', 'Doacao.created', 'Doacao.valor', 'Doacao.periodo_id',
			],
			'conditions' => $conditions,
			'recursive' => -1
		]);
		$this->set(compact('data'));
	}

	public function admin_adicionar($id = null)
	{
		$this->loadModel('Mensagem');
		$this->loadModel('Configuracao');

		$doacaoSession               = $this->Session->read($this->_tagSession);
		$doacaoSession['doador_id']  = $id;
		$doacaoSession['from_admin'] = true;

		if (isset($doacaoSession['concluiu'])) {
			$this->redirect('/complemento');
		}

		//se o usuário estiver logado salva o id na sessao
		if ($this->Session->check('Auth.User')) {
			//não permite que admin faça doação
			if ($this->Session->read('Auth.User.is_admin') && !isset($doacaoSession['from_admin'])) {
				$this->redirect('/');
			}

			//se from_admin não estiver setado é porque a doação não está sendo feita pelo admin em nome de outra pessoa
			if (!isset($doacaoSession['from_admin'])) {
				$doacaoSession['doador_id'] = $this->Session->read('Auth.User.Doador.id');
				$this->Session->write($this->_tagSession, $doacaoSession);
			}
		} else {
			if (!$doacaoSession || !isset($doacaoSession['doador']) || !isset($doacaoSession['valor']) || !isset($doacaoSession['tipo']))
				$this->redirect('/');
		}

		$dias = $this->Doacao->Pagamento->DiaDebito->find('list', [
			'conditions' => ['DiaDebito.status' => true]
		]);



		$this->set('dias', $dias);

		$pagamentoCredito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
			'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id'),
			'conditions' => array(
				'FormasPagamento.tipo'   => 'C',
				'FormasPagamento.status' => true
			),
			'recursive' => -1
		));

		$pagamentoDebito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
			'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id', 'FormasPagamento.ag_regex', 'FormasPagamento.conta_regex'),
			'conditions' => array(
				'FormasPagamento.tipo'   => 'D',
				'FormasPagamento.status' => true
			),
			'recursive' => -1
		));

		if ((isset($doacaoSession['doador']) && $doacaoSession['doador'] === 'PJ') ||
			($this->Session->check('Auth.User') && $this->Session->read('Auth.User.Doador.pessoa_id') == 2)) {
			$faixas = $this->Doacao->Doador->DpjComplemento->Faixa->find('list', [
				'conditions' => ['Faixa.status' => true]
			]);

			$naturezas = $this->Doacao->Doador->DpjComplemento->Natureza->find('list', [
				'conditions' => ['Natureza.status' => true]
			]);

			$mensagem = $this->Mensagem->find('first', [
				'conditions' => ['Mensagem.tipo' => 11]
			]);

			$aceite = $this->Doacao->AceiteDoacao->Aceite->find('first', [
				'conditions' => [
					'Aceite.status' => true,
					'Aceite.pessoa_id' => 2
				],
				'recursive' => -1
			]);

			$aceite2 = $this->Doacao->AceiteDoacao->Aceite->find('first', [
				'conditions' => [
					'Aceite.status' => true,
					'Aceite.pessoa_id' => 2,
					'Aceite.divulgacao' => true
				],
				'recursive' => -1
			]);

			$this->set(compact('aceite2'));
			$this->set(compact('faixas'));
			$this->set(compact('naturezas'));
			$this->set(compact('mensagem'));

		} else {
			$aceite = $this->Doacao->AceiteDoacao->Aceite->find('first', [
				'conditions' => [
					'Aceite.status' => true,
					'Aceite.pessoa_id' => 1
				],
				'recursive' => -1
			]);

		}

		$doadorInfo = $this->Doacao->Doador->find('first', [
			'conditions' => [
				'Doador.id' => $doacaoSession['doador_id']
			],
			'recursive' => -1
		]);

		$configs = $this->Configuracao->find('first');

		$this->set(compact('configs'));
		$this->set(compact('doadorInfo'));
		$this->set(compact('aceite'));
		$this->set(compact('pagamentoCredito'));
		$this->set(compact('pagamentoDebito'));

		//seta o sessionID do DeviceFingerPrint(Braspag)
		if ($this->Session->check('sessionID')) {
			$sessionID = $this->Session->read('sessionID');
		} else {
			$sessionID = substr(md5(date('dmY Hms')), 0, 50);
			$this->Session->write('sessionID', $sessionID);
		}
		$this->set('sessionID', $sessionID);

		if (empty($this->data)) {
			// $this->Geral->setMensagem(1);
			if (!isset($doacaoSession['valor'])) {
				$doacaoSession['valor'] = 0;
				$doacaoSession['tipo']  = 'M';
				$this->Session->write($this->_tagSession, $doacaoSession);
			}
		} else {
			$doacaoSession['valor'] = $this->data['Doacao']['valor'];
			$doacaoSession['tipo']  = $this->data['Doacao']['tipo'];
			$this->Session->write($this->_tagSession, $doacaoSession);

			$this->set('valor', $doacaoSession['valor']);
			$this->set('tipo', $doacaoSession['tipo']);

			$doacao = $this->data['Doacao'];
			unset($this->request->data['Doacao']);

			$this->request->data['Doacao'] = $doacao;

			$aceiteAdm = $this->Doacao->AceiteDoacao->Aceite->find('first', [
				'conditions' => [
					'Aceite.status' => true,
					'Aceite.pessoa_id' => $doadorInfo['Doador']['pessoa_id']
				],
				'recursive' => -1
			]);

			$this->request->data['AceiteDoacao']['aceite_id'] = $aceiteAdm['Aceite']['id'];

			//se doacao_id estiver na sessão é porque ela já foi iniciada e está salva no banco
			if (!isset($doacaoSession['doacao_id'])) {
				$salvou = $this->_salvarDadosDoacao($this->request->data, $doacaoSession['doador_id']);

				//se $salvou for array é porque deu erro
				if (is_array($salvou)) {
					foreach ($salvou as $erro) {
						$this->Geral->setFlash($erro[0], false);
						break;
					}
					return;
				}
			}

			$doacaoSession = $this->Session->read($this->_tagSession);

			$nossonumero          = '';
			$numero_doc           = '';
			$nsu                  = '';
			$autorizacao          = '';
			$mensagem_retorno     = '';
			$status_id            = '';
			$braspag_justclickkey = '';
			$data_pagamento       = date('Y-m-d H:i:s');
			$valor                = substr($this->data['Doacao']['valor'], 2);
			$valor                = str_replace(array('.', ','), array('', '.'), $valor);
			$order_id             = '';
			$cartao_final         = '';
			$cvv				  = '';
			$nova_tentativa		  = false;
			$pago                 = false;

			$doador = $this->Doacao->Doador->find('first', [
				'conditions' => [
					'Doador.id' => $doacaoSession['doador_id']
				],
				'recursive' => -1
			]);
			$emails = $this->Doacao->Doador->DoadorEmail->find('first', [
				'conditions' => [
					'DoadorEmail.doador_id' => $doacaoSession['doador_id']
				],
				'recursive' => -1
			]);
			$doador['DoadorEmail'] = $emails['DoadorEmail'];

			//efetua a transação
			if ($this->data['Doacao']['formaPagamento'] == 'credito') {

				$transacao = new TransacaoBraspag();
				$transacao->deviceFingerPrint = $sessionID;

				$qtd_dias_cliente = 0;
				if ($this->Session->check('Auth.User')) {
					if (isset($doacaoSession['from_admin'])) {
						$doador_array = $this->Doacao->Doador->find('first', [
							'conditions' => ['Doador.id' => $doacaoSession['doador_id']],
							'fields'     => ['Doador.nome', 'Doador.sobrenome', 'Doador.cpf_cnpj', 'Doador.created'],
							'recursive'  => -1
						]);

						$nome          = $doador_array['Doador']['nome'];
						$sobrenome     = $doador_array['Doador']['sobrenome'];
						$cpf_cnpj      = $doador_array['Doador']['cpf_cnpj'];
						$data_cadastro = $doador_array['Doador']['created'];
					} else {
						$nome          = $this->Session->read('Auth.User.Doador.nome');
						$sobrenome     = $this->Session->read('Auth.User.Doador.sobrenome');
						$cpf_cnpj      = $this->Session->read('Auth.User.Doador.cpf_cnpj');
						$data_cadastro = $this->Session->read('Auth.User.Doador.created');
					}

					$data_cadastro    = new DateTime($data_cadastro);
					$data_atual       = new DateTime(date('Ymd'));
					$diff             = $data_cadastro->diff($data_atual);
					$qtd_dias_cliente = $diff->format('%a');

					$transacao->customer_Name  = $nome . ' ' . $sobrenome;
					$transacao->customer_cpf   = $this->Geral->apenasNumeros($cpf_cnpj);

					$order_id = md5($cpf_cnpj . date('Ymd H:m:s'));
					$transacao->order_ID = substr($order_id, 0, 36);

					$transacao->billtodata_firstname     = $nome;
					$transacao->billtodata_lastname      = $sobrenome;


					$telefone = $this->Doacao->Doador->DoadorTelefone->find('first', [
						'conditions' => [
							'DoadorTelefone.doador_id' => $doacaoSession['doador_id'],
							'DoadorTelefone.telefone_id' => 1
						]
					]);
					$transacao->billtodata_phonenumber = $telefone['DoadorTelefone']['numero'];

					$email = $this->Doacao->Doador->DoadorEmail->find('first', [
						'conditions' => [
							'DoadorEmail.doador_id' => $doacaoSession['doador_id'],
							'DoadorEmail.email_id' => 1
						]
					]);
					$transacao->customer_Email = $email['DoadorEmail']['email'];
				} else {
					$transacao->customer_Name  = $this->data['Doador']['nome'] . ' ' . $this->data['Doador']['sobrenome'];

					$transacao->customer_Email = $this->data['DoadorEmail'][0]['email'];
					if (isset($this->data['Doador']['conta_brasil']) && $this->data['Doador']['conta_brasil'] == 1) {
						$transacao->customer_cpf   = $this->Geral->apenasNumeros($this->data['Doador']['cpf_cnpj']);
					}

					$order_id = '';
					if (isset($this->data['Doador']['conta_brasil']) && $this->data['Doador']['conta_brasil'] == 1) {
						$order_id = md5($this->data['Doador']['cpf_cnpj'] . date('Ymd H:m:s'));
					} else {
						$order_id = md5($this->data['DoadorEmail'][0]['email'] . date('Ymd H:m:s'));
					}
					$transacao->order_ID = substr($order_id, 0, 36);

					$transacao->billtodata_firstname     = $this->data['Doador']['nome'];
					$transacao->billtodata_lastname      = $this->data['Doador']['sobrenome'];
					$transacao->billtodata_phonenumber   = $this->data['DoadorTelefone'][0]['numero'];
				}

				$transacao->billtodata_city       = 'Mountain View';
				$transacao->billtodata_country    = 'US';
				$transacao->billtodata_state      = 'CA';
				$transacao->billtodata_street1    = '1295 Charleston Road';
				$transacao->billtodata_street2    = '';
				$transacao->billtodata_postalcode = '94043';

				$transacao->cartao                          = $this->request->data['Pagamento']['formas_pagamento_id'];
				$transacao->payment_CardNumber              = $this->data['Pagamento']['cartaoCreditoNum'];
				$transacao->payment_CardHolder              = $this->data['Pagamento']['cartaoCreditoNome'];
				$transacao->payment_CardMonthExpirationDate = $this->data['Pagamento']['validade_mes'];
				$transacao->payment_CardYearExpirationDate  = $this->data['Pagamento']['validade_ano'];
				$transacao->payment_CardSecurityCode        = $this->data['Pagamento']['cartaoCreditoCod'];
				$transacao->payment_Currency                = 'BRL';
				$transacao->payment_Country                 = 'BRA';
				$transacao->payment_Amount                  = str_replace(array('.', ','), array('', ''), $valor);
				$transacao->payment_AmountFormatado         = $valor;

				$transacao->customer_ID                     = $doacaoSession['doador_id'];
				$transacao->customer_tempo_cliente_em_dias  = $qtd_dias_cliente;
				$transacao->billtodata_ipaddress            = $_SERVER['REMOTE_ADDR'];

				$retorno_antifraude = $transacao->fraudAnalysis();

				CakeLog::write('antifraude', json_encode($retorno_antifraude));

				$braspag_codigos_erro    = $this->Geral->codigosErroBraspag();
				$antifraude_codigos_erro = $this->Geral->codigosErroAntifraude();
				$cyber_codigos_erro      = $this->Geral->codigosErroCyberAntifraude();
				// $redecard_codigos_erro   = $this->Geral->codigosErroRedecard();

				$mensagem_retorno = $retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->ReturnMessage;
				$status_id = (int)$retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->Status + 12;

				if (array_search($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->ReturnCode, $braspag_codigos_erro) ||
					array_search($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AntiFraudResponse->ReasonCode, $antifraude_codigos_erro)) {
					//não efetua transação
					$this->Geral->setFlash(__('Ocorreu um problema no processamento do pagamento.'), false);
				} else if (array_search($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AntiFraudResponse->ReasonCode, $cyber_codigos_erro)) {
					//não efetua transação e dispara email para o ias
					$this->Geral->setFlash(__('Sistema indisponível no momento'), false);

					$email = new CakeEmail('mandrill');
					$email->to('gabriel.medeiros@dominidesign.com.br');
					$email->subject('Erro no ias.doacaosenna.org.br');
					$email->send('Ocorreu um erro na integração com a Braspag: ' . $retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AntiFraudResponse->ReasonCode);

				} else {
					$order_id2 = md5($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->CorrelatedId . date('Ymd H:m:s'));
					$transacao->order_ID = substr($order_id2, 0, 36);

					$transacao->braspag_transactionId = $retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->BraspagTransactionId;

					$retorno_transacao = $transacao->efetuar_transacao();

					CakeLog::write('braspag', json_encode($retorno_transacao));

					// if ($retorno_transacao->PaymentDataCollection->PaymentDataResponse->Status == 0) {
					// 	$autorizacao = $retorno_transacao->PaymentDataCollection->PaymentDataResponse->AuthorizationCode;
					// 	$nsu = $retorno_transacao->PaymentDataCollection->PaymentDataResponse->ProofOfSale;

					// 	$pagina = new PaginasController();
					// 	$mandrill = $pagina->mandrill(1, $doador );

					// 	$mensagem_retorno = $retorno_transacao->PaymentDataCollection->PaymentDataResponse->ReturnMessage;
					// 	$status_id = (int)$retorno_transacao->PaymentDataCollection->PaymentDataResponse->Status + 12;

					// 	$pago = true;

					// 	$retorno_cartao = $transacao->salvarCartao();
					// 	if ($retorno_cartao->SaveCreditCardResult->Success) {
					// 		$braspag_justclickkey = $retorno_cartao->SaveCreditCardResult->JustClickKey;
					// 	}
					// } else {
					// 	$this->Geral->setFlash(__('Ocorreu um problema no processamento do pagamento.'), false);
					// }
					if ($retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->Status == 0) {
						$autorizacao = $retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->AuthorizationCode;
						$nsu = $retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->ProofOfSale;

						$pagina = new PaginasController();
						$mandrill = $pagina->mandrill(1, $doador);

						$mensagem_retorno = $retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->ReturnMessage;
						$status_id = (int)$retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->Status + 12;

						$pago = true;

						$retorno_cartao = $transacao->salvarCartao();
						if ($retorno_cartao->SaveCreditCardResult->Success) {
							$braspag_justclickkey = $retorno_cartao->SaveCreditCardResult->JustClickKey;
						}
					} else {
						$this->Geral->setFlash(__('Ocorreu um problema no processamento do pagamento.'), false);
					}
				}

				$cartao_final = substr($this->data['Pagamento']['cartaoCreditoNum'], -4);
				$cvv 		  = $this->data['Pagamento']['cartaoCreditoCod'];

			} else if ($this->data['Doacao']['formaPagamento'] == 'debito') {
				$pago = true;

				$dia_debito = $dias[$this->data['Pagamento']['dia_debito_id']];
				if (date('d') > $dia_debito) {
					if (date('m') == 12) {
						$ano_debito = date('Y') + 1;
						$data_pagamento = $ano_debito . '-01-' . $dia_debito;
					} else {
						$mes_debito = date('m') + 1;
						$data_pagamento = date('Y-' . $mes_debito . '-' . $dia_debito);
					}
				} else {
					$data_pagamento = date('Y-m-'.$dia_debito);
				}
				$status_id = 6;

				// $pagina = new PaginasController();
				// $mandrill = $pagina->mandrill( 3, $doador );

			} else if ($this->data['Doacao']['formaPagamento'] == 'boleto') {
				$this->request->data['Pagamento']['formas_pagamento_id'] = 1;

				$data_pagamento = $this->data['Pagamento']['dataVencimento'];

				$nossonumero = $doacaoSession['doacao_id'] . str_pad(1, 2, "0", STR_PAD_LEFT);
				$numero_doc  = $doacaoSession['doacao_id'] . '0' . str_pad(1, 2, "0", STR_PAD_LEFT);
				$status_id = 6;

				$pago = true;

				// $pagina = new PaginasController();
				// $mandrill = $pagina->mandrill( 4, $doador );
			}

			// if (empty($status_id)) {
			// 	$status_id = ($pago === true) ? 1 : 2;
			// }

			$parcela = $this->Doacao->Parcela->find('first', [
				'conditions' => ['Parcela.doacao_id' => $doacaoSession['doacao_id']],
				'recursive' => -1
			]);

			//salva a transacao independente se a doação foi paga ou não
			$transacao = $this->Doacao->Parcela->Transacao->create([
				'parcela_id'          => $parcela['Parcela']['id'],
				// 'tentativa_id'        => $tentativa_id,
				'formas_pagamento_id' => $this->data['Pagamento']['formas_pagamento_id'],
				'nossonumero'         => $nossonumero,
				'numero_documento'    => $numero_doc,
				'nsu'                 => $nsu,
				'autorizacao'         => $autorizacao,
				'mensagem_retorno'    => $mensagem_retorno,
				'status_id'           => $status_id
			]);
			$this->Doacao->Parcela->Transacao->save($transacao);

			//se estiver pago atualiza o status da doacao
			if ($pago === true) {
				$dataSource = $this->Doacao->Doador->getDataSource();
				$dataSource->begin();

				$pagamento = $this->Doacao->Pagamento->create([
					'formas_pagamento_id'  => $this->data['Pagamento']['formas_pagamento_id'],
					'banco_id'             => '',
					'agencia'              => $this->data['Pagamento']['debitoAgencia'],
					'conta'                => $this->Geral->apenasNumeros($this->data['Pagamento']['debitoConta']),
					'dia_debito_id'        => $this->data['Pagamento']['dia_debito_id'],
					'cartao_validade'      => $this->data['Pagamento']['validade_mes'].'/'.$this->data['Pagamento']['validade_ano'],
					'cartao_final'				 => $cartao_final,
					'braspag_justclickkey' => $braspag_justclickkey,
					'cvv'				   => $cvv
				]);
				$this->Doacao->Pagamento->save($pagamento);

				$pagamento_id = $this->Doacao->Pagamento->getLastInsertID();

				$doacao = $this->Doacao->find('first', [
					'conditions' => ['Doacao.id' => $doacaoSession['doacao_id']],
					'recursive' => -1
				]);
				$doacao['Doacao']['status']       = 'A';
				$doacao['Doacao']['pagamento_id'] = $pagamento_id;
				$doacao['Doacao']['valor']        = $valor;
				$this->Doacao->save($doacao);

				$parcela['Parcela']['formas_pagamento_id'] = $this->data['Pagamento']['formas_pagamento_id'];
				$parcela['Parcela']['status_id']           = $status_id;
				$parcela['Parcela']['valor']               = $valor;
				$parcela['Parcela']['order_id']            = $order_id;
				$parcela['Parcela']['nossonumero']         = $nossonumero;
				$parcela['Parcela']['numero_documento']    = $numero_doc;
				$parcela['Parcela']['nsu']                 = $nsu;
				$parcela['Parcela']['autorizacao']         = $autorizacao;
				$parcela['Parcela']['data_pagamento']      = $data_pagamento;
				$parcela['Parcela']['mensagem_retorno']    = $mensagem_retorno;
				$parcela['Parcela']['cartao_final']    	   = $cartao_final;
				$parcela['Parcela']['nova_tentativa']      = $nova_tentativa;
				$this->Doacao->Parcela->save($parcela);

				if (($this->data['Doacao']['formaPagamento'] == 'boleto') && ($this->data['Doacao']['tipo'] == 'M') && ($doacaoSession['doador'] == 'PJ')) {
					for ($i=1; $i < 12; $i++) {
						$data_tmp = explode('/', $this->data['Pagamento']['dataVencimento']);
						$data_geracao = $data_tmp[2] . '-' . $data_tmp[1] . '-' . $data_tmp[0];

						$dt_antiga = new DateTime($data_geracao);
						$dt_nova   = new DateTime($data_geracao);
						$dt_nova->add(new DateInterval('P'.$i.'M'));
						$mes_novo = $dt_nova->format('m');
						$mes_antigo = $dt_antiga->format('m');
						if (($mes_novo != ($mes_antigo + ($i % 12))) && ($mes_novo != '01')) {
							$dt_nova->sub(new DateInterval('P'.$dt_nova->format('d').'D'));
						}
						$nova_data_pagamento = $dt_nova->format('Y-m-d');

						$nossonumero = $doacaoSession['doacao_id'] . str_pad($i + 1, 2, "0", STR_PAD_LEFT);
						$numero_doc  = $doacaoSession['doacao_id'] . '0' . str_pad($i + 1, 2, "0", STR_PAD_LEFT);

						$nova_parcela = $this->Doacao->Parcela->create([
							'doacao_id'			  => $doacaoSession['doacao_id'],
							'formas_pagamento_id' => $this->data['Pagamento']['formas_pagamento_id'],
							'status_id'           => $status_id,
							'valor'               => $valor,
							'order_id'            => $order_id,
							'nossonumero'         => $nossonumero,
							'numero_documento'    => $numero_doc,
							'nsu'                 => $nsu,
							'autorizacao'         => $autorizacao,
							'data_pagamento'      => $nova_data_pagamento,
							'mensagem_retorno'    => $mensagem_retorno,
							'cartao_final' 	      => $cartao_final,
							'nova_tentativa'      => $nova_tentativa,
							'parcela'			  => $i + 1,
							'data_geracao'		  => date('Y-m-d H:i:s')
						]);

						$this->Doacao->Parcela->save($nova_parcela);
					}
				}

				//salva a tentativa de transacao
				$tentativa = $this->Doacao->Parcela->Tentativa->create([
					'parcela_id' => $parcela['Parcela']['id'],
					'tentativa'  => 1
				]);
				$this->Doacao->Parcela->Tentativa->save($tentativa);

				$tentativa_id = $this->Doacao->Parcela->Tentativa->getLastInsertID();

				$transacao['Transacao']['tentativa_id'] = $tentativa_id;
				$this->Doacao->Parcela->Transacao->save($transacao);

				$dataSource->commit();
			} else {
				return;
			}

			//se não tiver feito o login, localiza o usuário e salva na sessão com login feito
			if (!$this->Session->check('Auth.User')) {
				$this->loadModel('User');
				$this->loadModel('Doador');

				$doador = $this->Doador->find('first', [
					'conditions' => ['Doador.id' => $doacaoSession['doador_id']]
				]);
				$user = $this->User->find('first', [
					'conditions' => ['User.id' => $doador['Doador']['user_id']]
				]);
				$user['User']['Doador'] = $doador['Doador'];
				$this->Auth->login($user['User']);
			}

			$doacaoSession = $this->Session->read($this->_tagSession);
			$doacaoSession['concluiu'] = true;
			$this->Session->write($this->_tagSession, $doacaoSession);

			$pagina = new PaginasController();
			$EnviarXMLDoacoesParaCRM = $pagina->xmlgen( 'EnviarXMLDoacoesParaCRM', null );
			if (($this->data['formaPagamento'] == 'boleto') && ($this->data['Doacao']['tipo'] == 'M') && ($doacaoSession['doador'] == 'PJ')) {
				$EnviarXMLParcelaParaCRM = $pagina->xmlgen( 'EnviarXMLParcelaParaCRM', true, $doacaoSession['doacao_id'] );
			} else {
				$EnviarXMLParcelaParaCRM = $pagina->xmlgen( 'EnviarXMLParcelaParaCRM', null );
			}

			if ( $EnviarXMLDoacoesParaCRM && $EnviarXMLParcelaParaCRM ) {
				$this->redirect(['action' => 'complemento', 'admin' => true]);
			}
		}

		$this->set('valor', $doacaoSession['valor']);
		$this->set('tipo', $doacaoSession['tipo']);
	}

	// public function admin_nova($id = null)
	// {
	// 	$this->layout = false;
	// 	$this->autoRender = false;

	// 	if (!is_null($id)) {
	// 		$doacaoSession               = $this->Session->read($this->_tagSession);
	// 		$doacaoSession['doador_id']  = $id;
	// 		$doacaoSession['from_admin'] = true;

	// 		$this->Session->write($this->_tagSession, $doacaoSession);

	// 		if (!empty($this->Session->read($this->_tagSession))) {
	// 			$this->redirect('/doacao');
	// 		}
	// 	}
	// }

	public function admin_complemento()
	{
		// if ($this->request->is('post')) {
		// 	debug($this->request->data);die;
		// }

		$doacaoSession = $this->Session->read($this->_tagSession);

		if (!$doacaoSession || !isset($doacaoSession['valor']) || !isset($doacaoSession['tipo']))
				$this->redirect('/');

		$this->helpers[] = 'CakePtbr.Formatacao';

		$this->loadModel('Promocao');
		$this->loadModel('Mensagem');

		$this->Promocao->Item->Behaviors->load('Containable');

		$itens = $this->Promocao->Item->find('all', [
			'joins' => array(
				array(
					'table'      => 'promocoes',
					'alias'      => 'Promocao',
					'type'       => 'INNER',
					'conditions' => array(
						'Item.promocao_id = Promocao.id',
					)
				),
			),
			'fields' => ['Item.id', 'Item.item', 'Item.imagem'],
			'conditions' => [
				'Promocao.status' => true,
				'Item.status' => true
			],
			'contain' => ['Atributo', 'Atributo.Conteudo'],
			'recursive' => -1
		]);

		$doador = $this->Doacao->Doador->find('first', [
			'conditions' => [
				'Doador.id' => $doacaoSession['doador_id']
			]
		]);

		if (empty($this->data)) {
			// se o doador já tiver endereço e não tem brinde disponível redireciona para a próxima tela
			if (isset($doador['DoadorEndereco'][0]) && !isset($doacaoSession['from_admin']) && (count($itens) == 0)) {//AQUI - adicionar verificação de brindes disponíveis
				$doacaoSession = $this->Session->read($this->_tagSession);
				unset($doacaoSession['concluiu']);
				$this->Session->write($this->_tagSession, $doacaoSession);

				$this->redirect('/agradecimento');
			}
			$this->data = $doador;
			//
		} else {
			// if ( !isset($doador['DoadorEndereco'][0]) && !isset($doacaoSession['from_admin']) ){
			// 	$this->loadModel('Cidade');

			// 	$cidade = $this->Cidade->find('first', [
			// 		'conditions' => ['Cidade.id' => $this->data['DoadorEndereco'][0]['cidade_id']],
			// 	]);

			// 	$doador = $this->Doacao->Doador->find('first', [
			// 		'fields'     => ['Doador.nacionalidade'],
			// 		'conditions' => [
			// 			'Doador.id' => $this->Session->read('Auth.User.Doador.id')
			// 		],
			// 		'recursive' => -1
			// 	]);

			// 	$this->request->data['DoadorEndereco'][0]['cidade'] = $cidade['Cidade']['nome'];
			// 	$this->request->data['DoadorEndereco'][0]['estado'] = $cidade['Estado']['nome'];
			// 	$this->request->data['DoadorEndereco'][0]['uf']     = $cidade['Estado']['uf'];
			// 	$this->request->data['DoadorEndereco'][0]['pais']   = ( isset($doador['Doador']['nacionalidade']) ) ? $doador['Doador']['nacionalidade'] : 'BR';

			// 	if (isset($this->request->data['Doacao']['filial'])) {
			// 		for ($i=0; $i < count($this->request->data['DoadorEndereco']); $i++) {
			// 			$cidade = $this->Cidade->find('first', [
			// 				'conditions' => ['Cidade.id' => $this->data['DoadorEndereco'][$i]['cidade_id']],
			// 			]);

			// 			$this->request->data['DoadorEndereco'][$i]['cidade'] = $cidade['Cidade']['nome'];
			// 			$this->request->data['DoadorEndereco'][$i]['estado'] = $cidade['Estado']['nome'];
			// 			$this->request->data['DoadorEndereco'][$i]['uf']     = $cidade['Estado']['uf'];
			// 			$this->request->data['DoadorEndereco'][$i]['pais']   = ( isset($doador['Doador']['nacionalidade']) ) ? $doador['Doador']['nacionalidade'] : 'BR';
			// 		}
			// 	}
			// }

			if (count($itens) > 0) {
				$this->request->data['Brinde']['doacao_id'] = $doacaoSession['doacao_id'];
				$this->request->data['Brinde']['status']    = 1;

				$dataSource = $this->Doacao->getDataSource();
				$dataSource->begin();

				$this->Doacao->Brinde->save($this->request->data['Brinde']);
				$brinde_id = $this->Doacao->Brinde->getLastInsertID();

				$this->Doacao->Brinde->Item->Atributo->Conteudo->Behaviors->load('Containable');

				foreach ($this->data['BrindeConteudo'] as $brindeConteudo) {
					$conteudo = $this->Doacao->Brinde->Item->Atributo->Conteudo->find('first', [
						'conditions' => [
							'Conteudo.id' => $brindeConteudo['conteudo_id']
						],
						'contain' => 'Atributo',
						'recursive' => -1
					]);

					$novo_brinde_conteudo = $this->Doacao->Brinde->BrindeConteudo->create([
						'atributo' => $conteudo['Atributo']['atributo'],
						'conteudo' => $conteudo['Conteudo']['conteudo'],
						'brinde_id' => $brinde_id
					]);
					$this->Doacao->Brinde->BrindeConteudo->save($novo_brinde_conteudo);
				}

				$dataSource->commit();
			}

			if ($this->Doacao->Doador->saveAll($this->data)) {
				$doacaoSession = $this->Session->read($this->_tagSession);
				unset($doacaoSession['concluiu']);
				$this->Session->write($this->_tagSession, $doacaoSession);

				$pagina = new PaginasController();
				$EnviarXMLDoadorParaCRM  = $pagina->xmlgen( 'EnviarXMLDoadorParaCRM', null );
				$EnviarXMLDoacoesParaCRM = $pagina->xmlgen( 'EnviarXMLDoacoesParaCRM', null );

				if ($EnviarXMLDoadorParaCRM && $EnviarXMLDoacoesParaCRM) {
					$this->redirect('/agradecimento');
				}

			} else {
				foreach ($this->Doacao->Doador->validationErrors as $erro) {
					$this->Geral->setFlash($erro[0], false);
					break;
				}
			}
		}

		// $conheceu = $this->Doacao->Doador->Conheceu->find('list', [
		// 	'conditions' => ['Conheceu.status' => true],
		// 	'order' => 'Conheceu.tipo'
		// ]);

		$doacao = $this->Doacao->find('first', [
			'conditions' => ['Doacao.id' => $doacaoSession['doacao_id']]
		]);

		// if ($this->Session->read('Auth.User.Doador.pessoa_id' == 2)) {
		// 	$mensagem = $this->Mensagem->find('first', [
		// 		'conditions' => ['Mensagem.tipo' => 12]
		// 	]);
		// } else {
		// 	$mensagem = $this->Mensagem->find('first', [
		// 		'conditions' => ['Mensagem.tipo' => 2]
		// 	]);
		// }

		//salva na sessao que é boleto para não exibir o link na tela de agradecimento
		if (isset($doacao['Pagamento']) && ($doacao['Pagamento']['formas_pagamento_id'] == '1'))
			$this->Session->write('boleto', true);

		$this->set('itens', $itens);
		// $this->set('conheceu', $conheceu);
		$this->set('doacao', $doacao);
		// $this->set(compact('mensagem'));

		// $this->render('complemento');
	}

	public function admin_alterar($id = null)
	{
		// $this->layout = false;
		$this->autoRender = false;
		$this->loadModel('Configuracao');

		if (!is_null($id)) {
			$doadorSession = $this->Doacao->Doador->find('first', [
				'joins' => array(
					array(
						'table'      => 'doacoes',
						'alias'      => 'Doacao',
						'type'       => 'INNER',
						'conditions' => array(
							'Doador.id = Doacao.doador_id',
							'Doacao.id' => $id
						)
					),
				),
				'recursive' => -1
			]);
			$doadorSession['Doador']['modificado'] = $this->Session->read('Auth.User.email');
			$doadorSession['from_admin'] = true;

			$pagamentoCredito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
				'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id'),
				'conditions' => array(
					'FormasPagamento.tipo'   => 'C',
					'FormasPagamento.status' => true
				),
				'recursive' => -1
			));

			$pagamentoDebito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
				'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id', 'FormasPagamento.ag_regex', 'FormasPagamento.conta_regex'),
				'conditions' => array(
					'FormasPagamento.tipo'   => 'D',
					'FormasPagamento.status' => true
				),
				'recursive' => -1
			));

			$this->Doacao->Behaviors->load('Containable');
			$doacao = $this->Doacao->find('first', [
				'conditions' => [
					'Doacao.doador_id' => $doadorSession['Doador']['id'],
					'Doacao.id' => $id
				],
				'contain' => ['Pagamento', 'Pagamento.FormasPagamento' => [
					'fields' => 'conta_regex',
					'conditions' => [
						'status' => true
					]
				]],
				'recursive' => -1
			]);

			$parcelas = $this->Doacao->Parcela->find('all', [
				'conditions' => ['Parcela.doacao_id' => $id],
				'order' => 'Parcela.parcela',
			]);

			$dias = $this->Doacao->Pagamento->DiaDebito->find('list', [
				'conditions' => ['DiaDebito.status' => true]
			]);

			$doadorInfo = $this->Doacao->Doador->find('first', [
				'conditions' => [
					'Doador.id' => $doadorSession['Doador']['id']
				],
				'recursive' => -1
			]);

			$configs = $this->Configuracao->find('first');


			$this->set('parcelas', $parcelas);
			$this->set('parcela', $parcelas[0]);

			$this->set(compact('dias'));
			$this->set(compact('doacao'));
			$this->set(compact('configs'));
			$this->set(compact('doadorInfo'));
			$this->set(compact('pagamentoCredito'));
			$this->set(compact('pagamentoDebito'));
			$this->data = $doacao;

			if($this->Session->write($this->_tagSession, $doadorSession)) {
				// $this->redirect('/doacoes/' . $id);
				$this->render('doacoes');
			}
		}

		if ($this->request->is(['post', 'put'])) {
			//efetua a transação
			if ($this->data['Doacao']['formaPagamento'] == 'credito') {

				$transacao = new TransacaoBraspag();
				$transacao->deviceFingerPrint = $sessionID;

				$qtd_dias_cliente = 0;
				if ($this->Session->check('Auth.User')) {
					if (isset($doacaoSession['from_admin'])) {
						$doador_array = $this->Doacao->Doador->find('first', [
							'conditions' => ['Doador.id' => $doacaoSession['doador_id']],
							'fields'     => ['Doador.nome', 'Doador.sobrenome', 'Doador.cpf_cnpj', 'Doador.created'],
							'recursive'  => -1
						]);

						$nome          = $doador_array['Doador']['nome'];
						$sobrenome     = $doador_array['Doador']['sobrenome'];
						$cpf_cnpj      = $doador_array['Doador']['cpf_cnpj'];
						$data_cadastro = $doador_array['Doador']['created'];
					} else {
						$nome          = $this->Session->read('Auth.User.Doador.nome');
						$sobrenome     = $this->Session->read('Auth.User.Doador.sobrenome');
						$cpf_cnpj      = $this->Session->read('Auth.User.Doador.cpf_cnpj');
						$data_cadastro = $this->Session->read('Auth.User.Doador.created');
					}

					$data_cadastro    = new DateTime($data_cadastro);
					$data_atual       = new DateTime(date('Ymd'));
					$diff             = $data_cadastro->diff($data_atual);
					$qtd_dias_cliente = $diff->format('%a');

					$transacao->customer_Name  = $nome . ' ' . $sobrenome;
					$transacao->customer_cpf   = $this->Geral->apenasNumeros($cpf_cnpj);

					$order_id = md5($cpf_cnpj . date('Ymd H:m:s'));
					$transacao->order_ID = substr($order_id, 0, 36);

					$transacao->billtodata_firstname     = $nome;
					$transacao->billtodata_lastname      = $sobrenome;


					$telefone = $this->Doacao->Doador->DoadorTelefone->find('first', [
						'conditions' => [
							'DoadorTelefone.doador_id' => $doacaoSession['doador_id'],
							'DoadorTelefone.telefone_id' => 1
						]
					]);
					$transacao->billtodata_phonenumber = $telefone['DoadorTelefone']['numero'];

					$email = $this->Doacao->Doador->DoadorEmail->find('first', [
						'conditions' => [
							'DoadorEmail.doador_id' => $doacaoSession['doador_id'],
							'DoadorEmail.email_id' => 1
						]
					]);
					$transacao->customer_Email = $email['DoadorEmail']['email'];
				} else {
					$transacao->customer_Name  = $this->data['Doador']['nome'] . ' ' . $this->data['Doador']['sobrenome'];

					$transacao->customer_Email = $this->data['DoadorEmail'][0]['email'];
					if (isset($this->data['Doador']['conta_brasil']) && $this->data['Doador']['conta_brasil'] == 1) {
						$transacao->customer_cpf   = $this->Geral->apenasNumeros($this->data['Doador']['cpf_cnpj']);
					}

					$order_id = '';
					if (isset($this->data['Doador']['conta_brasil']) && $this->data['Doador']['conta_brasil'] == 1) {
						$order_id = md5($this->data['Doador']['cpf_cnpj'] . date('Ymd H:m:s'));
					} else {
						$order_id = md5($this->data['DoadorEmail'][0]['email'] . date('Ymd H:m:s'));
					}
					$transacao->order_ID = substr($order_id, 0, 36);

					$transacao->billtodata_firstname     = $this->data['Doador']['nome'];
					$transacao->billtodata_lastname      = $this->data['Doador']['sobrenome'];
					$transacao->billtodata_phonenumber   = $this->data['DoadorTelefone'][0]['numero'];
				}

				$transacao->billtodata_city       = 'Mountain View';
				$transacao->billtodata_country    = 'US';
				$transacao->billtodata_state      = 'CA';
				$transacao->billtodata_street1    = '1295 Charleston Road';
				$transacao->billtodata_street2    = '';
				$transacao->billtodata_postalcode = '94043';

				$transacao->cartao                          = $this->request->data['Pagamento']['formas_pagamento_id'];
				$transacao->payment_CardNumber              = $this->data['Pagamento']['cartaoCreditoNum'];
				$transacao->payment_CardHolder              = $this->data['Pagamento']['cartaoCreditoNome'];
				$transacao->payment_CardMonthExpirationDate = $this->data['Pagamento']['validade_mes'];
				$transacao->payment_CardYearExpirationDate  = $this->data['Pagamento']['validade_ano'];
				$transacao->payment_CardSecurityCode        = $this->data['Pagamento']['cartaoCreditoCod'];
				$transacao->payment_Currency                = 'BRL';
				$transacao->payment_Country                 = 'BRA';
				$transacao->payment_Amount                  = str_replace(array('.', ','), array('', ''), $valor);
				$transacao->payment_AmountFormatado         = $valor;

				$transacao->customer_ID                     = $doacaoSession['doador_id'];
				$transacao->customer_tempo_cliente_em_dias  = $qtd_dias_cliente;
				$transacao->billtodata_ipaddress            = $_SERVER['REMOTE_ADDR'];

				$retorno_antifraude = $transacao->fraudAnalysis();

				CakeLog::write('antifraude', json_encode($retorno_antifraude));

				$braspag_codigos_erro    = $this->Geral->codigosErroBraspag();
				$antifraude_codigos_erro = $this->Geral->codigosErroAntifraude();
				$cyber_codigos_erro      = $this->Geral->codigosErroCyberAntifraude();
				// $redecard_codigos_erro   = $this->Geral->codigosErroRedecard();

				$mensagem_retorno = $retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->ReturnMessage;
				$status_id = (int)$retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->Status + 12;

				if (array_search($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->ReturnCode, $braspag_codigos_erro) ||
					array_search($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AntiFraudResponse->ReasonCode, $antifraude_codigos_erro)) {
					//não efetua transação
					$this->Geral->setFlash(__('Ocorreu um problema no processamento do pagamento.'), false);
				} else if (array_search($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AntiFraudResponse->ReasonCode, $cyber_codigos_erro)) {
					//não efetua transação e dispara email para o ias
					$this->Geral->setFlash(__('Sistema indisponível no momento'), false);

					$email = new CakeEmail('mandrill');
					$email->to('gabriel.medeiros@dominidesign.com.br');
					$email->subject('Erro no ias.doacaosenna.org.br');
					$email->send('Ocorreu um erro na integração com a Braspag: ' . $retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AntiFraudResponse->ReasonCode);

				} else {
					$order_id2 = md5($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->CorrelatedId . date('Ymd H:m:s'));
					$transacao->order_ID = substr($order_id2, 0, 36);

					$transacao->braspag_transactionId = $retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->BraspagTransactionId;

					$retorno_transacao = $transacao->efetuar_transacao();

					CakeLog::write('braspag', json_encode($retorno_transacao));

					// if ($retorno_transacao->PaymentDataCollection->PaymentDataResponse->Status == 0) {
					// 	$autorizacao = $retorno_transacao->PaymentDataCollection->PaymentDataResponse->AuthorizationCode;
					// 	$nsu = $retorno_transacao->PaymentDataCollection->PaymentDataResponse->ProofOfSale;

					// 	$pagina = new PaginasController();
					// 	$mandrill = $pagina->mandrill(1, $doador );

					// 	$mensagem_retorno = $retorno_transacao->PaymentDataCollection->PaymentDataResponse->ReturnMessage;
					// 	$status_id = (int)$retorno_transacao->PaymentDataCollection->PaymentDataResponse->Status + 12;

					// 	$pago = true;

					// 	$retorno_cartao = $transacao->salvarCartao();
					// 	if ($retorno_cartao->SaveCreditCardResult->Success) {
					// 		$braspag_justclickkey = $retorno_cartao->SaveCreditCardResult->JustClickKey;
					// 	}
					// } else {
					// 	$this->Geral->setFlash(__('Ocorreu um problema no processamento do pagamento.'), false);
					// }
					if ($retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->Status == 0) {
						$autorizacao = $retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->AuthorizationCode;
						$nsu = $retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->ProofOfSale;

						$pagina = new PaginasController();
						$mandrill = $pagina->mandrill(1, $doador);

						$mensagem_retorno = $retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->ReturnMessage;
						$status_id = (int)$retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->Status + 12;

						$pago = true;

						$retorno_cartao = $transacao->salvarCartao();
						if ($retorno_cartao->SaveCreditCardResult->Success) {
							$braspag_justclickkey = $retorno_cartao->SaveCreditCardResult->JustClickKey;
						}
					} else {
						$this->Geral->setFlash(__('Ocorreu um problema no processamento do pagamento.'), false);
					}
				}

				$cartao_final = substr($this->data['Pagamento']['cartaoCreditoNum'], -4);
				$cvv 		  = $this->data['Pagamento']['cartaoCreditoCod'];

			} else if ($this->data['Doacao']['formaPagamento'] == 'debito') {
				$pago = true;

				$dia_debito = $dias[$this->data['Pagamento']['dia_debito_id']];
				if (date('d') > $dia_debito) {
					if (date('m') == 12) {
						$ano_debito = date('Y') + 1;
						$data_pagamento = $ano_debito . '-01-' . $dia_debito;
					} else {
						$mes_debito = date('m') + 1;
						$data_pagamento = date('Y-' . $mes_debito . '-' . $dia_debito);
					}
				} else {
					$data_pagamento = date('Y-m-'.$dia_debito);
				}
				$status_id = 6;

				// $pagina = new PaginasController();
				// $mandrill = $pagina->mandrill( 3, $doador );

			} else if ($this->data['Doacao']['formaPagamento'] == 'boleto') {
				$this->request->data['Pagamento']['formas_pagamento_id'] = 1;

				$data_pagamento = $this->data['Pagamento']['dataVencimento'];

				$nossonumero = $doacaoSession['doacao_id'] . str_pad(1, 2, "0", STR_PAD_LEFT);
				$numero_doc  = $doacaoSession['doacao_id'] . '0' . str_pad(1, 2, "0", STR_PAD_LEFT);
				$status_id = 6;

				$pago = true;

				// $pagina = new PaginasController();
				// $mandrill = $pagina->mandrill( 4, $doador );
			}

			// if (empty($status_id)) {
			// 	$status_id = ($pago === true) ? 1 : 2;
			// }

			$parcela = $this->Doacao->Parcela->find('first', [
				'conditions' => ['Parcela.doacao_id' => $doacaoSession['doacao_id']],
				'recursive' => -1
			]);

			//salva a transacao independente se a doação foi paga ou não
			$transacao = $this->Doacao->Parcela->Transacao->create([
				'parcela_id'          => $parcela['Parcela']['id'],
				// 'tentativa_id'        => $tentativa_id,
				'formas_pagamento_id' => $this->data['Pagamento']['formas_pagamento_id'],
				'nossonumero'         => $nossonumero,
				'numero_documento'    => $numero_doc,
				'nsu'                 => $nsu,
				'autorizacao'         => $autorizacao,
				'mensagem_retorno'    => $mensagem_retorno,
				'status_id'           => $status_id
			]);
			$this->Doacao->Parcela->Transacao->save($transacao);

			//se estiver pago atualiza o status da doacao
			if ($pago === true) {
				$dataSource = $this->Doacao->Doador->getDataSource();
				$dataSource->begin();

				$pagamento = $this->Doacao->Pagamento->create([
					'formas_pagamento_id'  => $this->data['Pagamento']['formas_pagamento_id'],
					'banco_id'             => '',
					'agencia'              => $this->data['Pagamento']['debitoAgencia'],
					'conta'                => $this->Geral->apenasNumeros($this->data['Pagamento']['debitoConta']),
					'dia_debito_id'        => $this->data['Pagamento']['dia_debito_id'],
					'cartao_validade'      => $this->data['Pagamento']['validade_mes'].'/'.$this->data['Pagamento']['validade_ano'],
					'cartao_final'				 => $cartao_final,
					'braspag_justclickkey' => $braspag_justclickkey,
					'cvv'				   => $cvv
				]);
				$this->Doacao->Pagamento->save($pagamento);

				$pagamento_id = $this->Doacao->Pagamento->getLastInsertID();

				$doacao = $this->Doacao->find('first', [
					'conditions' => ['Doacao.id' => $doacaoSession['doacao_id']],
					'recursive' => -1
				]);
				$doacao['Doacao']['status']       = 'A';
				$doacao['Doacao']['pagamento_id'] = $pagamento_id;
				$doacao['Doacao']['valor']        = $valor;
				$this->Doacao->save($doacao);

				$parcela['Parcela']['formas_pagamento_id'] = $this->data['Pagamento']['formas_pagamento_id'];
				$parcela['Parcela']['status_id']           = $status_id;
				$parcela['Parcela']['valor']               = $valor;
				$parcela['Parcela']['order_id']            = $order_id;
				$parcela['Parcela']['nossonumero']         = $nossonumero;
				$parcela['Parcela']['numero_documento']    = $numero_doc;
				$parcela['Parcela']['nsu']                 = $nsu;
				$parcela['Parcela']['autorizacao']         = $autorizacao;
				$parcela['Parcela']['data_pagamento']      = $data_pagamento;
				$parcela['Parcela']['mensagem_retorno']    = $mensagem_retorno;
				$parcela['Parcela']['cartao_final']    	   = $cartao_final;
				$parcela['Parcela']['nova_tentativa']      = $nova_tentativa;
				$this->Doacao->Parcela->save($parcela);

				if (($this->data['Doacao']['formaPagamento'] == 'boleto') && ($this->data['Doacao']['tipo'] == 'M') && ($doacaoSession['doador'] == 'PJ')) {
					for ($i=1; $i < 12; $i++) {
						$data_tmp = explode('/', $this->data['Pagamento']['dataVencimento']);
						$data_geracao = $data_tmp[2] . '-' . $data_tmp[1] . '-' . $data_tmp[0];

						$dt_antiga = new DateTime($data_geracao);
						$dt_nova   = new DateTime($data_geracao);
						$dt_nova->add(new DateInterval('P'.$i.'M'));
						$mes_novo = $dt_nova->format('m');
						$mes_antigo = $dt_antiga->format('m');
						if (($mes_novo != ($mes_antigo + ($i % 12))) && ($mes_novo != '01')) {
							$dt_nova->sub(new DateInterval('P'.$dt_nova->format('d').'D'));
						}
						$nova_data_pagamento = $dt_nova->format('Y-m-d');

						$nossonumero = $doacaoSession['doacao_id'] . str_pad($i + 1, 2, "0", STR_PAD_LEFT);
						$numero_doc  = $doacaoSession['doacao_id'] . '0' . str_pad($i + 1, 2, "0", STR_PAD_LEFT);

						$nova_parcela = $this->Doacao->Parcela->create([
							'doacao_id'			  => $doacaoSession['doacao_id'],
							'formas_pagamento_id' => $this->data['Pagamento']['formas_pagamento_id'],
							'status_id'           => $status_id,
							'valor'               => $valor,
							'order_id'            => $order_id,
							'nossonumero'         => $nossonumero,
							'numero_documento'    => $numero_doc,
							'nsu'                 => $nsu,
							'autorizacao'         => $autorizacao,
							'data_pagamento'      => $nova_data_pagamento,
							'mensagem_retorno'    => $mensagem_retorno,
							'cartao_final' 	      => $cartao_final,
							'nova_tentativa'      => $nova_tentativa,
							'parcela'			  => $i + 1,
							'data_geracao'		  => date('Y-m-d H:i:s')
						]);

						$this->Doacao->Parcela->save($nova_parcela);
					}
				}

				//salva a tentativa de transacao
				$tentativa = $this->Doacao->Parcela->Tentativa->create([
					'parcela_id' => $parcela['Parcela']['id'],
					'tentativa'  => 1
				]);
				$this->Doacao->Parcela->Tentativa->save($tentativa);

				$tentativa_id = $this->Doacao->Parcela->Tentativa->getLastInsertID();

				$transacao['Transacao']['tentativa_id'] = $tentativa_id;
				$this->Doacao->Parcela->Transacao->save($transacao);

				$dataSource->commit();
			} else {
				return;
			}

			//se não tiver feito o login, localiza o usuário e salva na sessão com login feito
			if (!$this->Session->check('Auth.User')) {
				$this->loadModel('User');
				$this->loadModel('Doador');

				$doador = $this->Doador->find('first', [
					'conditions' => ['Doador.id' => $doacaoSession['doador_id']]
				]);
				$user = $this->User->find('first', [
					'conditions' => ['User.id' => $doador['Doador']['user_id']]
				]);
				$user['User']['Doador'] = $doador['Doador'];
				$this->Auth->login($user['User']);
			}

			$doacaoSession = $this->Session->read($this->_tagSession);
			$doacaoSession['concluiu'] = true;
			$this->Session->write($this->_tagSession, $doacaoSession);

			$pagina = new PaginasController();
			$EnviarXMLDoacoesParaCRM = $pagina->xmlgen( 'EnviarXMLDoacoesParaCRM', null );
			if (($this->data['formaPagamento'] == 'boleto') && ($this->data['Doacao']['tipo'] == 'M') && ($doacaoSession['doador'] == 'PJ')) {
				$EnviarXMLParcelaParaCRM = $pagina->xmlgen( 'EnviarXMLParcelaParaCRM', true, $doacaoSession['doacao_id'] );
			} else {
				$EnviarXMLParcelaParaCRM = $pagina->xmlgen( 'EnviarXMLParcelaParaCRM', null );
			}

			if ( $EnviarXMLDoacoesParaCRM && $EnviarXMLParcelaParaCRM ) {
				$this->redirect(['action' => 'complemento', 'admin' => true]);
			}
		}
	}
	/*
	public function admin_doacoes($doacao_id = null)
	{
		if (!$doacao_id) {
			$this->redirect(['action' => 'index']);
		}

		if (null !== $this->Session->read($this->_tagSession . '.Doador')) {
			$doacao = $this->Doacao->find('first', [
				'conditions' => [
					// 'Doacao.doador_id' => $this->Session->read($this->_tagSession . '.Doador.id'),
					'Doacao.id' => $doacao_id
				]
			]);
		} else {
			$doacao = $this->Doacao->find('first', [
				'conditions' => [
					// 'Doacao.doador_id' => $this->Session->read('Auth.User.Doador.id'),
					'Doacao.id' => $doacao_id
				]
			]);
		}

		if (!$doacao)
			$this->redirect('/');

		$this->helpers[] = 'CakePtbr.Formatacao';
		$this->helpers[] = 'Geral';

		$parcelas = $this->Doacao->Parcela->find('all', [
			'conditions' => ['Parcela.doacao_id' => $doacao_id],
			'order' => 'Parcela.parcela',
		]);

		$pagamentoCredito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
			'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id'),
			'conditions' => array(
				'FormasPagamento.tipo'   => 'C',
				'FormasPagamento.status' => true
			),
			'recursive' => -1
		));

		$pagamentoDebito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
			'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id'),
			'conditions' => array(
				'FormasPagamento.tipo'   => 'D',
				'FormasPagamento.status' => true
			),
			'recursive' => -1
		));

		$this->set(compact('pagamentoCredito'));
		$this->set(compact('pagamentoDebito'));

		if (empty($this->data)) {
			$this->data = $doacao;
		} else {
			if ($this->data['Doacao']['alterar_forma_pagamento']) {
				$braspag_justclickkey = '';
				$cartao_final = '';

				$this->request->data['Pagamento']['conta'] = $this->Geral->apenasNumeros($this->data['Pagamento']['conta']);

				if ($this->data['formaPagamento'] == 'credito') {
					$cartao_final = substr($this->data['Pagamento']['cartaoCreditoNum'], -4);

					$transacao = new TransacaoBraspag();

					$transacao->customer_Name  = $this->Session->read('Auth.User.Doador.nome') . ' ' . $this->Session->read('Auth.User.Doador.sobrenome');
					$transacao->customer_cpf   = $this->Session->read('Auth.User.Doador.cpf_cnpj');

					$transacao->payment_CardNumber = $this->data['Pagamento']['cartaoCreditoNum'];
					$transacao->payment_CardHolder = $this->data['Pagamento']['cartaoCreditoNome'];
					$transacao->payment_CardMonthExpirationDate = $this->data['Pagamento']['validade_mes'];
					$transacao->payment_CardYearExpirationDate  = $this->data['Pagamento']['validade_ano'];
					$transacao->payment_CardSecurityCode = $this->data['Pagamento']['cartaoCreditoCod'];

					//Testing
					// $this->CreditCard->setMethod(BraspagCreditCardModel::METHOD_HOMOLOGATION);

					$retorno_cartao = $transacao->salvarCartao();
					if ($retorno_cartao->SaveCreditCardResult->Success) {
						$braspag_justclickkey = $retorno_cartao->SaveCreditCardResult->JustClickKey;
					}
				}

				$this->request->data['Pagamento']['braspag_justclickkey'] = $braspag_justclickkey;
				$this->request->data['Pagamento']['cartao_final']         = $cartao_final;

				$doacao['Pagamento'] = $this->data['Pagamento'];
			}

			$valor = substr($this->data['Doacao']['valor'], 2);
			$valor = str_replace(array('.', ','), array('', '.'), $valor);

			$doacao['Doacao']['valor'] = $valor;
			if ($this->Doacao->saveAll($doacao)) {

				$pagina = new PaginasController();
				$EnviarXMLDoacoesParaCRM = $pagina->xmlgen( 'EnviarXMLDoacoesParaCRM', null );

				if ($EnviarXMLDoacoesParaCRM) {
					$this->Geral->setFlash(__('Suas alterações foram salvas e passarão a ter efeito a partir da próxima doação.'), true);
					if ($this->Session->read($this->_tagSession . '.from_admin')) {
						$this->redirect(['action' => 'index', 'admin' => true]);
					} else {
						$this->redirect('/conta');
					}
				} else {
					$this->Geral->setFlash(__('Não foi possível fazer as alterações.'), true);
				}

			} else
			{
				print_r($this->Doacao->validationErrors);die;
			}
		}

		$dias = $this->Doacao->Pagamento->DiaDebito->find('list', [
			'conditions' => ['DiaDebito.status' => true]
		]);

		$this->set('doacao', $doacao);
		$this->set('parcelas', $parcelas);
		$this->set('parcela', $parcelas[0]);
		$this->set('dias', $dias);
	}
	*/
	public function admin_painel($doacao_id = null)
	{
		if (!$doacao_id) {
			$this->redirect(['action' => 'index']);
		}

		$this->helpers[] = 'CakePtbr.Formatacao';

		$this->Doacao->Behaviors->load('Containable');

		$doacao = $this->Doacao->find('first', [
			'conditions' => [
				'Doacao.id' => $doacao_id
			],
			'contain' => ['Parcela', 'Parcela.FormasPagamento', 'Pagamento', 'Pagamento.FormasPagamento', 'Doador', 'CancelamentoDoacao'],
			'recursive' => -1
		]);

		$cancelamentos = $this->Doacao->CancelamentoDoacao->Cancelamento->find('list', [
			'conditions' => ['Cancelamento.status' => true]
		]);

		$this->loadModel('LogAction');
		$logsDoacao = $this->LogAction->find('all', [
			'conditions' =>[
				'LogAction.model' => 'Doacao',
				'LogAction.row' => $doacao_id,
				'LogAction.before <>' => ''
			],
			'recursive' => -1
		]);

		$logsPagamento = $this->LogAction->find('all', [
			'conditions' =>[
				'LogAction.model' => 'Pagamento',
				'LogAction.row' => $doacao['Doacao']['pagamento_id'],
				'LogAction.before <>' => ''
			],
			'recursive' => -1
		]);

		$logsTmp = array_merge($logsDoacao, $logsPagamento);


		$formaPagamentos = $this->Doacao->Pagamento->FormasPagamento->find('all', [
			'fields' => ['FormasPagamento.id', 'FormasPagamento.nome'],
			'recursive' => -1
		]);
		$formaPagamentos = Hash::combine($formaPagamentos, '{n}.FormasPagamento.id', '{n}.FormasPagamento.nome');

		$diasDebitos = $this->Doacao->Pagamento->DiaDebito->find('list', [
			'recursive' => -1
		]);

		$this->Doacao->Doador->Behaviors->load('Containable');

		$logs = [];
		foreach ($logsTmp as $log) {
			$doador = $this->Doacao->Doador->find('first', [
				'conditions' => [
					'Doador.user_id' => $log['LogAction']['user_id']
				],
				'contain' => 'User',
				'recursive' => -1
			]);

			if ($log['LogAction']['field'] == 'formas_pagamento_id') {
				$log['LogAction']['before'] = $formaPagamentos[$log['LogAction']['before']];
				$log['LogAction']['after']  = $formaPagamentos[$log['LogAction']['after']];
			} else if ($log['LogAction']['field'] == 'dia_debito_id') {
				$log['LogAction']['before'] = $diasDebitos[$log['LogAction']['before']];
				$log['LogAction']['after']  = $diasDebitos[$log['LogAction']['after']];
			}

			$log = array_merge($log, $doador);
			array_push($logs, $log);
		}

		$this->set('logs', $logs);
		$this->set('doacao', $doacao);
		$this->set('cancelamentos', $cancelamentos);
	}

	public function inicio()
	{
		$this->loadModel('Configuracao');
		//remove tudo da sessão para começar toda a doação
		if ($this->Session->check($this->_tagSession)) {
			$this->Session->delete($this->_tagSession);
		}

		if ($this->request->is('post')) {
			$doador = '';
			if (isset($this->request->data['PF'])) {
				$doador = 'PF';
			} else if (isset($this->request->data['PJ'])) {
				$doador = 'PJ';
			} else {
				$this->redirect('/');
			}
			$this->Session->write($this->_tagSession, ['doador' => $doador]);
			$this->redirect('/valores');
		}

		$configs = $this->Configuracao->find('first');
		$this->set(compact('configs'));
	}

	public function valores()
	{
		$doacaoSession = $this->Session->read($this->_tagSession);

		if (!$doacaoSession || !isset($doacaoSession['doador']))
			$this->redirect('/');

		$this->loadModel('Valor');
		$this->loadModel('Configuracao');

		if ($this->Session->check('Auth.User')) {
			$pessoa_id = $this->Session->read('Auth.User.Doador.pessoa_id');
		} else {
			$pessoa_id = $doacaoSession['doador'] == 'PF' ? 1 : 2;
		}


		$valoresMensais = $this->Valor->find('all', [
			'conditions' => [
				'Valor.status' => true,
				'Valor.tipo' => 'M',
				'Valor.pessoa_id' => $pessoa_id
			],
			'order' => 'Valor.valor',
			'recursive' => -1
		]);
		$valoresAnuais = $this->Valor->find('all', [
			'conditions' => [
				'Valor.status' => true,
				'Valor.tipo' => 'A',
				'Valor.pessoa_id' => $pessoa_id
			],
			'order' => 'Valor.valor',
			'recursive' => -1
		]);

		$configs = $this->Configuracao->find('first');

		$this->set('valoresMensais', $valoresMensais);
		$this->set('valoresAnuais', $valoresAnuais);
		$this->set(compact('configs'));

		if (!empty($this->data)) {
			$doacaoSession['valor'] = $this->data['valorDeDoacao'];
			$doacaoSession['tipo']  = $this->data['tipoDeDoacao'];
			$this->Session->write($this->_tagSession, $doacaoSession);

			if (isset($this->data['novo'])) {
				$this->redirect('/doacao');
			} else {
				$this->redirect('/login');
			}
		}
	}

	public function doacao()
	{
		$this->loadModel('Mensagem');
		$this->loadModel('Configuracao');

		$doacaoSession = $this->Session->read($this->_tagSession);

		if (isset($doacaoSession['concluiu'])) {
			$this->redirect('/complemento');
		}

		//se o usuário estiver logado salva o id na sessao
		if ($this->Session->check('Auth.User')) {
			//não permite que admin faça doação
			if ($this->Session->read('Auth.User.is_admin') && !isset($doacaoSession['from_admin'])) {
				$this->redirect('/');
			}

			//se from_admin não estiver setado é porque a doação não está sendo feita pelo admin em nome de outra pessoa
			if (!isset($doacaoSession['from_admin'])) {
				$doacaoSession['doador_id'] = $this->Session->read('Auth.User.Doador.id');
				$doacaoSession['doador']    = ($this->Session->read('Auth.User.Doador.pessoa_id') == 1) ? 'PF' : 'PJ' ;
				$this->Session->write($this->_tagSession, $doacaoSession);
			}
		} else {
			if (!$doacaoSession || !isset($doacaoSession['doador']) || !isset($doacaoSession['valor']) || !isset($doacaoSession['tipo']))
				$this->redirect('/');
		}

		$dias = $this->Doacao->Pagamento->DiaDebito->find('list', [
			'conditions' => ['DiaDebito.status' => true]
		]);

		$this->set('dias', $dias);

		$pagamentoCredito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
			'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id'),
			'conditions' => array(
				'FormasPagamento.tipo'   => 'C',
				'FormasPagamento.status' => true
			),
			'recursive' => -1
		));

		$pagamentoDebito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
			'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id'),
			'conditions' => array(
				'FormasPagamento.tipo'   => 'D',
				'FormasPagamento.status' => true
			),
			'recursive' => -1
		));

		if ((isset($doacaoSession['doador']) && $doacaoSession['doador'] === 'PJ') ||
			($this->Session->check('Auth.User') && $this->Session->read('Auth.User.Doador.pessoa_id') == 2)) {
			$faixas = $this->Doacao->Doador->DpjComplemento->Faixa->find('list', [
				'conditions' => ['Faixa.status' => true]
			]);

			$naturezas = $this->Doacao->Doador->DpjComplemento->Natureza->find('list', [
				'conditions' => ['Natureza.status' => true]
			]);

			$mensagem = $this->Mensagem->find('first', [
				'conditions' => ['Mensagem.tipo' => 11]
			]);

			$aceite = $this->Doacao->AceiteDoacao->Aceite->find('first', [
				'conditions' => [
					'Aceite.status' => true,
					'Aceite.pessoa_id' => 2
				],
				'recursive' => -1
			]);

			$aceite2 = $this->Doacao->AceiteDoacao->Aceite->find('first', [
				'conditions' => [
					'Aceite.status' => true,
					'Aceite.pessoa_id' => 2,
					'Aceite.divulgacao' => true
				],
				'recursive' => -1
			]);

			$this->set(compact('aceite2'));
			$this->set(compact('faixas'));
			$this->set(compact('naturezas'));
			$this->set(compact('mensagem'));

		} else {
			$aceite = $this->Doacao->AceiteDoacao->Aceite->find('first', [
				'conditions' => [
					'Aceite.status' => true,
					'Aceite.pessoa_id' => 1
				],
				'recursive' => -1
			]);

		}

		$configs = $this->Configuracao->find('first');

		$this->set(compact('configs'));
		$this->set(compact('aceite'));
		$this->set(compact('pagamentoCredito'));
		$this->set(compact('pagamentoDebito'));

		//seta o sessionID do DeviceFingerPrint(Braspag)
		if ($this->Session->check('sessionID')) {
			$sessionID = $this->Session->read('sessionID');
		} else {
			$sessionID = substr(md5(date('dmY Hms')), 0, 50);
			$this->Session->write('sessionID', $sessionID);
		}
		$this->set('sessionID', $sessionID);

		if (empty($this->data)) {
			// $this->Geral->setMensagem(1);
			if (!isset($doacaoSession['valor'])) {
				$doacaoSession['valor'] = 0;
				$doacaoSession['tipo']  = 'M';
				$this->Session->write($this->_tagSession, $doacaoSession);
			}
		} else {
			$doacaoSession['valor'] = $this->data['Doacao']['valor'];
			$doacaoSession['tipo']  = $this->data['Doacao']['tipo'];
			$this->Session->write($this->_tagSession, $doacaoSession);

			$this->set('valor', $doacaoSession['valor']);
			$this->set('tipo', $doacaoSession['tipo']);

			$doacao = $this->data['Doacao'];
			unset($this->request->data['Doacao']);

			//se doador_id estiver na sessão é porque ele já está salvo no banco
			if (!isset($doacaoSession['doador_id'])) {
				if (isset($doacaoSession['doador']) && $doacaoSession['doador'] == 'PJ') {
					$this->request->data['Doador']['cpf_cnpj'] = $this->Geral->apenasNumeros($this->data['Doador']['cpf_cnpj']);
					//
				} else {
					if ($this->data['Doador']['conta_brasil'] === '1') {
						$this->request->data['Doador']['cpf_cnpj'] = $this->Geral->apenasNumeros($this->data['Doador']['cpf_cnpj']);
					} else {
						unset($this->request->data['Doador']['cpf_cnpj']);
					}
				}

				//verifica a senha
				if ((strlen($this->data['User']['password']) < 6) || (strlen($this->data['User']['temppassword']) < 6)) {
					$this->Geral->setFlash('A Senha deve conter pelo menos 6 caracteres', false);
					return;
				}
				if ($this->data['User']['password'] != $this->data['User']['temppassword']) {
					$this->Geral->setFlash('A senha a confirmação de senha digitadas não são iguais', false);
					return;
				}

				$salvou = $this->_salvarDadosDoador($this->request->data);
				if (is_array($salvou)) {
					$mensagem = '';
					foreach ($salvou as $erro) {
						if (is_array($erro)) {
							foreach ($erro as $e) {
								if (is_array($e)) {
									foreach ($e as $e1) {
										$mensagem = $e1[0];
										break;
									}
								} else {
									$mensagem = $e;
									break;
								}
							}
						} else {
							$mensagem = $erro[0];
							break;
						}
					}

					$this->Geral->setFlash($mensagem, false);
					return;
				}

				//recarrega a variável da sessão
				$doacaoSession = $this->Session->read($this->_tagSession);

				//integração Mail2Easy
				if ($doacaoSession['doador'] !== 'PJ' && $this->data['Doador']['recebe_email']) {
					$this->WebService = $this->Components->load('WebService');

					$retorno = $this->WebService->integracaoFormularioMail2Easy($this->data['Doador']['nome'], $this->data['Doador']['sobrenome'], $this->data['Doador']['nacionalidade'], $this->data['DoadorEmail'][0]['email']);

				// 	return json_encode(['Mail2Easy' => $retorno]);
				}
			}

			$this->request->data['Doacao'] = $doacao;
			// echo "<pre>";print_r($this->request->data);die('diacho');

			//se doacao_id estiver na sessão é porque ela já foi iniciada e está salva no banco
			if (!isset($doacaoSession['doacao_id'])) {
				$salvou = $this->_salvarDadosDoacao($this->request->data, $doacaoSession['doador_id']);

				//se $salvou for array é porque deu erro
				if (is_array($salvou)) {
					foreach ($salvou as $erro) {
						$this->Geral->setFlash($erro[0], false);
						break;
					}
					return;
				}
			}

			$doacaoSession = $this->Session->read($this->_tagSession);

			$nossonumero          = '';
			$numero_doc           = '';
			$nsu                  = '';
			$autorizacao          = '';
			$mensagem_retorno     = '';
			$status_id            = '';
			$braspag_justclickkey = '';
			$data_pagamento       = date('Y-m-d H:i:s');
			$valor                = substr($this->data['Doacao']['valor'], 2);
			$valor                = str_replace(array('.', ','), array('', '.'), $valor);
			$order_id             = '';
			$cartao_final         = '';
			$cvv				  = '';
			$nova_tentativa		  = false;
			$pago                 = false;

			$doador = $this->Doacao->Doador->find('first', [
				'conditions' => [
					'Doador.id' => $doacaoSession['doador_id']
				],
				'recursive' => -1
			]);
			$emails = $this->Doacao->Doador->DoadorEmail->find('first', [
				'conditions' => [
					'DoadorEmail.doador_id' => $doacaoSession['doador_id']
				],
				'recursive' => -1
			]);
			$doador['DoadorEmail'] = $emails['DoadorEmail'];

			//efetua a transação
			if ($this->data['formaPagamento'] == 'credito') {

				$this->request->data['Pagamento']['debitoAgencia'] = '';
				$this->request->data['Pagamento']['debitoConta'] = '';

				$transacao = new TransacaoBraspag();
				$transacao->deviceFingerPrint = $sessionID;

				$qtd_dias_cliente = 0;
				if ($this->Session->check('Auth.User')) {
					if (isset($doacaoSession['from_admin'])) {
						$doador_array = $this->Doacao->Doador->find('first', [
							'conditions' => ['Doador.id' => $doacaoSession['doador_id']],
							'fields'     => ['Doador.nome', 'Doador.sobrenome', 'Doador.cpf_cnpj', 'Doador.created'],
							'recursive'  => -1
						]);

						$nome          = $doador_array['Doador']['nome'];
						$sobrenome     = $doador_array['Doador']['sobrenome'];
						$cpf_cnpj      = $doador_array['Doador']['cpf_cnpj'];
						$data_cadastro = $doador_array['Doador']['created'];
					} else {
						$nome          = $this->Session->read('Auth.User.Doador.nome');
						$sobrenome     = $this->Session->read('Auth.User.Doador.sobrenome');
						$cpf_cnpj      = $this->Session->read('Auth.User.Doador.cpf_cnpj');
						$data_cadastro = $this->Session->read('Auth.User.Doador.created');
					}

					$data_cadastro    = new DateTime($data_cadastro);
					$data_atual       = new DateTime(date('Ymd'));
					$diff             = $data_cadastro->diff($data_atual);
					$qtd_dias_cliente = $diff->format('%a');

					$transacao->customer_Name  = $nome . ' ' . $sobrenome;
					$transacao->customer_cpf   = $this->Geral->apenasNumeros($cpf_cnpj);

					$order_id = md5($cpf_cnpj . date('Ymd H:m:s'));
					$transacao->order_ID = substr($order_id, 0, 36);

					$transacao->billtodata_firstname     = $nome;
					$transacao->billtodata_lastname      = $sobrenome;


					$telefone = $this->Doacao->Doador->DoadorTelefone->find('first', [
						'conditions' => [
							'DoadorTelefone.doador_id' => $doacaoSession['doador_id'],
							'DoadorTelefone.telefone_id' => 1
						]
					]);
					$transacao->billtodata_phonenumber = $telefone['DoadorTelefone']['numero'];

					$email = $this->Doacao->Doador->DoadorEmail->find('first', [
						'conditions' => [
							'DoadorEmail.doador_id' => $doacaoSession['doador_id'],
							'DoadorEmail.email_id' => 1
						]
					]);
					$transacao->customer_Email = $email['DoadorEmail']['email'];
				} else {
					$transacao->customer_Name  = $this->data['Doador']['nome'] . ' ' . $this->data['Doador']['sobrenome'];

					$transacao->customer_Email = $this->data['DoadorEmail'][0]['email'];
					if (isset($this->data['Doador']['conta_brasil']) && $this->data['Doador']['conta_brasil'] == 1) {
						$transacao->customer_cpf   = $this->Geral->apenasNumeros($this->data['Doador']['cpf_cnpj']);
					}

					$order_id = '';
					if (isset($this->data['Doador']['conta_brasil']) && $this->data['Doador']['conta_brasil'] == 1) {
						$order_id = md5($this->data['Doador']['cpf_cnpj'] . date('Ymd H:m:s'));
					} else {
						$order_id = md5($this->data['DoadorEmail'][0]['email'] . date('Ymd H:m:s'));
					}
					$transacao->order_ID = substr($order_id, 0, 36);

					$transacao->billtodata_firstname     = $this->data['Doador']['nome'];
					$transacao->billtodata_lastname      = $this->data['Doador']['sobrenome'];
					$transacao->billtodata_phonenumber   = $this->data['DoadorTelefone'][0]['numero'];
				}

				$transacao->billtodata_city       = 'Mountain View';
				$transacao->billtodata_country    = 'US';
				$transacao->billtodata_state      = 'CA';
				$transacao->billtodata_street1    = '1295 Charleston Road';
				$transacao->billtodata_street2    = '';
				$transacao->billtodata_postalcode = '94043';

				$transacao->cartao                          = $this->request->data['Pagamento']['formas_pagamento_id'];
				$transacao->payment_CardNumber              = $this->data['Pagamento']['cartaoCreditoNum'];
				$transacao->payment_CardHolder              = $this->data['Pagamento']['cartaoCreditoNome'];
				$transacao->payment_CardMonthExpirationDate = $this->data['Pagamento']['validade_mes'];
				$transacao->payment_CardYearExpirationDate  = $this->data['Pagamento']['validade_ano'];
				$transacao->payment_CardSecurityCode        = $this->data['Pagamento']['cartaoCreditoCod'];
				$transacao->payment_Currency                = 'BRL';
				$transacao->payment_Country                 = 'BRA';
				$transacao->payment_Amount                  = str_replace(array('.', ','), array('', ''), $valor);
				$transacao->payment_AmountFormatado         = $valor;

				$transacao->customer_ID                     = $doacaoSession['doador_id'];
				$transacao->customer_tempo_cliente_em_dias  = $qtd_dias_cliente;
				$transacao->billtodata_ipaddress            = $_SERVER['REMOTE_ADDR'];

				$retorno_antifraude = $transacao->fraudAnalysis();

				CakeLog::write('antifraude', json_encode($retorno_antifraude));

				$braspag_codigos_erro    = $this->Geral->codigosErroBraspag();
				$antifraude_codigos_erro = $this->Geral->codigosErroAntifraude();
				$cyber_codigos_erro      = $this->Geral->codigosErroCyberAntifraude();
				// $redecard_codigos_erro   = $this->Geral->codigosErroRedecard();

				$mensagem_retorno = $retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->ReturnMessage;
				$status_id = (int)$retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->Status + 12;

				if (array_search($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->ReturnCode, $braspag_codigos_erro) ||
					array_search($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AntiFraudResponse->ReasonCode, $antifraude_codigos_erro)) {
					//não efetua transação
					$this->Geral->setFlash(__('Ocorreu um problema no processamento do pagamento.'), false);
				} else if (array_search($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AntiFraudResponse->ReasonCode, $cyber_codigos_erro)) {
					//não efetua transação e dispara email para o ias
					$this->Geral->setFlash(__('Sistema indisponível no momento'), false);

					$email = new CakeEmail('mandrill');
					$email->to('lperaro@ias.org.br');
					$email->subject('Erro no ias.doacaosenna.org.br');
					$email->send('Ocorreu um erro na integração com a Braspag: ' . $retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AntiFraudResponse->ReasonCode);

				} else {
					$order_id2 = md5($retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->CorrelatedId . date('Ymd H:m:s'));
					$transacao->order_ID = substr($order_id2, 0, 36);

					$transacao->braspag_transactionId = $retorno_antifraude->FraudAnalysisResponse->FraudAnalysisResult->AuthorizeTransactionResponse->PaymentDataCollection->PaymentDataResponse->BraspagTransactionId;

					$retorno_transacao = $transacao->efetuar_transacao();

					CakeLog::write('braspag', json_encode($retorno_transacao));

					// if ($retorno_transacao->PaymentDataCollection->PaymentDataResponse->Status == 0) {
					// 	$autorizacao = $retorno_transacao->PaymentDataCollection->PaymentDataResponse->AuthorizationCode;
					// 	$nsu = $retorno_transacao->PaymentDataCollection->PaymentDataResponse->ProofOfSale;

					// 	$pagina = new PaginasController();
					// 	$mandrill = $pagina->mandrill(1, $doador);

					// 	$mensagem_retorno = $retorno_transacao->PaymentDataCollection->PaymentDataResponse->ReturnMessage;
					// 	$status_id = (int)$retorno_transacao->PaymentDataCollection->PaymentDataResponse->Status + 12;

					// 	$pago = true;

					// 	$retorno_cartao = $transacao->salvarCartao();
					// 	if ($retorno_cartao->SaveCreditCardResult->Success) {
					// 		$braspag_justclickkey = $retorno_cartao->SaveCreditCardResult->JustClickKey;
					// 	}
					// } else {
					// 	$this->Geral->setFlash(__('Ocorreu um problema no processamento do pagamento.'), false);
					// }
					if ($retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->Status == 0) {
						$autorizacao = $retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->AuthorizationCode;
						$nsu = $retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->ProofOfSale;

						$pagina = new PaginasController();
						$mandrill = $pagina->mandrill(1, $doador);

						$mensagem_retorno = $retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->ReturnMessage;
						$status_id = (int)$retorno_transacao->CaptureCreditCardTransactionResult->TransactionDataCollection->TransactionDataResponse->Status + 12;

						$pago = true;

						$retorno_cartao = $transacao->salvarCartao();
						if ($retorno_cartao->SaveCreditCardResult->Success) {
							$braspag_justclickkey = $retorno_cartao->SaveCreditCardResult->JustClickKey;
						}
					} else {
						$this->Geral->setFlash(__('Ocorreu um problema no processamento do pagamento.'), false);
					}
				}

				$cartao_final = substr($this->data['Pagamento']['cartaoCreditoNum'], -4);
				$cvv 		  = $this->data['Pagamento']['cartaoCreditoCod'];

			} else if ($this->data['formaPagamento'] == 'debito') {
				$pago = true;

				$dia_debito = $dias[$this->data['Pagamento']['dia_debito_id']];
				if (date('d') > $dia_debito) {
					if (date('m') == 12) {
						$ano_debito = date('Y') + 1;
						$data_pagamento = $ano_debito . '-01-' . $dia_debito;
					} else {
						$mes_debito = date('m') + 1;
						$data_pagamento = date('Y-' . $mes_debito . '-' . $dia_debito);
					}
				} else {
					$data_pagamento = date('Y-m-'.$dia_debito);
				}
				$status_id = 6;

				// $pagina = new PaginasController();
				// $mandrill = $pagina->mandrill( 3, $doador );

			} else if ($this->data['formaPagamento'] == 'boleto') {
				$this->request->data['Pagamento']['formas_pagamento_id'] = 1;
				$this->request->data['Pagamento']['debitoAgencia'] = '';
				$this->request->data['Pagamento']['debitoConta'] = '';

				$data_pagamento = $this->data['Pagamento']['dataVencimento'];

				$nossonumero = $doacaoSession['doacao_id'] . str_pad(1, 2, "0", STR_PAD_LEFT);
				$numero_doc  = $doacaoSession['doacao_id'] . '0' . str_pad(1, 2, "0", STR_PAD_LEFT);
				$status_id = 6;

				$pago = true;

				// $pagina = new PaginasController();
				// $mandrill = $pagina->mandrill( 4, $doador );
			}

			// if (empty($status_id)) {
			// 	$status_id = ($pago === true) ? 1 : 2;
			// }

			$parcela = $this->Doacao->Parcela->find('first', [
				'conditions' => ['Parcela.doacao_id' => $doacaoSession['doacao_id']],
				'recursive' => -1
			]);

			//salva a transacao independente se a doação foi paga ou não
			$transacao = $this->Doacao->Parcela->Transacao->create([
				'parcela_id'          => $parcela['Parcela']['id'],
				// 'tentativa_id'        => $tentativa_id,
				'formas_pagamento_id' => $this->data['Pagamento']['formas_pagamento_id'],
				'nossonumero'         => $nossonumero,
				'numero_documento'    => $numero_doc,
				'nsu'                 => $nsu,
				'autorizacao'         => $autorizacao,
				'mensagem_retorno'    => $mensagem_retorno,
				'status_id'           => $status_id
			]);
			$this->Doacao->Parcela->Transacao->save($transacao);

			//se estiver pago atualiza o status da doacao
			if ($pago === true) {
				$dataSource = $this->Doacao->Doador->getDataSource();
				$dataSource->begin();

				$pagamento = $this->Doacao->Pagamento->create([
					'formas_pagamento_id'  => $this->data['Pagamento']['formas_pagamento_id'],
					'banco_id'             => '',
					'agencia'              => $this->data['Pagamento']['debitoAgencia'],
					'conta'                => $this->data['Pagamento']['debitoConta'],
					'dia_debito_id'        => $this->data['Pagamento']['dia_debito_id'],
					'cartao_validade'      => $this->data['Pagamento']['validade_mes'].'/'.$this->data['Pagamento']['validade_ano'],
					'cartao_final'		   => $cartao_final,
					'braspag_justclickkey' => $braspag_justclickkey,
					'cvv'				   => $cvv
				]);
				$this->Doacao->Pagamento->save($pagamento);

				$pagamento_id = $this->Doacao->Pagamento->getLastInsertID();

				$doacao = $this->Doacao->find('first', [
					'conditions' => ['Doacao.id' => $doacaoSession['doacao_id']],
					'recursive' => -1
				]);
				$doacao['Doacao']['status']       = 'A';
				$doacao['Doacao']['pagamento_id'] = $pagamento_id;
				$doacao['Doacao']['valor']        = $valor;
				$this->Doacao->save($doacao);

				$parcela['Parcela']['formas_pagamento_id'] = $this->data['Pagamento']['formas_pagamento_id'];
				$parcela['Parcela']['status_id']           = $status_id;
				$parcela['Parcela']['valor']               = $valor;
				$parcela['Parcela']['order_id']            = $order_id;
				$parcela['Parcela']['nossonumero']         = $nossonumero;
				$parcela['Parcela']['numero_documento']    = $numero_doc;
				$parcela['Parcela']['nsu']                 = $nsu;
				$parcela['Parcela']['autorizacao']         = $autorizacao;
				$parcela['Parcela']['data_pagamento']      = $data_pagamento;
				$parcela['Parcela']['mensagem_retorno']    = $mensagem_retorno;
				$parcela['Parcela']['cartao_final']    	   = $cartao_final;
				$parcela['Parcela']['nova_tentativa']      = $nova_tentativa;
				$this->Doacao->Parcela->save($parcela);

				if (($this->data['formaPagamento'] == 'boleto') && ($this->data['Doacao']['tipo'] == 'M') && ($doacaoSession['doador'] == 'PJ')) {
					$data_tmp = explode('/', $this->data['Pagamento']['dataVencimento']);
					$data_geracao = $data_tmp[2] . '-' . $data_tmp[1] . '-' . $data_tmp[0];
					for ($i=1; $i < 12; $i++) {
						$dt_antiga = new DateTime($data_geracao);
						$dt_nova   = new DateTime($data_geracao);
						$dt_nova->add(new DateInterval('P'.$i.'M'));
						$mes_novo = $dt_nova->format('m');
						$mes_antigo = $dt_antiga->format('m');
						// if (($mes_novo != ($mes_antigo + ($i % 12))) && ($mes_novo != '01')) {
						if (($mes_novo != (($mes_antigo + $i) % 12)) && ((($mes_antigo + $i) % 12) != '0')) {
							$dt_nova->sub(new DateInterval('P'.$dt_nova->format('d').'D'));
						}
						$nova_data_pagamento = $dt_nova->format('Y-m-d');

						$nossonumero = $doacaoSession['doacao_id'] . str_pad($i + 1, 2, "0", STR_PAD_LEFT);
						$numero_doc  = $doacaoSession['doacao_id'] . '0' . str_pad($i + 1, 2, "0", STR_PAD_LEFT);

						$nova_parcela = $this->Doacao->Parcela->create([
							'doacao_id'			  => $doacaoSession['doacao_id'],
							'formas_pagamento_id' => $this->data['Pagamento']['formas_pagamento_id'],
							'status_id'           => $status_id,
							'valor'               => $valor,
							'order_id'            => $order_id,
							'nossonumero'         => $nossonumero,
							'numero_documento'    => $numero_doc,
							'nsu'                 => $nsu,
							'autorizacao'         => $autorizacao,
							'data_pagamento'      => $nova_data_pagamento,
							'mensagem_retorno'    => $mensagem_retorno,
							'cartao_final' 	      => $cartao_final,
							'nova_tentativa'      => $nova_tentativa,
							'parcela'			  => $i + 1,
							'data_geracao'		  => date('Y-m-d H:i:s')
						]);

						$this->Doacao->Parcela->save($nova_parcela);
					}
				}

				//salva a tentativa de transacao
				$tentativa = $this->Doacao->Parcela->Tentativa->create([
					'parcela_id' => $parcela['Parcela']['id'],
					'tentativa'  => 1
				]);
				$this->Doacao->Parcela->Tentativa->save($tentativa);

				$tentativa_id = $this->Doacao->Parcela->Tentativa->getLastInsertID();

				$transacao['Transacao']['tentativa_id'] = $tentativa_id;
				$this->Doacao->Parcela->Transacao->save($transacao);

				$dataSource->commit();
			} else {
				return;
			}

			//se não tiver feito o login, localiza o usuário e salva na sessão com login feito
			if (!$this->Session->check('Auth.User')) {
				$this->loadModel('User');
				$this->loadModel('Doador');

				$doador = $this->Doador->find('first', [
					'conditions' => ['Doador.id' => $doacaoSession['doador_id']]
				]);
				$user = $this->User->find('first', [
					'conditions' => ['User.id' => $doador['Doador']['user_id']]
				]);
				$user['User']['Doador'] = $doador['Doador'];
				$this->Auth->login($user['User']);
			}

			$doacaoSession = $this->Session->read($this->_tagSession);
			$doacaoSession['concluiu'] = true;
			$this->Session->write($this->_tagSession, $doacaoSession);

			$pagina = new PaginasController();
			$EnviarXMLDoadorParaCRM  = $pagina->xmlgen( 'EnviarXMLDoadorParaCRM', null );
			$EnviarXMLDoacoesParaCRM = $pagina->xmlgen( 'EnviarXMLDoacoesParaCRM', null );
			if (($this->data['formaPagamento'] == 'boleto') && ($this->data['Doacao']['tipo'] == 'M') && ($doacaoSession['doador'] == 'PJ')) {
				$EnviarXMLParcelaParaCRM = $pagina->xmlgen( 'EnviarXMLParcelaParaCRM', true, $doacaoSession['doacao_id'] );
			} else {
				$EnviarXMLParcelaParaCRM = $pagina->xmlgen( 'EnviarXMLParcelaParaCRM', null );
			}

			if ($EnviarXMLDoadorParaCRM && $EnviarXMLDoacoesParaCRM && $EnviarXMLParcelaParaCRM ) {
				$this->redirect(['action' => 'complemento']);
			}
		}

		$this->set('valor', $doacaoSession['valor']);
		$this->set('tipo', $doacaoSession['tipo']);
	}

	public function complemento()
	{
		// if ($this->request->is('post')) {
		// 	debug($this->request->data);die;
		// }

		$doacaoSession = $this->Session->read($this->_tagSession);

		if (!$doacaoSession || !isset($doacaoSession['valor']) || !isset($doacaoSession['tipo']))
				$this->redirect('/');

		$this->helpers[] = 'CakePtbr.Formatacao';

		$this->loadModel('Promocao');
		$this->loadModel('Mensagem');

		$this->Promocao->Item->Behaviors->load('Containable');

		$itens = $this->Promocao->Item->find('all', [
			'joins' => array(
				array(
					'table'      => 'promocoes',
					'alias'      => 'Promocao',
					'type'       => 'INNER',
					'conditions' => array(
						'Item.promocao_id = Promocao.id',
					)
				),
			),
			'fields' => ['Item.id', 'Item.item', 'Item.imagem'],
			'conditions' => [
				'Promocao.status' => true,
				'Item.status' => true
			],
			'contain' => ['Atributo', 'Atributo.Conteudo'],
			'recursive' => -1
		]);

		$doador = $this->Doacao->Doador->find('first', [
			'conditions' => [
				'Doador.id' => $this->Session->read('Auth.User.Doador.id')
			]
		]);

		if (empty($this->data)) {
			// se o doador já tiver endereço e não tem brinde disponível redireciona para a próxima tela
			if (isset($doador['DoadorEndereco'][0]) && !isset($doacaoSession['from_admin']) && (count($itens) == 0)) {//AQUI - adicionar verificação de brindes disponíveis
				$doacaoSession = $this->Session->read($this->_tagSession);
				unset($doacaoSession['concluiu']);
				$this->Session->write($this->_tagSession, $doacaoSession);

				$this->redirect('/agradecimento');
			}
			$this->data = $doador;
			//
		} else {
			if ( !isset($doador['DoadorEndereco'][0]) && !isset($doacaoSession['from_admin']) ){
				$this->loadModel('Cidade');

				$cidade = $this->Cidade->find('first', [
					'conditions' => ['Cidade.id' => $this->data['DoadorEndereco'][0]['cidade_id']],
				]);

				// $doador = $this->Doacao->Doador->find('first', [
				// 	'fields'     => ['Doador.nacionalidade'],
				// 	'conditions' => [
				// 		'Doador.id' => $this->Session->read('Auth.User.Doador.id')
				// 	],
				// 	'recursive' => -1
				// ]);

				$this->request->data['DoadorEndereco'][0]['cidade'] = $cidade['Cidade']['nome'];
				$this->request->data['DoadorEndereco'][0]['estado'] = $cidade['Estado']['nome'];
				$this->request->data['DoadorEndereco'][0]['uf']     = $cidade['Estado']['uf'];
				$this->request->data['DoadorEndereco'][0]['pais']   = ( isset($doador['Doador']['nacionalidade']) ) ? $doador['Doador']['nacionalidade'] : 'BR';

				if (isset($this->request->data['Doacao']['filial'])) {
					for ($i=0; $i < count($this->request->data['DoadorEndereco']); $i++) {
						$cidade = $this->Cidade->find('first', [
							'conditions' => ['Cidade.id' => $this->data['DoadorEndereco'][$i]['cidade_id']],
						]);

						$this->request->data['DoadorEndereco'][$i]['cidade'] = $cidade['Cidade']['nome'];
						$this->request->data['DoadorEndereco'][$i]['estado'] = $cidade['Estado']['nome'];
						$this->request->data['DoadorEndereco'][$i]['uf']     = $cidade['Estado']['uf'];
						$this->request->data['DoadorEndereco'][$i]['pais']   = ( isset($doador['Doador']['nacionalidade']) ) ? $doador['Doador']['nacionalidade'] : 'BR';
					}
				}
			}

			if (count($itens) > 0) {
				$this->request->data['Brinde']['doacao_id'] = $doacaoSession['doacao_id'];
				$this->request->data['Brinde']['status']    = 1;

				$dataSource = $this->Doacao->getDataSource();
				$dataSource->begin();

				$this->Doacao->Brinde->save($this->request->data['Brinde']);
				$brinde_id = $this->Doacao->Brinde->getLastInsertID();

				$this->Doacao->Brinde->Item->Atributo->Conteudo->Behaviors->load('Containable');

				if (isset($this->data['BrindeConteudo'])) {
					foreach ($this->data['BrindeConteudo'] as $brindeConteudo) {
						$conteudo = $this->Doacao->Brinde->Item->Atributo->Conteudo->find('first', [
							'conditions' => [
								'Conteudo.id' => $brindeConteudo['conteudo_id']
							],
							'contain' => 'Atributo',
							'recursive' => -1
						]);

						$novo_brinde_conteudo = $this->Doacao->Brinde->BrindeConteudo->create([
							'atributo' => $conteudo['Atributo']['atributo'],
							'conteudo' => $conteudo['Conteudo']['conteudo'],
							'brinde_id' => $brinde_id
						]);
						$this->Doacao->Brinde->BrindeConteudo->save($novo_brinde_conteudo);
					}
				}

				$dataSource->commit();
			}

			if ($this->Doacao->Doador->saveAll($this->data)) {
				$doacaoSession = $this->Session->read($this->_tagSession);
				unset($doacaoSession['concluiu']);
				$this->Session->write($this->_tagSession, $doacaoSession);

				$pagina = new PaginasController();
				$EnviarXMLDoadorParaCRM  = $pagina->xmlgen( 'EnviarXMLDoadorParaCRM', null );
				$EnviarXMLDoacoesParaCRM = $pagina->xmlgen( 'EnviarXMLDoacoesParaCRM', null );

				if ($EnviarXMLDoadorParaCRM) {
					$data['DoadorEmail']['email'] = $doador['DoadorEmail'][0]['email'];
					$data['Doador']['nome']       = $doador['Doador']['nome'];
					$data['Doador']['sobrenome']  = $doador['Doador']['sobrenome'];

					$mensagem_id = ($this->Session->read('Auth.User.Doador.pessoa_id') == 1) ? 2 : 12;
					$mandrill = $pagina->mandrill($mensagem_id, $data);

					$this->redirect('/agradecimento');
				}

			} else {
				foreach ($this->Doacao->Doador->validationErrors as $erro) {
					$this->Geral->setFlash($erro[0], false);
					break;
				}
			}
		}

		$conheceu = $this->Doacao->Doador->Conheceu->find('list', [
			'conditions' => ['Conheceu.status' => true],
			'order' => 'Conheceu.tipo'
		]);

		$doacao = $this->Doacao->find('first', [
			'conditions' => ['Doacao.id' => $doacaoSession['doacao_id']]
		]);

		if ($this->Session->read('Auth.User.Doador.pessoa_id') == 2) {
			$mensagem = $this->Mensagem->find('first', [
				'conditions' => ['Mensagem.tipo' => 12]
			]);
		} else {
			$mensagem = $this->Mensagem->find('first', [
				'conditions' => ['Mensagem.tipo' => 2]
			]);
		}

		//salva na sessao que é boleto para não exibir o link na tela de agradecimento
		if (isset($doacao['Pagamento']) && ($doacao['Pagamento']['formas_pagamento_id'] == '1'))
			$this->Session->write('boleto', true);

		$this->set('itens', $itens);
		$this->set('conheceu', $conheceu);
		$this->set('doacao', $doacao);
		$this->set(compact('mensagem'));
	}

	public function agradecimento()
	{
		$doacaoSession = $this->Session->read($this->_tagSession);

		$this->loadModel('Mensagem');

		$this->helpers[] = 'SocialMedia.SocialMedia';

		$doacao = $this->Doacao->find('first', [
			'conditions' => ['Doacao.id' => $doacaoSession['doacao_id']]
		]);

		$doadorEndereco = $this->Doacao->Doador->DoadorEndereco->find('first', [
			'conditions' => ['DoadorEndereco.doador_id' => $this->Session->read('Auth.User.Doador.id')]
		]);

		$this->Session->delete($this->_tagSession);

		if (isset($doacaoSession['from_admin'])) {
			$this->redirect(['action' => 'index', 'admin' => true]);
		}

		$tipo_mensagem = $this->Session->read('Auth.User.Doador.pessoa_id') == 1 ? 14 : 13;

		$mensagem = $this->Mensagem->find('first', [
			'conditions' => ['Mensagem.tipo' => $tipo_mensagem]
		]);

		$this->set('doacao', $doacao);
		$this->set('doadorEndereco', $doadorEndereco);
		$this->set(compact('mensagem'));

		if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por') {
			$this->set('shareMsg', 'Acabei de fazer uma doação ao Instituto Ayrton Senna e agora apoio a educação no Brasil. Seja você também um doador!');
		} elseif ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'eng') {
			$this->set('shareMsg', 'I\'ve just donated to Instituto Ayrton Senna to support Education in Brazil. Join this cause and give now!');
		}
	}

	public function doacoes()
	{
		$doacao_id = $this->request->params['doacao_id'];

		if (!$doacao_id)
			$this->redirect('/');

		if (null !== $this->Session->read($this->_tagSession . '.Doador')) {
			$doacao = $this->Doacao->find('first', [
				'conditions' => [
					'Doacao.doador_id' => $this->Session->read($this->_tagSession . '.Doador.id'),
					'Doacao.id' => $doacao_id
				]
			]);
		} else {
			$this->Doacao->Behaviors->load('Containable');
			$doacao = $this->Doacao->find('first', [
				'conditions' => [
					'Doacao.doador_id' => $this->Session->read('Auth.User.Doador.id'),
					'Doacao.id' => $doacao_id
				],
				'contain' => ['Pagamento', 'Pagamento.FormasPagamento' => [
					'fields' => 'conta_regex',
					'conditions' => [
						'status' => true
					]
				]],
				'recursive' => -1
			]);
		}

		if (!$doacao)
			$this->redirect('/');

		$this->helpers[] = 'CakePtbr.Formatacao';
		$this->helpers[] = 'Geral';

		$parcelas = $this->Doacao->Parcela->find('all', [
			'conditions' => ['Parcela.doacao_id' => $doacao_id],
			'order' => 'Parcela.parcela',
		]);

		$pagamentoCredito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
			'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id'),
			'conditions' => array(
				'FormasPagamento.tipo'   => 'C',
				'FormasPagamento.status' => true
			),
			'recursive' => -1
		));

		$pagamentoDebito = $this->Doacao->Pagamento->FormasPagamento->find('all', array(
			'fields'     => array('FormasPagamento.nome', 'FormasPagamento.id'),
			'conditions' => array(
				'FormasPagamento.tipo'   => 'D',
				'FormasPagamento.status' => true
			),
			'recursive' => -1
		));

		$this->set(compact('pagamentoCredito'));
		$this->set(compact('pagamentoDebito'));

		if (empty($this->data)) {
			$this->data = $doacao;
		} else {
			if ($this->data['Doacao']['alterar_forma_pagamento']) {
				$braspag_justclickkey = '';
				$cartao_final = '';

				$this->request->data['Pagamento']['conta'] = $this->Geral->apenasNumeros($this->data['Pagamento']['conta']);

				if ($this->data['formaPagamento'] == 'credito') {
					$cartao_final = substr($this->data['Pagamento']['cartaoCreditoNum'], -4);

					$transacao = new TransacaoBraspag();

					$transacao->customer_Name  = $this->Session->read('Auth.User.Doador.nome') . ' ' . $this->Session->read('Auth.User.Doador.sobrenome');
					$transacao->customer_cpf   = $this->Geral->apenasNumeros($this->Session->read('Auth.User.Doador.cpf_cnpj'));

					$transacao->payment_CardNumber = $this->data['Pagamento']['cartaoCreditoNum'];
					$transacao->payment_CardHolder = $this->data['Pagamento']['cartaoCreditoNome'];
					$transacao->payment_CardMonthExpirationDate = $this->data['Pagamento']['validade_mes'];
					$transacao->payment_CardYearExpirationDate  = $this->data['Pagamento']['validade_ano'];
					$transacao->payment_CardSecurityCode = $this->data['Pagamento']['cartaoCreditoCod'];

					//Testing
					// $this->CreditCard->setMethod(BraspagCreditCardModel::METHOD_HOMOLOGATION);

					$retorno_cartao = $transacao->salvarCartao();
					if ($retorno_cartao->SaveCreditCardResult->Success) {
						$braspag_justclickkey = $retorno_cartao->SaveCreditCardResult->JustClickKey;
					}
				}

				$this->request->data['Pagamento']['braspag_justclickkey'] = $braspag_justclickkey;
				$this->request->data['Pagamento']['cartao_final']         = $cartao_final;

				$doacao['Pagamento'] = $this->data['Pagamento'];
			}

			$valor = substr($this->data['Doacao']['valor'], 2);
			$valor = str_replace(array('.', ','), array('', '.'), $valor);

			unset($doacao['Doador']);
			unset($doacao['Campanha']);

			$doacao['Doacao']['valor'] = $valor;
			if ($this->Doacao->saveAll($doacao)) {

				$pagina = new PaginasController();
				$EnviarXMLDoacoesParaCRM = $pagina->xmlgen( 'EnviarXMLDoacoesParaCRM', null );

				if ($EnviarXMLDoacoesParaCRM) {
					$this->Geral->setFlash(__('Suas alterações foram salvas e passarão a ter efeito a partir da próxima doação.'), true);
					if ($this->Session->read($this->_tagSession . '.from_admin')) {
						$this->redirect(['action' => 'index', 'admin' => true]);
					} else {
						$this->redirect('/conta');
					}
				} else {
					$this->Geral->setFlash(__('Não foi possível fazer as alterações.'), true);
				}
			} else {
				// print_r($this->Doacao->validationErrors);die;
			}
		}

		$dias = $this->Doacao->Pagamento->DiaDebito->find('list', [
			'conditions' => ['DiaDebito.status' => true]
		]);

		$this->set('doacao', $doacao);
		$this->set('parcelas', $parcelas);
		$this->set('parcela', $parcelas[0]);
		$this->set('dias', $dias);
	}

	public function alterar_boleto($doacao_id)
	{
		if ($this->Doacao->Parcela->save($this->data)) {
			$url = '/boleto/'.$this->data['Parcela']['id'];
			$this->Geral->setFlash(__('Vencimento atualizado com sucesso.') . '<br />' . __('Para concluir sua doação, imprima e pague o boleto.') . '<br />' . '<a href="'.$url.'" target="blank" class="button info">'.__("Imprimir boleto").'</a>', true);
		} else {
			$this->Geral->setFlash(__('Não foi possível alterar a data de vencimento do boleto'), false);
		}

		$this->redirect('/doacoes/'.$doacao_id);
	}

	public function alterar_boletos()
	{
		if ($this->Doacao->Parcela->saveAll($this->data['Parcela'])) {
			$parcela = $this->Doacao->Parcela->find('first', [
				'conditions' => [
					'Parcela.id' => $this->data['Parcela'][0]['id']
				],
				'recursive' => -1
			]);

			$pagina = new PaginasController();
			$EnviarXMLParcelaParaCRM = $pagina->xmlgen('EnviarXMLParcelaParaCRM', true, $parcela['Parcela']['doacao_id']);

			$this->Geral->setFlash(__('Datas atualizadas com sucesso.'), true);
		}

		$this->redirect('/conta');
	}

	private function _salvarDadosDoador($data)
	{
		$doacaoSession = $this->Session->read($this->_tagSession);

		$dataSource = $this->Doacao->Doador->getDataSource();
		$dataSource->begin();

		$data['User']['username']       = $data['DoadorEmail'][0]['email'];
		$data['User']['email']          = $data['DoadorEmail'][0]['email'];
		$data['User']['password']       = AuthComponent::password($data['User']['password']);
		$data['User']['temppassword']   = AuthComponent::password($data['User']['temppassword']);
		$data['User']['active']         = '1';
		$data['User']['email_verified'] = '1';

		$pessoa_id = ($doacaoSession['doador'] == 'PF') ? 1 : 2;
		$data['Doador']['pessoa_id'] = $pessoa_id;

		if($pessoa_id == 2)
			$this->Doacao->Doador->validate = $this->Doacao->Doador->validateDpj;

		$doador = $this->Doacao->Doador->saveAll($data);

		if ($doador === false) {
			$dataSource->rollback();
			return $this->Doacao->Doador->validationErrors;
		}

		$doador_id = $this->Doacao->Doador->getLastInsertID();

		if ($dataSource->commit()) {
			//salva o id do doador na sessão
			$doacaoSession['doador_id']  = $doador_id;
			$this->Session->write($this->_tagSession, $doacaoSession);
		} else {
			return ['retorno' => false];
		}

		return true;
	}

	private function _salvarDadosDoacao($data, $doador_id)
	{
		$doacaoSession = $this->Session->read($this->_tagSession);

		$dataSource = $this->Doacao->Doador->getDataSource();
		$dataSource->begin();

		$periodo_id = ($data['Doacao']['tipo'] == 'M') ? 1 : 2;
		$valor = substr($data['Doacao']['valor'], 2);
		$campanha_id = NULL;

		$campanha = $this->Doacao->Campanha->find('first', [
			'conditions' => [
				'Campanha.data_inicial <=' => date('Y-m-d'),
				'Campanha.data_final >=' =>  date('Y-m-d')
			],
			'recursive' => -1
		]);
		if (!empty($campanha)) {
			$campanha_id = $campanha['Campanha']['id'];
		}

		$doacao = $this->Doacao->create([
			'doador_id' => $doador_id,
			'valor' => $valor,
			// 'status' => 1,
			'periodo_id' => $periodo_id,
			'campanha_id' => $campanha_id
		]);

		$parcela = $this->Doacao->Parcela->create([
			// 'formas_pagamento_id' => '',
			// 'status_id' => '',
			// 'nossonumero' => '',
			// 'nsu' => '',
			// 'autorizacao' => '',
			// 'data_pagamento' => date('Y-m-d H:i:s'),
			// 'mensagem_retorno' => '',
			'data_geracao' => date('Y-m-d H:i:s'),
			'parcela' => '1',
			'valor' => $valor
		]);

		$doacao['Parcela'][0] = $parcela['Parcela'];

		$aceite = $this->Doacao->AceiteDoacao->create([
			'aceite_id' => $data['AceiteDoacao']['aceite_id']
		]);

		$doacao['AceiteDoacao'] = $aceite['AceiteDoacao'];

		$doacao = $this->Doacao->saveAll($doacao);

		if ($doacao === false) {
			$dataSource->rollback();
			return $this->Doacao->Parcela->validationErrors;
		}

		$doacao_id = $this->Doacao->getLastInsertID();

		if ($dataSource->commit()) {
			//salva o id da doacao na sessão
			$doacaoSession['doacao_id']  = $doacao_id;
			$this->Session->write($this->_tagSession, $doacaoSession);
		} else {
			return ['retorno' => false];
		}

		return true;
	}
}