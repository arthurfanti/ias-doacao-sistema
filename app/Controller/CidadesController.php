<?php
class CidadesController extends AppController
{
	public $model = 'Cidade';

	public function all()
	{
		$this->layout = '';
		$estado = (int)$_GET['estado'];
		$this->Cidade->recursive = -1;
		$this->set( 'cidades', $this->Cidade->findAllByEstadoId( $estado ) );
	}

	public function index()
	{
		$this->layout = '';
		// $this->Cidade->limit = 10;
		$keyword = $_REQUEST['query'];
		$conditions = array( 'Cidade.nome LIKE _utf8' => "%{$keyword}%", 'Cidade.estado_id' => '25' );
		$this->set( 'cidades', $this->Cidade->find( 'all', array( 'conditions' => $conditions, 'limit' => 20 ) ) );
	}
}