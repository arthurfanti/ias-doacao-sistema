<?php
class FormasPagamentosController extends AppController
{

	public $model = 'FormasPagamento';
	public $title = 'Formas de Pagamento';

	public function admin_index()
	{
		$this->set('formasPagamentos', $this->Geral->getformasPagamentos());
		$this->set('formas', $this->FormasPagamento->find('all', array(
			'recursive' => -1
		)));
	}

	public function admin_adicionar()
	{
		if (!empty($this->data)) {
			if ($this->FormasPagamento->save($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->FormasPagamento->Mensagem->locale = 'por';
		$mensagens = $this->FormasPagamento->Mensagem->find('list', array(
			'conditions' => array('Mensagem.status' => true),
			'recursive' => -1
		));
		$this->set('mensagens', $mensagens);
		$this->set('formasPagamentos', $this->Geral->getFormasPagamentos());
	}

	public function admin_alterar($forma_id = null)
	{
		if (!$forma_id)
			$this->redirect(array('action' => 'index'));

		if (empty($this->data)) {
			$this->data = $this->FormasPagamento->find('first', array(
				'conditions' => array('FormasPagamento.id' => $forma_id),
				'recursive' => -1
			));
		} else {
			if ($this->FormasPagamento->save($this->data)) {
				$this->Geral->setFlash('Dados salvos com sucesso.', true);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Geral->setFlash('Não foi possível salvar os dados.', false);
			}
		}

		$this->FormasPagamento->Mensagem->locale = 'por';
		$mensagens = $this->FormasPagamento->Mensagem->find('list', array(
			'conditions' => array('Mensagem.status' => true),
			'recursive' => -1
		));
		$this->set('mensagens', $mensagens);
		$this->set('formasPagamentos', $this->Geral->getFormasPagamentos());
	}
}
