<?php
class GeralComponent extends Component {

	public $components = array(
		'Session',
	);

	public function getIdiomas()
	{
		return array('Português' => 'por', 'English' => 'eng');
	}

	public function getFormasPagamentos()
	{
		return array(
			'C' => 'Crédito',
			'D' => 'Débito',
			'B' => 'Boleto',
			'P' => 'Paypal',
			'E' => 'Depósito'
		);
	}

	public function getTiposPagamentos()
	{
		return array(
			'M' => 'Mensal',
			'A' => 'Anual'
		);
	}

	public function getNacionalidades()
	{
		return array(
			1 => 'Afegão',
			2 => 'Alemão',
			3 => 'Americano',
			4 => 'Angolano',
			5 => 'Antiguano',
			6 => 'Árabe, emiratense',
			7 => 'Argélia',
			8 => 'Argentino',
			9 => 'Armeno',
			10 => 'Australiano',
			11 => 'Austríaco',
			12 => 'Bahamense',
			13 => 'Bangladesh',
			14 => 'Barbadiano, barbadense',
			15 => 'Bechuano',
			16 => 'Belga',
			17 => 'Belizenho',
			18 => 'Boliviano',
			19 => 'Brasileiro',
			20 => 'Britânico',
			21 => 'Camaronense',
			22 => 'Canadense',
			23 => 'Chileno',
			24 => 'Chinês',
			25 => 'Cingalês',
			26 => 'Colombiano',
			27 => 'Comorense',
			28 => 'Costarriquenho',
			29 => 'Croata',
			30 => 'Cubano',
			31 => 'Dinamarquês',
			32 => 'Dominicana',
			33 => 'Dominicano',
			34 => 'Egípcio',
			35 => 'Equatoriano',
			36 => 'Escocês',
			37 => 'Eslovaco',
			38 => 'Esloveno',
			39 => 'Espanhol',
			40 => 'Francês',
			41 => 'Galês',
			42 => 'Ganês',
			43 => 'Granadino',
			44 => 'Grego',
			45 => 'Guatemalteco',
			46 => 'Guianense',
			47 => 'Guianês',
			48 => 'Haitiano',
			49 => 'Holandês',
			50 => 'Hondurenho',
			51 => 'Húngaro',
			52 => 'Iemenita',
			53 => 'Indiano',
			54 => 'Indonésio',
			55 => 'Inglês',
			56 => 'Iraniano',
			57 => 'Iraquiano',
			58 => 'Irlandês',
			59 => 'Israelita',
			60 => 'Italiano',
			61 => 'Jamaicano',
			62 => 'Japonês',
			63 => 'Líbio',
			64 => 'Malaio',
			65 => 'Marfinense',
			66 => 'Marroquino',
			67 => 'Mexicano',
			68 => 'Moçambicano',
			69 => 'Neozelandês',
			70 => 'Nepalês',
			71 => 'Nicaraguense',
			72 => 'Nigeriano',
			73 => 'Norte-coreano, coreano',
			74 => 'Noruego',
			75 => 'Omanense',
			76 => 'Palestino',
			77 => 'Panamenho',
			78 => 'Paquistanês',
			79 => 'Paraguaio',
			80 => 'Peruano',
			81 => 'Polonês',
			82 => 'Portorriquenho',
			83 => 'Português',
			84 => 'Qatarense',
			85 => 'Queniano',
			86 => 'Romeno',
			87 => 'Ruandês',
			88 => 'Russo',
			89 => 'Salvadorenho',
			90 => 'Santa-lucense',
			91 => 'São-cristovense',
			92 => 'São-vicentino',
			93 => 'Saudita',
			94 => 'Sérvio',
			95 => 'Sírio',
			96 => 'Somali',
			97 => 'Sueco',
			98 => 'Suíço',
			99 => 'Sul-africano',
			100 => 'Sul-coreano, coreano',
			101 => 'Surinamês',
			102 => 'Tailandês',
			103 => 'Timorense, maubere',
			104 => 'Trindadense',
			105 => 'Turco',
			106 => 'Ucraniano',
			107 => 'Ugandense',
			108 => 'Uruguaio',
			109 => 'Venezuelano',
			110 => 'Vietnamita',
			111 => 'Zimbabuense'
		);
	}

	// public function getStatus()
	// {
	// 	return array(true => 'Ativo', false => 'Inativo');
	// }

	public function setFlash($mensagem, $sucesso = null)
	{
		$options = array();
		if (is_null($sucesso)) {
			$options = null;
		} else if ($sucesso) {
			$options = array('class' => 'alert-box success');
		} else if (!$sucesso) {
			$options = array('class' => 'alert-box alert');
		}

		$close = (string) '<a href="#" class="close">&times;</a>';

		$this->Session->setFlash($mensagem . $close, 'default', $options);
	}

	public function setMensagem($tipo)
	{
		$Mensagem = ClassRegistry::init('Mensagem');
		$mensagem = $Mensagem->find('first', [
			'conditions' => [
				'Mensagem.tipo' => $tipo,
				'Mensagem.status' => true
			]
		]);
		$this->Session->setFlash($mensagem['Mensagem']['mensagem'], 'mensagem', [], 'mensagem');
	}

	public function check($controller, $action)
	{
		$controller = Inflector::camelize($controller);
		App::import('Controller', $controller.'Controller');
		$aMethods = get_class_methods($controller.'Controller');
		if($aMethods) {
			foreach ($aMethods as $idx => $method) {
				if($action==$method) return true;
			}
		} else  {
		//this is probably NOT a controller!
		}
		return false;
	}

	public function getTiposMensagens($tipo_id = null)
	{
		$tipos = [
			'1'  => 'Agradecimento por ter cadastrado doação (etapa 1) - PF', 
			'2'  => 'Agradecimento por ter cadastrado doação (etapa 2) - PF', 
			'3'  => 'Agradecimento - opção de pagamento cartão de crédito', 
			'4'  => 'Agradecimento - opção de pagamento débito em conta', 
			'5'  => 'Agradecimento - opção de pagamento boleto bancário', 
			'6'  => 'Agradecimento - opção de pagamento paypal', 
			'7'  => 'Mensagem de Cancelamento - Motivo: Cancelamento Voluntário - PF', 
			'8'  => 'Mensagem do Não recebimento das doações', 
			'9'  => 'Mensagem se parou no passo 1 - PF', 
			'10' => 'Mensagem se parou no passo 2 - PF', 
			'11' => 'Mensagem sobre empresa fora do país (etapa 1) - PJ', 
			'12' => 'Agradecimento por ter cadastrado doação (etapa 2) - PJ', 
			'13' => 'Tela de Agradecimento - PJ',
			'14' => 'Tela de Agradecimento - PF',
			'15' => 'Agradecimento por ter cadastrado doação (etapa 1) - PJ', 
			'16' => 'Mensagem de Cancelamento - Motivo: Cancelamento Voluntário - PJ', 
			'17' => 'Mensagem de Cancelamento ­- Motivo: Cancelamento involuntário - PF',
			'18' => 'Mensagem de Cancelamento ­- Motivo: Cancelamento involuntário - PJ',
			'19' => 'Mensagem se parou no passo 1 - PJ', 
			'20' => 'Mensagem se parou no passo 2 - PJ', 
	  	];

		if (!$tipo_id)
			return $tipos;

		return array_search($tipo_id, $tipos);
	}

	public function apenasNumeros($texto)
	{
		return preg_replace("/[^0-9]/", "", $texto);
	}

	public function codigosErroBraspag()
	{
		return ['6', '51', '76', '78', '91', '96', 'AA', '001', '003', '016', '097', '099', '100', '101', '102', '103', '104', '105', '106', '107', '108', '109', '110', '111', '112', '113', '114', '115', '116', '117', '118', '119', '120', '121', '122', '123', '124', '125', '126', '127', '128', '129', '130', '131', '132', '133', '134', '135', '199', 'BP07', 'BP900', 'BP901', 'BP902', 'BP903', 'BP904'];
	}

	public function codigosErroAntifraude()
	{
		return ['101', '102', '150', '151', '152', '202', '231'];
	}

	public function codigosSucessoAntifraude()
	{
		return ['100', '400', '480', '481'];
	}

	public function codigosErroCyberAntifraude()
	{
		return ['234'];
	}

	public function codigosErroRedecard()
	{
		return ['05', '14', '51', '53', '54', '56', '57', '58', '74'];
	}

	public function mesesCom30Dias()
	{
		return ['04', '06', '09', '11'];
	}
}