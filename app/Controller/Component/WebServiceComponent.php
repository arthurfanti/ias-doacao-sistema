<?php
App::uses('HttpSocket', 'Network/Http');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('Xml', 'Utility');

class WebServiceComponent extends Component {

	public function integracaoCompartilhamentoMail2Easy($email, $cadastrar = true)
	{
		if (!PRODUCTION)
			return false;
		
		$SMT_RECEBER = ($cadastrar) ? 1 : 2;

		$httpSocket = new HttpSocket();
		$url = 'http://cache.mail2easy.com.br/integracao';
		$data = [
			'CON_ID'              => '21708.0117ce33cedac883e1f18211b971513a',
			'DESTINO'             => 'http://www.empresa.com.br/obrigado.html',
			'GRUPOS_CADASTRAR'    => '87',
			'GRUPOS_DESCADASTRAR' => '',
			'SMT_email'           => $email,
			'SMT_RECEBER'         => $SMT_RECEBER
		];
		return $httpSocket->post($url, $data);
	}

	public function integracaoFormularioMail2Easy($nome, $sobrenome, $pais, $email, $cadastrar = true)
	{
		if (!PRODUCTION)
			return false;
		
		$SMT_RECEBER = ($cadastrar) ? 1 : 2;

		$httpSocket = new HttpSocket();
		$url = 'http://cache.mail2easy.com.br/integracao';
		$data = [
			'CON_ID'              => '21708.0117ce33cedac883e1f18211b971513a',
			'DESTINO'             => 'http://www.empresa.com.br/obrigado.html',
			'GRUPOS_CADASTRAR'    => '86',
			'GRUPOS_DESCADASTRAR' => '',
			'SMT_nome_completo'   => $nome . ' ' . $sobrenome,
			'SMT_confirme_seu_e_mail' => $email,
			'SMT_email'           => $email,
			'SMT_pais_de_residencia' => $pais,
			'SMT_RECEBER'         => $SMT_RECEBER
		];
		return $httpSocket->post($url, $data);
	}

	public function xml_post_test( $received_data = null, $soapAction = null ){
		$xml = Xml::build( $received_data );
		$xml = $xml->asXML();

		$xmlTeste = (string) '<?xml version="1.0" encoding="ISO-8859-1"?> <parcelas> <parcela> <id_doacao>27050</id_doacao> <Parcela>1</Parcela> <msg_retorno></msg_retorno> <Situacao>13</Situacao> <Valor>80,00</Valor> <data_emissao>2015-02-10 15:06:22</data_emissao> <data_pagamento></data_pagamento> <nosso_numero></nosso_numero> <NSU>1310033</NSU> <autorizacao>20231</autorizacao> </parcela> <parcela> <id_doacao>27051</id_doacao> <Parcela>1</Parcela> <msg_retorno></msg_retorno> <Situacao>6</Situacao> <Valor>40,00</Valor> <data_emissao>2015-02-10 10:06:22</data_emissao> <data_pagamento>2015-02-10 15:06:22</data_pagamento> <nosso_numero></nosso_numero> <NSU></NSU> <autorizacao></autorizacao> </parcela> <parcela> <id_doacao>27050</id_doacao> <Parcela>1</Parcela> <msg_retorno></msg_retorno> <Situacao>6</Situacao> <Valor>600,00</Valor> <data_emissao>2015-02-10 08:06:22</data_emissao> <data_pagamento></data_pagamento> <nosso_numero>2705001</nosso_numero> <NSU></NSU> <autorizacao></autorizacao> </parcela> </parcelas>';

		$options = array(
			'redirect' => true,
			'header'   => array(
				'Accept'         => 'text/xml',
				'Content-Type'   => 'text/xml; charset=UTF-8',
				'SOAPAction'     => 'http://tempuri.org/IIAS_Integracao_Doacoes/' . $soapAction,
				'Cache-Control'  => 'no-cache',
				'Pragma'         => 'no-cache',
				'Content-length' => strlen($xml)
			)
		);

		switch ($soapAction) {
			case 'EnviarXMLDoadorParaCRM':
				$soap_envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"> <soapenv:Header/> <soapenv:Body> <tem:EnviarXMLDoadorParaCRM> <!--Optional:--> <tem:xmlDoador><![CDATA['. $xml .']]></tem:xmlDoador> </tem:EnviarXMLDoadorParaCRM> </soapenv:Body> </soapenv:Envelope>';
				break;

			case 'EnviarXMLDoacoesParaCRM':
				$soap_envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"> <soapenv:Header/> <soapenv:Body> <tem:EnviarXMLDoacoesParaCRM> <!--Optional:--> <tem:xmlDoacao><![CDATA['. $xml .']]></tem:xmlDoacao> </tem:EnviarXMLDoacoesParaCRM> </soapenv:Body> </soapenv:Envelope>';
				break;

			case 'EnviarXMLParcelaParaCRM':
				$soap_envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"> <soapenv:Header/> <soapenv:Body> <tem:EnviarXMLParcelaParaCRM> <!--Optional:--> <tem:xmlParcela><![CDATA['. $xml .']]></tem:xmlParcela> </tem:EnviarXMLParcelaParaCRM> </soapenv:Body> </soapenv:Envelope>';
				break;

			default:
				throw new NotFoundException();
				break;
		}

		$dir  = new Folder('xml', true);
		$file = new File($dir->path . DS . $soapAction . '_' . date('m-d-Y_H:i:s', time()) . '.xml', true);

		if ($file->exists()) {
			$file->write($soap_envelope);
			$file->close();
		}

		$http     = new HttpSocket();
		if (PRODUCTION) {
			$url = 'https://doacaowcf.ias.org.br:5000/WCFIASDoacao/IAS_Integracao_Doacoes.svc?wsdl';//PRODUÇÃO
		} else {
			$url = 'http://189.16.50.118/WCFIASDoacao/IAS_Integracao_Doacoes.svc?wsdl';//TESTE
		}
		$response = $http->post($url, $soap_envelope, $options);

		CakeLog::write('webservice', json_encode($response));
		
		return $response->isOk();
	}

	public function consultar_parcelas($data){
		$xml = (string) '<?xml version="1.0" encoding="utf-8"?><ConsultarParcelas><DataConsulta>'.$data.'</DataConsulta></ConsultarParcelas>';
		$soapAction = 'ReceberXMLParcelasDoCRM';

		$options = array(
			'redirect' => true,
			'header'   => array(
				'Accept'         => 'text/xml',
				'Content-Type'   => 'text/xml; charset=UTF-8',
				'SOAPAction'     => 'http://tempuri.org/IIAS_Integracao_Doacoes/' . $soapAction,
				'Cache-Control'  => 'no-cache',
				'Pragma'         => 'no-cache',
				'Content-length' => strlen($xml)
			)
		);

		$soap_envelope = (string)'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"><soapenv:Header/><soapenv:Body><tem:'.$soapAction.'><tem:xmlConsulta><![CDATA['.$xml.']]></tem:xmlConsulta></tem:'.$soapAction.'></soapenv:Body></soapenv:Envelope>';

		$http = new HttpSocket();
		if (PRODUCTION) {
			$url = 'https://doacaowcf.ias.org.br:5000/WCFIASDoacao/IAS_Integracao_Doacoes.svc?wsdl';//PRODUÇÃO
		} else {
			$url = 'http://189.16.50.118/WCFIASDoacao/IAS_Integracao_Doacoes.svc?wsdl';//TESTE
		}
		$response = $http->post($url, $soap_envelope, $options);

		CakeLog::write('webservice', json_encode($response));

		$xml = Xml::build($response->body);
		$parcelas = Xml::toArray($xml);
		$xml = Xml::build($parcelas['Envelope']['s:Body']['ReceberXMLParcelasDoCRMResponse']['ReceberXMLParcelasDoCRMResult']);
		$parcelas = Xml::toArray($xml);

		return $parcelas;
	}
}