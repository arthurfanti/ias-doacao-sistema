'use strict';

var app = (function(document, $) {
	var docElem = document.documentElement,
		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		_fadeSibling = function(elem, callback) {
			var elemClass = '.' + elem.context.classList[0],
				index = elem.index( elemClass );

			$.each($(elemClass), function(i, val) {
				if (i !== index) {
					$(this).css('opacity', 0.5).attr('checked', false);
				} else {
					$(this).css('opacity', 1).attr( 'checked', true );
				}
			});

			if (typeof callback === 'function') {
				callback.call(this);
			};
		},
		_giftImage = function() {
			var list = document.getElementsByClassName('gift');

			if (typeof list !== 'undefined' && list.length > 0) {
				for (var i = 0; i < list.length; i++) {
					var src = list[i].getAttribute('data-image');
					list[i].style.backgroundImage='url(\'' + src + '\')';
				}

				$(list).on('click', function(event) {
					event.preventDefault();
					var attrEl = $('#' + $(this).data('attribute'));
					if (attrEl.hasClass('hide')) {
						attrEl.removeClass('hide');
						attrEl.siblings('fieldset').each(function(index, el) {
							if ($(el).hasClass('hide')) {
								return;
							} else {
								$(el).addClass('hide');
							}
						});
					};
				});
			}
		},
		_submitHandlers = function() {
			$('.tabs-content').find('.content > .columns').on('click', 'div.button', function(event) {
				// event.preventDefault();
				var tipoDoacao    = $(this).parents('.content').data('type')
				, elsEquivalencia = [$('#equivalencia').find('p'), $('#equivalencia').find('h2')]
				, valorDoacao     = $(this).data('value');

				switch ($.language) {
					case 'por':
						if ( 'undefined' !== typeof eqObject[valorDoacao] ) {
							elsEquivalencia[0].text(eqObject[valorDoacao]['textBefore']);
							elsEquivalencia[1].text(eqObject[valorDoacao]['equivalent'] + ' ' + eqObject[valorDoacao]['textAfter']);
							//
						} else {
							if (tipoDoacao === 'M') {
								elsEquivalencia[0].text(eqObject['standardMonthly']['textBefore']);
								elsEquivalencia[1].text(parseInt(valorDoacao / eqObject['standardMonthly']['divisor']) + ' ' + eqObject['standardMonthly']['textAfter']);
							} else if (tipoDoacao === 'U') {
								elsEquivalencia[0].text(eqObject['standardUnique']['textBefore']);
								elsEquivalencia[1].text(parseInt(valorDoacao / eqObject['standardUnique']['divisor']) + ' ' + eqObject['standardUnique']['textAfter']);
							};
						}
						break;
					case 'eng':
						if ( 'undefined' !== typeof eqObject[valorDoacao] ) {
							elsEquivalencia[0].text(eqObject[valorDoacao]['textBefore']);
							elsEquivalencia[1].text(parseInt(eqObject[valorDoacao]['equivalent']*valuesObject.dolar) + ' ' + eqObject[valorDoacao]['textAfter']);
							//
						} else {
							if (tipoDoacao === 'M') {
								elsEquivalencia[0].text(eqObject['standardMonthly']['textBefore']);
								elsEquivalencia[1].text(parseInt( (valorDoacao / eqObject['standardMonthly']['divisor'])*valuesObject.dolar ) + ' ' + eqObject['standardMonthly']['textAfter']);
							} else if (tipoDoacao === 'U') {
								elsEquivalencia[0].text(eqObject['standardUnique']['textBefore']);
								elsEquivalencia[1].text(parseInt( (valorDoacao / eqObject['standardUnique']['divisor'])*valuesObject.dolar ) + ' ' + eqObject['standardUnique']['textAfter']);
							};
						}
						break;
				}
				// , totalAjudado    = 0
				// , singPlur        = ''
				// , txtEquivalencia = '';

				// ( $.language === 'por' ) ? singPlur = 'crianças' : singPlur = 'children';

				// if ( tipoDoacao === 'M' ) {
				// 	totalAjudado = Math.floor(valorDoacao / 10);
				// 	( $.language === 'eng' && undefined !== typeof $.tipoDoador && $.tipoDoador !== 'PJ' ) ? totalAjudado = totalAjudado * 3 : null ;

				// 	if (totalAjudado == 1) {
				// 		if ( $.language === 'por' ) {
				// 		singPlur = 'criança';
				// 		} else if ( $.language === 'eng' ) {
				// 			singPlur = 'children';
				// 		};
				// 	};
				// } else if ( tipoDoacao === 'U') {
				// 	totalAjudado = Math.floor(valorDoacao / 120);
				// 	( $.language === 'eng' && undefined !== typeof $.tipoDoador && $.tipoDoador !== 'PJ' ) ? totalAjudado = totalAjudado * 3 : null ;

				// 	if (totalAjudado == 1) {
				// 		if ( $.language === 'por' ) {
				// 		singPlur = 'criança';
				// 		} else if ( $.language === 'eng' ) {
				// 			singPlur = 'children';
				// 		};
				// 	};
				// }

				// ($.language === 'por') ? txtEquivalencia = totalAjudado + ' ' + singPlur + ' por ano' : txtEquivalencia = totalAjudado + ' ' + singPlur + ' a year';
				// elEquivalencia.text(txtEquivalencia);
			});

			$('dd.single, dd.monthly').on('click', function(event) {
				// event.preventDefault();
				var target = '';

				if ( this.classList.contains('single') ) {
					target = $('div#single');
				} else if ( this.classList.contains('monthly') ) {
					target = $('div#monthly');
				};

				target.find('.button.active').trigger('click');
			});

			$('.button.qualquer-valor, #valorUnico, #valorMensal').on('click', function(event) {
				event.preventDefault();
				$('#valorMensal, #valorUnico').focus();
			});
			$('#valorUnico, #valorMensal')
				.keypress(function(e) {
				if(e.which !== 13) {
					var v = parseFloat($(this).val().replace(/[U|R|$|.]/gi, '').replace(',', '.'));
					$(this).parent().parent().data('value', v);
				} else {
					$(this).focus();
					return false;
				}
				})
				.blur(function(event) {
					var v = parseFloat($(this).val().replace(/[U|R|$|.]/gi, '').replace(',', '.'));
					$(this).select().parent().parent().data('value', v);
				})
				.focus(function(event) {
					var v = parseFloat($(this).val().replace(/[U|R|$|.]/gi, '').replace(',', '.'));
					$(this).select().parent().parent().data('value', v);
				});
			$('#formValores')
				.on('click', ':submit', function(event) {
					// define pra onde vai o scroll
					$('#formValores').data('target', $(this).data('target'))
				})
				.submit(function(event){
					// event.preventDefault();
					$._formTarget = $(this).data('target');

					// pega tipo e valor de doação
					$.each($('.tabs-content').find('.content'), function(index, el) {
						if ($(this).is(':visible')) {
							$._tipoDoacao  = $(this).data('type');
							$._valorDoacao = $(this).find('.active').data('value');
							$._cstmDoacao  = $(this).find('.active').index('') == 3 ? true : false ;
						};
					});

					$('#tipoDeDoacao').attr('value', $._tipoDoacao);
					$('#valorDeDoacao').attr('value', $._valorDoacao);

					// esconde/exibe informações de pagamento

					$('#min-value-error .close').trigger('click'); //fecha o alert anterior, se tiver um

					if ($._tipoDoacao == 'M') {
						$('input[type="radio"]#boleto').parent('div').hide();
						// validação básica
						if ($._cstmDoacao && $._valorDoacao < valuesObject.minMonthly) {
							if ($.language == 'por') {
								var textToReplace = 'O valor mínimo para doação mensal é de R$'+ valuesObject.minMonthly +',00';
							} else if ($.language == 'eng'){
								var textToReplace = 'The minimum value for monthly donation is U$'+ valuesObject.minMonthly +',00';
							};
							var alertBox = '<div data-alert class="alert-box" style="position:fixed; width:100%; z-index:2" id="min-value-error">';
							alertBox += textToReplace;
							alertBox += '<a href="#" class="close">&times;</a></div>';

							$('body').prepend(alertBox).foundation();
							return false;
						} else {
							return true;
						};
					} else if ($._tipoDoacao == 'U') {
						// validação básica
						if ($._cstmDoacao && $._valorDoacao < valuesObject.minUnique) {
							if ($.language == 'por') {
								var textToReplace = 'O valor mínimo para doação única é de R$'+ valuesObject.minUnique +',00';
							} else if ($.language == 'eng'){
								var textToReplace = 'The minimum value for single donation is U$'+ valuesObject.minUnique +',00';
							};
							var alertBox = '<div data-alert class="alert-box" style="position:fixed; width:100%; z-index:2" id="min-value-error">';
							alertBox += textToReplace;
							alertBox += '<a href="#" class="close">&times;</a></div>';

							$('body').prepend(alertBox).foundation();
							return false;
						} else {
							return true;
						};

						if ($('input[type="radio"]#boleto').parent('div').is(':hidden')) {
							$('input[type="radio"]#boleto').parent('div').show();
						};
					};


					// Isso aqui pode sere útil Steves!
					// $('#tabela-sua-doacao')
					// 	.find('input[type="text"]').val($._valorDoacao)
					// 	.end()
					// 	.find('select').val($._tipoDoacao).change();

					// esse ajax vai morrer!
					// $.ajax({
					// 	type: 'post',
					// 	url: 'salvarvalores',
					// 	beforeSend: function(xhr) {
					// 		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
					// 	},
					// 	success: function(response) {
					// 		$('html, body').animate({
					// 			scrollTop: $('div#login').offset().top
					// 		}, 500);

					// 		$('#tabela-sua-doacao')
					// 			.find('input[type="text"]').val($._valorDoacao)
					// 			.end()
					// 			.find('select').val($._tipoDoacao).change();

					// 		console.log(response);
					// 		if (response.retorno) {
					// 			if (response.redireciona == 'NOVO')
					// 				console.log('NOVO');
					// 			else
					// 				console.log('DOADOR');
					// 		}
					// 		// if (response.error) {
					// 		//     alert(response.error);
					// 		//     console.log(response.error);
					// 		// }
					// 		// if (response.content) {
					// 		//     $('#target').html(response.content);
					// 		// }
					// 		// $('#teste').html(response);
					// 	},
					// 	error: function(e) {
					// 		alert("An error occurred: " + e.responseText.message);
					// 		console.log(e);
					// 	}
					// });
				});

			$('#formLogin').on('valid.fndtn.abide', function(event) {
				event.preventDefault();

				$('div#cadastro')
					.parent('section').removeClass('hide').end()
					.find('div#dadosPessoais').remove();

				$('html,body').animate({
					scrollTop : $('div#cadastro').offset().top
				}, 500)
			});

			$('#formDoacao').on('valid', function(event) {
				$('#loadingModal').foundation('reveal', 'open');
			});

			$('#DoacaoComplementoForm').on('submit', function(event) {
				// event.preventDefault();
				var giftsContainer = $('#giftsContainer')
				, giftChosen       = giftsContainer.find('fieldset:visible');

				giftChosen.siblings('fieldset').remove();

				return true;

			});

			// $('#formDoacao').submit(function(event) {
			// 	event.preventDefault();
			// 	console.log('submit triggered');

			// 	if ( $(this).find('small.error:visible').length > 0 ) {
			// 		console.log('error visible');
			// 		return false;
			// 	} else {
			// 		console.log('no errors visible');
			// 		var alertBox = '<div data-alert class="alert-box alert" style="position:fixed; width:100%; z-index:2">';
			// 			alertBox += '<div class="row">';
			// 			alertBox += '<div class="small-12 column">';
			// 			alertBox += '<p class="lead">Olá,</p>';
			// 			alertBox += '<p>seu cadastro e doação foram processados com sucesso. Conclua seu cadastro e receba vantagens.</p>';
			// 			alertBox += '<p class="lead">O Protocolo de Doação é: ABX3729</p>';
			// 			alertBox += '</div>';
			// 			alertBox += '</div>';
			// 			alertBox += '<a href="" class="close">&times;</a>';
			// 			alertBox += '</div>';

			// 		$('body').prepend(alertBox).foundation();

			// 		$('div#cadastro').parent('section').addClass('hide');
			// 		$('div#cadastro-complemento').parent('section').removeClass('hide');

			// 		$('html,body').animate({
			// 			scrollTop : $('div#cadastro-complemento').offset().top
			// 		}, 500)

			// 		return true;
			// 	}



			// 	// $.ajax({
			// 	// 	type: 'post',
			// 	// 	url: 'efetuardoacao',
			// 	// 	dataType: 'json',
			// 	// 	data: $('#formDoacao').serialize(),
			// 	// 	beforeSend: function(xhr) {
			// 	// 		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			// 	// 	},
			// 	// 	success: function(response) {
			// 	// 		console.log(response);
			// 	// 		if (response.retorno) {

			// 	// 		}
			// 	// 		// if (response.error) {
			// 	// 		//     alert(response.error);
			// 	// 		//     console.log(response.error);
			// 	// 		// }
			// 	// 		// if (response.content) {
			// 	// 		//     $('#target').html(response.content);
			// 	// 		// }
			// 	// 		// $('#teste').html(response);
			// 	// 	},
			// 	// 	error: function(e) {
			// 	// 		alert("An error occurred: " + e.responseText);
			// 	// 		console.log(e);
			// 	// 	}
			// 	// });
			// });
		},
		_init = function() {
			$(document).foundation({
				abide : {
					timeout : 60000,
					patterns: {
						alphaSpaces: /^[a-zA-Z\s]+$/,
						passwords: /^[^\s]{6,}$/
					},
					validators : {
						testaAnoAtual: function(el, required, parent){
							var userYear = el.value.slice(6),
									currentYear = (new Date).getFullYear();

							if ( userYear >= currentYear) {
								if ($.language == 'por') {
									var textToReplace = 'A data tem que ser anterior a atual.';
								} else if ($.language == 'eng'){
									var textToReplace = 'The date must be previous than today.';
								};
								parent.find('small').replaceWith('<small class="error">'+textToReplace+'</small>');
								return false;
							}else{
								return true;
							};
						},
						testaValorDoado: function(el, required, parent) {
							var tipoDoacao   = $('#tipo-doacao')
							, valorDoacao    = $('#DoacaoValor')
							, valorDoacaoVal = parseInt( (valorDoacao.val().replace(/[^0-9]/g, ""))/100 );
							// , valorDoacaoVal = Math.round(valorDoacao.val().replace(".", "").replace(",", ".").replace("R$", ""));

							// console.log(valorDoacaoVal + ', ' + tipoDoacao.val());
							if (tipoDoacao.val() == 'M' || tipoDoacao.val() == 1) {
								if (valorDoacaoVal < valuesObject.minMonthly) {
									if ($.language == 'por') {
										var textToReplace = 'O valor mínimo para doação mensal é de R$'+ valuesObject.minMonthly +',00';
									} else if ($.language == 'eng'){
										var textToReplace = 'The minimum value for monthly donation is U$'+ valuesObject.minMonthly +',00';
									};
									valorDoacao.next().replaceWith('<small class="error">'+textToReplace+'</small>');
									return false;
								} else{
									return true;
								};
							} else if (tipoDoacao.val() == 'U' || tipoDoacao.val() == 2){
								if (valorDoacaoVal < valuesObject.minUnique) {
									// valorDoacao.select();
									if ($.language == 'por') {
										var textToReplace = 'O valor mínimo para doação única é de R$'+ valuesObject.minUnique +',00';
									} else if ($.language == 'eng'){
										var textToReplace = 'The minimum value for single donation is U$'+ valuesObject.minUnique +',00';
									};
									valorDoacao.next().replaceWith('<small class="error">'+textToReplace+'</small>');
									return false;
								} else{
									return true;
								};
							};
						},
						testaCPF: function(el, required, parent) {
							var numeros, digitos, soma, i, resultado, digitos_iguais, cpf, exp = /\.|\-|\_/g;
							cpf = el.value.toString().replace( exp, "" );

							digitos_iguais = 1;
							if (cpf.length < 11){
								return false;
							}

							for (i = 0; i < cpf.length - 1; i++){
								if (cpf.charAt(i) != cpf.charAt(i + 1)){
									digitos_iguais = 0;
									break;
								}
							}

							if (!digitos_iguais){
								numeros = cpf.substring(0,9);
								digitos = cpf.substring(9);
								soma = 0;

								for (i = 10; i > 1; i--){
									soma += numeros.charAt(10 - i) * i;
								}
								resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

								if (resultado != digitos.charAt(0)){
									return false;
								}

								numeros = cpf.substring(0,10);
								soma = 0;

								for (i = 11; i > 1; i--){
									soma += numeros.charAt(11 - i) * i;
								}
								resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

								if (resultado != digitos.charAt(1))
										return false;

								return true;
							} else {
								return false;
							}
						},
						testaCNPJ: function(el, required, parent) {
							var i = new Number, cnpj = el.value, valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2), dig1 = new Number, dig2 = new Number, exp = /\.|\-|\/|\_/g;

							cnpj = cnpj.toString().replace( exp, "" );
							var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));

							for(i = 0; i<valida.length; i++){
								dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);
								dig2 += cnpj.charAt(i)*valida[i];
							}

							dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
							dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));

							if( ( (dig1*10)+dig2 ) == digito ){
								return true;
							} else {
								return false;
							}
						}
					}
				}
			});
			// needed to use joyride
			// doc: http://foundation.zurb.com/docs/components/joyride.html
			$(document).on('click', '#start-jr', function () {
				$(document).foundation('joyride', 'start');
			});
			// Import Fonts
			WebFont.load({
				google: {
					families: ['Dosis:300,500,800:latin']
				}
			});

			$('input[type="radio"].flag, input[type="radio"].gift').on('click', {}, function(event) {
				event.preventDefault();
				var value = $(this).val();

				_fadeSibling( $(this), function(){

					if ( $('#formasPagamentoId').length ) {
						$('#formasPagamentoId').val(value);
					};
				} );

				// muda a mascara de acordo com o banco
				if ($('#debito *[checked]').attr('data-flag') == 'bradesco') {
					$('#debitoAgencia').inputmask('9999-9').attr('pattern', '[0-9]{4}\-[0-9]');
					$('#debitoConta').inputmask('9999999-9').attr('pattern', '[0-9]{7}\-[0-9]');
				} else if ($('#debito *[checked]').attr('data-flag') == 'itau') {
					$('#debitoAgencia').inputmask('9999').attr('pattern', '[0-9]{4}');
					$('#debitoConta').inputmask('99999-9').attr('pattern', '[0-9]{5}\-[0-9]');
				} else if ($('#debito *[checked]').attr('data-flag') == 'santander') {
					$('#debitoAgencia').inputmask('9999').attr('pattern', '[0-9]{4}');
					$('#debitoConta').inputmask('999999-9').attr('pattern', '[0-9]{6}\-[0-9]');
				} else if ($('#debito *[checked]').attr('data-flag') == 'hsbc') {
					$('#debitoAgencia').inputmask('9999').attr('pattern', '[0-9]{4}');
					$('#debitoConta').inputmask('99999-99').attr('pattern', '[0-9]{5}\-[0-9]{2}');
				};
			});

			$('input[type="radio"][name="gift"]').on('click', function(event) {
				var brindeItemId = $(this).val();
				$('#brindeItemId').val(brindeItemId);
			});

			// datepicker i18n
			$.extend( $.fn.pickadate.defaults, {
				monthsFull: [ 'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro' ],
				monthsShort: [ 'jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez' ],
				weekdaysFull: [ 'domingo', 'segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado' ],
				weekdaysShort: [ 'dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sab' ],
				today: 'hoje',
				clear: 'excluir',
				close: 'fechar',
				format: 'dddd, d !de mmmm !de yyyy',
				formatSubmit: 'dd/mm/yyyy',
				hiddenName: true,
				selectYears: true,
				selectMonths: true
			});

			// $('.datepicker').pickadate();
			$('.datepicker#data-vencimento').pickadate({
				min: new Date( Date.now() )
			});
			// $('.datepicker-birth').pickadate({
			// 	selectYears: 30,
			// 	min: new Date(1920,1,1),
			// 	max: true
			// });

			$('div#formasPagamento').on('change', 'input[name=\'formaPagamento\']', function(event) {
				event.preventDefault();
				/* Act on the event */
				var _id = $(this).val(),
					_el = $('article.formaPagamento#' + _id);

				if (_id === 'boleto') {

					$('button#gerar_boleto').parent('.columns').parent('.row').removeClass('hide');
					$('button#enviar_doacao').parent('.columns').parent('.row').addClass('hide');

				} else {

					if ($('button#gerar_boleto').parent('.columns').parent('.row').is(':visible')) {
						$('button#gerar_boleto').parent('.columns').parent('.row').addClass('hide');
					}

					if ($('button#enviar_doacao').parent('.columns').parent('.row').hasClass('hide')) {
						$('button#enviar_doacao').parent('.columns').parent('.row').removeClass('hide');
					}
				};

				if (_el.hasClass('hide')) {
					_el.removeClass('hide').find('input[type!="radio"], select').attr('required', 'required').end()
						.siblings('article.formaPagamento').each(function(index, el) {

							if ($(this).hasClass('hide')) {
								return;
							} else {
								$(this).addClass('hide');
								$(this).find('input[type!="radio"], select').removeAttr('required');
							};
						});
				};
			});

			$('input#DoadorContaBrasil').on('change', function(event) {
				var _targets   = $('article.formaPagamento, div#formasPagamento .columns')
				, _sbmtpypl    = $('button#checkout_paypal').parent('.columns').parent('.row')
				, _sbmtstnd    = $('button#enviar_doacao').parent('.columns').parent('.row')
				, _creditField = $('div#formasPagamento #credito').parent()
				, _creditInput = $('article.formaPagamento#credito');

				paymentMethods();

				if ( !$(this).is(':checked') ) {
					// void( _sbmtpypl.hasClass('hide') && _sbmtpypl.removeClass('hide') );
					// void( _sbmtstnd.not('.hide') && _sbmtstnd.addClass('hide') );

					_targets.each(function(index, el) {
						if ($(this).hasClass('hide')) {
							return;
						} else {
							$(this).addClass('hide').find('input[type!="radio"], select').removeAttr('required');
						}
					});

					$('#doadorCpf').parent().hide().appendTo('body');

					// $('input[type=tel]').inputmask('remove');

					_creditInput.removeClass('hide').find('input[type!="radio"], select').attr('required', true);
					_creditField.removeClass('hide').find('input[type!="radio"], select').attr('required', true);

				} else {
					_creditInput.addClass('hide').find('input[type!="radio"], select').attr('required', true);
					_creditField.addClass('hide').find('input[type!="radio"], select').attr('required', true);

					// void( _sbmtpypl.not('.hide') && _sbmtpypl.addClass('hide') );
					// void( _sbmtstnd.hasClass('hide') && _sbmtstnd.removeClass('hide') );

					_targets.each(function(index, el) {
						if ($(this).hasClass('hide')) {
							$(this).removeClass('hide').find('input[type!="radio"], select').attr('required', true);
							if ($(this).is('article#credito')) {
								return false;
							}
						} else {
							return;
						}
					});

					$('#doadorCpf').parent().appendTo('.cpf-place').show();

					// $('*[type="tel"]').inputmask({
					// 	mask: '(99) 9{4,5}-9999',
					// 	skipOptionalPartCharacter: " ",
					// 	clearMaskOnLostFocus: true
					// });


				};
			});

			$('select#residente').on('change', function(event) {
				var _country = $(this).val();
				if (_country !== 'BR') {
					$('input[type=tel]').inputmask('remove');
				} else {
					$('*[type="tel"]').inputmask({
						mask: '(99) 9{4,5}-9999',
						skipOptionalPartCharacter: " ",
						clearMaskOnLostFocus: true
					});
				}
			});

			// $('select#residente').on('change', function(event) {
			// 	var _country  = $(this).val(),
			// 		_targets  = $('article.formaPagamento, div#formasPagamento'),
			// 		_sbmtpypl = $('button#checkout_paypal').parent('.columns').parent('.row'),
			// 		_sbmtstnd = $('button#enviar_doacao').parent('.columns').parent('.row');

			// 	if ( _country !== 'BR') {
			// 		void( _sbmtpypl.hasClass('hide') && _sbmtpypl.removeClass('hide') );
			// 		void( _sbmtstnd.not('.hide') && _sbmtstnd.addClass('hide') );

			// 		_targets.each(function(index, el) {
			// 			if ($(this).hasClass('hide')) {
			// 				return;
			// 			} else {
			// 				$(this).addClass('hide').find('input').removeAttr('required');
			// 			}
			// 		});
			// 	} else {
			// 		void( _sbmtpypl.not('.hide') && _sbmtpypl.addClass('hide') );
			// 		void( _sbmtstnd.hasClass('hide') && _sbmtstnd.removeClass('hide') );

			// 		_targets.each(function(index, el) {
			// 			if ($(this).hasClass('hide')) {
			// 				$(this).removeClass('hide').find('input').attr('required', true);
			// 				if ($(this).is('article#credito')) {
			// 					return false;
			// 				}
			// 			} else {
			// 				return;
			// 			}
			// 		});
			// 	}
			// });

			/* Random Logo
			----------------> */
				function getRandomNumber(min, max) {
					var n = '';
					n = Math.random() * (max - min) + min;
					return Math.round(n) -1;
				}
				var randomNumber = getRandomNumber(1, 4);

				if (randomNumber === 0) {
					var sortedLogo = 'a';
				} else if(randomNumber === 1){
					var sortedLogo = 'b';
				} else if(randomNumber === 2){
					var sortedLogo = 'c';
				} else if(randomNumber === 3){
					var sortedLogo = 'd';
				};

				$('#instituto-ayrton-senna').addClass(sortedLogo);
				var favicon = '<!-- Favicon --><!--[if IE]><link rel="shortcut icon" href="/images/favicon-' + sortedLogo +'.ico"><![endif]--><link rel="icon" href="/images/favicon-' + sortedLogo +'.png" sizes="152x152"><!-- IOS --><link rel="apple-touch-icon" href="/images/favicon-' + sortedLogo +'.png"><!-- Windows 8 - Tiles --><meta name="msapplication-TileImage" content="/images/favicon-' + sortedLogo +'.png"><meta name="msapplication-TileColor" content="#f9f9f9">';
				$('head').append(favicon);

			/* Select a value
			----------------> */
				function selectValue(button){
					button.on('click', function(event) {
						event.preventDefault();
						if ($(this).is('.active')) {
							if($(this).siblings().is('.active')){
								$(this).removeClass('active');
							}
						} else if(!$(this).is('.active')){
							$(this).addClass('active');

							if($(this).siblings().is('.active')){
								$(this).siblings().removeClass('active');
							}
						};
					});
				};
				selectValue($('#monthly .button'));
				selectValue($('#single .button'));

			/* CEP
			----------------> */
				function cepAPI($url, $success, $fail) {
					$.ajax({
						url: $url,
						context: document.body,
						crossDomain: true,
						timeout: 5000,
						dataType: 'jsonp',
						success : $success,
						error : $fail
					});
				}

				$('input.cep').blur(function(event) {
					var self  = $(this)
					, context = self.parents('fieldset');

					if ($.language == 'por') {
						var $cep = $(this).val().replace(/[^0-9]/gi, ""),
							$url = 'http://correiosapi.apphb.com/cep/'+$cep,
							$success = function(data){
								context.find('input[id*=Logradouro]').attr('value', data.tipoDeLogradouro + ' ' + data.logradouro).addClass('disabled');
								context.find('#estado_id')
									.find('option[data-uf="'+ data.estado +'"]').prop('selected', true)
									.end()
									.trigger('change');

								// window.DoadorEnderecoCidade = data.cidade.replace(/[^\w\s]|_/g, "").replace(/\s+/g, " ");
								window.DoadorEnderecoCidade = data.cidade.replace(/[á|ã|â|à]/gi, "a").replace(/[é|ê|è]/gi, "e").replace(/[í|ì|î]/gi, "i").replace(/[õ|ò|ó|ô]/gi, "o").replace(/[ú|ù|û]/gi, "u").replace(/[ç]/gi, "c").replace(/[ñ]/gi, "n");
								// console.log(window.DoadorEnderecoCidade);

								context.find('input[id*=Bairro]').val(data.bairro);
								context.find('input[id*=Numero]').filter(function(index) {
									return this.id.match(/DoadorEndereco(\d)Numero/);
								}).focus();

								// console.log($data);
								// console.log($data.tipoDeLogradouro + ' ' + $data.logradouro );
								// console.log(data.estado);
								// console.log(data.bairro);
								// console.log(data.cidade );
								// console.log(data );
							},
							$fail = function(){
								context.find('input[id*=Logradouro]').attr({
									'placeholder': 'Nenhum logradouro encontrado para este CEP.',
									'value': ''
								}).removeAttr('disabled').focus();
							};

						if ($cep.length == 8) {
							context.find('input[id*=Logradouro]').attr({
								'value': '',
								'placeholder': 'Carregando...'
							}).addClass('disabled');
							cepAPI($url, $success, $fail);
						} else{
							$fail();
						};
						//
					} else if ($.language == 'eng') {
						$('#DoadorEndereco0Cep').parent('row').remove();
					};
				});

			/* Esconde boleto se for doação mensal
			----------------> */
				function paymentMethods(){
					if (document.getElementById('tipo-doacao') != null && document.getElementById('tipo-doacao').value === 'U') {
						if ( !$('input#DoadorContaBrasil').length || $('input#DoadorContaBrasil').length && $('input#DoadorContaBrasil').is(':checked') ) {
							$('#boleto').parent().show();
						} else {
							$('#boleto').parent().hide();
							$('#credito').trigger('click');
						}
					} else {
						if ($.tipoDoador == 'PJ') {
							$('#boleto').parent().show();
						} else {
							$('#boleto').parent().hide();
							$('#credito').trigger('click');
						}
					};
				};
				paymentMethods();

				$('#tipo-doacao').on('change', function(event) {
					event.preventDefault();
					paymentMethods();
					$('#DoacaoValor').select();
				});

			$('input[type="number"]#valor').blur(function(event) {
				var value = parseInt($(this).val());
				if (typeof value != NaN) {
					$(this).parent('div').data('value', value);
				};
			});


			$('*[type="tel"]').inputmask({
				mask: '(99) 9{4,5}-9999',
				skipOptionalPartCharacter: " ",
				clearMaskOnLostFocus: true
			});

			//bloqueia barra de espaço
			$('*[pattern="passwords"]').keypress(function(e) {
			if(e.which == 32) {
					return false;
			}
			});

			$('*[type=tel]').blur(function(event) {
				var reg = /^\([0-9]{2}\)\s[0-9]{5}\-[0-9]{3}\_$/g;
				if ( reg.test($(this).val()) ) {
					var holdVal = $(this).val().replace(/[^0-9]/g, "")
					, ddd       = holdVal.substr(0,2)
					, parte1    = holdVal.substr(2,4)
					, parte2    = holdVal.substr(6,4);

					$(this).val('('+ ddd +') '+ parte1 + '-' + parte2);
				}
			});

			$('*[type="tel"]').focus(function(event) {
				$(this).trigger('mouseenter');
			}).blur(function(event) {
				$(this).trigger('mouseleave');
			});;

			$('#doadorCpf').inputmask(
				{
					mask: '999.999.999-99',
					clearMaskOnLostFocus: true,
					clearIncomplete: true
				}
			);

			$('#DoadorCpfCnpj').inputmask(
				{
					mask: '99.999.999/9999-99',
					clearMaskOnLostFocus: true,
					clearIncomplete: true
				}
			);

			// $('#cartaoCreditoNum').inputmask('9999 9999 9999 9999');
			$('#cartaoCreditoCod').inputmask('999');
			$('#nascimento').inputmask("d/m/y",{ "placeholder": "DD/MM/AAAA" });
			$('input.cep').inputmask({ mask: "99999-999", greedy: false });
			$('#debitoAgencia').inputmask('9999').attr('pattern', '[0-9]{4}');
			// $('#debitoConta').inputmask('99999-9').attr('pattern', '[0-9]{5}\-[0-9]');
			$('#DoadorEndereco0Numero').inputmask({
				alias:"numeric",
				rightAlign: false
			});

			// carrega cidades
			$('select[name*=estado_id]').change(function(){
				var self  = $(this)
				, context = self.parents('fieldset')
				, cidades = context.find('select[name*=cidade_id]')
				, estado  = $(this).val();

				if (cidades.not(':visible')) {
					cidades.prev('input').hide().end().show();
				};

				if ( cidades.find('option').length = 1 ) {
					cidades.html('<option value="">Carregando...</option>');
				};
				$.get(
					'cidades/all',
					{ estado: estado },
					function(data) {
						cidades.html(data);
						cidades.find('option').each(function(index, el) {
							if ( $(this).text() == window.DoadorEnderecoCidade ) {
								$(this).prop('selected', true);
							};
						});
					}, 'html'
				);
			});

			$(document).ready(function() {
				if ( $('input[name="formas_pagamento_id"]').length ) {
					$('input[name="formas_pagamento_id"]').on('click', function(event) {
						$('button#enviar_doacao').removeClass('disabled').removeAttr('disabled');
					});
				} else {
					console.warn('you are alone!');
				}

				$.each($('article.formaPagamento'), function(index, val) {
					if ( $(this).find('input[name^="data[Pagamento]"]').val() !== '' ) {
						console.info($(this).attr('id') + ' form is filled');
						$('input[name="formaPagamento"]#' + $(this).attr('id')).trigger('click');

						var formaPagamento = document.getElementById('formasPagamentoId').value;
						$('input[name="formas_pagamento_id"][value="'+ formaPagamento +'"]').trigger('click');
					} else {
						console.info($(this).attr('id') + ' form is empty');
					}
				});

				if ( !$('input#DoadorContaBrasil').is(':checked') ) {
					$('input#DoadorContaBrasil').trigger('change')
				};

				if ($.language == 'por') {
						window.languagePrefix = 'R$ ';
					} else if ($.language == 'eng'){
						if ($.tipoDoador == 'PF') {
							window.languagePrefix = 'U$ ';
						} else if ($.tipoDoador == 'PJ') {
							window.languagePrefix = 'R$ ';
						};
					};

				$('#valorUnico, #valorMensal, #DoacaoValor').inputmask({
					prefix: window.languagePrefix,
					groupSeparator: ".",
					alias: "numeric",
					placeholder: "0",
					autoGroup: !0,
					digits: 2,
					radixPoint: ",",
					digitsOptional: !1,
					clearMaskOnLostFocus: !1,
					rightAlign: false
				});

				if ($('div[class^=alert-box]').length) {
					$('div[class^=alert-box]').attr('data-alert', '');
					$('body').foundation();
				};

				if ($.language.typeof !== 'undefined' && $.language == 'por') {
					$('#my-account-table').DataTable({
						responsive: {
							details: {
								type: 'inline',
								target: 'tr'
							}
						},
						columnDefs: [{
							className: 'control',
							orderable: true,
							targets: 0
						}],
						order: [1, 'asc'],
						language: {
							processing:     'processando dados...',
							search:         'Pesquisar',
							lengthMenu:     'Itens por p&aacute;gina: _MENU_',
							info:           'Exibindo itens de _START_ a _END_, num total de _TOTAL_ itens',
							infoEmpty:      'Não há itens para exibir',
							infoFiltered:   '(filtrado de _MAX_ itens no total)',
							infoPostFix:    '',
							loadingRecords: 'carregando dados...',
							zeroRecords:    'Não há itens para exibir',
							paginate: {
								first:      'Primeira',
								previous:   'Anterior',
								next:       'Seguinte',
								last:       '&uacute;ltima'
							},
							aria: {
								sortAscending:  ': habilite para classificar a coluna em ordem crescente',
								sortDescending: ': habilite para classificar a coluna em ordem decrescente'
							}
						}
					});
				} else {
					$('#my-account-table').DataTable({
						language: {
							'lengthMenu': 'Rows per page: _MENU_'
						}
					});
				}
			});

			/* Scroll na página de minha conta
			----------------> */
				$('#bt-aumentar-doacao').on('click', function(event) {
					event.preventDefault();
						$('html,body').animate({
					scrollTop: $('#my-donnations-head').offset().top},
					'slow');
				});

			$('select[name=language]').on('change', function(event) {
				event.preventDefault();

				var idioma = $(this).val()
				, APP = $.url;

				console.log(APP);

				var jqxhr = jQuery.post(APP + 'idioma/' + idioma, {lang: idioma}, function(data, textStatus, xhr) {
					console.info(data);
				}).done(function(data){
					void( data === idioma && location.reload() );
				});

			});

			$('#alterarFormaPagamento').on('click', function(event) {
				// event.preventDefault();
				if ($(this).is(':checked')) {
					$('#dadosPagamento').removeClass('hide').insertAfter('#insertHandler');
				} else if ($(this).not(':checked')) {
					$('#dadosPagamento').addClass('hide').insertAfter('section[role=forms]');
				};
			});

			if ( $('fieldset[role=filial]').length ) {
				var self   = $('fieldset[role=filial]')
				, checkbox = self.find('input[type=checkbox]')
				, elems    = self.find('input[type!=checkbox], select');

				if (checkbox.is(':checked')) {
					elems.prop('disabled', false);
				} else {
					elems.prop('disabled', true);
				}
				//

				checkbox.on('click', function(event) {

					if (checkbox.is(':checked')) {
						elems.prop('disabled', false);
					} else {
						elems.prop('disabled', true);
					}
					//
				});
			};

			_userAgentInit();
			_submitHandlers();
			_giftImage();
		};
	return {
		init: _init
	};
})(document, jQuery);

(function() {
	app.init();
})();
