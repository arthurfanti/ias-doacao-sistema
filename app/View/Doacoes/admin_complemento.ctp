<?php echo $this->Form->create(); ?>
<?php echo $this->Form->hidden('Doador.id', ['value' => $this->Session->read('Doacao.doador_id')]); ?>
<div class="row">
	<div class="small-12 columns">
		<?php if (count($itens) > 0) : ?>
		<div class="row">
			<div class="small-12 columns">
				<h3>Selecione o Brinde</h3>
			</div>
		</div>

		<div class="row">
			<div class="small-12 columns">
				<ul class="small-block-grid-1 medium-block-grid-3">
					<?php $count = 0; ?>
					<?php foreach ($itens as $item) : ?>
					<li>
						<label for="<?php echo strtolower(str_replace(' ', '_', $item['Item']['item'])) ?>">
							<input name="data[Brinde][item_id]" type="radio" id="<?php echo strtolower(str_replace(' ', '_', $item['Item']['item'])) ?>" value="<?php echo $item['Item']['id'] ?>">
							<span class="label"><?php echo $item['Item']['item'] ?></span>
							<?php echo $this->Html->image(
								'/files/item/imagem/'.$item['Item']['id'].'/'.$item['Item']['imagem'],
								['class' => 'th']
							); ?>
						</label>
						<div class="panel">
							<?php foreach ($item['Atributo'] as $atributo) : ?>
							<?php $opcoes = Hash::combine($atributo, 'Conteudo.{n}.id', 'Conteudo.{n}.conteudo'); ?>
							<div>
								<h3><?php echo $atributo['atributo'] ?></h3>
								<?php echo $this->Form->select('BrindeConteudo.'.$count.'.conteudo_id', $opcoes, ['empty' => $atributo['atributo']]) ?>
							</div>
							<?php $count++; ?>
							<?php endforeach ?>
						</div>
					</li>
					<?php endforeach ?>
				</ul>
				<?php //echo $this->Form->input('Brinde.item_id', array('type' => 'hidden', 'id' => 'brindeItemId')) ?>
			</div>
		</div>
		<?php endif ?>
	</div>

	<div class="row collapse">
		<div class="small-12 column">
			<?php echo $this->Form->input(
				$this->Html->tag('i', '', ['class' => 'fa fa-floppy-o']) . ' Salvar',
				['type' => 'button' , 'class' => 'button success large expand', 'id' => 'saveBtn', 'escape' => false]);
			?>
		</div>
	</div>
</div>
<?php echo $this->Form->end() ?>
<script>

</script>
<?php

$script = <<<HTML
$(document).ready(function() {
	$('#DoacaoAdminComplementoForm').on('submit', function(event) {
		var btn  = $('#saveBtn')
		, self   = $(this)
		, iconEl = '<i class="fa fa-spinner fa-pulse"></i> Gravando Dados...';

		btn.html(iconEl);
		self.find('input, select, button').prop('readonly', true);

		$.each($('input[type=radio]'), function(index, val) {
			if(!val.checked) {
				$(val).parents('li').find('select').prop('disabled', true);
			}
		});
	});
});
HTML;

echo $this->Html->scriptBlock(
	$script,
	['inline' => false]
); ?>