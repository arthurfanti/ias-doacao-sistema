<!-- /////
Bem-vindo
-->
<?php
	$configs = $configs['Configuracao'];

	if ($this->Session->check('Auth.User.Doador.pessoa_id')) {
		$tipo_pessoa = ($this->Session->read('Auth.User.Doador.pessoa_id') == 1) ? 'pf' : 'pj' ;
	} else {
		$tipo_pessoa = null;
	}
?>
<div id="main-box">
	<div id="bem-vindo" class="row">
		<h1 class="title"><?= __('Bem-vindo') ?></h1>
		<p class="subtitle"><?= __('Como você deseja fazer a sua doação') ?>?</p>
		<?php echo $this->Form->create() ?>
			<?php if ($this->Session->check('Auth.User')) : ?>
				<?php if ($this->Session->read('Auth.User.Doador.pessoa_id') == 1) : ?>
					<?php echo $this->Form->button(__('Pessoa <br />Física'), ['name' => 'PF', 'class' => 'button']) ?>
				<?php else: ?>
					<?php echo $this->Form->button(__('Pessoa<br>Jurídica'), ['name' => 'PJ', 'class' => 'button']) ?>
				<?php endif ?>
			<?php else: ?>
			<?php echo $this->Form->button(__('Pessoa <br />Física'), ['name' => 'PF', 'class' => 'button']) ?>
			<?php echo $this->Form->button(__('Pessoa<br>Jurídica'), ['name' => 'PJ', 'class' => 'button']) ?>
			<?php endif ?>
		<?php echo $this->Form->end() ?>
		<div class="small-10 small-centered columns">
			<p><?= __('A sua doação é a chave para trazer às nossas crianças a <br /><strong>Educação do futuro, agora.</strong>') ?>
				<!--<a href="mailto:doacao@ias.org.br">doacao@ias.org.br</a> <?= __('ou pelo telefone') ?> <a href="tel:+551129743062">+55 11 2974-3062</a>--></p>
		</div>
	</div>

	<?php echo $this->Geral->getBackground($configs, $tipo_pessoa) ?>
</div>