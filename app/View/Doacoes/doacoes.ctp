<?php
	if ($this->Session->check('Doacao.from_admin')) {
		$configs = $configs['Configuracao'];

		foreach ($pagamentoCredito as $credito) {
			$credito_array[$credito['FormasPagamento']['id']] = $credito['FormasPagamento']['nome'];
		}

		foreach ($pagamentoDebito as $debito) {
			$debito_array[$debito['FormasPagamento']['id']] = $debito['FormasPagamento']['nome'];
			$debito_array_js[$debito['FormasPagamento']['id']] = [
				'nome'        => $debito['FormasPagamento']['nome'],
				'ag_regex'    => $debito['FormasPagamento']['ag_regex'],
				'conta_regex' => $debito['FormasPagamento']['conta_regex']
			];
		}
	}

	$credito      = false;
	$debito       = false;
	$grid_credito = sizeof($pagamentoCredito);
	$grid_debito  = sizeof($pagamentoDebito);

	if (in_array($this->data['Pagamento']['formas_pagamento_id'], [4, 5, 6, 12, 13])) {
		$credito = true;
	} else if (in_array($this->data['Pagamento']['formas_pagamento_id'], [2, 3, 7, 10, 11, 15])) {
		$debito = true;
	}
?>

<?php echo $this->Session->flash(); ?>

<?php if ($this->Session->read('Auth.User.Doador.pessoa_id') == 2) : ?>
<div id="my-donnations">
	<div class="row">
		<table id="my-account-table" class="display responsive nowrap">
			<thead>
				<tr>
					<th></th>
					<th><?php echo __('Parcela') ?></th>
					<th><?php echo __('Data') ?></th>
					<th><?php echo __('Valor') ?></th>
					<th width="10%" class="desktop"><?php echo __('Forma de Pagamento') ?></th>
					<th><?php echo __('Pagamento') ?></th>
					<th><?php echo __('Ações') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($parcelas as $parcela) : ?>
				<tr>
					<td></td>
					<td><?php echo $parcela['Parcela']['parcela'] ?></td>
					<td><?php echo $this->Formatacao->data($parcela['Parcela']['data_pagamento']) ?></td>
					<td>
						<?= ($parcela['FormasPagamento']['tipo'] == 'B') ? $this->Formatacao->moeda($parcela['Parcela']['valor']) : $this->Formatacao->moeda($parcela['Parcela']['valor']) ?>
					</td>
					<td class="desktop"><?php echo __($this->Geral->getTipoFormaPagamento($parcela['FormasPagamento']['tipo'])) ?></td>
					<td><?php echo __($this->Geral->getStatusPagamento($parcela['Parcela']['status_id'])) ?></td>
					<td>
						<?php
							if ($parcela['Parcela']['formas_pagamento_id'] == 1) {
								echo $this->Html->link(__('Imprimir boleto'), '/boleto/'.$parcela['Parcela']['id'], array('target' => 'blank', 'class' => 'button success expand small', 'style' => 'margin:0'));
							}
						?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>

<section role="forms">
	<div class="row collapse">
		<?php //se a doação estiver ativa e for mensal exibe o form ?>
		<?php $tipo_pessoa = $this->Session->read('Auth.User.Doador.pessoa_id') ?>
		<?php if ( ($doacao['Doacao']['status'] == 'A') && ($doacao['Doacao']['periodo_id'] == 1) && $parcela['FormasPagamento']['tipo'] !== 'B'  ) : ?>
			<?php echo $this->Form->create(null, array('data-abide')) ?>
			<?php echo $this->Form->hidden('Doacao.id') ?>
			<?php echo $this->Form->hidden('Pagamento.id') ?>
			<?php echo $this->Form->hidden('Doacao.periodo_id', array('id' => 'tipo-doacao')); ?>

			<div class="small-12 medium-6 columns">
				<fieldset>
					<legend><?php echo  __('Alterar Valor de Doação') ?></legend>
					<?php echo $this->Form->input('Doacao.valor', array('type' => 'text', 'maxlength' => '15' ,'data-abide-validator' => 'testaValorDoado')) ?>
					<?php echo $this->Html->tag('small', 'O valor mínimo para esta doação é de R$20,00', array('class' => 'error')) ?>
				</fieldset>
			</div>
			<div class="small-12 medium-6 columns" id="insertHandler">
				<fieldset>
					<legend><?php echo __('Alterar Forma de Pagamento') ?></legend>
					<?php echo $this->Form->input('alterar_forma_pagamento',
						array(
							'type'  => 'checkbox',
							'id'    => 'alterarFormaPagamento',
							'label' => __('Desejo Alterar a forma de Pagamento')
						)
					) ?>
				</fieldset>
			</div>

			<?php if (!$this->request->isMobile()): ?>
			<?php echo $this->Form->end( array('label' => __('Salvar Dados'), 'class' => 'button right') ) ?>
			<?php else: ?>
			<?php echo $this->Form->end( array('label' => __('Salvar Dados'), 'class' => 'button right expand') ) ?>
			<?php endif ?>

		<?php elseif ( $doacao['Doacao']['status'] == 'A' && $doacao['Doacao']['periodo_id'] == 1 && $tipo_pessoa == 2 && $parcela['FormasPagamento']['tipo'] == 'B' ): ?>
		<?php if ($this->data['Pagamento']['formas_pagamento_id'] == 1): ?>
		<div class="clearfix">
			<div class="small-12 column">
				&nbsp;
			</div>
		</div>
		<div class="row panel">
			<?php echo $this->Form->create('Doacoes', array('url' => '/alterar_boletos')) ?>
				<h3><?php echo __('Alterar Vencimentos') ?></h3>
				<?php //foreach (array_reverse($parcelas, true) as $parcela): ?>
				<?php for ($i=0; $i < count($parcelas); $i++) : ?>
				<?php $p = $parcelas[$i]; ?>
				<?php if ($p['Parcela']['status_id'] == 6): ?>
					<div class="small-6 medium-4 columns">
						<fieldset style="background-color:whitesmoke; box-shadow: 0 6px 12px rgba(0,0,0,.04);">
							<legend><?php echo __('Parcela') . ' ' . $p['Parcela']['parcela'] ?></legend>
							<?php echo $this->Form->hidden('Parcela.'.$i.'.id', ['value' => $p['Parcela']['id']]); ?>
							<?php echo $this->Form->input('Parcela.'.$i.'.data_pagamento', ['type' => 'text', 'value' => $this->Formatacao->data($p['Parcela']['data_pagamento']), 'class' => 'datepicker']); ?>
							<?php echo $this->Form->hidden(null, ['value' => $this->Formatacao->data($p['Parcela']['data_pagamento'])]); ?>
						</fieldset>
					</div>
				<?php endif ?>
				<?php endfor ?>
				<?php echo $this->Form->end( array('label' => __('Salvar Dados'), 'class' => 'button right show-for-medium-up') ) ?>
				<?php echo $this->Form->end( array('label' => __('Salvar Dados'), 'class' => 'button right expand show-for-small-only') ) ?>
		</div>
		<?php endif ?>
		<!--  -->
		<?php elseif (($doacao['Doacao']['status'] == 'A') && ($doacao['Doacao']['periodo_id'] == 2) && ($parcela['Parcela']['status_id'] == '6') && ($parcela['FormasPagamento']['tipo'] == 'B')) : ?>
			<?php echo $this->Form->create(null, array('url' => array('controller' => 'doacoes', 'action' => 'alterar_boleto'), 'data-abide')) ?>
				<?php echo $this->Form->hidden('Parcela.id', array('value' => $parcela['Parcela']['id'])) ?>
				<input name="formas_pagamento_id" type="radio" class="hide" value="1">
				<div class="small-12 column">
					<fieldset>
						<legend><?php echo __('Gerar Segunda via de Boleto') ?></legend>
						<label for="data-vencimento"><?= __('Vencimento')?></label>
						<?php echo $this->Form->input('Parcela.data_pagamento', array('type' => 'text', 'class' => 'datepicker', 'id' => 'data-vencimento', 'placeholder' => 'Data', 'required' => true)) ?>
						<small class="error"><?= __('Campo Obrigatório') ?></small>
						<button class="button expand success" id="gerar_boleto" type="submit">
							<?= __('Emitir Boleto')?>
							<i class="sprite sprite-boleto-branco" style="vertical-align:sub"></i>
						</button>
					</fieldset>
				</div>
			<?php echo $this->Form->end() ?>
		<?php endif; ?>
	</div>
</section>

<?php if (!$this->Session->check('Doacao.from_admin')): ?>
<div class="small-12 column hide" id="dadosPagamento">
	<fieldset>
		<legend>
			<?php echo  __('Atualizar dados Bancários') ?>
			<?php if ($credito): ?>
				<span class="info radius label">Cartão atual: XXXX XXXX XXXX <?php echo $doacao['Pagamento']['cartao_final'] ?></span>
			<?php endif ?>
		</legend>
		<div class="row">
			<div class="small-12 columns">
				<h3><?= __('Dados Financeiros')?></h3>
			</div>
			<div id="formasPagamento">
				<div class="small-12 medium-6 columns">
					<input required type="radio" name="formaPagamento" id="credito" value="credito" <?php if($credito === true) echo 'checked' ?>>
					<label for="credito">
						<i class="sprite-credito"></i>&nbsp;<?= ($this->Session->read('Auth.User.Doador.pessoa_id') == 1) ? __('Cartão de Crédito') : __('Cartão de Crédito<br/> Corporativo'); ?>
					</label>
				</div>
				<div class="clearfix">&nbsp;</div>
				<div class="small-12 medium-6 columns">
					<input required type="radio" name="formaPagamento" id="debito" value="debito" <?php if($debito === true) echo 'checked' ?>>
					<label for="debito">
						<i class="sprite-debito"></i>&nbsp;<?= ($this->Session->read('Auth.User.Doador.pessoa_id') == 1) ? __('Débito em conta') : __('Débito em Conta<br/> Corporativo') ;?>
					</label>
				</div>
				<div class="clearfix">&nbsp;</div>
			</div>
		</div>

		<article class="formaPagamento" id="credito">
			<div class="row">
				<div class="small-12 columns">
					<ul class="small-block-grid-1 medium-block-grid-<?php echo $grid_credito; ?>">
						<?php foreach ($pagamentoCredito as $credito): ?>
							<li>
								<input name="formas_pagamento_id" value="<?php echo $credito['FormasPagamento']['id'] ?>" type="radio" data-flag="<?php echo strtolower($credito['FormasPagamento']['nome']) ?>" class="flag flag-<?php echo strtolower($credito['FormasPagamento']['nome']) ?>" style="opacity: 0.5">
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>

			<?= $this->Form->input('Pagamento.formas_pagamento_id', array('type' => 'hidden', 'id' => 'formasPagamentoId')) ?>

			<div class="row">
				<div class="small-6 columns end">
					<?php echo $this->Form->input('Pagamento.cartaoCreditoNome', array('placeholder' => __('Nome como está no cartão'))) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
			</div>
			<div class="row">
				<div class="small-6 columns">
					<?php echo $this->Form->input('Pagamento.cartaoCreditoNum', array('placeholder' => __('Núm. do cartão'), 'id' => 'cartaoCreditoNum', 'pattern' => 'number', 'required' => true, 'maxlength' => 16)) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('Pagamento.cartaoCreditoCod', array('placeholder' => __('Cód. de segurança'), 'id' => 'cartaoCreditoCod', 'required' => true, 'pattern' => 'cvv')) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
				<div class="small-2 columns"><i class="sprite-cod-seguranca"></i></div>
			</div>

			<div class="row">
				<div class="small-6 columns">
					<label for="validade-mes"><?= __('Validade')?></label>
					<?php echo $this->Form->select('Pagamento.validade_mes', $this->Geral->getMeses(), array('id' => 'validade-mes', 'required' => true, 'empty' => false)) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
				<div class="small-6 columns">
					<label for="validade-ano"><?= __('Ano')?></label>
					<?php echo $this->Form->select('Pagamento.validade_ano', $this->Geral->getAnos(), array('id' => 'validade-ano', 'required' => true, 'empty' => false)) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
			</div>
		</article>

		<article class="formaPagamento hide" id="debito">
			<div class="row">
				<div class="small-12 columns">
					<ul class="small-block-grid-1">
						<ul class="small-block-grid-1 medium-block-grid-<?php echo $grid_debito; ?>">
						<?php foreach ($pagamentoDebito as $debito): ?>
							<li>
								<input name="formas_pagamento_id" value="<?php echo $debito['FormasPagamento']['id'] ?>" type="radio" data-flag="<?php echo strtolower(str_replace('DÉBITO ', '', $debito['FormasPagamento']['nome'])) ?>" class="flag flag-<?php echo strtolower(str_replace('DÉBITO ', '', $debito['FormasPagamento']['nome'])) ?>" style="opacity: 0.5">
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-6 columns end">
					<?php echo $this->Form->select('Pagamento.dia_debito_id', $dias, array('id' => 'data-debito', 'empty' => __('Melhor dia para Débito'))) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<?php echo $this->Form->input('Pagamento.agencia', array('placeholder' => __('Agência'), 'id' => 'debitoAgencia')) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
				<div class="small-12 medium-6 columns">
					<?php echo $this->Form->input('Pagamento.conta', array('placeholder' => __('Conta'), 'id' => 'debitoConta')) ?>
					<?php echo $this->Form->hidden('Pagamento.FormasPagamento.conta_regex'); ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
			</div>
		</article>
	</fieldset>
</div>

<?php else: ?>
<div class="small-12 column hide" id="dadosPagamento">
	<fieldset>
		<legend>
			Atualizar dados bancários:
			<?php if ($credito): ?>
			<span class="info radius label">Cartão atual: XXXX XXXX XXXX <?php echo $doacao['Pagamento']['cartao_final'] ?></span>
			<?php endif ?>
		</legend>
		<div class="small-12 column">
			<dl id="myTabs" class="sub-nav" data-tab>
				<dt>Selecione:</dt>
				<dd <?php if($credito === true) echo 'class="active"' ?>><a href="#panel1">Cartão de Crédito</a></dd>
				<dd <?php if($debito === true) echo 'class="active"' ?>><a href="#panel2">Débito em Conta</a></dd>
				<!-- <dd><a href="#panel3">Boleto</a></dd> -->
				<!-- <dd><a href="#panel4">PayPal</a></dd> -->
			</dl>
			<hr>

			<div class="tabs-content">
				<div class="content <?php if($credito === true) echo 'active' ?>" id="panel1" data-forma-pagamento="credito">
					<?php echo $this->Form->radio('formas_pagamento_id', $credito_array, ['legend' => false]); ?>

					<article class="formaPagamento" id="credito">
						<div class="row">
							<div class="small-12 medium-6 columns end">
								<?php echo $this->Form->input('Pagamento.cartaoCreditoNome', array('placeholder' => __('Nome como está no cartão'), 'pattern' => 'alphaSpaces')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-6 columns">
								<?php echo $this->Form->input('Pagamento.cartaoCreditoNum', array('placeholder' => __('Número do cartão'), 'id' => 'cartaoCreditoNum', 'pattern' => 'card', 'maxlength' => 16)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-6 columns">
								<?php echo $this->Form->input('Pagamento.cartaoCreditoCod', array('placeholder' => __('Código de segurança'), 'id' => 'cartaoCreditoCod', 'pattern' => 'cvv')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="show-for-medium-up medium-2 columns"><i class="sprite-cod-seguranca"></i></div>
						</div>

						<div class="row">
							<div class="small-12 medium-6 columns">
								<label for="validade-mes">Validade (mês)</label>
								<?php echo $this->Form->select('Pagamento.validade_mes', $this->Geral->getMeses(), array('id' => 'validade-mes', 'empty' => false)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-6 columns">
								<label for="validade-ano">Validade (ano)</label>
								<?php echo $this->Form->select('Pagamento.validade_ano', $this->Geral->getAnos(), array('id' => 'validade-ano', 'empty' => false)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
					</article>
				</div>

				<div class="content <?php if($debito === true) echo 'active' ?>" id="panel2" data-forma-pagamento="debito">
					<?php echo $this->Form->radio('formas_pagamento_id', $debito_array, ['legend' => false]); ?>

					<article class="formaPagamento" id="debito">
						<div class="row">
							<div class="small-12 medium-4 columns end">
								<?php echo $this->Form->select('Pagamento.dia_debito_id', $dias, array('id' => 'data-debito', 'empty' => __('Melhor dia para Débito'))) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-4 columns">
								<!-- <input type="text" placeholder="<?= __('Agência') ?> " id="debitoAgencia" name="debitoAgencia"> -->
								<?php echo $this->Form->input('Pagamento.agencia', array('placeholder' => __('Agência'), 'id' => 'debitoAgencia')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-4 columns">
								<!-- <input type="text" placeholder="<?= __('Conta') ?> " id="debitoConta" name="debitoConta"> -->
								<?php echo $this->Form->input('Pagamento.conta', array('placeholder' => __('Conta'), 'id' => 'debitoConta')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
					</article>
				</div>

				<!-- <div class="content" id="panel3" data-forma-pagamento="boleto">
					<article class="formaPagamento" id="boleto">
						<div class="row">
							<div class="small-12 columns">
								<input name="formas_pagamento_id" type="radio" class="hide" value="1">
								<label for="data-vencimento"><?= __('Vencimento')?></label>
								<?php echo $this->Form->input('Pagamento.dataVencimento', array('class' => 'datepicker', 'id' => 'data-vencimento', 'placeholder' => 'Data')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
					</article>
				</div> -->
			</div>
		</div>
	</fieldset>
</div>
<?= $this->Form->input('Pagamento.formas_pagamento_id', array('type' => 'hidden', 'id' => 'formasPagamentoId')) ?>
<?= $this->Form->input('formaPagamento', array(
	'type' => 'hidden',
	'id' => 'formaPagamento',
	'value' => ($credito === true) ? 'credito' : 'debito'
)) ?>
<?php endif ?>

<?php if (isset($doacao['Pagamento']['FormasPagamento']['conta_regex'])) {
// $conta_regex = <<<HTML
// $(document).ready(function() {
// 	var conta_mask = '{$doacao['Pagamento']['FormasPagamento']['conta_regex']}'
// 	, conta_pattern = conta_mask.replace(/9/g, '[0-9]');

// 	$('#debitoContaEdicao').inputmask(conta_mask).attr('pattern', conta_pattern);
// });
// HTML;

// echo $this->Html->scriptBlock(
// 	$conta_regex,
// 	['inline' => false]);
} ?>

<?php if ($this->Session->check('Doacao.from_admin')) {
$js_vars_string = <<<HTML
var debitObj = {\n
HTML;

foreach ($debito_array_js as $key => $value) {
$nome = strtolower(str_replace('É', 'E', str_replace(' ', '_', $value['nome'])));
$js_vars_string .= <<<HTML
	$nome: {id:$key, ag_mask:'{$value['ag_regex']}', conta_mask:'{$value['conta_regex']}'},\n
HTML;
}

$js_vars_string .= <<<HTML
}
var valuesObject = {\n
HTML;

if ($doadorInfo['Doador']['pessoa_id'] == 1) {
$js_vars_string .= <<<HTML
	'minMonthly': {$configs['minimo_mensal_dpf']},
	'minUnique': {$configs['minimo_anual_dpf']}
}
HTML;
} elseif ($doadorInfo['Doador']['pessoa_id'] == 2) {
$js_vars_string .= <<<HTML
	'minMonthly': {$configs['minimo_mensal_dpj']},
	'minUnique': {$configs['minimo_anual_dpj']}
}
HTML;
}

$script = <<<HTML
$js_vars_string\n
$(document).ready(function() {
	$('#alterarFormaPagamento').on('click', function(event) {
		// event.preventDefault();
		if ($(this).is(':checked')) {
			$('#dadosPagamento').removeClass('hide').insertAfter('#insertHandler');
		} else if ($(this).not(':checked')) {
			$('#dadosPagamento').addClass('hide').insertAfter('section[role=forms]');
		};
	});

	// $('#DoacaoAdminAdicionarForm').on('valid', function(event) {
	// 	var btn  = $('#saveBtn')
	// 	, self   = $(this)
	// 	, iconEl = '<i class="fa fa-spinner fa-pulse"></i> Gravando Dados...';

	// 	btn.html(iconEl);
	// 	self.find('input, select, button').prop('readonly', true);
	// });

	// if ($('#formaPagamento').length)
	// 	$('#formaPagamento').val('credito');

	$('#DoacaoValor').inputmask({
		prefix: 'R$',
		groupSeparator: ".",
		alias: "numeric",
		placeholder: "0",
		autoGroup: !0,
		digits: 2,
		radixPoint: ",",
		digitsOptional: !1,
		clearMaskOnLostFocus: !1,
		rightAlign: false
	});

	$('input[type=\'radio\']').on('click', function(event) {
		// event.preventDefault();
		var self = $(this)
		, value = self.val();

		if ($('#formasPagamentoId').length)
			$('#formasPagamentoId').val(value);

		// muda a mascara de acordo com o banco
		$.each(debitObj, function(index, val){
			if (value == val.id)
			{
				ag_mask = val.ag_mask;
				ag_pattern = ag_mask.replace(/9/g, '[0-9]');

				conta_mask = val.conta_mask;
				conta_pattern = conta_mask.replace(/9/g, '[0-9]');
			}
		});

		$('#debitoAgencia').inputmask(ag_mask).attr('pattern', ag_pattern);
		$('#debitoConta').inputmask(conta_mask).attr('pattern', conta_pattern);
	});

	$('input[type=\'radio\']').each(function(index, el) {
		if ($(el).val() == {$this->data['Pagamento']['formas_pagamento_id']}) {
			$(el).prop('checked', true).trigger('click');
		};
	});

	var toggleCounter = 0;
	$('#myTabs').on('toggled', function (event, tab) {
		toggleCounter++;

		if(toggleCounter % 2 !== 0) {
			return;
		}
		// console.log(tab);
		formaPagamento = $(tab).data('forma-pagamento');

		if ($('#formaPagamento').length)
			$('#formaPagamento').val(formaPagamento);
	});

	$('#myTabs').trigger('click');
});
HTML;

echo $this->Html->scriptBlock(
	$script,
	['inline' => false]
);
} ?>

<?php if ($this->data['Pagamento']['formas_pagamento_id'] == 1): ?>
	<?php echo $this->Html->scriptBlock(
	"$(document).ready(function() {
		// Use the picker object directly.
		var \$input = $('.datepicker').pickadate()
		, pickerInstance = new Object()
		, pickerInitial = new String();

		// console.log(\$input);

		\$input.each(function(index, el) {
			// console.log(\$(el));
			pickerInitial = \$(el).siblings('input[type=hidden][id*=DataPagamento]').val();
			// console.log(pickerInitial);
			pickerInstance = \$(el).pickadate('picker');
			pickerInstance.set('select', pickerInitial, { format: 'dd/mm/yyyy' });
			\$(el).siblings('input[type=hidden][id*=DataPagamento]').remove();
		});
	});",
	['inline' => false]); ?>
<?php endif ?>