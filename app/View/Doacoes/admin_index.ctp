<div class="row">
	<div class="small-12 columns">
		<div class="panel">
			<span>para cadastrar manualmente uma doação, vá para a <?php echo $this->Html->link('listagem de doadores', array('controller' => 'doadores', 'action' => 'index', 'admin' => true)) ?> e, na coluna <i>ações</i>, escolha a opção <i>adicionar doação</i>.</span>
		</div>
		<?php echo $this->Form->create() ?>
			<fieldset>
				<legend>Filtros: </legend>
				<div style="position:relative">
					<!-- <div class="row">
						<div class="small-12 medium-6 columns">
							<select name="show_by" id="show_by">
								<option value="period">Exibir por período de tempo</option>
								<option value="day">Nas últimas 24 horas</option>
							</select>
						</div>

						<div class="small-12 medium-6 columns">
							<ul class="button-group">
								<li class="button tiny secondary disabled">Exportar:</li>
								<li><a href="#" class="button tiny">CSV</a></li>
								<li><a href="#" class="button tiny">XLS</a></li>
								<li><a href="#" class="button tiny">PDF</a></li>
							</ul>
						</div>
					</div> -->

					<div class="row">
						<div class="small-6 medium-5 columns">
							<label for="">De:</label>
							<?php
							$valor_data_inicial = isset($this->data['Doacao']['data_inicial']) ? $this->Formatacao->data($this->data['Doacao']['data_inicial']) : '';
							$valor_data_final   = isset($this->data['Doacao']['data_final']) ? $this->Formatacao->data($this->data['Doacao']['data_final']) : '';
							?>
							<?php echo $this->Form->input('data_inicial', ['class' => 'datepicker', 'placeholder' => '00/00/0000', 'value' => $valor_data_inicial]) ?>
						</div>

						<div class="small-6 medium-5 columns">
							<label for="">Até:</label>
							<?php echo $this->Form->input('data_final', ['class' => 'datepicker', 'placeholder' => '00/00/0000', 'value' => $valor_data_final]) ?>
						</div>
						<div class="small-12 medium-2 columns">
							<button type="submit" class="button tiny expand" style="margin-top:1.35rem">
								<i class="fa fa-filter">&nbsp;</i>Filtrar
							</button>
						</div>
					</div>

					<!-- <div style="position:absolute;width:100%;height:100%;background:white;opacity:.5;top:0"></div> -->
				</div>
			</fieldset>
		<?php echo $this->Form->end() ?>
	</div>
</div>

<article id="list">
	<div class="row">
		<div class="small-12 column">
			<table width="100%" id="datatable" class="display responsive nowrap">
				<thead>
					<tr>
						<th></th>
						<th>Doador</th>
						<th>Data</th>
						<th>Valor</th>
						<th class="desktop">Doador</th>
						<th class="desktop">Tipo de Doação</th>
						<th class="desktop">Forma de Pagamento</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th>Doador</th>
						<th>Data</th>
						<th>Valor</th>
						<th class="desktop">Doador</th>
						<th class="desktop">Tipo de Doação</th>
						<th class="desktop">Forma de Pagamento</th>
						<th>Ações</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($data as $doacao): ?>
						<tr>
							<td></td>
							<td><?php echo $doacao['Doador']['nome'] . ' ' . $doacao['Doador']['sobrenome'] ?></td>
							<td><?php echo $this->Formatacao->data($doacao['Doacao']['created']) ?></td>
							<td><?php echo $this->Formatacao->moeda($doacao['Doacao']['valor']) ?></td>
							<td class="desktop"><?php echo $doacao['Doador']['nome'] . ' ' . $doacao['Doador']['sobrenome'] ?></td>
							<td class="desktop"><?php echo ($doacao['Doacao']['periodo_id'] == 1) ? 'Mensal' : 'Única' ?></td>
							<td class="desktop"><?php echo ucwords(mb_strtolower($doacao['Pagamento']['FormasPagamento']['nome'])) ?></td>
							<td>
								<?php 
								if ($doacao['Doacao']['periodo_id'] == 1) {
									echo $this->Html->link(
										$this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o']) . ' Editar',
										['action' => 'alterar', $doacao['Doacao']['id']],
										['class' => 'button radius tiny info', 'style' => 'margin-bottom:0', 'escape' => false]
									);
								}
								?>
								<?php 
								echo $this->Html->link(
								$this->Html->tag('i', '', array('class' => 'fa fa-plus-square')) . ' Painel',
									array('action' => 'painel', $doacao['Doacao']['id']),
									array(
										'class' => 'button tiny radius info',
										'style' => 'margin-bottom:0',
										'escape' => false
								)); 
								?>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</article>

<?php
	echo $this->Html->scriptBlock(
		"$('table.responsive.nowrap').DataTable({
			responsive: {
				details: {
					type: 'inline',
					target: 'tr'
				}
			},
			columnDefs: [{
				className: 'control',
				orderable: false,
				targets: 0
			}],
			order: [2, 'asc'],
			language: {
				processing:     'processando dados...',
				search:         '',
				lengthMenu:     'Itens por p&aacute;gina: _MENU_',
				info:           'Exibindo itens de _START_ a _END_, num total de _TOTAL_ itens',
				infoEmpty:      'Não há itens para exibir',
				infoFiltered:   '(filtrado de _MAX_ itens no total)',
				infoPostFix:    '',
				loadingRecords: 'carregando dados...',
				zeroRecords:    'Não há itens para exibir',
				paginate: {
					first:      'Primeira',
					previous:   'Anterior',
					next:       'Seguinte',
					last:       '&uacute;ltima'
				},
				aria: {
					sortAscending:  ': habilite para classificar a coluna em ordem crescente',
					sortDescending: ': habilite para classificar a coluna em ordem decrescente'
				}
			}
		});

		$('.dataTables_filter input').attr('placeholder', 'Buscar');",
		array('inline' => false));
?>