<?php echo $this->Session->flash('mensagem'); ?>
<?php if ($this->Session->check('Doacao.doador') && $this->Session->read('Doacao.doador') === 'PJ'): ?>
	<div data-alert class="alert-box alert">
	  <div class="row">
		<div class="small-12 columns">
			<?php echo $mensagem['Mensagem']['descricao'] ?>
			<style>
				.alert-box a {
					color:gold;
				}
			</style>
		</div>
	  </div>
	  <a href="#" class="close">&times;</a>
	</div>
<?php endif ?>
<section role="forms">
	<div id="cadastro">
		<div class="row">
			<div class="small-7 medium-3 small-centered columns">
				<center>
					<ul style="display:inline-block" class="sub-nav donation-steps">
						<li><a disabled href="#" class="button round active">1</a></li>
						<li><a disabled href="#" class="button round">2</a></li>
					</ul>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="small-12 text-center">
				<p> <?= __('Para seguir com sua doação, preencha seus dados cadastrais e financeiros') ?></p>
			</div>
			<?php echo $this->Session->flash() ?>
		</div>

		<?php if (($this->Session->check('Doacao.doador') && $this->Session->read('Doacao.doador') === 'PF') ||
			      ($this->Session->check('Auth.User') && $this->Session->read('Auth.User.Doador.pessoa_id') == 1)): ?>
		<div class="row collapse">
			<div class="small-12 small-centered columns">
				<?php echo $this->Form->create(null, array('data-abide', 'id' => 'formDoacao', 'inputDefaults' => array('error' => false))) ?>
					<?php echo $this->Form->hidden('AceiteDoacao.aceite_id', array('value' => $aceite['Aceite']['id'])) ?>
					<fieldset>
						<?php if (!$this->Session->check('Auth.User')) : ?>
							<div id="dadosPessoais">
								<div class="row small-collapse medium-uncollapse">
									<div class="small-12 medium-6 columns">
										<label><?= __('País onde reside')?>:</label>
										<select required name="data[Doador][nacionalidade]" id="residente">
											<option value="AF">Afeganistão (‫افغانستان‬‎)</option>
											<option value="ZA">África do Sul (South Africa)</option>
											<option value="AL">Albânia (Shqipëri)</option>
											<option value="DE">Alemanha (Deutschland)</option>
											<option value="AD">Andorra</option>
											<option value="AO">Angola</option>
											<option value="AI">Anguilla</option>
											<option value="AQ">Antártida (Antarctica)</option>
											<option value="AG">Antígua e Barbuda (Antigua and Barbuda)</option>
											<option value="SA">Arábia Saudita (‫المملكة العربية السعودية‬‎)</option>
											<option value="DZ">Argélia (‫الجزائر‬‎)</option>
											<option value="AR">Argentina</option>
											<option value="AM">Armênia (Հայաստան)</option>
											<option value="AW">Aruba</option>
											<option value="AU">Austrália (Australia)</option>
											<option value="AT">Áustria (Österreich)</option>
											<option value="AZ">Azerbaijão (Azərbaycan)</option>
											<option value="BS">Bahamas</option>
											<option value="BH">Bahrein (‫البحرين‬‎)</option>
											<option value="BD">Bangladesh (বাংলাদেশ)</option>
											<option value="BB">Barbados</option>
											<option value="BE">Bélgica (België)</option>
											<option value="BZ">Belize</option>
											<option value="BJ">Benin (Bénin)</option>
											<option value="BM">Bermudas (Bermuda)</option>
											<option value="BY">Bielorrússia (Беларусь)</option>
											<option value="BO">Bolívia (Bolivia)</option>
											<option value="BA">Bósnia e Herzegovina (Босна и Херцеговина)</option>
											<option value="BW">Botsuana (Botswana)</option>
											<option value="BR" selected>Brasil</option>
											<option value="BN">Brunei</option>
											<option value="BG">Bulgária (България)</option>
											<option value="BF">Burquina Faso (Burkina Faso)</option>
											<option value="BI">Burundi (Uburundi)</option>
											<option value="BT">Butão (འབྲུག)</option>
											<option value="CV">Cabo Verde (Kabu Verdi)</option>
											<option value="KH">Camboja (កម្ពុជា)</option>
											<option value="CA">Canadá (Canada)</option>
											<option value="QA">Catar (‫قطر‬‎)</option>
											<option value="KZ">Cazaquistão (Казахстан)</option>
											<option value="EA">Ceuta e Melilha (Ceuta y Melilla)</option>
											<option value="TD">Chade (Tchad)</option>
											<option value="CL">Chile</option>
											<option value="CN">China (中国)</option>
											<option value="CY">Chipre (Κύπρος)</option>
											<option value="VA">Cidade do Vaticano (Città del Vaticano)</option>
											<option value="SG">Cingapura (Singapore)</option>
											<option value="CO">Colômbia (Colombia)</option>
											<option value="KM">Comores (‫جزر القمر‬‎)</option>
											<option value="CG">Congo - Brazzaville (Congo-Brazzaville)</option>
											<option value="CD">Congo - Kinshasa (Jamhuri ya Kidemokrasia ya Kongo)</option>
											<option value="KP">Coreia do Norte (조선 민주주의 인민 공화국)</option>
											<option value="KR">Coreia do Sul (대한민국)</option>
											<option value="CI">Costa do Marfim (Côte d’Ivoire)</option>
											<option value="CR">Costa Rica</option>
											<option value="HR">Croácia (Hrvatska)</option>
											<option value="CU">Cuba</option>
											<option value="CW">Curaçao</option>
											<option value="DG">Diego Garcia</option>
											<option value="DK">Dinamarca (Danmark)</option>
											<option value="DJ">Djibuti (Djibouti)</option>
											<option value="DM">Dominica</option>
											<option value="EG">Egito (‫مصر‬‎)</option>
											<option value="SV">El Salvador</option>
											<option value="AE">Emirados Árabes Unidos (‫الإمارات العربية المتحدة‬‎)</option>
											<option value="EC">Equador (Ecuador)</option>
											<option value="ER">Eritreia (Eritrea)</option>
											<option value="SK">Eslováquia (Slovensko)</option>
											<option value="SI">Eslovênia (Slovenija)</option>
											<option value="ES">Espanha (España)</option>
											<option value="US">Estados Unidos (United States)</option>
											<option value="EE">Estônia (Eesti)</option>
											<option value="ET">Etiópia (Ethiopia)</option>
											<option value="FJ">Fiji</option>
											<option value="PH">Filipinas (Philippines)</option>
											<option value="FI">Finlândia (Suomi)</option>
											<option value="FR">França (France)</option>
											<option value="GA">Gabão (Gabon)</option>
											<option value="GM">Gâmbia (Gambia)</option>
											<option value="GH">Gana (Gaana)</option>
											<option value="GE">Geórgia (საქართველო)</option>
											<option value="GS">Geórgia do Sul e Ilhas Sandwich do Sul (South Georgia &amp; South Sandwich Islands)</option>
											<option value="GI">Gibraltar</option>
											<option value="GD">Granada (Grenada)</option>
											<option value="GR">Grécia (Ελλάδα)</option>
											<option value="GL">Groênlandia (Kalaallit Nunaat)</option>
											<option value="GP">Guadalupe (Guadeloupe)</option>
											<option value="GU">Guam</option>
											<option value="GT">Guatemala</option>
											<option value="GG">Guernsey</option>
											<option value="GY">Guiana (Guyana)</option>
											<option value="GF">Guiana Francesa (Guyane française)</option>
											<option value="GN">Guiné (Guinée)</option>
											<option value="GW">Guiné Bissau</option>
											<option value="GQ">Guiné Equatorial (Guinea Ecuatorial)</option>
											<option value="HT">Haiti</option>
											<option value="NL">Holanda (Nederland)</option>
											<option value="HN">Honduras</option>
											<option value="HK">Hong Kong (香港)</option>
											<option value="HU">Hungria (Magyarország)</option>
											<option value="YE">Iêmen (‫اليمن‬‎)</option>
											<option value="BV">Ilha Bouvet (Bouvet Island)</option>
											<option value="AC">Ilha de Ascensão (Ascension Island)</option>
											<option value="CP">Ilha de Clipperton (Clipperton Island)</option>
											<option value="IM">Ilha de Man (Isle of Man)</option>
											<option value="HM">Ilha Heard e Ilhas McDonald (Heard &amp; McDonald Islands)</option>
											<option value="NF">Ilha Norfolk (Norfolk Island)</option>
											<option value="AX">Ilhas Åland (Åland)</option>
											<option value="KY">Ilhas Caiman (Cayman Islands)</option>
											<option value="IC">Ilhas Canárias (islas Canarias)</option>
											<option value="CC">Ilhas Coco (Kepulauan Cocos (Keeling))</option>
											<option value="CK">Ilhas Cook (Cook Islands)</option>
											<option value="UM">Ilhas Distantes dos EUA (U.S. Outlying Islands)</option>
											<option value="FO">Ilhas Faroe (Føroyar)</option>
											<option value="FK">Ilhas Malvinas (Falkland Islands (Islas Malvinas))</option>
											<option value="MP">Ilhas Marianas do Norte (Northern Mariana Islands)</option>
											<option value="MH">Ilhas Marshall (Marshall Islands)</option>
											<option value="CX">Ilhas Natal (Christmas Island)</option>
											<option value="PN">Ilhas Pitcairn (Pitcairn Islands)</option>
											<option value="SB">Ilhas Salomão (Solomon Islands)</option>
											<option value="TC">Ilhas Turks e Caicos (Turks and Caicos Islands)</option>
											<option value="VG">Ilhas Virgens Britânicas (British Virgin Islands)</option>
											<option value="VI">Ilhas Virgens dos EUA (U.S. Virgin Islands)</option>
											<option value="IN">Índia (भारत)</option>
											<option value="ID">Indonésia (Indonesia)</option>
											<option value="IR">Irã (‫ایران‬‎)</option>
											<option value="IQ">Iraque (‫العراق‬‎)</option>
											<option value="IE">Irlanda (Ireland)</option>
											<option value="IS">Islândia (Ísland)</option>
											<option value="IL">Israel (‫ישראל‬‎)</option>
											<option value="IT">Itália (Italia)</option>
											<option value="JM">Jamaica</option>
											<option value="JP">Japão (日本)</option>
											<option value="JE">Jersey</option>
											<option value="JO">Jordânia (‫الأردن‬‎)</option>
											<option value="XK">Kosovo (Kosovë)</option>
											<option value="KW">Kuwait (‫الكويت‬‎)</option>
											<option value="LA">Laos (ລາວ)</option>
											<option value="LS">Lesoto (Lesotho)</option>
											<option value="LV">Letônia (Latvija)</option>
											<option value="LB">Líbano (‫لبنان‬‎)</option>
											<option value="LR">Libéria (Liberia)</option>
											<option value="LY">Líbia (‫ليبيا‬‎)</option>
											<option value="LI">Liechtenstein</option>
											<option value="LT">Lituânia (Lietuva)</option>
											<option value="LU">Luxemburgo (Luxembourg)</option>
											<option value="MO">Macau (澳門)</option>
											<option value="MK">Macedônia (Македонија)</option>
											<option value="MG">Madagascar (Madagasikara)</option>
											<option value="MY">Malásia (Malaysia)</option>
											<option value="MW">Malawi</option>
											<option value="MV">Maldivas (Maldives)</option>
											<option value="ML">Mali</option>
											<option value="MT">Malta</option>
											<option value="MA">Marrocos (‫المغرب‬‎)</option>
											<option value="MQ">Martinica (Martinique)</option>
											<option value="MU">Maurício (Moris)</option>
											<option value="MR">Mauritânia (‫موريتانيا‬‎)</option>
											<option value="YT">Mayotte</option>
											<option value="MX">México</option>
											<option value="MM">Mianmar (Birmânia) (မြန်မာ)</option>
											<option value="FM">Micronésia (Micronesia)</option>
											<option value="MZ">Moçambique</option>
											<option value="MD">Moldávia (Republica Moldova)</option>
											<option value="MC">Mônaco (Monaco)</option>
											<option value="MN">Mongólia (Монгол)</option>
											<option value="ME">Montenegro (Crna Gora)</option>
											<option value="MS">Montserrat</option>
											<option value="NA">Namíbia (Namibië)</option>
											<option value="NR">Nauru</option>
											<option value="NP">Nepal (नेपाल)</option>
											<option value="NI">Nicarágua (Nicaragua)</option>
											<option value="NE">Níger (Nijar)</option>
											<option value="NG">Nigéria (Nigeria)</option>
											<option value="NU">Niue</option>
											<option value="NO">Noruega (Norge)</option>
											<option value="NC">Nova Caledônia (Nouvelle-Calédonie)</option>
											<option value="NZ">Nova Zelândia (New Zealand)</option>
											<option value="OM">Omã (‫عُمان‬‎)</option>
											<option value="BQ">Países Baixos Caribenhos (Caribbean Netherlands)</option>
											<option value="PW">Palau</option>
											<option value="PS">Palestina (‫فلسطين‬‎)</option>
											<option value="PA">Panamá</option>
											<option value="PG">Papua-Nova Guiné (Papua New Guinea)</option>
											<option value="PK">Paquistão (‫پاکستان‬‎)</option>
											<option value="PY">Paraguai (Paraguay)</option>
											<option value="PE">Peru (Perú)</option>
											<option value="PF">Polinésia Francesa (Polynésie française)</option>
											<option value="PL">Polônia (Polska)</option>
											<option value="PR">Porto Rico (Puerto Rico)</option>
											<option value="PT">Portugal</option>
											<option value="KE">Quênia (Kenya)</option>
											<option value="KG">Quirguistão (Кыргызстан)</option>
											<option value="KI">Quiribati (Kiribati)</option>
											<option value="GB">Reino Unido (United Kingdom)</option>
											<option value="CF">República Centro-Africana (République centrafricaine)</option>
											<option value="DO">República Dominicana</option>
											<option value="CM">República dos Camarões (Cameroun)</option>
											<option value="CZ">República Tcheca (Česká republika)</option>
											<option value="RE">Reunião (La Réunion)</option>
											<option value="RO">Romênia (România)</option>
											<option value="RW">Ruanda (Rwanda)</option>
											<option value="RU">Rússia (Россия)</option>
											<option value="EH">Saara Ocidental (‫الصحراء الغربية‬‎)</option>
											<option value="PM">Saint Pierre e Miquelon (Saint-Pierre-et-Miquelon)</option>
											<option value="WS">Samoa</option>
											<option value="AS">Samoa Americana (American Samoa)</option>
											<option value="SM">San Marino</option>
											<option value="SH">Santa Helena (Saint Helena)</option>
											<option value="LC">Santa Lúcia (Saint Lucia)</option>
											<option value="BL">São Bartolomeu (Saint-Barthélemy)</option>
											<option value="KN">São Cristovão e Nevis (Saint Kitts and Nevis)</option>
											<option value="MF">São Martinho (Saint-Martin (partie française))</option>
											<option value="ST">São Tomé e Príncipe</option>
											<option value="VC">São Vicente e Granadinas (St. Vincent &amp; Grenadines)</option>
											<option value="SN">Senegal (Sénégal)</option>
											<option value="SL">Serra Leoa (Sierra Leone)</option>
											<option value="RS">Sérvia (Србија)</option>
											<option value="SC">Seychelles</option>
											<option value="SX">Sint Maarten</option>
											<option value="SY">Síria (‫سوريا‬‎)</option>
											<option value="SO">Somália (Soomaaliya)</option>
											<option value="LK">Sri Lanka (ශ්‍රී ලංකාව)</option>
											<option value="SZ">Suazilândia (Swaziland)</option>
											<option value="SD">Sudão (‫السودان‬‎)</option>
											<option value="SS">Sudão do Sul (‫جنوب السودان‬‎)</option>
											<option value="SE">Suécia (Sverige)</option>
											<option value="CH">Suíça (Schweiz)</option>
											<option value="SR">Suriname</option>
											<option value="SJ">Svalbard e Jan Mayen (Svalbard og Jan Mayen)</option>
											<option value="TJ">Tadjiquistão (Tajikistan)</option>
											<option value="TH">Tailândia (ไทย)</option>
											<option value="TW">Taiwan (台灣)</option>
											<option value="TZ">Tanzânia (Tanzania)</option>
											<option value="IO">Território Britânico do Oceano Índico (British Indian Ocean Territory)</option>
											<option value="TF">Territórios Franceses do Sul (Terres australes françaises)</option>
											<option value="TL">Timor-Leste</option>
											<option value="TG">Togo</option>
											<option value="TK">Tokelau</option>
											<option value="TO">Tonga</option>
											<option value="TT">Trinidad e Tobago (Trinidad and Tobago)</option>
											<option value="TA">Tristão da Cunha (Tristan da Cunha)</option>
											<option value="TN">Tunísia (‫تونس‬‎)</option>
											<option value="TM">Turcomenistão (Turkmenistan)</option>
											<option value="TR">Turquia (Türkiye)</option>
											<option value="TV">Tuvalu</option>
											<option value="UA">Ucrânia (Україна)</option>
											<option value="UG">Uganda</option>
											<option value="UY">Uruguai (Uruguay)</option>
											<option value="UZ">Uzbequistão (Oʻzbekiston)</option>
											<option value="VU">Vanuatu</option>
											<option value="VE">Venezuela</option>
											<option value="VN">Vietnã (Việt Nam)</option>
											<option value="WF">Wallis e Futuna (Wallis and Futuna)</option>
											<option value="ZM">Zâmbia (Zambia)</option>
											<option value="ZW">Zimbábue (Zimbabwe)</option>
										</select>
										<small class="error"><?= __('Selecione um país')?></small>
									</div>

									<div class="small-12 medium-6 columns">
										<label for="conta-brasil"><?= __('Possui conta no Brasil')?>?</label>
										<?php if ( isset($this->data['Doador']['conta_brasil']) && !$this->data['Doador']['conta_brasil'] ): ?>
											<?php echo $this->Form->input('Doador.conta_brasil', array('type' => 'checkbox', 'checked' => false)) ?>
										<?php else: ?>
											<?php echo $this->Form->input('Doador.conta_brasil', array('type' => 'checkbox', 'checked' => true)) ?>
										<?php endif ?>
									</div>
								</div>

								<div class="row small-collapse medium-uncollapse">
									<div class="small-12 columns">
										<h3><?= __('Dados Cadastrais')?></h3>
									</div>

									<div class="small-12 medium-6 columns">
										<?php echo $this->Form->input('Doador.nome', array('placeholder' => __('Nome'), 'required' => true)) ?>
										<small class="error"><?= __('Preencha seu Nome')?></small>
									</div>

									<div class="small-12 medium-6 columns">
										<?php echo $this->Form->input('Doador.sobrenome', array('placeholder' => __('Sobrenome'), 'required' => true)) ?>
										<small class="error"><?= __('Preencha seu Sobrenome')?></small>
									</div>
								</div>

								<div class="row small-collapse medium-uncollapse">
									<div class="small-12 columns">
										<?php echo $this->Form->hidden('DoadorEmail.0.email_id', array('value' => 1)) ?>
										<?php echo $this->Form->input('DoadorEmail.0.email', array('placeholder' => __('Seu email'), 'type' => 'email', 'required' => true)) ?>
										<small class="error"><?= __('Preencha com um email válido')?></small>
									</div>
									<div class="small-12 columns">
										<?php //echo $this->Form->input('DoadorEmail.email2', array('placeholder' => 'Confirme seu email', 'type' => 'email', 'required' => true)) ?>
									</div>
								</div>

								<div class="row small-collapse medium-uncollapse">
									<div class="small-12 medium-6 columns">
										<?php echo $this->Form->input('User.password', array('placeholder' => __('Senha'), 'type' => 'password', 'required' => true, 'pattern' => 'passwords', 'minlength' =>'6')) ?>
										<small class="error"><?= __('Sua senha deve conter ao menos 6 caracteres, sem espaço entre eles')?>.</small>
									</div>
									<div class="small-12 medium-6 columns">
										<?php echo $this->Form->input('User.temppassword', array('placeholder' => __('Confirme sua senha'), 'type' => 'password', 'required' => true, 'data-equalto' => 'UserPassword', 'pattern' => 'passwords', 'minlength' =>'6')) ?>
										<small class="error"><?= __('As senhas digitadas devem ser iguais')?></small>
									</div>
								</div>

								<div class="row small-collapse medium-uncollapse cpf-place">
									<div class="small-12 medium-6 columns">
										<?php echo $this->Form->hidden('DoadorTelefone.0.telefone_id', array('value' => 1)) ?>
										<?php echo $this->Form->input('DoadorTelefone.0.numero', array('placeholder' => __('Telefone Residencial'), 'type' => 'tel', 'required' => true, 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.') )) ?>
										<small class="error"><?= __('Formato Inválido')?></small>

									</div>

									<div class="small-12 medium-6 columns">
										<?php echo $this->Form->input('Doador.cpf_cnpj', array('id' => 'doadorCpf', 'placeholder' => 'CPF', 'type' => 'text', 'required' => true, 'datcnpja-abide-validator' => 'testaCPF')) ?>
										<small class="error"><?= __('Formato Inválido')?></small>
									</div>
								</div>
							</div>
						<?php endif; ?>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 columns">
								<h3><?= __('Sua Doação')?></h3>
							</div>
							<div class="small-12 columns">
								<table id="tabela-sua-doacao" width="100%" align="center" class="text-center">
									<thead>
										<tr>
											<th width="50%"><?= __('Valor')?></th>
											<th width="50%"><?= __('Frequência da Doação')?></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<div class="small-12">
													<?php echo $this->Form->input('Doacao.valor', array('placeholder' => __('Valor da doação'), 'type' => 'text', 'required' => true, 'value' => $valor, 'minlength' => '6','maxlength' => '15', 'data-abide-validator' => 'testaValorDoado')) ?>
													<small class="error"></small>
												</div>
											</td>
											<td>
												<?php echo $this->Form->input('tipo',
													array('options' => array(
														'U' => __('Única'),
														'M' => __('Mensal')
													), 'id' => 'tipo-doacao', 'required' => true, 'default' => $tipo)) ?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="row">
							<div class="small-12 columns">
								<h3><?= __('Forma de Pagamento')?></h3>
							</div>
							<div class="row collapse" id="formasPagamento">
								<div class="small-12 medium-4 columns">
									<input style="float:left;" required type="radio" name="formaPagamento" id="credito" value="credito" checked>
									<label style="float:left;" for="credito">
										<i class="sprite-credito"></i>&nbsp;<?= __('Cartão de Credito')?>
									</label>
								</div>
								<div class="clearfix show-for-small-only">&nbsp;</div>
								<div class="small-12 medium-5 columns">
									<input style="float:left;" required type="radio" name="formaPagamento" id="debito" value="debito">
									<label style="float:left;" for="debito">
										<i class="sprite-debito"></i>&nbsp;<?= __('Débito em conta')?>
									</label>
								</div>
								<div class="clearfix show-for-small-only">&nbsp;</div>
								<div class="small-12 medium-3 columns">
									<input style="float:left;" required type="radio" name="formaPagamento" id="boleto" value="boleto">
									<label style="float:left;" for="boleto">
										<i class="sprite-boleto"></i>&nbsp;<?= __('Boleto')?>
									</label>
								</div>
								<div class="clearfix show-for-small-only">&nbsp;</div>
							</div>
						</div>

						<article class="formaPagamento" id="credito">
							<div class="row small-collapse medium-uncollapse">
								<div class="small-12 columns">
									<ul class="small-block-grid-2 medium-block-grid-4">
										<?php foreach ($pagamentoCredito as $credito): ?>
											<li>
												<input name="formas_pagamento_id" value="<?php echo $credito['FormasPagamento']['id'] ?>" type="radio" data-flag="<?php echo strtolower($credito['FormasPagamento']['nome']) ?>" class="flag flag-<?php echo strtolower($credito['FormasPagamento']['nome']) ?>" style="opacity: 0.5">
											</li>
										<?php endforeach ?>
									</ul>
								</div>
							</div>

							<div class="row small-collapse medium-uncollapse">
								<div class="small-12 medium-6 columns end">
									<?php echo $this->Form->input('Pagamento.cartaoCreditoNome', array('placeholder' => __('Nome como está no cartão'), 'pattern' => 'alphaSpaces', 'required' => true)) ?>
									<small class="error"><?= __('Campo Obrigatório') ?></small>
								</div>
							</div>
							<div class="row small-collapse medium-uncollapse">
								<div class="small-9 medium-6 columns">
									<?php echo $this->Form->input('Pagamento.cartaoCreditoNum', array('placeholder' => __('Número do cartão'), 'id' => 'cartaoCreditoNum', 'pattern' => 'number', 'required' => true, 'maxlength' => 16)) ?>
									<small class="error"><?= __('Campo Obrigatório') ?></small>
								</div>
								<div class="small-2 small-push-1 medium-push-0 medium-4 columns">
									<?php echo $this->Form->input('Pagamento.cartaoCreditoCod', array('placeholder' => __('Código de segurança'), 'id' => 'cartaoCreditoCod', 'required' => true, 'pattern' => 'cvv')) ?>
									<small class="error"><?= __('Campo Obrigatório') ?></small>
								</div>
								<div class="show-for-medium-up medium-2 columns"><i class="sprite-cod-seguranca"></i></div>
							</div>

							<div class="row small-collapse medium-uncollapse">
								<div class="small-12 medium-6 columns">
									<label for="validade-mes"><?= __('Validade')?></label>
									<?php echo $this->Form->select('Pagamento.validade_mes', $this->Geral->getMeses(), array('id' => 'validade-mes', 'required' => true, 'empty' => false)) ?>
									<small class="error"><?= __('Campo Obrigatório') ?></small>
								</div>
								<div class="small-12 medium-6 columns">
									<label for="validade-ano"><?= __('.')?></label>
									<?php echo $this->Form->select('Pagamento.validade_ano', $this->Geral->getAnos(), array('id' => 'validade-ano', 'required' => true, 'empty' => false)) ?>
									<small class="error"><?= __('Campo Obrigatório') ?></small>
								</div>
							</div>
						</article>

						<article class="formaPagamento hide" id="debito">
							<div class="row small-collapse medium-uncollapse">
								<div class="small-12 columns">
									<ul class="small-block-grid-2 medium-block-grid-4">
										<?php foreach ($pagamentoDebito as $debito): ?>
											<li>
												<input name="formas_pagamento_id" value="<?php echo $debito['FormasPagamento']['id'] ?>" type="radio" data-flag="<?php echo strtolower(str_replace('DÉBITO ', '', $debito['FormasPagamento']['nome'])) ?>" class="flag flag-<?php echo strtolower(str_replace('DÉBITO ', '', $debito['FormasPagamento']['nome'])) ?>" style="opacity: 0.5">
											</li>
										<?php endforeach ?>
									</ul>
								</div>
							</div>
							<div class="row small-collapse medium-uncollapse">
								<div class="small-12 medium-6 columns end">
									<?php echo $this->Form->select('Pagamento.dia_debito_id', $dias, array('id' => 'data-debito', 'empty' => __('Melhor dia para Débito'))) ?>
									<small class="error"><?= __('Campo Obrigatório') ?></small>
								</div>
							</div>
							<div class="row small-collapse medium-uncollapse">
								<div class="small-12 medium-6 columns">
									<!-- <input type="text" placeholder="<?= __('Agência') ?> " id="debitoAgencia" name="debitoAgencia"> -->
									<?php echo $this->Form->input('Pagamento.debitoAgencia', array('placeholder' => __('Agência'), 'id' => 'debitoAgencia')) ?>
									<small class="error"><?= __('Campo Obrigatório') ?></small>
								</div>
								<div class="small-12 medium-6 columns">
									<!-- <input type="text" placeholder="<?= __('Conta') ?> " id="debitoConta" name="debitoConta"> -->
									<?php echo $this->Form->input('Pagamento.debitoConta', array('placeholder' => __('Conta'), 'id' => 'debitoConta')) ?>
									<small class="error"><?= __('Campo Obrigatório') ?></small>
								</div>
							</div>
						</article>

						<article class="formaPagamento hide" id="boleto">
							<div class="row">
								<div class="small-12 columns">
									<input name="formas_pagamento_id" type="radio" class="hide" value="1">
									<label for="data-vencimento"><?= __('Vencimento')?></label>
									<?php echo $this->Form->input('Pagamento.dataVencimento', array('class' => 'datepicker', 'id' => 'data-vencimento', 'placeholder' => 'Data')) ?>
									<small class="error"><?= __('Campo Obrigatório') ?></small>
								</div>
							</div>
						</article>

						<?= $this->Form->input('Pagamento.formas_pagamento_id', array('type' => 'hidden', 'id' => 'formasPagamentoId')) ?>


						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-6 column text-center">
								<?php echo $this->Form->input('Doador.termo', array('type' => 'checkbox', 'id' => 'terms', 'required' => true)) ?>
								<label for="terms"><small style="text-transform: initial;"><?= __('Autorizo  o Instituto a efetivar minhas doações  junto a instituição financeira, seguindo as opções por mim assinaladas acima. Li e aceito os termos do programa')?>. <a href="#" data-reveal-id="modal-aceite"><?= __('Ler termos do programa')?></a>.</small></label>
								<small class="error"><?= __('Você precisa aceitar os termos e condições para prosseguir')?>.</small>
							</div>

							<div class="small-12 medium-6 column text-center">
								<?php echo $this->Form->input('Doador.recebe_email', array('type' => 'checkbox', 'id' => 'news', 'checked' => true)) ?>
								<label for="news"><small style="text-transform: initial;"><?= __('Aceito receber informações sobre o Instituto Ayrton Senna por e-mail')?>.</small></label>
							</div>
						</div>

						<div class="clearfix">&nbsp;</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 columns text-center">
								<button disabled type="submit" class="button disabled" id="enviar_doacao"><?= __('Concluir doação')?>&nbsp;<i class="sprite-check"></i></button>
							</div>
						</div>
						<div class="row small-collapse medium-uncollapse hide">
							<div class="small-12 columns">
								<button class="button expand success" id="gerar_boleto" type="submit"><?= __('Emitir Boleto')?> <i class="sprite sprite-boleto-branco" style="vertical-align:sub"></i></button>
							</div>
						</div>
						<div class="row small-collapse medium-uncollapse hide">
							<div class="small-12 columns">
								<button class="button expand success" type="submit" id="checkout_paypal"><?= __('Pagar com')?> <i class="sprite sprite-paypal" style="vertical-align:sub"></i></button>
							</div>
						</div>
					</fieldset>
				<?php echo $this->Form->end() ?>
			</div>
		</div>
		<?php elseif(($this->Session->check('Doacao.doador') && $this->Session->read('Doacao.doador') === 'PJ') || ($this->Session->check('Auth.User') && $this->Session->read('Auth.User.Doador.pessoa_id') == 2)): ?>
		<div class="row collapse">
			<div class="small-12 columns">
				<?php echo $this->Form->create('Doador', ['data-abide', 'id' => 'formDoacao', 'inputDefaults' => ['error' => false]]) ?>
				<?php echo $this->Form->hidden('AceiteDoacao.aceite_id', array('value' => $aceite['Aceite']['id'])) ?>
				<fieldset>
					<?php if (!$this->Session->check('Auth.User')): ?>
						<div class="row">
							<div class="small-12 medium-6 columns">
								<label for="conta-brasil"><?= __('A empresa possui conta no Brasil')?>?</label>
								<?php if ( isset($this->data['Doador']['conta_brasil']) && !$this->data['Doador']['conta_brasil'] ): ?>
									<?php echo $this->Form->input('Doador.conta_brasil', array('type' => 'checkbox', 'checked' => false)) ?>
								<?php else: ?>
									<?php echo $this->Form->input('Doador.conta_brasil', array('type' => 'checkbox', 'checked' => true)) ?>
								<?php endif ?>
							</div>
							<div class="medium-12 column">
								<h3><?php echo __('Dados Profissionais'); ?></h3>
							</div>
							<div class="medium-12 columns">
								<?php echo $this->Form->input('nome', ['placeholder' => __('Nome Fantasia'), 'required' => false]); ?>
							</div>

							<div class="medium-6 columns">
								<?php echo $this->Form->input('sobrenome', ['placeholder' => __('Razão Social'), 'required']); ?>
								<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
							</div>
							<div class="medium-6 columns">
								<?php echo $this->Form->input('cpf_cnpj', array('placeholder' => __('CNPJ'), 'data-abide-validator' => 'testaCNPJ', 'required' => true, 'maxlength' => 18)) ?>
								<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
							</div>

							<div class="medium-6 columns">
								<?php echo $this->Form->input('DpjComplemento.nome', ['placeholder' => __('Nome do Contato'), 'required']); ?>
								<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
							</div>
							<div class="medium-6 columns">
								<?php echo $this->Form->input('DpjComplemento.cargo', ['placeholder' => __('Cargo'), 'required']); ?>
								<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
							</div>

							<div class="medium-6 columns">
								<?php echo $this->Form->select('DpjComplemento.sexo', ['1' => __('Masculino'), '2' => __('Feminino')], ['empty' => __('Sexo')], ['required']); ?>
								<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
							</div>
							<div class="medium-6 columns">
								<?php echo $this->Form->hidden('DoadorTelefone.0.telefone_id', array('value' => 1)) ?>
								<?php echo $this->Form->input('DoadorTelefone.0.numero', array('placeholder' => __('Telefone Principal'), 'type' => 'tel', 'required' => true, 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.') )) ?>
							</div>

							<div class="medium-12 columns">
								<?php echo $this->Form->hidden('DoadorEmail.0.email_id', ['value' => '1']); ?>
								<?php echo $this->Form->input('DoadorEmail.0.email', ['placeholder' => __('E-mail para contato e login'), 'type' => 'email', 'required']); ?>
								<small class="error"><?= __('Preencha com um email válido')?></small>
							</div>

							<div class="small-12 medium-6 columns">
								<?php echo $this->Form->input('User.password', array('placeholder' => __('Senha'), 'type' => 'password', 'required' => true, 'pattern' => 'passwords', 'minlength' =>'6')) ?>
								<small class="error"><?= __('Sua senha deve conter ao menos 6 caracteres, sem espaço entre eles')?>.</small>
							</div>
							<div class="small-12 medium-6 columns">
								<?php echo $this->Form->input('User.temppassword', array('placeholder' => __('Confirme sua senha'), 'type' => 'password', 'required' => true, 'data-equalto' => 'UserPassword', 'pattern' => 'passwords', 'minlength' =>'6')) ?>
								<small class="error"><?= __('As senhas digitadas devem ser iguais')?></small>
							</div>

							<div class="small-12 columns">
								<?php echo $this->Form->input('DpjComplemento.representante', ['placeholder' => __('Nome do Representante legal'), 'required']); ?>
								<small class="error"><?= __('Campo Obrigatório')?></small>
							</div>
							<div class="small-12 columns">
								<?php echo $this->Form->hidden('DoadorEmail.1.email_id', ['value' => 2]); ?>
								<?php echo $this->Form->input('DoadorEmail.1.email', ['placeholder' => __('Email do Representante Legal'), 'type' => 'email', 'required']); ?>
								<small class="error"><?= __('Preencha com um email válido')?></small>
							</div>

							<div class="small-6 columns">
								<?php echo $this->Form->input('DpjComplemento.faixa_id', ['empty' => __('Faixa de faturamento anual')]); ?>
							</div>
							<div class="small-6 columns">
								<?php echo $this->Form->input('DpjComplemento.natureza_id', ['empty' => __('Natureza Jurídica')]); ?>
							</div>
						</div>
					<?php endif ?>

					<div class="row small-collapse medium-uncollapse">
						<div class="small-12 columns">
							<h3><?= __('Sua Doação')?></h3>
						</div>
						<div class="small-12 columns">
							<table id="tabela-sua-doacao" width="100%" align="center" class="text-center">
								<thead>
									<tr>
										<th width="50%"><?= __('Valor')?></th>
										<th width="50%"><?= __('Frequência da Doação')?></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<div class="small-12">
												<?php echo $this->Form->input('Doacao.valor', array('placeholder' => __('Valor da doação'), 'type' => 'text', 'required' => true, 'value' => $valor, 'minlength' => '6','maxlength' => '15', 'data-abide-validator' => 'testaValorDoado')) ?>
												<small class="error"></small>
											</div>
										</td>
										<td>
											<?php echo $this->Form->input('Doacao.tipo',
												array('options' => array(
													'U' => __('Única'),
													'M' => __('Mensal')
												), 'id' => 'tipo-doacao', 'required' => true, 'default' => $tipo)) ?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="row">
						<div class="small-12 columns">
							<h3><?= __('Forma de Pagamento')?></h3>
						</div>
						<div class="row collapse" id="formasPagamento">
							<div class="small-12 medium-4 columns">
								<input style="float:left;" required type="radio" name="formaPagamento" id="credito" value="credito" checked>
								<label style="float:left;" for="credito">
									<i class="sprite-credito"></i>&nbsp;<?= __('Cartão de Crédito<br/> Corporativo')?>
								</label>
							</div>
							<div class="clearfix show-for-small-only">&nbsp;</div>
							<div class="small-12 medium-5 columns">
								<input style="float:left;" required type="radio" name="formaPagamento" id="debito" value="debito">
								<label style="float:left;" for="debito">
									<i class="sprite-debito"></i>&nbsp;<?= __('Débito em Conta<br/> Corporativo')?>
								</label>
							</div>
							<div class="clearfix show-for-small-only">&nbsp;</div>
							<div class="small-12 medium-3 columns">
								<input style="float:left;" required type="radio" name="formaPagamento" id="boleto" value="boleto">
								<label style="float:left;" for="boleto">
									<i class="sprite-boleto"></i>&nbsp;<?= __('Boleto')?>
								</label>
							</div>
							<div class="clearfix show-for-small-only">&nbsp;</div>
						</div>
					</div>

					<article class="formaPagamento" id="credito">
						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 columns">
								<ul class="small-block-grid-2 medium-block-grid-4">
									<?php foreach ($pagamentoCredito as $credito): ?>
										<li>
											<input name="formas_pagamento_id" value="<?php echo $credito['FormasPagamento']['id'] ?>" type="radio" data-flag="<?php echo strtolower($credito['FormasPagamento']['nome']) ?>" class="flag flag-<?php echo strtolower($credito['FormasPagamento']['nome']) ?>" style="opacity: 0.5">
										</li>
									<?php endforeach ?>
								</ul>
							</div>
						</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-6 columns end">
								<?php echo $this->Form->input('Pagamento.cartaoCreditoNome', array('placeholder' => __('Nome como está no cartão'), 'pattern' => 'alphaSpaces', 'required' => true)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
						<div class="row small-collapse medium-uncollapse">
							<div class="small-9 medium-6 columns">
								<?php echo $this->Form->input('Pagamento.cartaoCreditoNum', array('placeholder' => __('Número do cartão'), 'id' => 'cartaoCreditoNum', 'pattern' => 'number', 'required' => true, 'maxlength' => 16)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-2 small-push-1 medium-push-0 medium-4 columns">
								<?php echo $this->Form->input('Pagamento.cartaoCreditoCod', array('placeholder' => __('Código de segurança'), 'id' => 'cartaoCreditoCod', 'required' => true, 'pattern' => 'cvv')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="show-for-medium-up medium-2 columns"><i class="sprite-cod-seguranca"></i></div>
						</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-6 columns">
								<label for="validade-mes"><?= __('Validade')?></label>
								<?php echo $this->Form->select('Pagamento.validade_mes', $this->Geral->getMeses(), array('id' => 'validade-mes', 'required' => true, 'empty' => false)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-6 columns">
								<label for="validade-ano"><?= __('.')?></label>
								<?php echo $this->Form->select('Pagamento.validade_ano', $this->Geral->getAnos(), array('id' => 'validade-ano', 'required' => true, 'empty' => false)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
					</article>

					<article class="formaPagamento hide" id="debito">
						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 columns">
								<ul class="small-block-grid-2 medium-block-grid-4">
									<?php foreach ($pagamentoDebito as $debito): ?>
										<li>
											<input name="formas_pagamento_id" value="<?php echo $debito['FormasPagamento']['id'] ?>" type="radio" data-flag="<?php echo strtolower(str_replace('DÉBITO ', '', $debito['FormasPagamento']['nome'])) ?>" class="flag flag-<?php echo strtolower(str_replace('DÉBITO ', '', $debito['FormasPagamento']['nome'])) ?>" style="opacity: 0.5">
										</li>
									<?php endforeach ?>
								</ul>
							</div>
						</div>
						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-6 columns end">
								<?php echo $this->Form->select('Pagamento.dia_debito_id', $dias, array('id' => 'data-debito', 'empty' => __('Melhor dia para Débito'))) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-6 columns">
								<!-- <input type="text" placeholder="<?= __('Agência') ?> " id="debitoAgencia" name="debitoAgencia"> -->
								<?php echo $this->Form->input('Pagamento.debitoAgencia', array('placeholder' => __('Agência'), 'id' => 'debitoAgencia')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-6 columns">
								<!-- <input type="text" placeholder="<?= __('Conta') ?> " id="debitoConta" name="debitoConta"> -->
								<?php echo $this->Form->input('Pagamento.debitoConta', array('placeholder' => __('Conta'), 'id' => 'debitoConta')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
					</article>

					<article class="formaPagamento hide" id="boleto">
						<div class="row">
							<div class="small-12 columns">
								<input name="formas_pagamento_id" type="radio" class="hide" value="1">
								<label for="data-vencimento"><?= __('Vencimento')?></label>
								<?php echo $this->Form->input('Pagamento.dataVencimento', array('class' => 'datepicker', 'id' => 'data-vencimento', 'placeholder' => 'Data')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
					</article>

					<?= $this->Form->input('Pagamento.formas_pagamento_id', array('type' => 'hidden', 'id' => 'formasPagamentoId')) ?>


					<div class="row small-collapse medium-uncollapse">
						<div class="small-12 medium-6 column text-center">
							<?php echo $this->Form->input('Doador.termo', array('type' => 'checkbox', 'id' => 'terms', 'required' => true)) ?>
							<label for="terms"><small style="text-transform: initial;"><?= __('Autorizo  o Instituto a efetivar minhas doações  junto a instituição financeira, seguindo as opções por mim assinaladas acima. Li e aceito os termos do programa')?>. <a href="#" data-reveal-id="modal-aceite"><?= __('Ler termos do programa')?></a>.</small></label>
							<small class="error"><?= __('Você precisa aceitar os termos e condições para prosseguir')?>.</small>
						</div>

						<div class="small-12 medium-6 column text-center">
							<?php echo $this->Form->input('Doador.recebe_email', array('type' => 'checkbox', 'id' => 'news', 'required' => true)) ?>
							<label for="news">
								<small style="text-transform: initial;">
									<?= __('Autorizo a divulgação do nome da minha empresa nos canais de comunicação selecionados pelo Instituto Ayrton Senna')?>.
									<a href="#" data-reveal-id="modal-autorizacao-empresa"><?= __('Clique e saiba mais')?></a>.
								</small>
							</label>
							<small class="error"><?= __('Você precisa aceitar os termos e condições para prosseguir')?>.</small>
						</div>
					</div>

					<div class="clearfix">&nbsp;</div>

					<div class="row small-collapse medium-uncollapse">
						<div class="small-12 columns text-center">
							<button disabled type="submit" class="button disabled" id="enviar_doacao"><?= __('Concluir doação')?>&nbsp;<i class="sprite-check"></i></button>
						</div>
					</div>
					<div class="row small-collapse medium-uncollapse hide">
						<div class="small-12 columns">
							<button class="button expand success" id="gerar_boleto" type="submit"><?= __('Emitir Boleto')?> <i class="sprite sprite-boleto-branco" style="vertical-align:sub"></i></button>
						</div>
					</div>
					<div class="row small-collapse medium-uncollapse hide">
						<div class="small-12 columns">
							<button class="button expand success" type="submit" id="checkout_paypal"><?= __('Pagar com')?> <i class="sprite sprite-paypal" style="vertical-align:sub"></i></button>
						</div>
					</div>
				</fieldset>
				<?php echo $this->Form->end($options = null); ?>
			</div>
		</div>
		<?php endif ?>
	</div>
</section>

<!-- Modal contato para aceite -->
<?php echo $this->element('modal-aceite'); ?>
<?php echo $this->element('modal-autorizacao-empresa'); ?>

<div id="loadingModal" class="reveal-modal full" data-reveal="">
	<div style="display:table; width:100%; height:100%; position:relative;">
		<div class="row collapse" style="vertical-align:middle; display:table-cell">
			<div class="row">
				<div class="small-2 small-centered column">
					<center><?php echo $this->Html->image('/images/loading.gif') ?></center>
				</div>
			</div>
			<div class="row">
				<div class="small-6 small-centered column text-center">
					<p><?php echo __('Por favor, aguarde alguns segundos enquanto processamos a sua solicitação.')?></p>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$tipo_pessoa = ($this->Session->check('Doacao.doador')) ? strtolower($this->Session->read('Doacao.doador')) : $this->Session->read('Auth.User.Doador.pessoa_id');
$configs     = $configs['Configuracao'];

$js_vars_string = <<<HTML
var valuesObject = {\n
HTML;

if ($tipo_pessoa == 'pf' || $tipo_pessoa == 1) {
$js_vars_string .= <<<HTML
	'minMonthly': {$configs['minimo_mensal_dpf']},
	'minUnique': {$configs['minimo_anual_dpf']}
}
HTML;
} elseif ($tipo_pessoa == 'pj' || $tipo_pessoa == 2) {
$js_vars_string .= <<<HTML
	'minMonthly': {$configs['minimo_mensal_dpj']},
	'minUnique': {$configs['minimo_anual_dpj']}
}
HTML;
}

echo $this->Html->scriptBlock(
	$js_vars_string,
	['inline' => false]
);
?>