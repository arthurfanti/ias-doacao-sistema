<?php
	foreach ($pagamentoCredito as $credito) {
		$credito_array[$credito['FormasPagamento']['id']] = $credito['FormasPagamento']['nome'];
	}

	foreach ($pagamentoDebito as $debito) {
		$debito_array[$debito['FormasPagamento']['id']] = $debito['FormasPagamento']['nome'];
		$debito_array_js[$debito['FormasPagamento']['id']] = [
			'nome'        => $debito['FormasPagamento']['nome'],
			'ag_regex'    => $debito['FormasPagamento']['ag_regex'],
			'conta_regex' => $debito['FormasPagamento']['conta_regex']
		];
	}
	$configs = $configs['Configuracao'];
?>
<?php echo $this->Form->create('Doacao', ['data-abide']) ?>
<div class="row">
	<fieldset>
		<legend>
			Cadastrar Doação&nbsp;
			<small class="label">Doador: <?php echo ($doadorInfo['Doador']['pessoa_id'] == 1) ? $doadorInfo['Doador']['nome'] . ' ' . $doadorInfo['Doador']['sobrenome'] : $doadorInfo['Doador']['sobrenome'] ?></small>
		</legend>
		<table id="tabela-sua-doacao" width="100%" align="center" class="text-center">
			<thead>
				<tr>
					<th width="50%"><?= __('Valor')?></th>
					<th width="50%"><?= __('Frequência da Doação')?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<div class="small-12">
							<?php echo $this->Form->input('Doacao.valor', array('placeholder' => __('Valor da doação'), 'type' => 'text', 'required' => true, 'minlength' => '6','maxlength' => '15', 'data-abide-validator' => 'testaValorDoado')) ?>
							<small class="error"></small>
						</div>
					</td>
					<td>
						<?php echo $this->Form->input('tipo',
							array('options' => array(
								'M' => __('Mensal'),
								'U' => __('Única')
							), 'id' => 'tipo-doacao', 'required')) ?>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>
</div>

<div class="row">
	<fieldset>
		<legend>Forma de Pagamento</legend>
		<div class="small-12 column">
			<dl id="myTabs" class="sub-nav" data-tab>
				<dt>Selecione:</dt>
				<dd class="active"><a href="#panel1">Cartão de Crédito</a></dd>
				<dd><a href="#panel2">Débito em Conta</a></dd>
				<dd><a href="#panel3">Boleto</a></dd>
				<!-- <dd><a href="#panel4">PayPal</a></dd> -->
			</dl>
			<hr>

			<div class="tabs-content">
				<div class="content active" id="panel1" data-forma-pagamento="credito">
					<?php echo $this->Form->radio('formas_pagamento_id', $credito_array, ['legend' => false]); ?>

					<article class="formaPagamento" id="credito">
						<div class="row">
							<div class="small-12 medium-6 columns end">
								<?php echo $this->Form->input('Pagamento.cartaoCreditoNome', array('placeholder' => __('Nome como está no cartão'), 'pattern' => 'alphaSpaces')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-6 columns">
								<?php echo $this->Form->input('Pagamento.cartaoCreditoNum', array('placeholder' => __('Número do cartão'), 'id' => 'cartaoCreditoNum', 'pattern' => 'card', 'maxlength' => 16)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-6 columns">
								<?php echo $this->Form->input('Pagamento.cartaoCreditoCod', array('placeholder' => __('Código de segurança'), 'id' => 'cartaoCreditoCod', 'pattern' => 'cvv')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="show-for-medium-up medium-2 columns"><i class="sprite-cod-seguranca"></i></div>
						</div>

						<div class="row">
							<div class="small-12 medium-6 columns">
								<label for="validade-mes">Validade (mês)</label>
								<?php echo $this->Form->select('Pagamento.validade_mes', $this->Geral->getMeses(), array('id' => 'validade-mes', 'empty' => false)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-6 columns">
								<label for="validade-ano">Validade (ano)</label>
								<?php echo $this->Form->select('Pagamento.validade_ano', $this->Geral->getAnos(), array('id' => 'validade-ano', 'empty' => false)) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
					</article>
				</div>

				<div class="content" id="panel2" data-forma-pagamento="debito">
					<?php echo $this->Form->radio('formas_pagamento_id', $debito_array, ['legend' => false]); ?>

					<article class="formaPagamento" id="debito">
						<div class="row">
							<div class="small-12 medium-4 columns end">
								<?php echo $this->Form->select('Pagamento.dia_debito_id', $dias, array('id' => 'data-debito', 'empty' => __('Melhor dia para Débito'))) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-4 columns">
								<!-- <input type="text" placeholder="<?= __('Agência') ?> " id="debitoAgencia" name="debitoAgencia"> -->
								<?php echo $this->Form->input('Pagamento.debitoAgencia', array('placeholder' => __('Agência'), 'id' => 'debitoAgencia')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
							<div class="small-12 medium-4 columns">
								<!-- <input type="text" placeholder="<?= __('Conta') ?> " id="debitoConta" name="debitoConta"> -->
								<?php echo $this->Form->input('Pagamento.debitoConta', array('placeholder' => __('Conta'), 'id' => 'debitoConta')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
					</article>
				</div>

				<div class="content" id="panel3" data-forma-pagamento="boleto">
					<article class="formaPagamento" id="boleto">
						<div class="row">
							<div class="small-12 columns">
								<input name="formas_pagamento_id" type="radio" class="hide" value="1">
								<label for="data-vencimento"><?= __('Vencimento')?></label>
								<?php echo $this->Form->input('Pagamento.dataVencimento', array('class' => 'datepicker', 'id' => 'data-vencimento', 'placeholder' => 'Data')) ?>
								<small class="error"><?= __('Campo Obrigatório') ?></small>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</fieldset>
</div>

<?= $this->Form->input('Pagamento.formas_pagamento_id', array('type' => 'hidden', 'id' => 'formasPagamentoId')) ?>
<?= $this->Form->input('formaPagamento', array('type' => 'hidden', 'id' => 'formaPagamento')) ?>

<div class="clearfix">&nbsp;</div>

<div class="row collapse">
	<div class="small-12 column">
		<?php echo $this->Form->input(
			$this->Html->tag('i', '', ['class' => 'fa fa-floppy-o']) . ' Salvar',
			['type' => 'button' , 'class' => 'button success large expand', 'id' => 'saveBtn', 'escape' => false]);
		?>
	</div>
</div>
<?php echo $this->Form->end(); ?>

<?php echo $this->element('braspag_fingerprint'); ?>

<?php
$js_vars_string = <<<HTML
var debitObj = {\n
HTML;

foreach ($debito_array_js as $key => $value) {
$nome = strtolower(str_replace('É', 'E', str_replace(' ', '_', $value['nome'])));
$js_vars_string .= <<<HTML
	$nome: {id:$key, ag_mask:'{$value['ag_regex']}', conta_mask:'{$value['conta_regex']}'},\n
HTML;
}

$js_vars_string .= <<<HTML
}
var valuesObject = {\n
HTML;

if ($doadorInfo['Doador']['pessoa_id'] == 1) {
$js_vars_string .= <<<HTML
	'minMonthly': {$configs['minimo_mensal_dpf']},
	'minUnique': {$configs['minimo_anual_dpf']}
}
HTML;
} elseif ($doadorInfo['Doador']['pessoa_id'] == 2) {
$js_vars_string .= <<<HTML
	'minMonthly': {$configs['minimo_mensal_dpj']},
	'minUnique': {$configs['minimo_anual_dpj']}
}
HTML;
}

$script = <<<HTML
$js_vars_string\n
$(document).ready(function() {
	$('#DoacaoAdminAdicionarForm').on('valid', function(event) {
		var btn  = $('#saveBtn')
		, self   = $(this)
		, iconEl = '<i class="fa fa-spinner fa-pulse"></i> Gravando Dados...';

		btn.html(iconEl);
		self.find('input, select, button').prop('readonly', true);
	});

	if ($('#formaPagamento').length)
		$('#formaPagamento').val('credito');

	$('#DoacaoValor').inputmask({
		prefix: 'R$',
		groupSeparator: ".",
		alias: "numeric",
		placeholder: "0",
		autoGroup: !0,
		digits: 2,
		radixPoint: ",",
		digitsOptional: !1,
		clearMaskOnLostFocus: !1,
		rightAlign: false
	});

	$('input[type=\'radio\']').on('click', function(event) {
		// event.preventDefault();
		var self = $(this)
		, value = self.val();

		if ($('#formasPagamentoId').length)
			$('#formasPagamentoId').val(value);

		// muda a mascara de acordo com o banco
		$.each(debitObj, function(index, val){
			if (value == val.id)
			{
				ag_mask = val.ag_mask;
				ag_pattern = ag_mask.replace(/9/g, '[0-9]');

				conta_mask = val.conta_mask;
				conta_pattern = conta_mask.replace(/9/g, '[0-9]');
			}
		});

		$('#debitoAgencia').inputmask(ag_mask).attr('pattern', ag_pattern);
		$('#debitoConta').inputmask(conta_mask).attr('pattern', conta_pattern);
	});

	var toggleCounter = 0;
	$('#myTabs').on('toggled', function (event, tab) {
		toggleCounter++;

		if(toggleCounter % 2 !== 0) {
			return;
		}
		// console.log(tab);
		formaPagamento = $(tab).data('forma-pagamento');

		if ($('#formaPagamento').length)
			$('#formaPagamento').val(formaPagamento);
	});
});
HTML;

echo $this->Html->scriptBlock(
	$script,
	['inline' => false]
);
?>