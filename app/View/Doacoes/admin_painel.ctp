<ul class="tabs" data-tab role="tablist">
  <li class="tab-title active" role="presentational" >
    <a href="#panel2-1" role="tab" tabindex="0"aria-selected="false" controls="panel2-1">Parcelas</a>
  </li>
  <li class="tab-title" role="presentational" >
    <a href="#panel2-2" role="tab" tabindex="0"aria-selected="false" controls="panel2-2">Atividades da doação</a>
  </li>
  <?php if ($doacao['Pagamento']['FormasPagamento']['tipo'] == 'B') : ?>
  <li class="tab-title" role="presentational" >
    <a href="#panel2-3" role="tab" tabindex="0"aria-selected="false" controls="panel2-3">Boletos</a>
  </li>
  <?php endif ?>
  <?php if (($doacao['Doacao']['status'] == 'A') && ($doacao['Doacao']['periodo_id'] == 1)) : ?>
  <li class="tab-title" role="presentational" >
    <a href="#panel2-4" role="tab" tabindex="0"aria-selected="false" controls="panel2-4">Cancelamento</a>
  </li>
  <?php endif ?>
</ul>

<div class="tabs-content">
  <section role="tabpanel" aria-hidden="false" class="content active" id="panel2-1">
    <table>
      <thead>
        <tr>
          <td>Parcela</td>
          <td>Data</td>
          <td>Valor</td>
          <td>Forma de Pagamento</td>
          <td>Status</td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($doacao['Parcela'] as $parcela) : ?>
        <tr>
          <td><?php echo $parcela['parcela'] ?></td>
          <td><?php echo $this->Formatacao->data($parcela['data_pagamento']) ?></td>
          <td><?php echo $this->Formatacao->moeda($parcela['valor']) ?></td>
          <td><?php echo $parcela['FormasPagamento']['nome'] ?></td>
          <td><?php echo $this->Geral->getStatusPagamentoAdmin($parcela['status_id']) ?></td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </section>
  <section role="tabpanel" aria-hidden="false" class="content" id="panel2-2">
		<table>
      <thead>
        <tr>
          <th>Data</th>
          <th>Usuário</th>
          <th>Campo</th>
          <th>Valor anterior</th>
          <th>Valor novo</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($logs as $log) : ?>
        <tr>
          <td><?php echo $this->Formatacao->data($log['LogAction']['created']) ?></td>
          <td>
          	<?php
          	if (isset($log['User'])) {
          		echo ($log['User']['is_admin']) ? $log['User']['username'] : 'DOADOR';
          	} else {
          		echo 'DOADOR';
          	}
          	?>
          </td>
          <td><?php echo $log['LogAction']['field'] ?></td>
          <td>
          	<?php 
          	if ($log['LogAction']['field'] == 'valor') {
          		echo $this->Formatacao->moeda($log['LogAction']['before']);
          	} else if ($log['LogAction']['field'] == 'status') {
          		if ($log['LogAction']['before'] == 'A') {
          			echo 'Ativa';
          		} else if ($log['LogAction']['before'] == 'C') {
          			echo 'Cancelada';
          		} else {
          			echo $log['LogAction']['before'];
          		}
          	} else {
	          	echo $log['LogAction']['before'];
	          }
          	?>
          </td>
          <td>
          	<?php 
          	if ($log['LogAction']['field'] == 'valor') {
          		echo $this->Formatacao->moeda($log['LogAction']['after']);
          	} else if ($log['LogAction']['field'] == 'status') {
          		if ($log['LogAction']['after'] == 'A') {
          			echo 'Ativa';
          		} else if ($log['LogAction']['after'] == 'C') {
          			echo 'Cancelada';
          		} else {
          			echo $log['LogAction']['after'];
          		}
          	} else {
	          	echo $log['LogAction']['after'];
	          }
          	?>
        	</td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </section>
  <?php if ($doacao['Pagamento']['FormasPagamento']['tipo'] == 'B') : ?>
	<section role="tabpanel" aria-hidden="false" class="content" id="panel2-3">
		<?php if (($doacao['Pagamento']['FormasPagamento']['tipo'] == 'B')) : ?>
  	<table>
  		<thead>
  			<tr>
  				<td>Parcela</td>
  				<td>Data</td>
  				<td>Valor</td>
  				<td>Status</td>
  				<td>Ações</td>
  			</tr>
  		</thead>
  		<tbody>
  			<?php foreach ($doacao['Parcela'] as $parcela) : ?>
  			<tr>
  				<td><?php echo $parcela['parcela'] ?></td>
  				<td><?php echo $this->Formatacao->data($parcela['data_pagamento']) ?></td>
  				<td><?php echo $this->Formatacao->moeda($parcela['valor']) ?></td>
  				<td><?php echo $this->Geral->getStatusPagamentoAdmin($parcela['status_id']) ?></td>
  				<td>
						<?php
							if ($parcela['status_id'] == 6) {
								echo $this->Html->link(__('Imprimir boleto'), '/boleto/'.$parcela['id'], array('target' => 'blank'));
							}
						?>
  				</td>
  			</tr>
  			<?php endforeach ?>
  		</tbody>
  	</table>
  	<?php endif ?>
  </section>
  <?php endif ?>
  <?php if (($doacao['Doacao']['status'] == 'A') && ($doacao['Doacao']['periodo_id'] == 1)) : ?>
  <section role="tabpanel" aria-hidden="false" class="content" id="panel2-4">
    <?php $ativo = ($doacao['Doacao']['status'] == 'A') ?>
    <?php echo $this->Form->create('CancelamentoDoacao', ['url' => ['controller' => 'cancelamentodoacoes', 'action' => 'cancelar']]) ?>
    <?php echo $this->Form->hidden('CancelamentoDoacao.doacao_id', ['value' => $doacao['Doacao']['id']]) ?>
    <?php echo $this->Form->input('CancelamentoDoacao.cancelamento_id', ['label' => 'Tipo', 'disabled' => !$ativo, 'selected' => $doacao['CancelamentoDoacao']['cancelamento_id']]) ?>
    <?php echo $this->Form->input('CancelamentoDoacao.obs', ['label' => 'Observações', 'disabled' => !$ativo, 'value' => $doacao['CancelamentoDoacao']['obs']]) ?>
    <?php if ($ativo) : ?>
    <?php echo $this->Form->submit('Efetuar cancelamento', ['class' => 'button right']) ?>
    <?php endif ?>
    <?php echo $this->Form->end() ?>
  </section>
  <?php endif ?>
</div>