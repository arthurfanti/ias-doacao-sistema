<?php
	$configs     = $configs['Configuracao'];
	$tipo_pessoa = strtolower($this->Session->read('Doacao.doador'));
?>
<div id="main-box">
	<div id="valor" class="row small-collapse medium-uncollapse">
    <p class="small-12 columns"><?= __('Escolha a frequência e valor que gostaria de doar.')?></p>
		<dl class="tabs row" data-tab>
			<dd class="medium-offset-2 large-offset-3 active monthly small-6 medium-4 large-3">
				<a href="#monthly" class="small-12"><?= __('mensal')?></a>
			</dd>
			<dd class="single small-6 medium-4 large-3">
				<a href="#single" class="small-12"><?= __('única')?></a>
			</dd>
		</dl>
		<?php echo $this->Form->create('Valor', array('id' => 'formValores', 'class' => 'small-12 columns')) ?>
			<div class="tabs-content row">
				<div class="content active row" id="monthly" data-type="M">
					<div class="small-12 medium-10 medium-centered large-12 large-uncentered columns">
						<?php for ($i=0; $i < count($valoresMensais); $i++) : ?>
							<?php $valor = $valoresMensais[$i]; ?>
							<div data-value="<?php echo (int)$valor['Valor']['valor'] ?>" class="small-6 medium-4 large-3 columns button <?php echo ($i == 1) ? 'active' : '' ?>">
							<h2><?= __('Doando')?></h2>
							<p><small><?php echo ($tipo_pessoa == 'pj') ? "R$" : __('R$') ?></small><?php echo (int)$valor['Valor']['valor'] ?><small>,00</small></p>
						</div>
						<?php endfor ?>

						<div data-value="" class="small-6 medium-12 large-3 qualquer-valor columns button">
							<h2 class="small-12 columns"><?= __('Doe quanto desejar')?></h2>
							<div class="small-12">
								<input type="text" id="valorMensal" name="valorMensal" value="<?php echo ($tipo_pessoa == 'pj') ? $configs['minimo_mensal_dpj'] : $configs['minimo_mensal_dpf'] ?>" minlength="6" maxlength="15">
							</div>
							<p class="small-12 columns">*<?= ($tipo_pessoa == 'pj') ? __('valor mínimo') . ' R$' . intval($configs['minimo_mensal_dpj']) . ',00' : __('valor mínimo R$') . intval($configs['minimo_mensal_dpf']) . ',00' ?></p>
						</div>
					</div>
				</div>
				<div class="content row" id="single" data-type="U">
					<div class="small-12 medium-10 medium-centered large-12 large-uncentered columns">
						<?php for ($i=0; $i < count($valoresAnuais); $i++) : ?>
							<?php $valor = $valoresAnuais[$i]; ?>
						<div data-value="<?php echo (int)$valor['Valor']['valor'] ?>" class="small-6 medium-4 large-3 columns button <?php echo ($i == 0) ? 'active' : '' ?>">
							<h2><?= __("Doando") ?></h2>
							<p><small><?php echo ($tipo_pessoa == 'pj') ? "R$" : __('R$') ?></small><?php echo (int)$valor['Valor']['valor'] ?><small>,00</small></p>
						</div>
						<?php endfor ?>

						<div data-value="" class="small-6 medium-12 large-3 qualquer-valor columns button">
							<h2 class="small-12 columns"><?= __('Doe quanto desejar')?></h2>
							<div class="small-12">
								<input type="text" id="valorUnico" name="valorUnico" value="<?php echo ($tipo_pessoa == 'pj') ? $configs['minimo_anual_dpj'] : $configs['minimo_anual_dpf'] ?>" minlength="6" maxlength="15">
							</div>
							<p class="small-12 columns">*<?= ($tipo_pessoa == 'pj') ? __('valor mínimo') . ' R$' . intval($configs['minimo_anual_dpj']) . ',00' : __('valor mínimo R$') . intval($configs['minimo_anual_dpf']) . ',00' ?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="revenue row">
				<div class="row" id="equivalencia">
					<p><?php echo $valoresMensais[1]['Valor']['texto_antes'] ?></p>
					<?php if ($this->Session->read('Config.language') == 'por'): ?>
					<h2><?php echo intval($valoresMensais[1]['Valor']['valor'] / $valoresMensais[1]['Valor']['divisor']) . ' ' . $valoresMensais[1]['Valor']['texto_depois'] ?></h2>
					<?php else: ?>
					<h2><?php echo intval( ($valoresMensais[1]['Valor']['valor'] / $valoresMensais[1]['Valor']['divisor'])*$configs['valor_dolar'] ) . ' ' . $valoresMensais[1]['Valor']['texto_depois'] ?></h2>
					<?php endif ?>
				</div>
				<div class="row">
					<button data-target="cadastro" name="novo" class="button small-10 medium-4 small-centered columns" type="submit"><?= __('Quero doar')?> <i class="sprite-quero-ajudar show-for-medium-up"></i></button>
					<?php if (!$this->Session->check('Auth.User')) : ?>
					<button data-target="login" name="login" class="button small-10 medium-4 small-centered columns" type="submit"><?= __('Já sou doador')?> <i class="sprite-sou-doador show-for-medium-up"></i></button>
					<?php endif ?>
				</div>
			</div>
			<input type="hidden" id="tipoDeDoacao" name="tipoDeDoacao" value="">
			<input type="hidden" id="valorDeDoacao" name="valorDeDoacao" value="">
		<?php echo $this->Form->end() ?>
	</div>

	<!-- <div id="background-image"></div> -->
	<?php echo $this->Geral->getBackground($configs, $tipo_pessoa); ?>
</div>

<div id="noDonationsModal" class="reveal-modal alert-box success xlarge" data-reveal aria-labelledby="we can not accept foreign donations yet" aria-hidden="true" role="dialog">
	<h2>So sorry :(</h2>
	<p>We are currently not accepting donations from companies outside <code>Brasil</code>.</p>
	<p>If you have a company outside Brazil and want to support Instituto Ayrton Senna please contact us by e-mail doacao@ias.org.br  or phone +55 11 2974-3062.</p>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<?php
	$scriptString = 'var eqObject = {' . "\n";
	foreach (array_merge($valoresMensais, $valoresAnuais) as $valor) {
		$scriptString .= intval($valor['Valor']['valor']) . ': {textBefore:"' . $valor['Valor']['texto_antes'] . '", equivalent:"' . intval($valor['Valor']['valor'] / $valor['Valor']['divisor']) . '", textAfter:"' . $valor['Valor']['texto_depois'] . '"}' . "\n" . ', ';
	}

	$scriptString .= '"standardMonthly": {textBefore:"' . $valoresMensais[0]['Valor']['texto_antes'] . '", divisor:"' . $valoresMensais[0]['Valor']['divisor'] . '", textAfter:"' . $valoresMensais[0]['Valor']['texto_depois'] . '"}' . "\n" . ', ';

	$scriptString .= '"standardUnique": {textBefore:"' . $valoresAnuais[0]['Valor']['texto_antes'] . '", divisor:"' . $valoresAnuais[0]['Valor']['divisor'] . '", textAfter:"' . $valoresAnuais[0]['Valor']['texto_depois'] . '"}' . "\n" . '}';

	$scriptString .= "\n" . 'var valuesObject = {';
	$scriptString .= "\n" . '"dolar": ' . $configs['valor_dolar'];
	if ($tipo_pessoa == 'pf') {
		$scriptString .= "\n" . ', "minMonthly": ' . $configs['minimo_mensal_dpf'];
		$scriptString .= "\n" . ', "minUnique": ' . $configs['minimo_anual_dpf'];
	} elseif ($tipo_pessoa == 'pj') {
		$scriptString .= "\n" . ', "minMonthly": ' . $configs['minimo_mensal_dpj'];
		$scriptString .= "\n" . ', "minUnique": ' . $configs['minimo_anual_dpj'];
	}
	$scriptString .= "\n" . '}';
	// $scriptString = substr($scriptString, 0, -3) . '';
	// $scriptString .= '}';

	echo $this->Html->scriptBlock(
		// 'var minimoMensal = ' . $minimo_mensal . ', minimoUnico = ' . $minimo_unico . ','
		$scriptString
		, ['inline' => false]
	); ?>

<?php if($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'eng' && $this->Session->check('Doacao.doador') && $this->Session->read('Doacao.doador') == 'PJ'): ?>
	<?php echo $this->Html->scriptBlock(
		"$(document).ready(function() {
		$('#noDonationsModal').foundation('reveal', 'open');
	});",
		['inline' => false]
	); ?>
<?php endif ?>