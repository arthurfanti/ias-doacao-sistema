<?php if (isset($doacao['Pagamento']) && ($doacao['Pagamento']['formas_pagamento_id'] == '1')) : ?>
	<script type="text/javascript">
	$(function() {
		window.open ($.url + "boleto/<?php echo $doacao['Parcela'][0]['id'] ?>", '_blank')
	});
	</script>
<?php endif ?>
<div data-alert class="alert-box alert" style="width:100%;">
	<div class="row">
		<div class="small-12 column">
			<p><?php echo $mensagem['Mensagem']['descricao']; ?></p>
			<?php if (isset($doacao['Pagamento']) && ($doacao['Pagamento']['formas_pagamento_id'] == '1')) : ?>
			<p>
				<a href="<?php echo $this->Html->url("/boleto/" . $doacao['Parcela'][0]['id']) ?>" class="button success" target="_blank"><?= __('Imprimir boleto')?></a>
			</p>
			<?php endif ?>
		</div>
	</div>
	<a href="" class="close">&times;</a>
</div>

<section role="forms">
	<div id="cadastro-complemento">
		<div class="row">
			<div class="small-7 medium-3 small-centered columns">
				<center>
					<ul style="display:inline-block" class="sub-nav donation-steps">
						<li><a disabled href="#" class="button round">1</a></li>
						<li><a disabled href="#" class="button round active">2</a></li>
					</ul>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="small-12 text-center">
				<p><?= __('Complete seu cadastro')?>!</p>
			</div>
			<?php echo $this->Session->flash() ?>
		</div>

		<?php if ( $this->Session->read('Auth.User.Doador.pessoa_id') == 1): ?>
		<div class="row collapse">
			<div class="small-12 column">
				<?php echo $this->Form->create(array('data-abide' => 'data-abide')) ?>
					<?php echo $this->Form->hidden('Doador.id') ?>
					<fieldset>
						<?php if ( !isset($this->data['DoadorEndereco'][0]) && !$this->Session->check('Doacao.from_admin') ) : ?>
						<div class="row">
							<div class="small-12 column">
								<h3><?= __('Dados Pessoais')?></h3>
							</div>
						</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-4 columns">
								<?php echo $this->Form->input('Doador.nascimento', array('placeholder' => __('Data de Nascimento'), 'id' => 'nascimento', 'type' => 'text', 'data-abide-validator' => 'testaAnoAtual')) ?>
								<?php echo $this->Html->tag(
									'small',
									__('Preenchimento Obrigatório'),
									array('class' => 'error')
								) ?>
							</div>
							<div class="small-12 medium-8 columns">
								<?php echo $this->Form->radio('Doador.sexo', array('1' => __('Masculino'), '2' => __('Feminino')), array('legend' => false)) ?>
							</div>
						</div>

						<?php echo $this->Form->hidden('DoadorEndereco.0.endereco_id', array('value' => 2)) ?>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-4 column">
								<?php
									if ($this->request->data['Doador']['conta_brasil'] == 1) {
										echo $this->Form->input('DoadorEndereco.0.cep', array('placeholder' => __('CEP'), 'class' => 'cep', 'type' => 'text'));
										echo $this->Html->tag('small', __('Preenchimento Obrigatório'), array('class' => 'error'));
									}
								?>
							</div>
						</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-7 columns">
								<?php echo $this->Form->input('DoadorEndereco.0.logradouro', array('placeholder' => __('Logradouro') )) ?>
								<?php echo $this->Html->tag(
									'small',
									__('Preenchimento Obrigatório'),
									array('class' => 'error')
								) ?>
							</div>
							<div class="small-12 medium-2 columns">
								<?php echo $this->Form->input('DoadorEndereco.0.numero', array('placeholder' => __('Número') , 'type' => 'text')) ?>
								<?php echo $this->Html->tag(
									'small',
									__('Preenchimento Obrigatório'),
									array('class' => 'error')
								) ?>
							</div>
							<div class="small-12 medium-3 columns">
								<?php echo $this->Form->input('DoadorEndereco.0.complemento', array('placeholder' => __('Complemento') )) ?>
							</div>
						</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-4 columns">
								<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
									<select required id="estado_id" name="data[DoadorEndereco][0][estado_id]">
										<option value="">Estado</option>
										<option data-uf="AC" value="1">Acre</option>
										<option data-uf="AL" value="2">Alagoas</option>
										<option data-uf="AP" value="3">Amapá</option>
										<option data-uf="AM" value="4">Amazonas</option>
										<option data-uf="BA" value="5">Bahia</option>
										<option data-uf="CE" value="6">Ceará</option>
										<option data-uf="DF" value="7">Distrito Federal</option>
										<option data-uf="ES" value="8">Espirito Santo</option>
										<option data-uf="GO" value="9">Goiás</option>
										<option data-uf="MA" value="10">Maranhão</option>
										<option data-uf="MT" value="11">Mato Grosso do Sul</option>
										<option data-uf="MS" value="12">Mato Grosso</option>
										<option data-uf="MG" value="13">Minas Gerais</option>
										<option data-uf="PA" value="14">Pará</option>
										<option data-uf="PB" value="15">Paraíba</option>
										<option data-uf="PR" value="16">Paraná</option>
										<option data-uf="PE" value="17">Pernambuco</option>
										<option data-uf="PI" value="18">Piauí</option>
										<option data-uf="RJ" value="19">Rio de Janeiro</option>
										<option data-uf="RN" value="20">Rio Grande do Norte</option>
										<option data-uf="RS" value="21">Rio Grande do Sul</option>
										<option data-uf="RO" value="22">Rondônia</option>
										<option data-uf="RR" value="23">Roraima</option>
										<option data-uf="SC" value="24">Santa Catarina</option>
										<option data-uf="SP" value="25">São Paulo</option>
										<option data-uf="SE" value="26">Sergipe</option>
										<option data-uf="TO" value="27">Tocantins</option>
									</select>
									<?php echo $this->Html->tag(
										'small',
										__('Preenchimento Obrigatório'),
										array('class' => 'error')
									) ?>
								<?php else: ?>
									<?php echo $this->Form->input('DoadorEndereco.0.estado', array('type' => 'text', 'placeholder' => __('Estado'))) ?>
								<?php endif ?>
							</div>

							<div class="small-12 medium-4 columns">
								<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
									<select required id="cidade_id" name="data[DoadorEndereco][0][cidade_id]">
										<option disabled value="">Cidade</option>
									</select>
									<?php echo $this->Html->tag(
										'small',
										__('Preenchimento Obrigatório'),
										array('class' => 'error')
									) ?>
								<?php else: ?>
									<?php echo $this->Form->input('DoadorEndereco.0.cidade', array('type' => 'text', 'placeholder' => __('Cidade'))) ?>
								<?php endif ?>
							</div>

							<div class="small-12 medium-4 columns">
								<?php echo $this->Form->input('DoadorEndereco.0.bairro', array('placeholder' => __('Bairro') )) ?>
								<?php echo $this->Html->tag(
									'small',
									__('Preenchimento Obrigatório'),
									array('class' => 'error')
								) ?>
							</div>
						</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 column">
								<ul class="small-block-grid-1 medium-block-grid-3">
									<li>
										<fieldset>
											<h5><?php echo __('Telefone Residencial') ?></h5>
											<?php echo $this->Form->hidden('DoadorTelefone.0.id') ?>
											<?php echo $this->Form->input('DoadorTelefone.0.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
										</fieldset>
									</li>
									<li>
										<fieldset>
											<h5><?php echo __('Telefone Comercial') ?></h5>
											<?php echo $this->Form->hidden('DoadorTelefone.1.telefone_id', array('value' => 2)) ?>
											<?php echo $this->Form->input('DoadorTelefone.1.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
										</fieldset>
									</li>
									<li>
										<fieldset>
											<h5><?php echo __('Telefone Celular') ?></h5>
											<?php echo $this->Form->hidden('DoadorTelefone.2.telefone_id', array('value' => 3)) ?>
											<?php echo $this->Form->input('DoadorTelefone.2.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
										</fieldset>
									</li>
								</ul>
							</div>
						</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 columns">
								<label><?php echo __('Como você conheceu o Instituto Ayrton Senna?') ?></label>
								<?php echo $this->Form->input('Doador.conheceu_id', array('id' => 'conheceu', 'empty' => __('Selecione'))) ?>
							</div>
						</div>
						<?php endif ?>

						<?php if (count($itens) > 0) : ?>
						<div class="row">
							<div class="small-12 columns">
								<h3><?= __('Escolha seu Presente')?></h3>
							</div>
						</div>

						<div class="row">
							<div class="small-12 columns">
								<ul class="small-block-grid-1 medium-block-grid-4">
									<?php foreach ($itens as $item) : ?>
									<?php $item_underscore = strtolower(str_replace(' ', '_', $item['Item']['item'])); ?>
									<li>
										<h5><?php echo $item['Item']['item']; ?></h5>
										<input type="radio" class="gift" data-attribute="<?php echo $item_underscore; ?>" data-image="<?php echo $this->Html->webroot('/files/item/imagem/'.$item['Item']['id'].'/'.$item['Item']['imagem']) ?>" name="gift" id="opt1" value='<?php echo $item['Item']['id'] ?>'>
									</li>
									<?php endforeach ?>
								</ul>

								<?php echo $this->Form->input('Brinde.item_id', array('type' => 'hidden', 'id' => 'brindeItemId')) ?>
								<?php $cont = 0; ?>
								<div id="giftsContainer" class="row">
									<?php foreach ($itens as $item) : ?>
									<?php $item_underscore = strtolower(str_replace(' ', '_', $item['Item']['item'])); ?>
									<?php //echo $item['Item']['id'] ?>
									<fieldset class="hide" id="<?php echo $item_underscore; ?>">
										<legend><?php echo __('Opções') ?></legend>
									<?php foreach ($item['Atributo'] as $atributo) : ?>
									<?php $opcoes = Hash::combine($atributo, 'Conteudo.{n}.id', 'Conteudo.{n}.conteudo'); ?>
										<h5><?php echo $atributo['atributo'] ?></h5>
										<?php echo $this->Form->select('BrindeConteudo.'.$cont.'.conteudo_id', $opcoes, ['empty' => false]) ?>
									<?php $cont++; ?>
									<?php endforeach ?>
									</fieldset>
									<?php endforeach ?>
								</div>
							</div>
						</div>
						<?php endif ?>

						<!-- <div class="row">
							<div class="small-6 columns">
								<label for=""><?= __('Tamanho')?></label>
								<select name="tamanho" id="tamanho">
									<option selected disabled value="0">P, M ou G</option>
								</select>
							</div>
							<div class="small-6 columns">
								<label for=""><?= __('Cor')?></label>
								<select name="cor" id="cor">
									<option selected disabled value="0">Escolha a cor da camiseta</option>
								</select>
							</div>
						</div> -->

						<div class="row">
							<div class="small-12 columns text-center">
								<button class="button" type="submit">
									<?= __('Concluir Cadastro')?>&nbsp;<i class="sprite-check"></i>
								</button>
							</div>
						</div>
					</fieldset>
				<?php echo $this->Form->end() ?>
			</div>
		</div>
		<?php elseif ( $this->Session->read('Auth.User.Doador.pessoa_id') == 2): ?>
		<div class="row collapse">
			<div class="small-12 column">
				<?php echo $this->Form->create(array('data-abide' => 'data-abide')) ?>
					<?php echo $this->Form->hidden('Doador.id') ?>
					<?php if ( !isset($this->data['DoadorEndereco'][0]) && !$this->Session->check('Doacao.from_admin') ) : ?>
					<div class="row">
						<div class="small-12 column">
							<h3><?= __('Dados complementares da empresa')?></h3>
						</div>
					</div>

					<fieldset>
						<legend><?= __('Endereço sede (Cartão CNPJ)')?></legend>
						<div class="small-6 columns end">
							<?php echo $this->Form->input('DoadorEndereco.0.cep', ['placeholder' => __('CEP'), 'class' => 'cep', 'required']); ?>
							<small class="error"><?php echo __('Preenchimento Obrigatório') ?></small>
						</div>

						<div class="small-7 columns">
							<?php echo $this->Form->input('DoadorEndereco.0.logradouro', ['placeholder' => __('Logradouro'), 'required']); ?>
							<small class="error"><?php echo __('Preenchimento Obrigatório') ?></small>
						</div>
						<div class="small-2 column">
							<?php echo $this->Form->input('DoadorEndereco.0.numero', ['placeholder' => __('Número'), 'pattern' => 'number', 'required']); ?>
							<small class="error"><?php echo __('Preenchimento Obrigatório') ?></small>
						</div>
						<div class="small-3 columns">
							<?php echo $this->Form->input('DoadorEndereco.0.complemento', ['placeholder' => __('Complemento')]); ?>
						</div>

						<div class="small-12 medium-4 columns">
							<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
							<select required id="estado_id" name="data[DoadorEndereco][0][estado_id]">
								<option value="">Estado</option>
								<option data-uf="AC" value="1">Acre</option>
								<option data-uf="AL" value="2">Alagoas</option>
								<option data-uf="AP" value="3">Amapá</option>
								<option data-uf="AM" value="4">Amazonas</option>
								<option data-uf="BA" value="5">Bahia</option>
								<option data-uf="CE" value="6">Ceará</option>
								<option data-uf="DF" value="7">Distrito Federal</option>
								<option data-uf="ES" value="8">Espirito Santo</option>
								<option data-uf="GO" value="9">Goiás</option>
								<option data-uf="MA" value="10">Maranhão</option>
								<option data-uf="MT" value="11">Mato Grosso do Sul</option>
								<option data-uf="MS" value="12">Mato Grosso</option>
								<option data-uf="MG" value="13">Minas Gerais</option>
								<option data-uf="PA" value="14">Pará</option>
								<option data-uf="PB" value="15">Paraíba</option>
								<option data-uf="PR" value="16">Paraná</option>
								<option data-uf="PE" value="17">Pernambuco</option>
								<option data-uf="PI" value="18">Piauí</option>
								<option data-uf="RJ" value="19">Rio de Janeiro</option>
								<option data-uf="RN" value="20">Rio Grande do Norte</option>
								<option data-uf="RS" value="21">Rio Grande do Sul</option>
								<option data-uf="RO" value="22">Rondônia</option>
								<option data-uf="RR" value="23">Roraima</option>
								<option data-uf="SC" value="24">Santa Catarina</option>
								<option data-uf="SP" value="25">São Paulo</option>
								<option data-uf="SE" value="26">Sergipe</option>
								<option data-uf="TO" value="27">Tocantins</option>
							</select>
							<?php echo $this->Html->tag(
								'small',
								__('Preenchimento Obrigatório'),
								array('class' => 'error')
							) ?>
							<?php else: ?>
								<?php echo $this->Form->input('DoadorEndereco.0.estado', array('type' => 'text', 'placeholder' => __('Estado'))) ?>
							<?php endif ?>
						</div>

						<div class="small-12 medium-4 columns">
							<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
							<select required id="cidade_id" name="data[DoadorEndereco][0][cidade_id]">
								<option disabled value="">Cidade</option>
							</select>
							<?php echo $this->Html->tag(
								'small',
								__('Preenchimento Obrigatório'),
								array('class' => 'error')
							) ?>
							<?php else: ?>
								<?php echo $this->Form->input('DoadorEndereco.0.cidade', array('type' => 'text', 'placeholder' => __('Cidade'))) ?>
							<?php endif ?>
						</div>

						<div class="small-12 medium-4 columns">
							<?php echo $this->Form->input('DoadorEndereco.0.bairro', array('placeholder' => __('Bairro') )) ?>
							<?php echo $this->Html->tag(
								'small',
								__('Preenchimento Obrigatório'),
								array('class' => 'error')
							) ?>
						</div>

					</fieldset>
					<fieldset role="filial">
						<legend>
							<?php echo $this->Form->input('filial', ['type' => 'checkbox', 'label' => false, 'style' => '  vertical-align: middle; margin: 0px 4px 4px 0;']); ?>
							<?= __('Endereço de correspondência não é o endereço da sede')?>
						</legend>

						<div class="small-6 columns end">
							<?php echo $this->Form->input('DoadorEndereco.1.cep', ['placeholder' => __('CEP'), 'class' => 'cep', 'required']); ?>
							<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
						</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-7 columns">
								<?php echo $this->Form->input('DoadorEndereco.1.logradouro', array('placeholder' => __('Logradouro') )) ?>
								<?php echo $this->Html->tag(
									'small',
									__('Preenchimento Obrigatório'),
									array('class' => 'error')
								) ?>
							</div>
							<div class="small-12 medium-2 columns">
								<?php echo $this->Form->input('DoadorEndereco.1.numero', array('placeholder' => __('Número') , 'type' => 'text')) ?>
								<?php echo $this->Html->tag(
									'small',
									__('Preenchimento Obrigatório'),
									array('class' => 'error')
								) ?>
							</div>
							<div class="small-12 medium-3 columns">
								<?php echo $this->Form->input('DoadorEndereco.1.complemento', array('placeholder' => __('Complemento') )) ?>
							</div>
						</div>

						<div class="row small-collapse medium-uncollapse">
							<div class="small-12 medium-4 columns">
								<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
									<select required id="estado_id" name="data[DoadorEndereco][1][estado_id]">
										<option value="">Estado</option>
										<option data-uf="AC" value="1">Acre</option>
										<option data-uf="AL" value="2">Alagoas</option>
										<option data-uf="AP" value="3">Amapá</option>
										<option data-uf="AM" value="4">Amazonas</option>
										<option data-uf="BA" value="5">Bahia</option>
										<option data-uf="CE" value="6">Ceará</option>
										<option data-uf="DF" value="7">Distrito Federal</option>
										<option data-uf="ES" value="8">Espirito Santo</option>
										<option data-uf="GO" value="9">Goiás</option>
										<option data-uf="MA" value="10">Maranhão</option>
										<option data-uf="MT" value="11">Mato Grosso do Sul</option>
										<option data-uf="MS" value="12">Mato Grosso</option>
										<option data-uf="MG" value="13">Minas Gerais</option>
										<option data-uf="PA" value="14">Pará</option>
										<option data-uf="PB" value="15">Paraíba</option>
										<option data-uf="PR" value="16">Paraná</option>
										<option data-uf="PE" value="17">Pernambuco</option>
										<option data-uf="PI" value="18">Piauí</option>
										<option data-uf="RJ" value="19">Rio de Janeiro</option>
										<option data-uf="RN" value="20">Rio Grande do Norte</option>
										<option data-uf="RS" value="21">Rio Grande do Sul</option>
										<option data-uf="RO" value="22">Rondônia</option>
										<option data-uf="RR" value="23">Roraima</option>
										<option data-uf="SC" value="24">Santa Catarina</option>
										<option data-uf="SP" value="25">São Paulo</option>
										<option data-uf="SE" value="26">Sergipe</option>
										<option data-uf="TO" value="27">Tocantins</option>
									</select>
									<?php echo $this->Html->tag(
										'small',
										__('Preenchimento Obrigatório'),
										array('class' => 'error')
									) ?>
								<?php else: ?>
									<?php echo $this->Form->input('DoadorEndereco.1.estado', array('type' => 'text', 'placeholder' => __('Estado'))) ?>
								<?php endif ?>
							</div>

							<div class="small-12 medium-4 columns">
								<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
									<select required id="cidade_id" name="data[DoadorEndereco][1][cidade_id]">
										<option disabled value="">Cidade</option>
									</select>
									<?php echo $this->Html->tag(
										'small',
										__('Preenchimento Obrigatório'),
										array('class' => 'error')
									) ?>
								<?php else: ?>
									<?php echo $this->Form->input('DoadorEndereco.1.cidade', array('type' => 'text', 'placeholder' => __('Cidade'))) ?>
								<?php endif ?>
							</div>

							<div class="small-12 medium-4 columns">
								<?php echo $this->Form->input('DoadorEndereco.1.bairro', array('placeholder' => __('Bairro') )) ?>
								<?php echo $this->Html->tag(
									'small',
									__('Preenchimento Obrigatório'),
									array('class' => 'error')
								) ?>
							</div>
						</div>
					</fieldset>

					<div class="row small-collapse medium-uncollapse">
						<div class="small-12 column">
							<ul class="small-block-grid-1 medium-block-grid-3">
								<li>
									<fieldset>
										<h5><?php echo __('Telefone Principal') ?></h5>
										<?php echo $this->Form->hidden('DoadorTelefone.0.id') ?>
										<?php echo $this->Form->input('DoadorTelefone.0.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
									</fieldset>
								</li>
								<li>
									<fieldset>
										<h5><?php echo __('Telefone Secundário') ?></h5>
										<?php echo $this->Form->hidden('DoadorTelefone.1.telefone_id', array('value' => 2)) ?>
										<?php echo $this->Form->input('DoadorTelefone.1.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
									</fieldset>
								</li>
								<li>
									<fieldset>
										<h5><?php echo __('Telefone Celular') ?></h5>
										<?php echo $this->Form->hidden('DoadorTelefone.2.telefone_id', array('value' => 3)) ?>
										<?php echo $this->Form->input('DoadorTelefone.2.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
									</fieldset>
								</li>
							</ul>
						</div>
					</div>

					<div class="row small-collapse medium-uncollapse">
						<div class="small-12 columns">
							<label><?php echo __('Como você conheceu o Instituto Ayrton Senna?') ?></label>
							<?php echo $this->Form->input('Doador.conheceu_id', array('id' => 'conheceu', 'empty' => __('Selecione'))) ?>
						</div>
					</div>
					<?php endif ?>

						<?php if (count($itens) > 0) : ?>
						<div class="row">
							<div class="small-12 columns">
								<h3><?= __('Escolha seu Presente')?></h3>
							</div>
						</div>



						<div class="row">
							<div class="small-12 columns">
								<ul class="small-block-grid-1 medium-block-grid-4">
									<?php foreach ($itens as $item) : ?>
									<?php $item_underscore = strtolower(str_replace(' ', '_', $item['Item']['item'])); ?>
									<li>
										<h5><?php echo $item['Item']['item']; ?></h5>
										<input type="radio" class="gift" data-attribute="<?php echo $item_underscore; ?>" data-image="<?php echo $this->Html->webroot('/files/item/imagem/'.$item['Item']['id'].'/'.$item['Item']['imagem']) ?>" name="gift" id="opt1" value='<?php echo $item['Item']['id'] ?>'>
									</li>
									<?php endforeach ?>
								</ul>

								<?php echo $this->Form->input('Brinde.item_id', array('type' => 'hidden', 'id' => 'brindeItemId')) ?>
								<?php $cont = 0; ?>
								<div id="giftsContainer" class="row">
									<?php foreach ($itens as $item) : ?>
									<?php $item_underscore = strtolower(str_replace(' ', '_', $item['Item']['item'])); ?>
									<?php //echo $item['Item']['id'] ?>
									<fieldset class="hide" id="<?php echo $item_underscore; ?>">
										<legend><?php echo __('Opções') ?></legend>
									<?php foreach ($item['Atributo'] as $atributo) : ?>
									<?php $opcoes = Hash::combine($atributo, 'Conteudo.{n}.id', 'Conteudo.{n}.conteudo'); ?>
										<h5><?php echo $atributo['atributo'] ?></h5>
										<?php echo $this->Form->select('BrindeConteudo.'.$cont.'.conteudo_id', $opcoes, ['empty' => false]) ?>
									<?php $cont++; ?>
									<?php endforeach ?>
									</fieldset>
									<?php endforeach ?>
								</div>
							</div>
						</div>
						<?php endif ?>

						<!-- <div class="row">
							<div class="small-6 columns">
								<label for=""><?= __('Tamanho')?></label>
								<select name="tamanho" id="tamanho">
									<option selected disabled value="0">P, M ou G</option>
								</select>
							</div>
							<div class="small-6 columns">
								<label for=""><?= __('Cor')?></label>
								<select name="cor" id="cor">
									<option selected disabled value="0">Escolha a cor da camiseta</option>
								</select>
							</div>
						</div> -->

						<div class="row">
							<div class="small-12 columns text-center">
								<button class="button" type="submit">
									<?= __('Concluir Cadastro')?>&nbsp;<i class="sprite-check"></i>
								</button>
							</div>
						</div>
					</fieldset>
				<?php echo $this->Form->end() ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>