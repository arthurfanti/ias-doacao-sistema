<?php
	$credito      = false;
	$debito       = false;
	$grid_credito = sizeof($pagamentoCredito);
	$grid_debito  = sizeof($pagamentoDebito);

	if (in_array($this->data['Pagamento']['formas_pagamento_id'], [4, 5, 6, 12, 13])) {
		$credito = true;
	} else if (in_array($this->data['Pagamento']['formas_pagamento_id'], [2, 3, 7, 10, 11, 15])) {
		$debito = true;
	}
?>

<?php echo $this->Session->flash(); ?>

<div id="my-donnations">
	<div class="row">
		<table id="my-account-table" class="display responsive nowrap">
			<thead>
				<tr>
					<th></th>
					<th><?php echo __('Parcela') ?></th>
					<th><?php echo __('Data') ?></th>
					<th><?php echo __('Valor') ?></th>
					<th width="10%" class="desktop"><?php echo __('Forma de Pagamento') ?></th>
					<th><?php echo __('Pagamento') ?></th>
					<!-- <th><?php echo __('Ações') ?></th> -->
				</tr>
			</thead>
			<tbody>
				<?php foreach ($parcelas as $parcela) : ?>
				<tr>
					<td></td>
					<td><?php echo $parcela['Parcela']['parcela'] ?></td>
					<td><?php echo $this->Formatacao->data($parcela['Parcela']['data_pagamento']) ?></td>
					<td><?php echo $this->Formatacao->moeda($parcela['Parcela']['valor']) ?></td>
					<td class="desktop"><?php echo __($this->Geral->getTipoFormaPagamento($parcela['FormasPagamento']['tipo'])) ?></td>
					<td><?php echo __($this->Geral->getStatusPagamento($parcela['Parcela']['status_id'])) ?></td>
					<!-- <td>
						<?php
							// if ($parcela['Parcela']['formas_pagamento_id'] == 1) {
							// 	echo $this->Html->link('Imprimir boleto', '/boleto/'.$parcela['Parcela']['id'], array('target' => 'blank', 'class' => 'button success expand small', 'style' => 'margin:0'));
							// }
						?>
					</td> -->
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<!--
<section role="forms">
	<div class="row collapse">
		<?php //se a doação estiver ativa e for mensal exibe o form ?>
		<?php $tipo_pessoa = $this->Session->read('Auth.User.Doador.pessoa_id') ?>
		<?php if ( ($doacao['Doacao']['status'] == 'A') && ($doacao['Doacao']['periodo_id'] == 1) && $parcela['FormasPagamento']['tipo'] !== 'B'  ) : ?>
			<?php echo $this->Form->create(null, array('data-abide')) ?>
			<?php echo $this->Form->hidden('Doacao.id') ?>
			<?php echo $this->Form->hidden('Pagamento.id') ?>
			<?php echo $this->Form->hidden('Doacao.periodo_id', array('id' => 'tipo-doacao')); ?>

			<div class="small-12 medium-6 columns">
				<fieldset>
					<legend><?php echo  __('Alterar Valor de Doação') ?></legend>
					<?php echo $this->Form->input('Doacao.valor', array('type' => 'text', 'maxlength' => '15' ,'data-abide-validator' => 'testaValorDoado')) ?>
					<?php echo $this->Html->tag('small', 'O valor mínimo para esta doação é de R$20,00', array('class' => 'error')) ?>
				</fieldset>
			</div>
			<div class="small-12 medium-6 columns" id="insertHandler">
				<fieldset>
					<legend><?php echo __('Alterar Forma de Pagamento') ?></legend>
					<?php echo $this->Form->input('alterar_forma_pagamento',
						array(
							'type'  => 'checkbox',
							'id'    => 'alterarFormaPagamento',
							'label' => __('Desejo Alterar a forma de Pagamento')
						)
					) ?>
				</fieldset>
			</div>

			<?php echo $this->Form->end( array('label' => __('Salvar Dados'), 'class' => 'button right show-for-medium-up') ) ?>
			<?php echo $this->Form->end( array('label' => __('Salvar Dados'), 'class' => 'button right expand show-for-small-only') ) ?>

		<?php elseif ( $doacao['Doacao']['status'] == 'A' && $doacao['Doacao']['periodo_id'] == 1 && $tipo_pessoa == 2 && $parcela['FormasPagamento']['tipo'] == 'B' ): ?>
		<?php if ($this->data['Pagamento']['formas_pagamento_id'] == 1): ?>
		<div class="clearfix">
			<div class="small-12 column">
				&nbsp;
			</div>
		</div>
		<div class="row panel">
			<?php echo $this->Form->create('Doacoes', array('url' => '/alterar_boletos')) ?>
				<h3><?php echo __('Alterar Vencimentos') ?></h3>
				<?php //foreach (array_reverse($parcelas, true) as $parcela): ?>
				<?php for ($i=0; $i < count($parcelas); $i++) : ?>
				<?php $p = $parcelas[$i]; ?>
				<?php if ($p['Parcela']['status_id'] == 6): ?>
					<div class="small-6 medium-4 columns">
						<fieldset style="background-color:whitesmoke; box-shadow: 0 6px 12px rgba(0,0,0,.04);">
							<legend><?php echo __('Parcela') . ' ' . $p['Parcela']['parcela'] ?></legend>
							<?php echo $this->Form->hidden('Parcela.'.$i.'.id'); ?>
							<?php echo $this->Form->input('Parcela.'.$i.'.data_pagamento', ['type' => 'text', 'value' => $this->Formatacao->data($p['Parcela']['data_pagamento']), 'class' => 'datepicker']); ?>
							<?php echo $this->Form->hidden(null, ['value' => $this->Formatacao->data($p['Parcela']['data_pagamento'])]); ?>
						</fieldset>
					</div>
				<?php endif ?>
				<?php endfor ?>
				<?php echo $this->Form->end( array('label' => __('Salvar Dados'), 'class' => 'button right show-for-medium-up') ) ?>
				<?php echo $this->Form->end( array('label' => __('Salvar Dados'), 'class' => 'button right expand show-for-small-only') ) ?>
		</div>
		<?php endif ?>
		
		<?php elseif (($doacao['Doacao']['status'] == 'A') && ($doacao['Doacao']['periodo_id'] == 2) && ($parcela['Parcela']['status_id'] == '6') && ($parcela['FormasPagamento']['tipo'] == 'B')) : ?>
			<?php echo $this->Form->create(null, array('url' => array('controller' => 'doacoes', 'action' => 'alterar_boleto'), 'data-abide')) ?>
				<?php echo $this->Form->hidden('Parcela.id', array('value' => $parcela['Parcela']['id'])) ?>
				<input name="formas_pagamento_id" type="radio" class="hide" value="1">
				<div class="small-12 column">
					<fieldset>
						<legend><?php echo __('Gerar Segunda via de Boleto') ?></legend>
						<label for="data-vencimento"><?= __('Vencimento')?></label>
						<?php echo $this->Form->input('Parcela.data_pagamento', array('type' => 'text', 'class' => 'datepicker', 'id' => 'data-vencimento', 'placeholder' => 'Data', 'required' => true)) ?>
						<small class="error"><?= __('Campo Obrigatório') ?></small>
						<button class="button expand success" id="gerar_boleto" type="submit">
							<?= __('Emitir Boleto')?>
							<i class="sprite sprite-boleto-branco" style="vertical-align:sub"></i>
						</button>
					</fieldset>
				</div>
			<?php echo $this->Form->end() ?>
		<?php endif; ?>
	</div>
</section>
-->
<!--
<div class="small-12 column hide" id="dadosPagamento">
	<fieldset>
		<legend>
			<?php echo  __('Atualizar dados Bancários') ?>
			<?php if ($credito): ?>
				<span class="info radius label">Cartão atual: XXXX XXXX XXXX <?php echo $doacao['Pagamento']['cartao_final'] ?></span>
			<?php endif ?>
		</legend>
		<div class="row">
			<div class="small-12 columns">
				<h3><?= __('Dados Financeiros')?></h3>
			</div>
			<div id="formasPagamento">
				<div class="small-12 medium-6 columns">
					<input required type="radio" name="formaPagamento" id="credito" value="credito" <?php if($credito === true) echo 'checked' ?>>
					<label for="credito">
						<i class="sprite-credito"></i>&nbsp;<?= __('Cartão de Credito')?>
					</label>
				</div>
				<div class="clearfix">&nbsp;</div>
				<div class="small-12 medium-6 columns">
					<input required type="radio" name="formaPagamento" id="debito" value="debito" <?php if($debito === true) echo 'checked' ?>>
					<label for="debito">
						<i class="sprite-debito"></i>&nbsp;<?= __('Débito em conta')?>
					</label>
				</div>
				<div class="clearfix">&nbsp;</div>
			</div>
		</div>

		<article class="formaPagamento" id="credito">
			<div class="row">
				<div class="small-12 columns">
					<ul class="small-block-grid-1 medium-block-grid-<?php echo $grid_credito; ?>">
						<?php foreach ($pagamentoCredito as $credito): ?>
							<li>
								<input name="formas_pagamento_id" value="<?php echo $credito['FormasPagamento']['id'] ?>" type="radio" data-flag="<?php echo strtolower($credito['FormasPagamento']['nome']) ?>" class="flag flag-<?php echo strtolower($credito['FormasPagamento']['nome']) ?>" style="opacity: 0.5">
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>

			<?= $this->Form->input('Pagamento.formas_pagamento_id', array('type' => 'hidden', 'id' => 'formasPagamentoId')) ?>

			<div class="row">
				<div class="small-6 columns end">
					<?php echo $this->Form->input('Pagamento.cartaoCreditoNome', array('placeholder' => __('Nome como está no cartão'))) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
			</div>
			<div class="row">
				<div class="small-6 columns">
					<?php echo $this->Form->input('Pagamento.cartaoCreditoNum', array('placeholder' => __('Núm. do cartão'), 'id' => 'cartaoCreditoNum', 'pattern' => 'number', 'required' => true, 'maxlength' => 16)) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('Pagamento.cartaoCreditoCod', array('placeholder' => __('Cód. de segurança'), 'id' => 'cartaoCreditoCod', 'required' => true, 'pattern' => 'cvv')) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
				<div class="small-2 columns"><i class="sprite-cod-seguranca"></i></div>
			</div>

			<div class="row">
				<div class="small-6 columns">
					<label for="validade-mes"><?= __('Validade')?></label>
					<?php echo $this->Form->select('Pagamento.validade_mes', $this->Geral->getMeses(), array('id' => 'validade-mes', 'required' => true, 'empty' => false)) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
				<div class="small-6 columns">
					<label for="validade-ano"><?= __('Ano')?></label>
					<?php echo $this->Form->select('Pagamento.validade_ano', $this->Geral->getAnos(), array('id' => 'validade-ano', 'required' => true, 'empty' => false)) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
			</div>
		</article>

		<article class="formaPagamento hide" id="debito">
			<div class="row">
				<div class="small-12 columns">
					<ul class="small-block-grid-1">
						<ul class="small-block-grid-1 medium-block-grid-<?php echo $grid_debito; ?>">
						<?php foreach ($pagamentoDebito as $debito): ?>
							<li>
								<input name="formas_pagamento_id" value="<?php echo $debito['FormasPagamento']['id'] ?>" type="radio" data-flag="<?php echo strtolower(str_replace('DÉBITO ', '', $debito['FormasPagamento']['nome'])) ?>" class="flag flag-<?php echo strtolower(str_replace('DÉBITO ', '', $debito['FormasPagamento']['nome'])) ?>" style="opacity: 0.5">
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-6 columns end">
					<?php echo $this->Form->select('Pagamento.dia_debito_id', $dias, array('id' => 'data-debito', 'empty' => __('Melhor dia para Débito'))) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<?php echo $this->Form->input('Pagamento.agencia', array('placeholder' => __('Agência'), 'id' => 'debitoAgencia')) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
				<div class="small-12 medium-6 columns">
					<?php echo $this->Form->input('Pagamento.conta', array('placeholder' => __('Conta'), 'id' => 'debitoConta')) ?>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>
			</div>
		</article>
	</fieldset>
	<div class="row">
		<div class="small-12 column">
		</div>
	</div>
</div>

<?php if ($this->data['Pagamento']['formas_pagamento_id'] == 1): ?>
	<?php echo $this->Html->scriptBlock(
	"$(document).ready(function() {
		// Use the picker object directly.
		var \$input = $('.datepicker').pickadate()
		, pickerInstance = new Object()
		, pickerInitial = new String();

		// console.log(\$input);

		\$input.each(function(index, el) {
			// console.log(\$(el));
			pickerInitial = \$(el).siblings('input[type=hidden][id*=DataPagamento]').val();
			// console.log(pickerInitial);
			pickerInstance = \$(el).pickadate('picker');
			pickerInstance.set('select', pickerInitial, { format: 'dd/mm/yyyy' });
			\$(el).siblings('input[type=hidden][id*=DataPagamento]').remove();
		});
	});",
	['inline' => false]); ?>
<?php endif ?>
-->