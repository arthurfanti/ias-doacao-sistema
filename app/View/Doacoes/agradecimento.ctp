<?php //se for boleto exibe o link de impressão do boleto ?>
<?php if (isset($doacao['Pagamento']) && ($doacao['Pagamento']['formas_pagamento_id'] == '1') && !$this->Session->check('boleto')) : ?>
	<div data-alert class="alert-box alert" style="width:100%;">
		<div class="row small-collapse">
			<div class="small-12 column">
				<p><?= __('Para concluir sua doação, imprima e pague o boleto abaixo.') ?></p>

				<p>
				<?php
					echo
					$this->Html->link('Imprimir boleto', '/boleto/'.$doacao['Parcela'][0]['id'], array('target' => 'blank', 'class' => 'button success show-for-medium-up'))
					. $this->Html->link('Imprimir boleto', '/boleto/'.$doacao['Parcela'][0]['id'], array('target' => 'blank', 'class' => 'button success expand show-for-small-only'))
				?>
				</p>
			</div>
		</div>
		<a href="" class="close show-for-medium-up">&times;</a>
	</div>
	<script type="text/javascript">
	$(function() {
		window.open ($.url + "boleto/<?php echo $doacao['Parcela'][0]['id'] ?>", '_blank')
	});
	</script>
<?php endif ?>

<section role="agradecimento">
	 <div data-alert class="alert-box info">
		<div class="row small-collapse">
			<div class="small-10 medium-8 small-centered columns text-center">
				<p class="lead">
					<?php
						echo ($this->Session->read('Auth.User.Doador.nome') != '') ? $this->Session->read('Auth.User.Doador.nome') : $this->Session->read('Auth.User.Doador.sobrenome');
						echo ", " . $mensagem['Mensagem']['descricao'];
					?>
				</p>
				<?= $this->Html->link(
					__('Minha Conta') . ' ' . $this->Html->tag('i', '', array('class' => 'show-for-medium-up sprite-minha-conta')),
					array(
						'controller' => 'doadores',
						'action'     => 'conta'
					),
					array(
						'class' => 'button info',
						'style' => 'border:2px solid white',
						'escape' => false
					)
				) ?>
			</div>
		</div>

		<div class="row small-collapse hide">
			<div class="small-10 medium-4 small-centered columns text-center">
				<small><?= __('Conte para seus amigos que você é um doador do Instituto Ayrton Senna. Compartilhe essa ideia:')?></small>
				<div class="clearfix">&nbsp;</div>
				<ul class="small-block-grid-3">
					<li>
						<?php
							echo $this->SocialMedia->facebook(
								$this->Html->tag('i', '', array('class' => 'sprite-facebook')),
								array(
									'link'        => $this->Html->url('/', true),
									'name'        => 'Instituto Ayrton Senna',
									'caption'     => __('Doe agora'),
									'picture'     => $this->Html->url('/images/favicon-c.png', true),
									'description' => $shareMsg
								),
								array('escape' => false, 'target' => 'blank')
							);
						?>
					</li>
					<li>
						<?php
							echo $this->SocialMedia->twitter(
								$this->Html->tag('i', '', array('class' => 'sprite-twitter')),
								array(
									'url'  => $this->Html->url('/', true),
									'via'  => 'instayrtonsenna',
									'text' => $shareMsg
								),
								array('escape' => false, 'target' => 'blank')
							);
						?>
					</li>
					<!-- <li><a href="#" class="sprite-google"></a></li> -->
					<li><a data-reveal-id="modal-indicacao" href="#" class="sprite-email"></a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Modal indicação por e-mail -->
	<?php echo $this->element('modal-indicacao'); ?>

</section>