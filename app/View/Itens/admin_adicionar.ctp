<?php echo $this->Form->create(array('type' => 'file')) ?>
<?php echo $this->Form->input('item', array('label' => 'Nome')) ?>
<?php echo $this->Form->input('promocao_id', array('label' => 'Promoção')) ?>
<?php echo $this->Form->input('imagem', array('type' => 'file', 'label' => 'Imagem', 'div' => 'panel')) ?>
<?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => 'Status')) ?>
<div class="row">
	<fieldset>
	    <legend>Atributo 1</legend>
		<?php echo $this->Form->input('Atributo.0.atributo', ['label' => 'Atributo']) ?>
		<?php echo $this->Form->input('Atributo.0.Conteudo.0.conteudo', ['label' => 'Valor 1', 'div' => 'large-3 columns']) ?>
		<?php echo $this->Form->input('Atributo.0.Conteudo.1.conteudo', ['label' => 'Valor 2', 'div' => 'large-3 columns']) ?>
		<?php echo $this->Form->input('Atributo.0.Conteudo.2.conteudo', ['label' => 'Valor 3', 'div' => 'large-3 columns']) ?>
		<?php echo $this->Form->input('Atributo.0.Conteudo.3.conteudo', ['label' => 'Valor 4', 'div' => 'large-3 columns']) ?>
	</fieldset>
</div>
<div class="row">
	<fieldset>
	    <legend>Atributo 2</legend>
		<?php echo $this->Form->input('Atributo.1.atributo', ['label' => 'Atributo']) ?>
		<?php echo $this->Form->input('Atributo.1.Conteudo.0.conteudo', ['label' => 'Valor 1', 'div' => 'large-3 columns']) ?>
		<?php echo $this->Form->input('Atributo.1.Conteudo.1.conteudo', ['label' => 'Valor 2', 'div' => 'large-3 columns']) ?>
		<?php echo $this->Form->input('Atributo.1.Conteudo.2.conteudo', ['label' => 'Valor 3', 'div' => 'large-3 columns']) ?>
		<?php echo $this->Form->input('Atributo.1.Conteudo.3.conteudo', ['label' => 'Valor 4', 'div' => 'large-3 columns']) ?>
	</fieldset>
</div>
<?php echo $this->Form->submit('Salvar', array('class' => 'button right')) ?>
<?php echo $this->Form->end() ?>