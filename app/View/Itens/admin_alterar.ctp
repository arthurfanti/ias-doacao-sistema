<?php echo $this->Form->create(array('type' => 'file')) ?>
<?php echo $this->Form->input('id') ?>
<?php echo $this->Form->input('item', array('label' => 'Nome')) ?>
<?php echo $this->Form->input('promocao_id', array('label' => 'Promoção')) ?>
<?php echo $this->Form->input('imagem', array('type' => 'file', 'label' => 'Imagem')) ?>
<?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => 'Status')) ?>
<div class="row">
	<fieldset>
	    <legend>Atributo 1</legend>
	    <?php if (isset($atributos[0]['Atributo']['atributo'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.0.id', ['value' => $atributos[0]['Atributo']['id']]) ?>
	    <?php endif ?>
	    <?php if (isset($atributos[0]['Conteudo'][0]['id'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.0.Conteudo.0.id', ['value' => $atributos[0]['Conteudo'][0]['id']]) ?>
	    <?php endif ?>
	    <?php if (isset($atributos[0]['Conteudo'][1]['id'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.0.Conteudo.1.id', ['value' => $atributos[0]['Conteudo'][1]['id']]) ?>
	    <?php endif ?>
	    <?php if (isset($atributos[0]['Conteudo'][2]['id'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.0.Conteudo.2.id', ['value' => $atributos[0]['Conteudo'][2]['id']]) ?>
	    <?php endif ?>
	    <?php if (isset($atributos[0]['Conteudo'][3]['id'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.0.Conteudo.3.id', ['value' => $atributos[0]['Conteudo'][3]['id']]) ?>
	    <?php endif ?>
		<?php echo $this->Form->input('Atributo.0.atributo', ['type' => 'text', 'label' => 'Atributo', 'value' => isset($atributos[0]['Atributo']['atributo']) ? $atributos[0]['Atributo']['atributo'] : '']) ?>
		<?php echo $this->Form->input('Atributo.0.Conteudo.0.conteudo', ['type' => 'text', 'label' => 'Valor 1', 'div' => 'large-3 columns', 'value' => isset($atributos[0]['Conteudo'][0]['conteudo']) ? $atributos[0]['Conteudo'][0]['conteudo'] : '']) ?>
		<?php echo $this->Form->input('Atributo.0.Conteudo.1.conteudo', ['type' => 'text', 'label' => 'Valor 2', 'div' => 'large-3 columns', 'value' => isset($atributos[0]['Conteudo'][1]['conteudo']) ? $atributos[0]['Conteudo'][1]['conteudo'] : '']) ?>
		<?php echo $this->Form->input('Atributo.0.Conteudo.2.conteudo', ['type' => 'text', 'label' => 'Valor 3', 'div' => 'large-3 columns', 'value' => isset($atributos[0]['Conteudo'][2]['conteudo']) ? $atributos[0]['Conteudo'][2]['conteudo'] : '']) ?>
		<?php echo $this->Form->input('Atributo.0.Conteudo.3.conteudo', ['type' => 'text', 'label' => 'Valor 4', 'div' => 'large-3 columns', 'value' => isset($atributos[0]['Conteudo'][3]['conteudo']) ? $atributos[0]['Conteudo'][3]['conteudo'] : '']) ?>
	</fieldset>
</div>
<div class="row">
	<fieldset>
	    <legend>Atributo 2</legend>
	    <?php if (isset($atributos[1]['Atributo']['atributo'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.1.id', ['value' => $atributos[1]['Atributo']['id']]) ?>
	    <?php endif ?>
	    <?php if (isset($atributos[1]['Conteudo'][0]['id'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.1.Conteudo.0.id', ['value' => $atributos[1]['Conteudo'][0]['id']]) ?>
	    <?php endif ?>
	    <?php if (isset($atributos[1]['Conteudo'][1]['id'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.1.Conteudo.1.id', ['value' => $atributos[1]['Conteudo'][1]['id']]) ?>
	    <?php endif ?>
	    <?php if (isset($atributos[1]['Conteudo'][2]['id'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.1.Conteudo.2.id', ['value' => $atributos[1]['Conteudo'][2]['id']]) ?>
	    <?php endif ?>
	    <?php if (isset($atributos[1]['Conteudo'][3]['id'])) : ?>
	    	<?php echo $this->Form->hidden('Atributo.1.Conteudo.3.id', ['value' => $atributos[1]['Conteudo'][3]['id']]) ?>
	    <?php endif ?>
		<?php echo $this->Form->input('Atributo.1.atributo', ['type' => 'text', 'label' => 'Atributo', 'value' => isset($atributos[1]['Atributo']['atributo']) ? $atributos[1]['Atributo']['atributo'] : '']) ?>
		<?php echo $this->Form->input('Atributo.1.Conteudo.0.conteudo', ['type' => 'text', 'label' => 'Valor 1', 'div' => 'large-3 columns', 'value' => isset($atributos[1]['Conteudo'][0]['conteudo']) ? $atributos[1]['Conteudo'][0]['conteudo'] : '']) ?>
		<?php echo $this->Form->input('Atributo.1.Conteudo.1.conteudo', ['type' => 'text', 'label' => 'Valor 2', 'div' => 'large-3 columns', 'value' => isset($atributos[1]['Conteudo'][1]['conteudo']) ? $atributos[1]['Conteudo'][1]['conteudo'] : '']) ?>
		<?php echo $this->Form->input('Atributo.1.Conteudo.2.conteudo', ['type' => 'text', 'label' => 'Valor 3', 'div' => 'large-3 columns', 'value' => isset($atributos[1]['Conteudo'][2]['conteudo']) ? $atributos[1]['Conteudo'][2]['conteudo'] : '']) ?>
		<?php echo $this->Form->input('Atributo.1.Conteudo.3.conteudo', ['type' => 'text', 'label' => 'Valor 4', 'div' => 'large-3 columns', 'value' => isset($atributos[1]['Conteudo'][3]['conteudo']) ? $atributos[1]['Conteudo'][3]['conteudo'] : '']) ?>
	</fieldset>
</div>
<?php echo $this->Form->submit('Salvar') ?>
<?php echo $this->Form->end() ?>