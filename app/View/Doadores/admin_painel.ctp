<?php echo $this->Html->link('Nova doação', array('controller' => 'doacoes', 'action' => 'adicionar', $doador_id), array('class' => 'button')) ?>

<ul class="tabs" data-tab role="tablist">
  <li class="tab-title active" role="presentational" >
    <a href="#panel2-1" role="tab" tabindex="0" aria-selected="true" controls="panel2-1">Histórico de doações</a>
  </li>
  <li class="tab-title" role="presentational" >
    <a href="#panel2-2" role="tab" tabindex="0"aria-selected="false" controls="panel2-2">Atividades da conta</a>
  </li>
  <li class="tab-title" role="presentational">
    <a href="#panel2-3" role="tab" tabindex="0" aria-selected="false" controls="panel2-3">Emails Transacionais</a>
  </li>
</ul>
<div class="tabs-content">
  <section role="tabpanel" aria-hidden="false" class="content active" id="panel2-1">
    <table id="my-account-table" class="display responsive nowrap">
        <thead>
            <tr>
              <th></th>
                <th><?php echo __('Data do Cadastro') ?></th>
                <th><?php echo __('Valor') ?></th>
                <th><?php echo __('Frequência') ?></th>
                <th class="desktop"><?php echo __('Status') ?></th>
                <th><?php echo __('Forma de Pagamento') ?></th>
                <!--<th><?php echo __('Pagamento') ?></th>-->
                <th class="desktop">Ações</th>
            </tr>
        </thead>
        <tbody>
          <?php foreach ($doacoes as $doacao) : ?>
          <tr>
            <td></td>
                <td><?php echo $this->Formatacao->data($doacao['Doacao']['created']) ?></td>
                <td><?php echo $this->Formatacao->moeda($doacao['Doacao']['valor']) ?></td>
                <td><?php echo __($doacao['Periodo']['tipo']) ?></td>
                <td class="desktop"><?php echo ($doacao['Doacao']['status'] == 'A') ? __('Ativa') : __('Cancelada') ?></td>
                <td>
                  <?php echo __($this->Geral->getTipoFormaPagamento($doacao['Pagamento']['FormasPagamento']['tipo'])) ?>
                  <?php if ( $this->Geral->getTipoFormaPagamento($doacao['Pagamento']['FormasPagamento']['tipo']) === 'Crédito'  ): ?>
                    &nbsp;<span class="secondary radius label">Cartão XXX XXX XXX <?php echo $doacao['Pagamento']['cartao_final'] ?></span>
                  <?php endif ?>
                </td>
                <!-- <td><?php // echo $this->Geral->getStatusPagamento($doacao['Parcela']['status_id']) ?></td> -->
                <td class="desktop">
                  <?php echo $this->Html->link(
                    $this->Html->tag('span', __('Editar'), array('class' => 'label radius info')),
                    array('controller' => 'doacoes', 'action' => 'alterar', $doacao['Doacao']['id']),
                    array('escape' => false)
                  ); ?>
                  <?php echo $this->Html->link(
                    $this->Html->tag('span', __('Painel'), array('class' => 'label radius info')),
                    array('controller' => 'doacoes', 'action' => 'painel', $doacao['Doacao']['id']),
                    array('escape' => false)
                  ); ?>
                </td>
            </tr>
          <?php endforeach; ?>
            <!-- <tr>
                <td>01/05/15</td>
                <td>R$20,00</td>
                <td>Única</td>
                <td>Efetuado</td>
                <td>Boleto</td>
                <td>Em aberto</td>
                <td><a href="#">+</a></td>
            </tr> -->
        </tbody>
    </table>
  </section>
  <section role="tabpanel" aria-hidden="true" class="content" id="panel2-2">
    <table>
      <thead>
        <tr>
          <th>Data</th>
          <th>Usuário</th>
          <th>Campo</th>
          <th>Valor anterior</th>
          <th>Valor novo</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($logs as $log) : ?>
        <tr>
          <td><?php echo $this->Formatacao->data($log['LogAction']['created']) ?></td>
          <td>
            <?php
            if (isset($log['User'])) {
              echo ($log['User']['is_admin']) ? $log['User']['username'] : 'DOADOR';
            } else {
              echo 'DOADOR';
            }
            ?>
          </td>
          <td><?php echo $log['LogAction']['field'] ?></td>
          <td><?php echo $log['LogAction']['before'] ?></td>
          <td><?php echo $log['LogAction']['after'] ?></td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </section>
  <section role="tabpanel" aria-hidden="true" class="content" id="panel2-3">
    <h2>Third panel content goes here...</h2>
  </section>
</div>