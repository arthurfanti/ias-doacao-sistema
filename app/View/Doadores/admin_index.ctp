
<div class="row">
	<div class="small-12 columns">
			<?php echo $this->Form->create() ?>
				<fieldset>
					<legend>Filtros:</legend>
					<div style="position:relative">
						<!-- <div class="row">
							<div class="small-6 columns">
								<select name="show_by" id="show_by">
									<option value="period">Exibir por período de tempo</option>
									<option value="day">Nas últimas 24 horas</option>
								</select>
							</div>

							<div class="small-6 columns">
								<ul class="button-group">
									<li class="button tiny secondary disabled">Exportar:</li>
									<li><a href="#" class="button tiny">CSV</a></li>
									<li><a href="#" class="button tiny">XLS</a></li>
									<li><a href="#" class="button tiny">PDF</a></li>
								</ul>
							</div>
						</div> -->

						<div class="row">
							<div class="small-5 columns">
								<label for="">Valor</label>
								<?php echo $this->Form->input('valor') ?>
							</div>
							<div class="small-5 columns">
								<!-- <label for="">Até:</label>
								<input type="text" class="datepicker" placeholder="00/00/0000"> -->
								<label for="">Filtro</label>
								<?php echo $this->Form->select('filtro', [0 => 'NOME', 1 => 'CPF/CNPJ'], ['empty' => false]) ?>
							</div>
							<div class="small-2 column">
								<button type="submit" class="button tiny expand" style="margin-top:1.35rem">
									<i class="fa fa-filter">&nbsp;</i>Filtrar
								</button>
							</div>
						</div>

						<!-- <div style="position:absolute;width:100%;height:100%;background:white;opacity:.5;top:0"></div> -->
					</div>
				</fieldset>
			<?php echo $this->Form->end() ?>
	</div>
</div>

<article id="list">
	<div class="row">
		<div class="small-12 column">
			<table width="100%" id="datatable" class="display responsive">
				<thead>
					<tr>
						<th></th>
						<th width="20%">Nome</th>
						<th>CPF/CNPJ</th>
						<th>Conta no Brasil</th>
						<th>Tipo de doador</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th width="20%">Nome</th>
						<th>CPF</th>
						<th>Conta no Brasil</th>
						<th>Tipo de doador</th>
						<th>Ações</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($data as $doador): ?>
					<tr>
						<td></td>
						<td width="20%"><?php echo $doador['Doador']['nome'] . ' ' . $doador['Doador']['sobrenome'] ?></td>
						<td><?php echo $doador['Doador']['cpf_cnpj'] ?></td>
						<td><?php echo ($doador['Doador']['conta_brasil']) ? 'Sim' : 'Não' ?></td>
						<td><?php echo $doador['Pessoa']['tipo'] ?></td>
						<td>
							<?php echo $this->Html->link(
								$this->Html->tag('i', '', array('class' => 'fa fa-pencil-square-o')) . ' Editar',
								array('action' => 'alterar', $doador['Doador']['id']),
								array(
									'class' => 'button tiny radius',
									'style' => 'margin-bottom:0',
									'escape' => false
								)); ?>
							<?php echo $this->Html->link(
								$this->Html->tag('i', '', array('class' => 'fa fa-plus-square')) . ' Painel',
								array('action' => 'painel', $doador['Doador']['id']),
								array(
									'class' => 'button tiny radius info',
									'style' => 'margin-bottom:0',
									'escape' => false
								)); ?>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</article>
<?php
	echo $this->Html->scriptBlock(
		"$('table.responsive').DataTable({
			responsive: {
				details: {
					type: 'inline',
					target: 'tr'
				}
			},
			columnDefs: [{
				className: 'control',
				orderable: false,
				targets: 0
			}],
			order: [1, 'asc'],
			language: {
				processing:     'processando dados...',
				search:         'Pesquisar',
				lengthMenu:     'Itens por p&aacute;gina: _MENU_',
				info:           'Exibindo itens de _START_ a _END_, num total de _TOTAL_ itens',
				infoEmpty:      'Não há itens para exibir',
				infoFiltered:   '(filtrado de _MAX_ itens no total)',
				infoPostFix:    '',
				loadingRecords: 'carregando dados...',
				zeroRecords:    'Não há itens para exibir',
				paginate: {
					first:      'Primeira',
					previous:   'Anterior',
					next:       'Seguinte',
					last:       '&uacute;ltima'
				},
				aria: {
					sortAscending:  ': habilite para classificar a coluna em ordem crescente',
					sortDescending: ': habilite para classificar a coluna em ordem decrescente'
				}
			}
		});

		$('.dataTables_filter input').attr('placeholder', 'Buscar');",
		array('inline' => false));
?>