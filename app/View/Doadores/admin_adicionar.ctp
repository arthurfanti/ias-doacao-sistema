<?php if ($this->request['action'] != 'admin_alterar') : ?>
<div class="right" id="switch">
	<h6 class="left" style="margin:0; line-height:2.5">ALTERAR PARA CADASTRO DPJ&nbsp;</h6>
	<div class="switch medium right">
		<input id="tipoPessoaSwitch" type="checkbox">
		<label for="tipoPessoaSwitch"></label>
	</div>
</div>
<?php endif ?>

<?php echo $this->Form->create('Doador', ['data-abide']); ?>
	<div id="formDpf" role="form">
		<div class="row">
			<div class="panel small-12 columns">
				<span>Após concluir o cadastro, navegue para <?php echo $this->Html->link('Esqueci minha Senha', $this->Html->url('/nova-senha')); ?> para que uma nova senha seja enviada ao email do doador.</span>
			</div>
		</div>

		<fieldset>
			<legend>Opções</legend>
			<div class="row">
				<div class="small-12 column">
					<?php echo $this->Form->input(
						'conta_brasil',
						['type' => 'checkbox', 'label' => 'Conta no Brasil', 'checked']
					); ?>
					<?php echo $this->Form->input(
						'recebe_email',
						['type' => 'checkbox', 'label' => 'Aceita receber email', 'checked']
					); ?>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Dados Pessoais</legend>
			<div class="row">
				<div class="small-6 columns">
					<?php echo $this->Form->input('nome', ['label' => 'Nome', 'pattern' => '[^0-9]+', 'required']); ?>
					<small class="error">Campo Obrigatório</small>
				</div>
				<div class="small-6 columns">
					<?php echo $this->Form->input('sobrenome', ['label' => 'Sobrenome', 'pattern' => '[^0-9]+', 'required']); ?>
					<small class="error">Campo Obrigatório</small>
				</div>
			</div>

			<div class="row">
				<div class="small-6 columns">
					<?php echo $this->Form->hidden('DoadorEmail.0.email_id', array('value' => 1)) ?>
					<?php echo $this->Form->input('DoadorEmail.0.email', ['label' => 'email', 'pattern' => 'email', 'required']) ?>
					<small class="error">Campo Obrigatório</small>
				</div>
				<div class="small-3 columns">
					<?php echo $this->Form->input('DoadorTelefone.0.numero', ['label' => 'Telefone', 'type' => 'tel']); ?>
					<?php echo $this->Form->hidden('DoadorTelefone.0.telefone_id', ['value' => 1]); ?>
				</div>
				<div class="small-3 columns">
					<?php echo $this->Form->input('sexo', ['label' => 'Sexo', 'type' => 'select', 'options' => ['1' => 'Masculino', '2' => 'Feminino']]) ?>
				</div>
			</div>

			<div class="row">
				<div class="small-4 columns">
					<?php $dataNascimento = isset($this->data['Doador']['nascimento']) ? $this->Formatacao->data($this->data['Doador']['nascimento']) : ''; ?>
					<?php echo $this->Form->input('nascimento', ['label' => 'Nascimento', 'placeholder' => 'Data de Nascimento', 'id' => 'nascimento', 'type' => 'text', 'data-abide-validator' => 'testaAnoAtual', 'value' => $dataNascimento]) ?>
					<small class="error">Campo Obrigatório</small>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('cpf_cnpj', ['label' => 'CPF', 'placeholder' => 'CPF', 'id' => 'doadorCpf', 'required', 'data-abide-validator' => 'testaCPF']) ?>
					<small class="error">CPF Inválido</small>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('rg', ['label' => 'RG']) ?>
					<small class="error">CPF Inválido</small>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Endereço</legend>
			<div class="row">
				<div class="small-2 columns end">
					<?php echo $this->Form->hidden('DoadorEndereco.0.id', ['value' => 1]) ?>
					<?php echo $this->Form->input('DoadorEndereco.0.cep', array('id' => 'cep', 'class' => 'cep', 'placeholder' => __('CEP'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
				<div class="small-6 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.logradouro', array('placeholder' => __('Logradouro'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>

				<div class="small-2 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.numero', array('placeholder' => __('Número'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
				<div class="small-2 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.complemento', array('placeholder' => __('Complemento'))) ?>
				</div>
			</div>

			<div class="row">
				<div class="small-3 columns">
					<select required name="data[Doador][nacionalidade]" id="residente">
						<option value="AF">Afeganistão (‫افغانستان‬‎)</option>
						<option value="ZA">África do Sul (South Africa)</option>
						<option value="AL">Albânia (Shqipëri)</option>
						<option value="DE">Alemanha (Deutschland)</option>
						<option value="AD">Andorra</option>
						<option value="AO">Angola</option>
						<option value="AI">Anguilla</option>
						<option value="AQ">Antártida (Antarctica)</option>
						<option value="AG">Antígua e Barbuda (Antigua and Barbuda)</option>
						<option value="SA">Arábia Saudita (‫المملكة العربية السعودية‬‎)</option>
						<option value="DZ">Argélia (‫الجزائر‬‎)</option>
						<option value="AR">Argentina</option>
						<option value="AM">Armênia (Հայաստան)</option>
						<option value="AW">Aruba</option>
						<option value="AU">Austrália (Australia)</option>
						<option value="AT">Áustria (Österreich)</option>
						<option value="AZ">Azerbaijão (Azərbaycan)</option>
						<option value="BS">Bahamas</option>
						<option value="BH">Bahrein (‫البحرين‬‎)</option>
						<option value="BD">Bangladesh (বাংলাদেশ)</option>
						<option value="BB">Barbados</option>
						<option value="BE">Bélgica (België)</option>
						<option value="BZ">Belize</option>
						<option value="BJ">Benin (Bénin)</option>
						<option value="BM">Bermudas (Bermuda)</option>
						<option value="BY">Bielorrússia (Беларусь)</option>
						<option value="BO">Bolívia (Bolivia)</option>
						<option value="BA">Bósnia e Herzegovina (Босна и Херцеговина)</option>
						<option value="BW">Botsuana (Botswana)</option>
						<option value="BR" selected>Brasil</option>
						<option value="BN">Brunei</option>
						<option value="BG">Bulgária (България)</option>
						<option value="BF">Burquina Faso (Burkina Faso)</option>
						<option value="BI">Burundi (Uburundi)</option>
						<option value="BT">Butão (འབྲུག)</option>
						<option value="CV">Cabo Verde (Kabu Verdi)</option>
						<option value="KH">Camboja (កម្ពុជា)</option>
						<option value="CA">Canadá (Canada)</option>
						<option value="QA">Catar (‫قطر‬‎)</option>
						<option value="KZ">Cazaquistão (Казахстан)</option>
						<option value="EA">Ceuta e Melilha (Ceuta y Melilla)</option>
						<option value="TD">Chade (Tchad)</option>
						<option value="CL">Chile</option>
						<option value="CN">China (中国)</option>
						<option value="CY">Chipre (Κύπρος)</option>
						<option value="VA">Cidade do Vaticano (Città del Vaticano)</option>
						<option value="SG">Cingapura (Singapore)</option>
						<option value="CO">Colômbia (Colombia)</option>
						<option value="KM">Comores (‫جزر القمر‬‎)</option>
						<option value="CG">Congo - Brazzaville (Congo-Brazzaville)</option>
						<option value="CD">Congo - Kinshasa (Jamhuri ya Kidemokrasia ya Kongo)</option>
						<option value="KP">Coreia do Norte (조선 민주주의 인민 공화국)</option>
						<option value="KR">Coreia do Sul (대한민국)</option>
						<option value="CI">Costa do Marfim (Côte d’Ivoire)</option>
						<option value="CR">Costa Rica</option>
						<option value="HR">Croácia (Hrvatska)</option>
						<option value="CU">Cuba</option>
						<option value="CW">Curaçao</option>
						<option value="DG">Diego Garcia</option>
						<option value="DK">Dinamarca (Danmark)</option>
						<option value="DJ">Djibuti (Djibouti)</option>
						<option value="DM">Dominica</option>
						<option value="EG">Egito (‫مصر‬‎)</option>
						<option value="SV">El Salvador</option>
						<option value="AE">Emirados Árabes Unidos (‫الإمارات العربية المتحدة‬‎)</option>
						<option value="EC">Equador (Ecuador)</option>
						<option value="ER">Eritreia (Eritrea)</option>
						<option value="SK">Eslováquia (Slovensko)</option>
						<option value="SI">Eslovênia (Slovenija)</option>
						<option value="ES">Espanha (España)</option>
						<option value="US">Estados Unidos (United States)</option>
						<option value="EE">Estônia (Eesti)</option>
						<option value="ET">Etiópia (Ethiopia)</option>
						<option value="FJ">Fiji</option>
						<option value="PH">Filipinas (Philippines)</option>
						<option value="FI">Finlândia (Suomi)</option>
						<option value="FR">França (France)</option>
						<option value="GA">Gabão (Gabon)</option>
						<option value="GM">Gâmbia (Gambia)</option>
						<option value="GH">Gana (Gaana)</option>
						<option value="GE">Geórgia (საქართველო)</option>
						<option value="GS">Geórgia do Sul e Ilhas Sandwich do Sul (South Georgia &amp; South Sandwich Islands)</option>
						<option value="GI">Gibraltar</option>
						<option value="GD">Granada (Grenada)</option>
						<option value="GR">Grécia (Ελλάδα)</option>
						<option value="GL">Groênlandia (Kalaallit Nunaat)</option>
						<option value="GP">Guadalupe (Guadeloupe)</option>
						<option value="GU">Guam</option>
						<option value="GT">Guatemala</option>
						<option value="GG">Guernsey</option>
						<option value="GY">Guiana (Guyana)</option>
						<option value="GF">Guiana Francesa (Guyane française)</option>
						<option value="GN">Guiné (Guinée)</option>
						<option value="GW">Guiné Bissau</option>
						<option value="GQ">Guiné Equatorial (Guinea Ecuatorial)</option>
						<option value="HT">Haiti</option>
						<option value="NL">Holanda (Nederland)</option>
						<option value="HN">Honduras</option>
						<option value="HK">Hong Kong (香港)</option>
						<option value="HU">Hungria (Magyarország)</option>
						<option value="YE">Iêmen (‫اليمن‬‎)</option>
						<option value="BV">Ilha Bouvet (Bouvet Island)</option>
						<option value="AC">Ilha de Ascensão (Ascension Island)</option>
						<option value="CP">Ilha de Clipperton (Clipperton Island)</option>
						<option value="IM">Ilha de Man (Isle of Man)</option>
						<option value="HM">Ilha Heard e Ilhas McDonald (Heard &amp; McDonald Islands)</option>
						<option value="NF">Ilha Norfolk (Norfolk Island)</option>
						<option value="AX">Ilhas Åland (Åland)</option>
						<option value="KY">Ilhas Caiman (Cayman Islands)</option>
						<option value="IC">Ilhas Canárias (islas Canarias)</option>
						<option value="CC">Ilhas Coco (Kepulauan Cocos (Keeling))</option>
						<option value="CK">Ilhas Cook (Cook Islands)</option>
						<option value="UM">Ilhas Distantes dos EUA (U.S. Outlying Islands)</option>
						<option value="FO">Ilhas Faroe (Føroyar)</option>
						<option value="FK">Ilhas Malvinas (Falkland Islands (Islas Malvinas))</option>
						<option value="MP">Ilhas Marianas do Norte (Northern Mariana Islands)</option>
						<option value="MH">Ilhas Marshall (Marshall Islands)</option>
						<option value="CX">Ilhas Natal (Christmas Island)</option>
						<option value="PN">Ilhas Pitcairn (Pitcairn Islands)</option>
						<option value="SB">Ilhas Salomão (Solomon Islands)</option>
						<option value="TC">Ilhas Turks e Caicos (Turks and Caicos Islands)</option>
						<option value="VG">Ilhas Virgens Britânicas (British Virgin Islands)</option>
						<option value="VI">Ilhas Virgens dos EUA (U.S. Virgin Islands)</option>
						<option value="IN">Índia (भारत)</option>
						<option value="ID">Indonésia (Indonesia)</option>
						<option value="IR">Irã (‫ایران‬‎)</option>
						<option value="IQ">Iraque (‫العراق‬‎)</option>
						<option value="IE">Irlanda (Ireland)</option>
						<option value="IS">Islândia (Ísland)</option>
						<option value="IL">Israel (‫ישראל‬‎)</option>
						<option value="IT">Itália (Italia)</option>
						<option value="JM">Jamaica</option>
						<option value="JP">Japão (日本)</option>
						<option value="JE">Jersey</option>
						<option value="JO">Jordânia (‫الأردن‬‎)</option>
						<option value="XK">Kosovo (Kosovë)</option>
						<option value="KW">Kuwait (‫الكويت‬‎)</option>
						<option value="LA">Laos (ລາວ)</option>
						<option value="LS">Lesoto (Lesotho)</option>
						<option value="LV">Letônia (Latvija)</option>
						<option value="LB">Líbano (‫لبنان‬‎)</option>
						<option value="LR">Libéria (Liberia)</option>
						<option value="LY">Líbia (‫ليبيا‬‎)</option>
						<option value="LI">Liechtenstein</option>
						<option value="LT">Lituânia (Lietuva)</option>
						<option value="LU">Luxemburgo (Luxembourg)</option>
						<option value="MO">Macau (澳門)</option>
						<option value="MK">Macedônia (Македонија)</option>
						<option value="MG">Madagascar (Madagasikara)</option>
						<option value="MY">Malásia (Malaysia)</option>
						<option value="MW">Malawi</option>
						<option value="MV">Maldivas (Maldives)</option>
						<option value="ML">Mali</option>
						<option value="MT">Malta</option>
						<option value="MA">Marrocos (‫المغرب‬‎)</option>
						<option value="MQ">Martinica (Martinique)</option>
						<option value="MU">Maurício (Moris)</option>
						<option value="MR">Mauritânia (‫موريتانيا‬‎)</option>
						<option value="YT">Mayotte</option>
						<option value="MX">México</option>
						<option value="MM">Mianmar (Birmânia) (မြန်မာ)</option>
						<option value="FM">Micronésia (Micronesia)</option>
						<option value="MZ">Moçambique</option>
						<option value="MD">Moldávia (Republica Moldova)</option>
						<option value="MC">Mônaco (Monaco)</option>
						<option value="MN">Mongólia (Монгол)</option>
						<option value="ME">Montenegro (Crna Gora)</option>
						<option value="MS">Montserrat</option>
						<option value="NA">Namíbia (Namibië)</option>
						<option value="NR">Nauru</option>
						<option value="NP">Nepal (नेपाल)</option>
						<option value="NI">Nicarágua (Nicaragua)</option>
						<option value="NE">Níger (Nijar)</option>
						<option value="NG">Nigéria (Nigeria)</option>
						<option value="NU">Niue</option>
						<option value="NO">Noruega (Norge)</option>
						<option value="NC">Nova Caledônia (Nouvelle-Calédonie)</option>
						<option value="NZ">Nova Zelândia (New Zealand)</option>
						<option value="OM">Omã (‫عُمان‬‎)</option>
						<option value="BQ">Países Baixos Caribenhos (Caribbean Netherlands)</option>
						<option value="PW">Palau</option>
						<option value="PS">Palestina (‫فلسطين‬‎)</option>
						<option value="PA">Panamá</option>
						<option value="PG">Papua-Nova Guiné (Papua New Guinea)</option>
						<option value="PK">Paquistão (‫پاکستان‬‎)</option>
						<option value="PY">Paraguai (Paraguay)</option>
						<option value="PE">Peru (Perú)</option>
						<option value="PF">Polinésia Francesa (Polynésie française)</option>
						<option value="PL">Polônia (Polska)</option>
						<option value="PR">Porto Rico (Puerto Rico)</option>
						<option value="PT">Portugal</option>
						<option value="KE">Quênia (Kenya)</option>
						<option value="KG">Quirguistão (Кыргызстан)</option>
						<option value="KI">Quiribati (Kiribati)</option>
						<option value="GB">Reino Unido (United Kingdom)</option>
						<option value="CF">República Centro-Africana (République centrafricaine)</option>
						<option value="DO">República Dominicana</option>
						<option value="CM">República dos Camarões (Cameroun)</option>
						<option value="CZ">República Tcheca (Česká republika)</option>
						<option value="RE">Reunião (La Réunion)</option>
						<option value="RO">Romênia (România)</option>
						<option value="RW">Ruanda (Rwanda)</option>
						<option value="RU">Rússia (Россия)</option>
						<option value="EH">Saara Ocidental (‫الصحراء الغربية‬‎)</option>
						<option value="PM">Saint Pierre e Miquelon (Saint-Pierre-et-Miquelon)</option>
						<option value="WS">Samoa</option>
						<option value="AS">Samoa Americana (American Samoa)</option>
						<option value="SM">San Marino</option>
						<option value="SH">Santa Helena (Saint Helena)</option>
						<option value="LC">Santa Lúcia (Saint Lucia)</option>
						<option value="BL">São Bartolomeu (Saint-Barthélemy)</option>
						<option value="KN">São Cristovão e Nevis (Saint Kitts and Nevis)</option>
						<option value="MF">São Martinho (Saint-Martin (partie française))</option>
						<option value="ST">São Tomé e Príncipe</option>
						<option value="VC">São Vicente e Granadinas (St. Vincent &amp; Grenadines)</option>
						<option value="SN">Senegal (Sénégal)</option>
						<option value="SL">Serra Leoa (Sierra Leone)</option>
						<option value="RS">Sérvia (Србија)</option>
						<option value="SC">Seychelles</option>
						<option value="SX">Sint Maarten</option>
						<option value="SY">Síria (‫سوريا‬‎)</option>
						<option value="SO">Somália (Soomaaliya)</option>
						<option value="LK">Sri Lanka (ශ්‍රී ලංකාව)</option>
						<option value="SZ">Suazilândia (Swaziland)</option>
						<option value="SD">Sudão (‫السودان‬‎)</option>
						<option value="SS">Sudão do Sul (‫جنوب السودان‬‎)</option>
						<option value="SE">Suécia (Sverige)</option>
						<option value="CH">Suíça (Schweiz)</option>
						<option value="SR">Suriname</option>
						<option value="SJ">Svalbard e Jan Mayen (Svalbard og Jan Mayen)</option>
						<option value="TJ">Tadjiquistão (Tajikistan)</option>
						<option value="TH">Tailândia (ไทย)</option>
						<option value="TW">Taiwan (台灣)</option>
						<option value="TZ">Tanzânia (Tanzania)</option>
						<option value="IO">Território Britânico do Oceano Índico (British Indian Ocean Territory)</option>
						<option value="TF">Territórios Franceses do Sul (Terres australes françaises)</option>
						<option value="TL">Timor-Leste</option>
						<option value="TG">Togo</option>
						<option value="TK">Tokelau</option>
						<option value="TO">Tonga</option>
						<option value="TT">Trinidad e Tobago (Trinidad and Tobago)</option>
						<option value="TA">Tristão da Cunha (Tristan da Cunha)</option>
						<option value="TN">Tunísia (‫تونس‬‎)</option>
						<option value="TM">Turcomenistão (Turkmenistan)</option>
						<option value="TR">Turquia (Türkiye)</option>
						<option value="TV">Tuvalu</option>
						<option value="UA">Ucrânia (Україна)</option>
						<option value="UG">Uganda</option>
						<option value="UY">Uruguai (Uruguay)</option>
						<option value="UZ">Uzbequistão (Oʻzbekiston)</option>
						<option value="VU">Vanuatu</option>
						<option value="VE">Venezuela</option>
						<option value="VN">Vietnã (Việt Nam)</option>
						<option value="WF">Wallis e Futuna (Wallis and Futuna)</option>
						<option value="ZM">Zâmbia (Zambia)</option>
						<option value="ZW">Zimbábue (Zimbabwe)</option>
					</select>
					<small class="error"><?= __('Selecione um país')?></small>
				</div>
				<div class="small-3 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.estado_id', array(
						'id'      => 'estado_id',
						'class'   => 'estado_id',
						'type'    => 'select',
						'options' => $options
					)) ?>
				</div>
				<div class="small-3 columns">
					<?= $this->Form->input('DoadorEndereco.0.cidade', array('id' => 'cidade', 'placeholder' => 'Cidade', 'disabled')) ?>
					<?= $this->Form->input('DoadorEndereco.0.cidade_id', array(
						'id'    => 'cidade_id',
						'class' => 'cidade_id',
						'style' => 'display:none'
					)) ?>
				</div>
				<div class="small-3 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.bairro', array('placeholder' => __('Bairro'))) ?>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Como Conheceu</legend>
			<div class="row">
				<div class="small-12 columns">
					<?php echo $this->Form->input('conheceu_id', ['id' => 'conheceu', 'empty' => 'Selecione']) ?>
				</div>
			</div>
		</fieldset>
		<?php echo $this->Form->hidden('pessoa_id', ['value' => 1]); ?>

		<div class="row collapse">
			<div class="small-12 column">
				<?php echo $this->Form->input('id', ['type' => 'hidden']) ?>
				<?php echo $this->Form->input('DoadorEmail.0.id', ['type' => 'hidden']) ?>
				<?php echo $this->Form->input('DoadorEndereco.0.id', ['type' => 'hidden']) ?>
				<?php echo $this->Form->input(
					$this->Html->tag('i', '', ['class' => 'fa fa-floppy-o']) . ' Salvar',
					['type' => 'button' , 'class' => 'button success large expand', 'escape' => false]);
				?>
			</div>
		</div>
	</div>
<?php echo $this->Form->end(); ?>

<?php echo $this->Form->create('Doador', ['data-abide']); ?>
<div id="formDpj" class="hide" id="formHolder">
	<div role="form">
		<div class="row">
			<div class="panel small-12 columns">
				<span>Após concluir o cadastro, navegue para <?php echo $this->Html->link('Esqueci minha Senha', $this->Html->url('/nova-senha')); ?> para que uma nova senha seja enviada ao email do doador.</span>
			</div>
		</div>

		<fieldset>
			<legend>Opções</legend>
			<div class="row">
				<div class="small-12 column">
					<?php echo $this->Form->input(
						'conta_brasil',
						['type' => 'checkbox', 'label' => 'Conta no Brasil', 'checked']
					); ?>
					<?php echo $this->Form->input(
						'recebe_email',
						['type' => 'checkbox', 'label' => 'Aceita receber email', 'checked']
					); ?>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Dados Cadastrais</legend>
			<div class="row">
				<div class="small-4 columns">
					<?php echo $this->Form->input('nome', ['label' => 'Nome Fantasia', 'pattern' => '[^0-9]+', 'required']); ?>
					<small class="error">Campo Obrigatório</small>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('sobrenome', ['label' => 'Razão Social', 'pattern' => '[^0-9]+', 'required']); ?>
					<small class="error">Campo Obrigatório</small>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('cpf_cnpj', ['label' => 'CNPJ', 'id' => 'doadorCnpj', 'required', 'data-abide-validator' => 'testaCNPJ']) ?>
					<small class="error">CNPJ Inválido</small>
				</div>
			</div>

			<div class="row">
				<div class="small-4 columns">
					<?php echo $this->Form->input('DpjComplemento.nome', ['label' => __('Nome do Contato'), 'required']); ?>
					<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
					<small class="error">Campo Obrigatório</small>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('DpjComplemento.cargo', ['label' => __('Cargo'), 'required']); ?>
					<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('DpjComplemento.sexo', ['label' => 'Sexo', 'type' => 'select', 'required', 'options' => ['1' => 'Masculino', '2' => 'Feminino']]) ?>
					<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
				</div>
			</div>

			<div class="row">
				<div class="small-6 columns">
					<?php echo $this->Form->hidden('DoadorTelefone.0.telefone_id', array('value' => 1)) ?>
					<?php echo $this->Form->input('DoadorTelefone.0.numero', array('label' => __('Telefone Principal'), 'type' => 'tel', 'required' => true, 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.') )) ?>
				</div>
				<div class="small-6 columns">
					<?php echo $this->Form->hidden('DoadorEmail.0.email_id', ['value' => '1']); ?>
					<?php echo $this->Form->input('DoadorEmail.0.email', ['label' => __('E-mail para contato e login'), 'type' => 'email', 'required']); ?>
					<small class="error"><?= __('Preencha com um email válido')?></small>
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-6 columns">
					<?php echo $this->Form->input('DpjComplemento.representante', ['placeholder' => __('Nome do Representante legal'), 'required']); ?>
					<small class="error"><?= __('Campo Obrigatório')?></small>
				</div>
				<div class="small-12 medium-6 columns">
					<?php echo $this->Form->hidden('DoadorEmail.1.email_id', ['value' => 2]); ?>
					<?php echo $this->Form->input('DoadorEmail.1.email', ['placeholder' => __('Email do Representante Legal'), 'type' => 'email', 'required']); ?>
					<small class="error"><?= __('Preencha com um email válido')?></small>
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-6 columns">
					<?php echo $this->Form->input('DpjComplemento.faixa_id', ['empty' => __('Faixa de faturamento anual')]); ?>
				</div>
				<div class="small-12 medium-6 columns">
					<?php echo $this->Form->input('DpjComplemento.natureza_id', ['empty' => __('Natureza Jurídica')]); ?>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Endereço</legend>
			<div class="row">
				<div class="small-2 columns end">
					<?php echo $this->Form->hidden('DoadorEndereco.0.id', ['value' => 1]) ?>
					<?php echo $this->Form->input('DoadorEndereco.0.cep', array('id' => 'cep', 'class' => 'cep', 'placeholder' => __('CEP'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
				<div class="small-6 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.logradouro', array('placeholder' => __('Logradouro'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>

				<div class="small-2 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.numero', array('placeholder' => __('Número'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
				<div class="small-2 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.complemento', array('placeholder' => __('Complemento'))) ?>
				</div>
			</div>

			<div class="row">
				<div class="small-3 columns">
					<select required name="data[Doador][nacionalidade]" id="residente">
						<option value="AF">Afeganistão (‫افغانستان‬‎)</option>
						<option value="ZA">África do Sul (South Africa)</option>
						<option value="AL">Albânia (Shqipëri)</option>
						<option value="DE">Alemanha (Deutschland)</option>
						<option value="AD">Andorra</option>
						<option value="AO">Angola</option>
						<option value="AI">Anguilla</option>
						<option value="AQ">Antártida (Antarctica)</option>
						<option value="AG">Antígua e Barbuda (Antigua and Barbuda)</option>
						<option value="SA">Arábia Saudita (‫المملكة العربية السعودية‬‎)</option>
						<option value="DZ">Argélia (‫الجزائر‬‎)</option>
						<option value="AR">Argentina</option>
						<option value="AM">Armênia (Հայաստան)</option>
						<option value="AW">Aruba</option>
						<option value="AU">Austrália (Australia)</option>
						<option value="AT">Áustria (Österreich)</option>
						<option value="AZ">Azerbaijão (Azərbaycan)</option>
						<option value="BS">Bahamas</option>
						<option value="BH">Bahrein (‫البحرين‬‎)</option>
						<option value="BD">Bangladesh (বাংলাদেশ)</option>
						<option value="BB">Barbados</option>
						<option value="BE">Bélgica (België)</option>
						<option value="BZ">Belize</option>
						<option value="BJ">Benin (Bénin)</option>
						<option value="BM">Bermudas (Bermuda)</option>
						<option value="BY">Bielorrússia (Беларусь)</option>
						<option value="BO">Bolívia (Bolivia)</option>
						<option value="BA">Bósnia e Herzegovina (Босна и Херцеговина)</option>
						<option value="BW">Botsuana (Botswana)</option>
						<option value="BR" selected>Brasil</option>
						<option value="BN">Brunei</option>
						<option value="BG">Bulgária (България)</option>
						<option value="BF">Burquina Faso (Burkina Faso)</option>
						<option value="BI">Burundi (Uburundi)</option>
						<option value="BT">Butão (འབྲུག)</option>
						<option value="CV">Cabo Verde (Kabu Verdi)</option>
						<option value="KH">Camboja (កម្ពុជា)</option>
						<option value="CA">Canadá (Canada)</option>
						<option value="QA">Catar (‫قطر‬‎)</option>
						<option value="KZ">Cazaquistão (Казахстан)</option>
						<option value="EA">Ceuta e Melilha (Ceuta y Melilla)</option>
						<option value="TD">Chade (Tchad)</option>
						<option value="CL">Chile</option>
						<option value="CN">China (中国)</option>
						<option value="CY">Chipre (Κύπρος)</option>
						<option value="VA">Cidade do Vaticano (Città del Vaticano)</option>
						<option value="SG">Cingapura (Singapore)</option>
						<option value="CO">Colômbia (Colombia)</option>
						<option value="KM">Comores (‫جزر القمر‬‎)</option>
						<option value="CG">Congo - Brazzaville (Congo-Brazzaville)</option>
						<option value="CD">Congo - Kinshasa (Jamhuri ya Kidemokrasia ya Kongo)</option>
						<option value="KP">Coreia do Norte (조선 민주주의 인민 공화국)</option>
						<option value="KR">Coreia do Sul (대한민국)</option>
						<option value="CI">Costa do Marfim (Côte d’Ivoire)</option>
						<option value="CR">Costa Rica</option>
						<option value="HR">Croácia (Hrvatska)</option>
						<option value="CU">Cuba</option>
						<option value="CW">Curaçao</option>
						<option value="DG">Diego Garcia</option>
						<option value="DK">Dinamarca (Danmark)</option>
						<option value="DJ">Djibuti (Djibouti)</option>
						<option value="DM">Dominica</option>
						<option value="EG">Egito (‫مصر‬‎)</option>
						<option value="SV">El Salvador</option>
						<option value="AE">Emirados Árabes Unidos (‫الإمارات العربية المتحدة‬‎)</option>
						<option value="EC">Equador (Ecuador)</option>
						<option value="ER">Eritreia (Eritrea)</option>
						<option value="SK">Eslováquia (Slovensko)</option>
						<option value="SI">Eslovênia (Slovenija)</option>
						<option value="ES">Espanha (España)</option>
						<option value="US">Estados Unidos (United States)</option>
						<option value="EE">Estônia (Eesti)</option>
						<option value="ET">Etiópia (Ethiopia)</option>
						<option value="FJ">Fiji</option>
						<option value="PH">Filipinas (Philippines)</option>
						<option value="FI">Finlândia (Suomi)</option>
						<option value="FR">França (France)</option>
						<option value="GA">Gabão (Gabon)</option>
						<option value="GM">Gâmbia (Gambia)</option>
						<option value="GH">Gana (Gaana)</option>
						<option value="GE">Geórgia (საქართველო)</option>
						<option value="GS">Geórgia do Sul e Ilhas Sandwich do Sul (South Georgia &amp; South Sandwich Islands)</option>
						<option value="GI">Gibraltar</option>
						<option value="GD">Granada (Grenada)</option>
						<option value="GR">Grécia (Ελλάδα)</option>
						<option value="GL">Groênlandia (Kalaallit Nunaat)</option>
						<option value="GP">Guadalupe (Guadeloupe)</option>
						<option value="GU">Guam</option>
						<option value="GT">Guatemala</option>
						<option value="GG">Guernsey</option>
						<option value="GY">Guiana (Guyana)</option>
						<option value="GF">Guiana Francesa (Guyane française)</option>
						<option value="GN">Guiné (Guinée)</option>
						<option value="GW">Guiné Bissau</option>
						<option value="GQ">Guiné Equatorial (Guinea Ecuatorial)</option>
						<option value="HT">Haiti</option>
						<option value="NL">Holanda (Nederland)</option>
						<option value="HN">Honduras</option>
						<option value="HK">Hong Kong (香港)</option>
						<option value="HU">Hungria (Magyarország)</option>
						<option value="YE">Iêmen (‫اليمن‬‎)</option>
						<option value="BV">Ilha Bouvet (Bouvet Island)</option>
						<option value="AC">Ilha de Ascensão (Ascension Island)</option>
						<option value="CP">Ilha de Clipperton (Clipperton Island)</option>
						<option value="IM">Ilha de Man (Isle of Man)</option>
						<option value="HM">Ilha Heard e Ilhas McDonald (Heard &amp; McDonald Islands)</option>
						<option value="NF">Ilha Norfolk (Norfolk Island)</option>
						<option value="AX">Ilhas Åland (Åland)</option>
						<option value="KY">Ilhas Caiman (Cayman Islands)</option>
						<option value="IC">Ilhas Canárias (islas Canarias)</option>
						<option value="CC">Ilhas Coco (Kepulauan Cocos (Keeling))</option>
						<option value="CK">Ilhas Cook (Cook Islands)</option>
						<option value="UM">Ilhas Distantes dos EUA (U.S. Outlying Islands)</option>
						<option value="FO">Ilhas Faroe (Føroyar)</option>
						<option value="FK">Ilhas Malvinas (Falkland Islands (Islas Malvinas))</option>
						<option value="MP">Ilhas Marianas do Norte (Northern Mariana Islands)</option>
						<option value="MH">Ilhas Marshall (Marshall Islands)</option>
						<option value="CX">Ilhas Natal (Christmas Island)</option>
						<option value="PN">Ilhas Pitcairn (Pitcairn Islands)</option>
						<option value="SB">Ilhas Salomão (Solomon Islands)</option>
						<option value="TC">Ilhas Turks e Caicos (Turks and Caicos Islands)</option>
						<option value="VG">Ilhas Virgens Britânicas (British Virgin Islands)</option>
						<option value="VI">Ilhas Virgens dos EUA (U.S. Virgin Islands)</option>
						<option value="IN">Índia (भारत)</option>
						<option value="ID">Indonésia (Indonesia)</option>
						<option value="IR">Irã (‫ایران‬‎)</option>
						<option value="IQ">Iraque (‫العراق‬‎)</option>
						<option value="IE">Irlanda (Ireland)</option>
						<option value="IS">Islândia (Ísland)</option>
						<option value="IL">Israel (‫ישראל‬‎)</option>
						<option value="IT">Itália (Italia)</option>
						<option value="JM">Jamaica</option>
						<option value="JP">Japão (日本)</option>
						<option value="JE">Jersey</option>
						<option value="JO">Jordânia (‫الأردن‬‎)</option>
						<option value="XK">Kosovo (Kosovë)</option>
						<option value="KW">Kuwait (‫الكويت‬‎)</option>
						<option value="LA">Laos (ລາວ)</option>
						<option value="LS">Lesoto (Lesotho)</option>
						<option value="LV">Letônia (Latvija)</option>
						<option value="LB">Líbano (‫لبنان‬‎)</option>
						<option value="LR">Libéria (Liberia)</option>
						<option value="LY">Líbia (‫ليبيا‬‎)</option>
						<option value="LI">Liechtenstein</option>
						<option value="LT">Lituânia (Lietuva)</option>
						<option value="LU">Luxemburgo (Luxembourg)</option>
						<option value="MO">Macau (澳門)</option>
						<option value="MK">Macedônia (Македонија)</option>
						<option value="MG">Madagascar (Madagasikara)</option>
						<option value="MY">Malásia (Malaysia)</option>
						<option value="MW">Malawi</option>
						<option value="MV">Maldivas (Maldives)</option>
						<option value="ML">Mali</option>
						<option value="MT">Malta</option>
						<option value="MA">Marrocos (‫المغرب‬‎)</option>
						<option value="MQ">Martinica (Martinique)</option>
						<option value="MU">Maurício (Moris)</option>
						<option value="MR">Mauritânia (‫موريتانيا‬‎)</option>
						<option value="YT">Mayotte</option>
						<option value="MX">México</option>
						<option value="MM">Mianmar (Birmânia) (မြန်မာ)</option>
						<option value="FM">Micronésia (Micronesia)</option>
						<option value="MZ">Moçambique</option>
						<option value="MD">Moldávia (Republica Moldova)</option>
						<option value="MC">Mônaco (Monaco)</option>
						<option value="MN">Mongólia (Монгол)</option>
						<option value="ME">Montenegro (Crna Gora)</option>
						<option value="MS">Montserrat</option>
						<option value="NA">Namíbia (Namibië)</option>
						<option value="NR">Nauru</option>
						<option value="NP">Nepal (नेपाल)</option>
						<option value="NI">Nicarágua (Nicaragua)</option>
						<option value="NE">Níger (Nijar)</option>
						<option value="NG">Nigéria (Nigeria)</option>
						<option value="NU">Niue</option>
						<option value="NO">Noruega (Norge)</option>
						<option value="NC">Nova Caledônia (Nouvelle-Calédonie)</option>
						<option value="NZ">Nova Zelândia (New Zealand)</option>
						<option value="OM">Omã (‫عُمان‬‎)</option>
						<option value="BQ">Países Baixos Caribenhos (Caribbean Netherlands)</option>
						<option value="PW">Palau</option>
						<option value="PS">Palestina (‫فلسطين‬‎)</option>
						<option value="PA">Panamá</option>
						<option value="PG">Papua-Nova Guiné (Papua New Guinea)</option>
						<option value="PK">Paquistão (‫پاکستان‬‎)</option>
						<option value="PY">Paraguai (Paraguay)</option>
						<option value="PE">Peru (Perú)</option>
						<option value="PF">Polinésia Francesa (Polynésie française)</option>
						<option value="PL">Polônia (Polska)</option>
						<option value="PR">Porto Rico (Puerto Rico)</option>
						<option value="PT">Portugal</option>
						<option value="KE">Quênia (Kenya)</option>
						<option value="KG">Quirguistão (Кыргызстан)</option>
						<option value="KI">Quiribati (Kiribati)</option>
						<option value="GB">Reino Unido (United Kingdom)</option>
						<option value="CF">República Centro-Africana (République centrafricaine)</option>
						<option value="DO">República Dominicana</option>
						<option value="CM">República dos Camarões (Cameroun)</option>
						<option value="CZ">República Tcheca (Česká republika)</option>
						<option value="RE">Reunião (La Réunion)</option>
						<option value="RO">Romênia (România)</option>
						<option value="RW">Ruanda (Rwanda)</option>
						<option value="RU">Rússia (Россия)</option>
						<option value="EH">Saara Ocidental (‫الصحراء الغربية‬‎)</option>
						<option value="PM">Saint Pierre e Miquelon (Saint-Pierre-et-Miquelon)</option>
						<option value="WS">Samoa</option>
						<option value="AS">Samoa Americana (American Samoa)</option>
						<option value="SM">San Marino</option>
						<option value="SH">Santa Helena (Saint Helena)</option>
						<option value="LC">Santa Lúcia (Saint Lucia)</option>
						<option value="BL">São Bartolomeu (Saint-Barthélemy)</option>
						<option value="KN">São Cristovão e Nevis (Saint Kitts and Nevis)</option>
						<option value="MF">São Martinho (Saint-Martin (partie française))</option>
						<option value="ST">São Tomé e Príncipe</option>
						<option value="VC">São Vicente e Granadinas (St. Vincent &amp; Grenadines)</option>
						<option value="SN">Senegal (Sénégal)</option>
						<option value="SL">Serra Leoa (Sierra Leone)</option>
						<option value="RS">Sérvia (Србија)</option>
						<option value="SC">Seychelles</option>
						<option value="SX">Sint Maarten</option>
						<option value="SY">Síria (‫سوريا‬‎)</option>
						<option value="SO">Somália (Soomaaliya)</option>
						<option value="LK">Sri Lanka (ශ්‍රී ලංකාව)</option>
						<option value="SZ">Suazilândia (Swaziland)</option>
						<option value="SD">Sudão (‫السودان‬‎)</option>
						<option value="SS">Sudão do Sul (‫جنوب السودان‬‎)</option>
						<option value="SE">Suécia (Sverige)</option>
						<option value="CH">Suíça (Schweiz)</option>
						<option value="SR">Suriname</option>
						<option value="SJ">Svalbard e Jan Mayen (Svalbard og Jan Mayen)</option>
						<option value="TJ">Tadjiquistão (Tajikistan)</option>
						<option value="TH">Tailândia (ไทย)</option>
						<option value="TW">Taiwan (台灣)</option>
						<option value="TZ">Tanzânia (Tanzania)</option>
						<option value="IO">Território Britânico do Oceano Índico (British Indian Ocean Territory)</option>
						<option value="TF">Territórios Franceses do Sul (Terres australes françaises)</option>
						<option value="TL">Timor-Leste</option>
						<option value="TG">Togo</option>
						<option value="TK">Tokelau</option>
						<option value="TO">Tonga</option>
						<option value="TT">Trinidad e Tobago (Trinidad and Tobago)</option>
						<option value="TA">Tristão da Cunha (Tristan da Cunha)</option>
						<option value="TN">Tunísia (‫تونس‬‎)</option>
						<option value="TM">Turcomenistão (Turkmenistan)</option>
						<option value="TR">Turquia (Türkiye)</option>
						<option value="TV">Tuvalu</option>
						<option value="UA">Ucrânia (Україна)</option>
						<option value="UG">Uganda</option>
						<option value="UY">Uruguai (Uruguay)</option>
						<option value="UZ">Uzbequistão (Oʻzbekiston)</option>
						<option value="VU">Vanuatu</option>
						<option value="VE">Venezuela</option>
						<option value="VN">Vietnã (Việt Nam)</option>
						<option value="WF">Wallis e Futuna (Wallis and Futuna)</option>
						<option value="ZM">Zâmbia (Zambia)</option>
						<option value="ZW">Zimbábue (Zimbabwe)</option>
					</select>
					<small class="error"><?= __('Selecione um país')?></small>
				</div>
				<div class="small-3 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.estado_id', array(
						'id'      => 'estado_id',
						'class'   => 'estado_id',
						'type'    => 'select',
						'options' => $options
					)) ?>
				</div>
				<div class="small-3 columns">
					<?= $this->Form->input('DoadorEndereco.0.cidade', array('id' => 'cidade', 'placeholder' => 'Cidade', 'disabled')) ?>
					<?= $this->Form->input('DoadorEndereco.0.cidade_id', array(
						'id'    => 'cidade_id',
						'class' => 'cidade_id',
						'style' => 'display:none'
					)) ?>
				</div>
				<div class="small-3 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.bairro', array('placeholder' => __('Bairro'))) ?>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Como Conheceu</legend>
			<div class="row">
				<div class="small-12 columns">
					<?php echo $this->Form->input('conheceu_id', ['id' => 'conheceu', 'empty' => 'Selecione']) ?>
				</div>
			</div>
		</fieldset>
		<?php echo $this->Form->hidden('pessoa_id', ['value' => 1]); ?>

		<div class="row collapse">
			<div class="small-12 column">
				<?php echo $this->Form->input('id', ['type' => 'hidden']) ?>
				<?php echo $this->Form->input('DoadorEmail.0.id', ['type' => 'hidden']) ?>
				<?php echo $this->Form->input('DoadorEndereco.0.id', ['type' => 'hidden']) ?>
				<?php echo $this->Form->input(
					$this->Html->tag('i', '', ['class' => 'fa fa-floppy-o']) . ' Salvar',
					['type' => 'button' , 'class' => 'button success large expand', 'escape' => false]);
				?>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>

<script>
</script>

<?php
$scriptString = <<<HTML
$(document).ready(function() {
	$('#tipoPessoaSwitch').on('change', function(event) {
		var self        = $(this)
		, formDpf       = $('#formDpf')
		, formDpj       = $('#formDpj')
		, formContainer = $('form')
		, formHolder    = $('#formHolder');

		if (self.is(':checked')) {
			// formContainer.append( formDpj );
			// formHolder.append( formDpf );
			formDpf.toggleClass('hide');
			formDpj.toggleClass('hide');
		} else {
			// formContainer.append( formDpf );
			// formHolder.append( formDpj );
			formDpf.toggleClass('hide');
			formDpj.toggleClass('hide');
		}
	});
});
HTML;

echo $this->Html->scriptBlock(
	$scriptString,
	['inline' => false]);

if (($this->request['action'] == 'admin_alterar') && ($this->data['Doador']['pessoa_id'] == 2)) :
$scriptSwitchDPJ = <<<HTML
$(document).ready(function() {
	// $('#tipoPessoaSwitch').prop('checked', true);
	$('#formDpf').toggleClass('hide');
	$('#formDpj').toggleClass('hide');
});
HTML;

echo $this->Html->scriptBlock(
	$scriptSwitchDPJ,
	['inline' => false]);
endif;
?>

<?php echo $this->Html->scriptBlock(
	"$(document).ready(function() {
		$('*[type=tel]').inputmask({
			mask: '(99) 9{4,5}-9999',
			skipOptionalPartCharacter: \" \",
			clearMaskOnLostFocus: true
		});

		$('*[type=tel]').blur(function(event) {
			var reg = /^\([0-9]{2}\)\s[0-9]{5}\-[0-9]{3}\_$/g;
			if ( reg.test($(this).val()) ) {
				var holdVal = $(this).val().replace(/[^0-9]/g, \"\")
				, ddd       = holdVal.substr(0,2)
				, parte1    = holdVal.substr(2,4)
				, parte2    = holdVal.substr(6,4);

				$(this).val('('+ ddd +') '+ parte1 + '-' + parte2);
			}
		});

		$('#doadorCpf').inputmask(
			{
				mask: '999.999.999-99',
				clearMaskOnLostFocus: true,
				clearIncomplete: true
			}
		);

		$('#doadorCnpj').inputmask(
			{
				mask: '99.999.999/9999-99',
				clearMaskOnLostFocus: true,
				clearIncomplete: true
			}
		);

		$('#nascimento').inputmask(\"d/m/y\",{ \"placeholder\": \"DD/MM/AAAA\" });

		$('.cep').inputmask({ mask: \"99999-999\", greedy: false });

		$('.estado_id').change(function(){
			var estado = $(this).val()
			, cidades  = $('.cidade_id');

			if (cidades.not(':visible')) {
				cidades.prev('input').hide().end().show();
			};

			if ( cidades.find('option').length = 1 ) {
				cidades.html('<option value=\"\">Carregando...</option>');
			};
			$.get(
				'/cidades/all',
				{ estado: estado },
				function(data) {
					cidades.html(data);
					cidades.find('option').each(function(index, el) {
						if ( $(this).text() == window.DoadorEnderecoCidade ) {
							$(this).prop('selected', true);
						};
					});
				}, 'html'
			);
		});

		function cepAPI(\$url, \$success, \$fail) {
			$.ajax({
				url: \$url,
				context: document.body,
				crossDomain: true,
				timeout: 5000,
				dataType: 'jsonp',
				success : \$success,
				error : \$fail
			});
		}

		$('.cep').blur(function(event) {
			var \$cep = $(this).val().replace(/[^0-9]/gi, \"\"),
					\$url = 'http://correiosapi.apphb.com/cep/'+\$cep,
					\$success = function(data){
						$('#DoadorEndereco0Logradouro').attr('value', data.tipoDeLogradouro + ' ' + data.logradouro).addClass('disabled');
						$('.estado_id')
							.find('option[data-uf=\"'+ data.estado +'\"]').prop('selected', true)
							.end()
							.trigger('change');

						// window.DoadorEnderecoCidade = data.cidade.replace(/[^\w\s]|_/g, \"\").replace(/\s+/g, \" \");
						window.DoadorEnderecoCidade = data.cidade.replace(/[á|ã|â|à]/gi, \"a\").replace(/[é|ê|è]/gi, \"e\").replace(/[í|ì|î]/gi, \"i\").replace(/[õ|ò|ó|ô]/gi, \"o\").replace(/[ú|ù|û]/gi, \"u\").replace(/[ç]/gi, \"c\").replace(/[ñ]/gi, \"n\")

						$('#DoadorEndereco0Bairro').val(data.bairro);

						$('#DoadorEndereco0Numero').select();
					},
					\$fail = function(){
						$('#DoadorEndereco0Logradouro').attr({
							'placeholder': 'Nenhum logradouro encontrado para este CEP.',
							'value': ''
						}).removeAttr('disabled').select();
					};
			if (\$cep.length == 8) {
				$('#DoadorEndereco0Logradouro').attr({
					'value': '',
					'placeholder': 'Carregando...'
				}).addClass('disabled');
				cepAPI(\$url, \$success, \$fail);
			} else{
				\$fail();
			};
		});
	});",
	['inline' => false]); ?>