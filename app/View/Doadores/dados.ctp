<?php
	if ( isset($this->data['DoadorEndereco']) && empty($this->data['DoadorEndereco']) ) {
		array_unshift($options, array(
			'name' => 'Estado',
			'disabled' => true,
			'selected' => true,
			'value' => 0
		));
		array_unshift($options, null);
		unset($options[0]);
		// debug($options);
	}
?>
<?php echo $this->Session->flash() ?>
<div id="personal-data-edit">
	<?php if ($this->Session->read('Auth.User.Doador.pessoa_id') == 1): ?>
	<?php echo $this->Form->create(null, array('data-abide', 'inputDefaults' => array('error' => false))) ?>
		<?php echo $this->Form->hidden('Doador.id') ?>
		<?php echo $this->Form->hidden('User.id') ?>
		<?php //echo $this->Form->hidden('User.email') ?>
		<fieldset>
			<div class="row">
				<div class="small-12 column">
					<h3><?php echo __('Informações Pessoais') ?></h3>
				</div>
			</div>

			<div class="row">
				<div class="small-6 columns">
					<?php echo $this->Form->input('Doador.nome', array('placeholder' => __('Nome'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
				<div class="small-6 columns">
					<?php echo $this->Form->input('Doador.sobrenome', array('placeholder' => __('Sobrenome'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
				<div class="small-12 columns">
					<?php echo $this->Form->hidden('DoadorEmail.0.id') ?>
					<?php echo $this->Form->input('DoadorEmail.0.email', array('placeholder' => __('Seu email'), 'type' => 'email', 'required' => true)) ?>
					<?= $this->Html->tag('small', __('Preenchimento Obrigatório'), array('class' => 'error')) ?>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="small-4 columns">
					<?php echo $this->Form->input('Doador.cpf_cnpj', array('id' => 'doadorCpf' ,'placeholder' => 'CPF', 'data-abide-validator' => 'testaCPF', 'readonly' => true)) ?>
					<?= $this->Html->tag('small', __('Formato Inválido'), array('class' => 'error')) ?>
				</div>
				<div class="small-4 columns">
					<?php
						if (isset($this->data['Doador']['nascimento']) && !empty($this->data['Doador']['nascimento'])) {
							$birthday = $this->Formatacao->data($this->data['Doador']['nascimento']);
						} else {
							$birthday = null;
						}
						echo $this->Form->input('Doador.nascimento', array(
								'id'                   => 'nascimento',
								'placeholder'          => __('Data de Nascimento'),
								'value'                => $birthday,
								'type'                 => 'text',
								'data-abide-validator' => 'testaAnoAtual'
							)
						); ?>
					<?= $this->Html->tag('small', __('Preenchimento Obrigatório'), array('class' => 'error')) ?>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->select(
						'Doador.sexo',
						array(
							'1' => __('Masculino'),
							'2' => __('Feminino')
						),
						array('empty' => __('Sexo'), 'id' => 'genero')
					) ?>
				</div>
				<div class="small-12 columns">
					<br>
				</div>
				<div class="small-4 columns">
					<select name="genero" id="genero" disabled>
						<option selected value="principal"><?= __('Principal')?></option>
						<option value="comercial"><?= __('Comercial')?></option>
						<option value="celular"><?= __('Celular')?></option>
						<option value="residencial"><?= __('Residencial')?></option>
					</select>
				</div>
				<div class="small-4 columns">
					<select name="genero" id="genero" disabled>
						<option value="principal"><?= __('Principal')?></option>
						<option selected value="comercial"><?= __('Comercial')?></option>
						<option value="celular"><?= __('Celular')?></option>
						<option value="residencial"><?= __('Residencial')?></option>
					</select>
				</div>
				<div class="small-4 columns">
					<select name="genero" id="genero" disabled>
						<option selected value="principal"><?= __('Principal')?></option>
						<option value="comercial"><?= __('Comercial')?></option>
						<option selected value="celular"><?= __('Celular')?></option>
						<option value="residencial"><?= __('Residencial')?></option>
					</select>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->hidden('DoadorTelefone.0.id') ?>
					<?php echo $this->Form->hidden('DoadorTelefone.0.telefone_id', ['value' => 1]) ?>
					<?php echo $this->Form->input('DoadorTelefone.0.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->hidden('DoadorTelefone.1.id') ?>
					<?php echo $this->Form->hidden('DoadorTelefone.1.telefone_id', ['value' => 2]) ?>
					<?php echo $this->Form->input('DoadorTelefone.1.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->hidden('DoadorTelefone.2.id') ?>
					<?php echo $this->Form->hidden('DoadorTelefone.2.telefone_id', ['value' => 3]) ?>
					<?php echo $this->Form->input('DoadorTelefone.2.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="small-4 columns end">
					<?php echo $this->Form->hidden('DoadorEndereco.0.id') ?>
					<?php echo $this->Form->input('DoadorEndereco.0.cep', array('id' => 'cep' ,'placeholder' => __('CEP'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
			</div>
			<div class="row">
				<div class="small-8 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.logradouro', array('placeholder' => __('Logradouro'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.numero', array('placeholder' => __('Número'))) ?>
					<?= $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
				<div class="small-12 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.complemento', array('placeholder' => __('Complemento'))) ?>
				</div>
			</div>
			<div class="row">
				<div class="small-4 columns">
					<!-- <?= $this->Form->input('DoadorEndereco.0.estado_id', array('id' => 'estado_id')) ?> -->
					<?php echo $this->Form->input('DoadorEndereco.0.estado_id', array(
						'id'      => 'estado_id',
						'type'    => 'select',
						'options' => $options
					)) ?>
				</div>
				<div class="small-4 columns">
					<?= $this->Form->input('DoadorEndereco.0.cidade', array('id' => 'cidade', 'placeholder' => 'Cidade', 'disabled')) ?>
					<!-- <select name="data[DoadorEndereco][0][cidade_id]" id="cidade_id" style="display:none">
						<option value="">Cidade</option>
					</select> -->
					<?= $this->Form->input('DoadorEndereco.0.cidade_id', array(
						'id' => 'cidade_id',
						'style' => 'display:none'
					)) ?>
				</div>
				<div class="small-4 columns">
					<?php echo $this->Form->input('DoadorEndereco.0.bairro', array('placeholder' => __('Bairro'))) ?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<button class="button right"><?php echo __('Salvar dados') ?></button>
				</div>
			</div>
		</fieldset>
	<?php echo $this->Form->end() ?>
	<!--  -->
	<?php elseif ($this->Session->read('Auth.User.Doador.pessoa_id') == 2): ?>
	<div class="row collapse">
		<div class="small-12 columns">
			<?php echo $this->Form->create(null, ['data-abide', 'id' => 'formDoacao', 'inputDefaults' => ['error' => false]]) ?>
			<?php echo $this->Form->hidden('Doador.id') ?>
			<?php echo $this->Form->hidden('User.id') ?>
			<fieldset>
				<div class="row">
					<div class="medium-12 column">
						<h3><?php echo __('Dados Profissionais'); ?></h3>
					</div>
					<?php echo $this->Form->hidden('DpjComplemento.id'); ?>
					<div class="row">
						<div class="medium-12 columns">
							<?php echo $this->Form->input('Doador.nome', ['placeholder' => __('Nome Fantasia'), 'required']); ?>
							<?php echo $this->Html->tag('small', __('Campo Obrigatório'), ['class' => 'error']); ?>
						</div>
					</div>

					<div class="row">
						<div class="medium-6 columns">
							<?php echo $this->Form->input('Doador.sobrenome', ['placeholder' => __('Razão Social'), 'required']); ?>
							<?php echo $this->Html->tag('small', __('Campo Obrigatório'), ['class' => 'error']); ?>
						</div>
						<div class="medium-6 columns">
							<?php echo $this->Form->input('Doador.cpf_cnpj', ['placeholder' => __('CNPJ'), 'readonly', 'required']); ?>
							<?php echo $this->Html->tag('small', __('Campo Obrigatório'), ['class' => 'error']); ?>
						</div>
					</div>

					<div class="row">
						<div class="medium-6 columns">
							<?php echo $this->Form->input('DpjComplemento.nome', ['placeholder' => __('Nome do Contato'), 'required']); ?>
							<?php echo $this->Html->tag('small', __('Campo Obrigatório'), ['class' => 'error']); ?>
						</div>
						<div class="medium-6 columns">
							<?php echo $this->Form->input('DpjComplemento.cargo', ['placeholder' => __('Cargo'), 'required']); ?>
							<?php echo $this->Html->tag('small', __('Campo Obrigatório'), ['class' => 'error']); ?>
						</div>
					</div>

					<div class="row">
						<div class="medium-6 columns">
							<?php echo $this->Form->input('DpjComplemento.sexo', ['type' => 'select', 'options' => ['1' => __('Masculino'), '2' => __('Feminino')], ['empty' => 'Sexo']]); ?>
						</div>
					</div>

					<div class="row">
						<div class="medium-12 columns">
							<?php echo $this->Form->hidden('DoadorEmail.0.id'); ?>
							<?php echo $this->Form->input('DoadorEmail.0.email', ['placeholder' => __('Email para contato'), 'type' => 'email', 'required']); ?>
							<small class="error"><?= __('Preencha com um email válido')?></small>
						</div>
					</div>

					<div class="row">
						<div class="small-12 columns">
							<?php echo $this->Form->input('DpjComplemento.representante', ['placeholder' => __('Nome do Representante legal'), 'required']); ?>
							<small class="error"><?= __('Campo Obrigatório')?></small>
						</div>
					</div>

					<div <div class="row">
						<div class="small-12 columns">
							<?php echo $this->Form->hidden('DoadorEmail.1.id'); ?>
							<?php echo $this->Form->input('DoadorEmail.1.email', ['placeholder' => __('Email do Representante Legal'), 'type' => 'email', 'required']); ?>
							<small class="error"><?= __('Preencha com um email válido')?></small>
						</div>
					</div>		<div class="row">
						<div class="small-6 columns">
							<?php echo $this->Form->select('DpjComplemento.faixa_id', $faixa, ['empty' => __('Faixa de Faturamento')]); ?>
						</div>
						<div class="small-6 columns">
							<?php echo $this->Form->select('DpjComplemento.natureza_id', $natureza, ['empty' => __('Natureza Jurídica')]); ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="small-4 columns">
						<select name="genero" id="genero" disabled>
							<option selected value="principal"><?= __('Principal')?></option>
							<option value="comercial"><?= __('Comercial')?></option>
							<option value="celular"><?= __('Celular')?></option>
							<option value="residencial"><?= __('Residencial')?></option>
						</select>
					</div>
					<div class="small-4 columns">
						<select name="genero" id="genero" disabled>
							<option value="principal"><?= __('Principal')?></option>
							<option selected value="comercial"><?= __('Comercial')?></option>
							<option value="celular"><?= __('Celular')?></option>
							<option value="residencial"><?= __('Residencial')?></option>
						</select>
					</div>
					<div class="small-4 columns">
						<select name="genero" id="genero" disabled>
							<option selected value="principal"><?= __('Principal')?></option>
							<option value="comercial"><?= __('Comercial')?></option>
							<option selected value="celular"><?= __('Celular')?></option>
							<option value="residencial"><?= __('Residencial')?></option>
						</select>
					</div>
					<div class="small-4 columns">
						<?php echo $this->Form->hidden('DoadorTelefone.0.id') ?>
						<?php echo $this->Form->hidden('DoadorTelefone.0.telefone_id', ['value' => 1]) ?>
						<?php echo $this->Form->input('DoadorTelefone.0.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
					</div>
					<div class="small-4 columns">
						<?php echo $this->Form->hidden('DoadorTelefone.1.id') ?>
						<?php echo $this->Form->hidden('DoadorTelefone.1.telefone_id', ['value' => 2]) ?>
						<?php echo $this->Form->input('DoadorTelefone.1.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
					</div>
					<div class="small-4 columns">
						<?php echo $this->Form->hidden('DoadorTelefone.2.id') ?>
						<?php echo $this->Form->hidden('DoadorTelefone.2.telefone_id', ['value' => 3]) ?>
						<?php echo $this->Form->input('DoadorTelefone.2.numero', array('type' => 'tel', 'placeholder' => __('Telefone'), 'data-tooltip', 'aria-haspopup' => 'true', 'data-options' => 'disable_for_touch:true', 'title' => __('Para telefones com 8 digitos (Ex: Telefone fixo), pressione "espaço" depois do quarto digito.'))) ?>
					</div>
				</div>
			</fieldset>

			<?php for ($i=0; $i < count($this->data['DoadorEndereco']) ; $i++): ?>
				<?php echo $this->Form->input('DoadorEndereco.'.$i.'.id') ?>
			<fieldset>
				<legend><?php echo ($i == 0) ? __('Endereço da Sede') : __('Endereço de Filial (' . $i . ')'); ?></legend>
				<div class="small-6 columns end">
					<?php echo $this->Form->input('DoadorEndereco.'.$i.'.cep', ['placeholder' => __('CEP'), 'class' => 'cep', 'required']); ?>
					<small class="error"><?php echo __('Preenchimento Obrigatório') ?></small>
				</div>

				<div class="small-7 columns">
					<?php echo $this->Form->input('DoadorEndereco.'.$i.'.logradouro', ['placeholder' => __('Logradouro'), 'required']); ?>
					<small class="error"><?php echo __('Preenchimento Obrigatório') ?></small>
				</div>
				<div class="small-2 column">
					<?php echo $this->Form->input('DoadorEndereco.'.$i.'.numero', ['placeholder' => __('Número'), 'pattern' => 'number', 'required']); ?>
					<small class="error"><?php echo __('Preenchimento Obrigatório') ?></small>
				</div>
				<div class="small-3 columns">
					<?php echo $this->Form->input('DoadorEndereco.'.$i.'.complemento', ['placeholder' => __('Complemento')]); ?>
				</div>

				<div class="small-12 medium-4 columns">
					<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
					<?php echo $this->Form->input('DoadorEndereco.'.$i.'.estado_id', array(
						'id'      => 'estado_id',
						'type'    => 'select',
						'options' => $options
					)) ?>
					<?php echo $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
					<?php else: ?>
						<?php echo $this->Form->input('DoadorEndereco.'.$i.'.estado', array('type' => 'text', 'placeholder' => __('Estado'))) ?>
					<?php endif ?>
				</div>

				<div class="small-12 medium-4 columns">
					<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
					<?= $this->Form->input('DoadorEndereco.'.$i.'.cidade', array('id' => 'cidade', 'placeholder' => 'Cidade', 'disabled')) ?>
					<?= $this->Form->input('DoadorEndereco.'.$i.'.cidade_id', array(
						'id' => 'cidade_id',
						'style' => 'display:none'
					)) ?>

					<?php echo $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
					<?php else: ?>
						<?php echo $this->Form->input('DoadorEndereco.'.$i.'.cidade', array('type' => 'text', 'placeholder' => __('Cidade'))) ?>
					<?php endif ?>
				</div>

				<div class="small-12 medium-4 columns">
					<?php echo $this->Form->input('DoadorEndereco.'.$i.'.bairro', array('placeholder' => __('Bairro') )) ?>
					<?php echo $this->Html->tag(
						'small',
						__('Preenchimento Obrigatório'),
						array('class' => 'error')
					) ?>
				</div>
			</fieldset>
			<?php endfor ?>

			<fieldset role="filial">
				<legend>
					<?php // if (isset($this->data['DoadorEndereco'][1]['cep'])) {$filial_checked = true; } else {$filial_checked = false; } ?>
					<?php echo $this->Form->input('Doacao.filial', ['type' => 'checkbox', 'label' => false, 'style' => '  vertical-align: middle; margin: 0px 4px 4px 0;']); ?>
				<?php	__('Cadastrar Endereço de Filial') ?>
				</legend>

				<div class="small-6 columns end">
					<?php echo $this->Form->input('DoadorEndereco.'.$i.'.cep', ['placeholder' => __('CEP'), 'class' => 'cep', 'required']); ?>
					<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
				</div>

				<div class="row small-collapse medium-uncollapse">
					<div class="small-12 medium-7 columns">
						<?php echo $this->Form->input('DoadorEndereco.'.$i.'.logradouro', array('placeholder' => __('Logradouro') )) ?>
						<?php echo $this->Html->tag(
							'small',
							__('Preenchimento Obrigatório'),
							array('class' => 'error')
						) ?>
					</div>
					<div class="small-12 medium-2 columns">
						<?php echo $this->Form->input('DoadorEndereco.'.$i.'.numero', array('placeholder' => __('Número') , 'type' => 'text')) ?>
						<?php echo $this->Html->tag(
							'small',
							__('Preenchimento Obrigatório'),
							array('class' => 'error')
						) ?>
					</div>
					<div class="small-12 medium-3 columns">
						<?php echo $this->Form->input('DoadorEndereco.'.$i.'.complemento', array('placeholder' => __('Complemento') )) ?>
					</div>
				</div>

				<div class="row small-collapse medium-uncollapse">
					<div class="small-12 medium-4 columns">
						<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
							<?php echo $this->Form->input('DoadorEndereco.'.$i.'.estado_id', array(
								'id'      => 'estado_id',
								'type'    => 'select',
								'options' => $options
							)) ?>
							<?php echo $this->Html->tag(
								'small',
								__('Preenchimento Obrigatório'),
								array('class' => 'error')
							) ?>
						<?php else: ?>
							<?php echo $this->Form->input('DoadorEndereco.'.$i.'.estado', array('type' => 'text', 'placeholder' => __('Estado'))) ?>
						<?php endif ?>
					</div>

					<div class="small-12 medium-4 columns">
						<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
							<?= $this->Form->input('DoadorEndereco.'.$i.'.cidade', array('id' => 'cidade', 'placeholder' => 'Cidade', 'disabled')) ?>
							<?= $this->Form->input('DoadorEndereco.'.$i.'.cidade_id', array(
								'id' => 'cidade_id',
								'style' => 'display:none'
							)) ?>
							<?php echo $this->Html->tag(
								'small',
								__('Preenchimento Obrigatório'),
								array('class' => 'error')
							) ?>
						<?php else: ?>
							<?php echo $this->Form->input('DoadorEndereco.'.$i.'.cidade', array('type' => 'text', 'placeholder' => __('Cidade'))) ?>
						<?php endif ?>
					</div>

					<div class="small-12 medium-4 columns">
						<?php echo $this->Form->input('DoadorEndereco.'.$i.'.bairro', array('placeholder' => __('Bairro') )) ?>
						<?php echo $this->Html->tag(
							'small',
							__('Preenchimento Obrigatório'),
							array('class' => 'error')
						) ?>
					</div>
				</div>
			</fieldset>
				<a href="#" name="novaFilial" class="button info small right"><?php echo __('Cadastrar Nova Filial'); ?></a>
				<hr>

			<div class="row">
				<div class="small-12 columns">
					<button class="button right"><?php echo __('Salvar dados') ?></button>
				</div>
			</div>
		</div>
	</div>
	<!--  -->
	<?php endif ?>
</div>

<?php echo $this->Html->scriptBlock(
	"$('a[name=novaFilial]').on('click', function(event) {
		event.preventDefault();
		var index = $('fieldset').length -1;
		$.ajax({
			url: '" . $this->Html->url(['controller' => 'doadores', 'action' => 'nova_filial']) . "',
			type: 'POST',
			data: {index: index},
		})
		.done(function(data) {
			$('fieldset').eq(index).after(data);
			app.init();
			$('a[name=excluirFilial]').on('click', function(event) {
				event.preventDefault();
				$(this).parents('fieldset').remove();
			});
		})
		.fail(function(data, textStatus, xhr) {
			console.log(xhr);
		})
		.always(function() {
			console.log('complete');
		});
	});",
	['inline' => false]
); ?>