<!-- /////
Minha conta
-->
<?php echo $this->Session->flash(); ?>
<div id="my-account">
	<div id="resume-account" class="row">
		<?php
			echo $this->Html->link(
				__('Quero fazer uma doação extra') . ' <i class="sprite-nova-doacao"></i>',
				'/doacao',
				array(
					'class' => 'button show-for-medium-up',
					'id' => 'bt-nova-doacao',
					'escape' => false
				)
			);

			echo $this->Html->link(
				__('Quero fazer uma doação extra') . ' <i class="sprite-nova-doacao"></i>',
				'/doacao',
				array(
					'class' => 'button show-for-small-only expand',
					'id' => 'bt-nova-doacao',
					'escape' => false
				)
			);
		?>

		<?php
			echo $this->Html->link(
				__('Quero aumentar o valor da minha doação') . ' <i class="sprite-aumentar-doacao"></i>',
				array('#' => 'my-donnations'),
				array(
					'class' => 'button show-for-medium-up',
					'id' => 'bt-aumentar-doacao',
					'escape' => false
				)
			);

			echo $this->Html->link(
				__('Quero aumentar o valor da minha doação') . ' <i class="sprite-aumentar-doacao"></i>',
				array('#' => 'my-donnations'),
				array(
					'class' => 'button show-for-small-only expand',
					'id' => 'bt-aumentar-doacao',
					'escape' => false
				)
			);
		?>

		<!-- <button class="button" id="bt-aumentar-doacao">Quero aumentar o valor da minha doação <i class="sprite-aumentar-doacao"></i></button> -->
		<h1>
			<?php
				$screenName = ($this->Session->read('Auth.User.Doador.nome') != '') ? $this->Session->read('Auth.User.Doador.nome') : $this->Session->read('Auth.User.Doador.sobrenome');
				echo __('Olá') . ' ' . $screenName
			?>,
		</h1>
		<p><?php echo __('Aqui você pode consultar e alterar a sua conta') ?>.</p>
	</div>
	<div id="personal-data" class="row">
		<h1>
			<i class="sprite-dados-pessoais"></i>
			<?php
				echo __('Dados Pessoais') . ' ' . $this->Html->link( __('Editar').' <i class="sprite-atualizar"></i>', '/dados', array('class' => 'button show-for-medium-up', 'escape' => false)) . $this->Html->link( __('Editar').' <i class="sprite-atualizar"></i>', '/dados', array('class' => 'button show-for-small-only expand', 'escape' => false))
			?>
		</h1>
		<div class="panel callout">
			<dd>
				<dl><?= __('Nome') ?></dl>
				<dt>
					<?php echo $this->Session->read('Auth.User.Doador.nome') . ' ' ?>
					<?php if ($this->Session->read('Auth.User.Doador.pessoa_id') == 1) : ?>
						<?php echo ' ' . $this->Session->read('Auth.User.Doador.sobrenome')  ?>
					<?php endif ?>
				</dt>
				<dl><?= __('Email') ?></dl>
				<dt><?php echo $this->Session->read('Auth.User.email') ?></dt>
			</dd>
		</div>

		<h1><?= __('Seu código de doador é') ?>: <?php echo $this->Session->read('Auth.User.Doador.id') ?> <a href="#" class="questions" data-tooltip aria-haspopup="true" class="has-tip" title="<?= __('Código de doador é sua identificação na base de doadores do Instituto Ayrton Senna') ?>.">?</a></h1>
	</div>

	<div id="my-donnations-head">
		<div class="row">
			<h1><i class="sprite-minhas-doacoes"></i> <?php echo __('Minhas doações') ?></h1>
			<p><?php echo __('Abaixo você pode conferir e alterar suas doações ativas') ?>.</p>
		</div>
	</div>
	<div id="my-donnations">
		<div class="row small-collapse">
			<table id="my-account-table" class="display responsive nowrap">
			    <thead>
			        <tr>
			        	<th></th>
			            <th><?php echo __('Data do Cadastro') ?></th>
			            <th><?php echo __('Valor') ?></th>
			            <th><?php echo __('Frequência') ?></th>
			            <th class="desktop"><?php echo __('Status') ?></th>
			            <th><?php echo __('Forma de Pagamento') ?></th>
			            <!--<th><?php echo __('Pagamento') ?></th>-->
			            <th class="desktop">Ações</th>
			        </tr>
			    </thead>
			    <tbody>
			    	<?php foreach ($doacoes as $doacao) : ?>
			    	<tr>
			    		<td></td>
			            <td><?php echo $this->Formatacao->data($doacao['Doacao']['created']) ?></td>
			            <td><?php echo $this->Formatacao->moeda($doacao['Doacao']['valor']) ?></td>
			            <td><?php echo __($doacao['Periodo']['tipo']) ?></td>
			            <td class="desktop"><?php echo ($doacao['Doacao']['status'] == 'A') ? __('Ativa') : __('Cancelada') ?></td>
			            <td>
			            	<?php echo __($this->Geral->getTipoFormaPagamento($doacao['FormasPagamento']['tipo'])) ?>
			            	<?php if ( $this->Geral->getTipoFormaPagamento($doacao['FormasPagamento']['tipo']) === 'Crédito'): ?>
			            		&nbsp;<span class="secondary radius label">Cartão XXX XXX XXX <?php echo $doacao['Pagamento']['cartao_final'] ?></span>
			            	<?php endif ?>
			            	<?php if ($doacao['FormasPagamento']['id'] !== $doacao['Pagamento']['formas_pagamento_id']): ?>
			            		&nbsp;<span class="label warning round has-tip" data-tooltip aria-haspopup="true" title="As alterações na forma de pagamento passarão a valer a partir do próximo mês.">?</span>
			            	<?php endif ?>
			            </td>
			            <!-- <td><?php // echo $this->Geral->getStatusPagamento($doacao['Parcela']['status_id']) ?></td> -->
			            <td class="desktop">
			            	<?php echo $this->Html->link(
			            		$this->Html->tag('span', __('Editar'), array('class' => 'label radius info')),
			            		'/doacoes/'.$doacao['Doacao']['id'],
			            		array('escape' => false)
			            	); ?>

			            	<?php if (($doacao['Doacao']['status'] == 'A') && ($doacao['Doacao']['periodo_id'] == 2) && ($doacao['Parcela']['status_id'] == '6') && ($doacao['FormasPagamento']['tipo'] == 'B')) {
			            		echo ' ' . $this->Html->link(
			            			$this->Html->tag('span', __('Imprimir'), array('class' => 'label radius success')),
			            			'/boleto/' . $doacao['Parcela']['id'],
									array('escape' => false, 'target' => 'blank')
			            		);
			            	} ?>
			            </td>
			        </tr>
			    	<?php endforeach; ?>
			    </tbody>
			</table>
		</div>
	</div>
	<div id="results">
		<div class="row">
			<!--<p><?php echo __('Até hoje você já colaborou com') ?>:</p>
			<h2><?php echo __('A alfabetização de cerca de 100 crianças') ?>.</h2>-->
			<div class="small-12">
				<?php echo $this->Html->link(
					__('Indique a um Amigo') . ' <i class="sprite-email"></i>',
					array(
						'#' => ''
					),
					array(
						'escape'         => false,
						'class'          => 'button info',
						'data-reveal-id' => 'modal-indicacao'
					)
				) ?>
			</div>
			<p class="red-text"><?php echo __('Caso necessite cancelar sua doação, por favor, entre em contato com') ?> <a href="mailto:doacao@ias.org.br">doacao@ias.org.br</a> <?php echo __('ou pelo telefone') ?> <a href="tel:+551129743062">+55 11 2974-3062</a>.</p>
		</div>
	</div>

	<!-- Modal indicação por e-mail -->
	<?php echo $this->element('modal-indicacao'); ?>

</div>

