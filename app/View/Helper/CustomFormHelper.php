<?php
App::uses('FormHelper', 'View/Helper');
class CustomFormHelper extends FormHelper {

	public function input($fieldName, $options = array()){
		if(!isset($options['label']))
			$options['label'] = false;
		if(!isset($options['div']))
			$options['div'] = false;

		return parent::input($fieldName, $options); 
	}

	// public function create($model = null, $options = array()){
	// 	if (!isset($options["error"])) {
	// 		$options['inputDefaults'] = array(
 //        		'error' => array(
 //            		'attributes' => array(
 //                		'wrap' => 'small', 'class' => 'error'
 //            		)
 //        		)
 //    		);
	// 	}

	// 	return parent::create($model, $options);
	// }
}