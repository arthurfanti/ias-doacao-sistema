<?php
class GeralHelper extends AppHelper
{
	public $helpers = array('Html');

	public function getTipoFormaPagamento($tipo)
	{
		$tipos = [
			'Crédito' => 'C',
			'Débito' => 'D',
			'Boleto' => 'B',
			'Paypal' => 'P',
			'Depósito' => 'E'
		];

		if (array_search($tipo, $tipos) === false)
			return '';

		return array_search($tipo, $tipos);
	}

	public function getStatusPagamento($status_id)
	{
		$status = [
			'Enviada' => 1,
			'Pago' => 2,
			'Cancelada' => 3,
			'Baixa por Inadimplência' => 4,
			'Não Paga' => 5,
			'Gerada' => 6,
			'Devolvida' => 7,
			'Estornada' => 8,
			'Pagto. Parcial' => 9,
			'Permuta' => 10,
			'Suspensa' => 11
		];

		if (array_search($status_id, $status) === false)
			return '';

		return array_search($status_id, $status);
	}

	public function getStatusPagamentoAdmin($status_id)
	{
		$status = [
			'Enviada' => 1,
			'Pago' => 2,
			'Cancelada' => 3,
			'Baixa por Inadimplência' => 4,
			'Não Paga' => 5,
			'Gerada' => 6,
			'Devolvida' => 7,
			'Estornada' => 8,
			'Pagto. Parcial' => 9,
			'Permuta' => 10,
			'Suspensa' => 11,
			'Transação Capturada' => 12,
			'Transação Autorizada, pendente de captura' => 13,
			'Transação não Autorizada, pela Adquirente' => 14,
			'Transação com erro Desqualificante' => 15,
			'Transação aguardando resposta' => 16,
		];

		if (array_search($status_id, $status) === false)
			return '';

		return array_search($status_id, $status);
	}

	public function getMeses()
	{
		return ['' => __('Mês'),
				'01' => '01',
				'02' => '02',
				'03' => '03',
				'04' => '04',
				'05' => '05',
				'06' => '06',
				'07' => '07',
				'08' => '08',
				'09' => '09',
				'10' => '10',
				'11' => '11',
				'12' => '12'];
	}

	public function getAnos()
	{
		$ano = date('Y');
		return ['' => __('Ano'),
				$ano => $ano,
				$ano+1 => $ano+1,
				$ano+2 => $ano+2,
				$ano+3 => $ano+3,
				$ano+4 => $ano+4,
				$ano+5 => $ano+5,
				$ano+6 => $ano+6,
				$ano+7 => $ano+7,
				$ano+8 => $ano+8,
				$ano+9 => $ano+9];
	}

	public function getBackground($configs = null, $tipo_pessoa = null)
	{
		if(is_null($tipo_pessoa))
			$tipo_pessoa = 'pf';

		$img = $configs['background_site_d' . $tipo_pessoa];

		$large  = $this->Html->url('/files/configuracao/background_site_d' . $tipo_pessoa . '/1/big_' . $img);
		$medium = $this->Html->url('/files/configuracao/background_site_d' . $tipo_pessoa . '/1/medium_' . $img);
		$tablet = $this->Html->url('/files/configuracao/background_site_d' . $tipo_pessoa . '/1/tablet_' . $img);
		$mobile = $this->Html->url('/files/configuracao/background_site_d' . $tipo_pessoa . '/1/mobile_' . $img);

		return $this->Html->tag('div', '', ['id' => 'background-image', 'data-interchange' => "[$mobile, (retina)], [$tablet, (medium-only)], [$medium, (medium)], [$large, (xlarge)]"]);
		// return $this->Html->url('/files/configuracao/background_site_d'.$tipo_pessoa.'/1/big_'.$configs['background_site_d'.$tipo_pessoa])
	}
}