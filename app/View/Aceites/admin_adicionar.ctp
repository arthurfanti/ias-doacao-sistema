<?php echo $this->Form->create() ?>
<?php foreach ($idiomas as $idioma => $sigla) : ?>
	<?php echo $this->Form->input("Aceite.titulo.$sigla", array('label' => 'Título')) ?>
	<?php echo $this->Form->input("Aceite.termo.$sigla", array('type' => 'textarea', 'label' => 'Termo')) ?>
	<hr />
<?php endforeach ?>
<?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => 'Status')) ?>
<?php echo $this->Form->input('divulgacao', array('type' => 'checkbox', 'label' => 'Divulgação em nome da empresa')) ?>
<?php echo $this->Form->input('pessoa_id', array('label' => 'Pessoa')) ?>
<br />
<?php echo $this->Form->button('Salvar', array('type' => 'submit')) ?>
<?php echo $this->Form->end() ?>

<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
<script>
	CKEDITOR.replace('AceiteTermoPor');
	CKEDITOR.replace('AceiteTermoEng');
</script>