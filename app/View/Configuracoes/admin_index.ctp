<style>
	.file-upload {
		/*opacity: 0;
		filter: alpha(opacity=0);*/
		position: absolute;
		left: -9999px;
	}

	span[role="filename"]{
		padding:0.5em;
		float:left;
		width:auto;
		min-width:100%;
		white-space: nowrap;
		overflow:hidden;
		background:#f0f0f0;
		border-bottom: 1px solid #3A539B;
		color: #34495E;
		text-shadow: 1px 1px white;
	}

	span[role="filename"] + label{
		position: absolute;
  		right:.9375rem;
		padding:0.5em;
		display:inline-block;
		background:#4B77BE;
		border-bottom: 1px solid #3A539B;
		color: #f0f0f0;
		text-shadow: 1px 1px rgba(0,0,0,.3);
		font-size: 1rem;
		text-transform: uppercase;
		cursor:pointer;
	}
</style>
<div class="row">
	<div class="small-12 columns">
		<?php echo $this->Form->create('Configuracao', ['data-abide', 'type' => 'file']) ?>
		<?php echo $this->Form->hidden('id'); ?>
		<fieldset>
			<legend>Background da Home</legend>
			<div class="small-6 columns">
				<span role="filename">Fundo para PF</span>
				<?php echo $this->Form->input('background_site_dpf', ['type' => 'file', 'class' => 'file-upload', 'label' => $this->Html->tag('i', '', ['class' => 'fa fa-file-image-o']) . ' Escolher']); ?>
				<div class="row collapse">
					<div class="clearfix">&nbsp;</div>
					<div class="small-12 column">
						<a target="_blank" href="<?php echo $this->Html->url('/files/configuracao/background_site_dpf/1/' . $this->data['Configuracao']['background_site_dpf']) ?>" class="th" role="button" arial-label="Thumbnail"><?php echo $this->Html->image('/files/configuracao/background_site_dpf/1/' . $this->data['Configuracao']['background_site_dpf']); ?></a>
					</div>
				</div>
			</div>
			<div class="small-6 columns">
				<span role="filename">Fundo para PJ</span>
				<?php echo $this->Form->input('background_site_dpj', ['type' => 'file', 'class' => 'file-upload', 'label' => $this->Html->tag('i', '', ['class' => 'fa fa-file-image-o']) . ' Escolher']); ?>
				<div class="row collapse">
					<div class="clearfix">&nbsp;</div>
					<div class="small-12 column">
						<a target="_blank" href="<?php echo $this->Html->url('/files/configuracao/background_site_dpj/1/' . $this->data['Configuracao']['background_site_dpj']) ?>" class="th" role="button" arial-label="Thumbnail"><?php echo $this->Html->image('/files/configuracao/background_site_dpj/1/' . $this->data['Configuracao']['background_site_dpj']); ?></a>
					</div>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Valores</legend>
			<div class="row">
				<div class="small-4 columns end">
					<?php echo $this->Form->input('valor_dolar', ['label' => 'Cotação Dólar']); ?>
				</div>
			</div>

			<div class="row">
				<div class="small-6 columns">
					<?php echo $this->Form->input('minimo_mensal_dpf', ['label' => 'Doação Mínima Mensal para PF']); ?>
				</div>
				<div class="small-6 columns">
					<?php echo $this->Form->input('minimo_anual_dpf', ['label' => 'Doação Mínima Anual para PF']); ?>
				</div>
				<hr>

				<div class="small-6 columns">
					<?php echo $this->Form->input('minimo_mensal_dpj', ['label' => 'Doação Mínima Mensal para PJ']); ?>
				</div>
				<div class="small-6 columns">
					<?php echo $this->Form->input('minimo_anual_dpj', ['label' => 'Doação Mínima Anual para PJ']); ?>
				</div>
			</div>
		</fieldset>

		<div class="row panel">
			<?php echo $this->Form->button('Salvar Configurações', ['class' => 'button success']); ?>
			<?php echo $this->Form->end() ?>
		</div>
	</div>
</div>
<?php echo $this->Html->scriptBlock(
	"$('.file-upload').change(function() {
		var filepath = this.value;
		var m = filepath.match(/([^\/\\\\]+)$/);
		var filename = m[1];
		$(this).siblings('span').html(filename);

	});",
	['inline' => false]
); ?>