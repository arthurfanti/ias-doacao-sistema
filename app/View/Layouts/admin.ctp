<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Área administrativa</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<?= $this->Html->css(array('adm/libraries.min', 'adm/app.min')); ?>
	<?= $this->Html->script('adm/modernizr.min') ?>
</head>
<body>

<div class="off-canvas-wrap" data-offcanvas>
	<div class="inner-wrap" style="min-height:100vh">
		<nav class="tab-bar">

			<section class="left-small">
				<a class="left-off-canvas-toggle menu-icon" ><span></span></a>
			</section>

			<section class="middle tab-bar-section">
				<h1 class="title">Instituto Ayrton Senna - Doações</h1>
			</section>

		</nav>

		<!-- Off Canvas Menu -->
		<aside class="left-off-canvas-menu">
			<ul class="off-canvas-list">
				<li>
					<label><small>Logado como</small><br><?php echo $this->Session->read('Auth.User.Doador.nome') . ' ' . $this->Session->read('Auth.User.Doador.sobrenome') ?></label>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Doações',
							array(
								'controller' => 'doacoes',
								'action' => 'index',
								'plugin' => false
							),
							array(
								'class' => 'fa fa-ticket'
							)
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Relatórios',
							array(
								'controller' => 'relatorios',
								'action' => 'index',
								'plugin' => false
							),
							array(
								'class' => 'fa fa-ticket'
							)
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Doadores',
							array(
								'controller' => 'doadores',
								'action' => 'index',
								'plugin' => false
							),
							array(
								'class' => 'fa fa-users'
							)
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Usuários',
							array(
								'controller' => 'users',
								'action' => 'index',
								'plugin' => 'users'
							),
							array(
								'class' => 'fa fa-user fa-inverse'
							)
						);
					?>
				</li>
				<li>
					<label>Configurações</label>
				</li>
				<!-- <li class="has-submenu"><a href="#" class="fa fa-cogs"> Configurações</a> -->
					<!-- <ul class="left-submenu"> -->
						<!-- <li class="back"><a href="#">Voltar</a></li> -->
				<li>
					<?php
						echo $this->Html->link(
							' Configurações Gerais',
							array(
								'controller' => 'configuracoes',
								'action' => 'index',
								'plugin' => false
							),
							array('class' => 'fa fa-wrench')
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Termos / Aceites',
							array(
								'controller' => 'aceites',
								'action' => 'index',
								'plugin' => false
							),
							array('class' => 'fa fa-gavel')
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Valores',
							array(
								'controller' => 'valores',
								'action' => 'index',
								'plugin' => false
							),
							array('class' => 'fa fa-dollar')
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Mensagens de sistema',
							array(
								'controller' => 'mensagens',
								'action' => 'index',
								'plugin' => false
							),
							array('class' => 'fa fa-quote-left')
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Formas de Pagamento',
							array(
								'controller' => 'FormasPagamentos',
								'action' => 'index',
								'plugin' => false
							),
							array('class' => 'fa fa-credit-card')
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Campanhas',
							array(
								'controller' => 'campanhas',
								'action' => 'index',
								'plugin' => false
							),
							array('class' => 'fa fa-cubes')
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Promoções',
							array(
								'controller' => 'promocoes',
								'action' => 'index',
								'plugin' => false
							),
							array('class' => 'fa fa-bullhorn fa-invert')
						);
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link(
							' Brindes',
							array(
								'controller' => 'itens',
								'action' => 'index',
								'plugin' => false
							),
							array('class' => 'fa fa-gift')
						);
					?>
				</li>
					<!-- </ul> -->
				<!-- </li> -->
				<li>
					<label><?php echo $this->Html->link('sair <i class="fa fa-sign-out"></i>', '/logout', ['escape' => false, 'class' => 'logout']) ?></label>
				</li>
			</ul>
		</aside>

		<section id="content" class="row collapse main-section" style="min-height:1vh">
			<div class="row collapse">
				<div class="small-12 column">
					<?php echo $this->Session->flash(); ?>

					<h2 style="text-transform:capitalize"><?= $title ?>
					<?php
						if (isset($has_add) && $has_add && $this->params->action == 'admin_index' && $this->params->controller != 'doacoes') {
							echo $this->Html->link(
								$this->Html->tag('i', ' Adicionar ' . $title, array('class' => 'fa fa-plus-square')),
								array('action' => 'adicionar'),
								array(
									'class'  => 'button right',
									'escape' => false
								)
							);
						}
					?>
					</h2>
					<hr>
				</div>
			</div>
			<!-- content goes in here -->
			<?php echo $content_for_layout ?>
		</section>

	<!-- close the off-canvas menu -->
	<a class="exit-off-canvas"></a>

	</div>
</div>
	<?= $this->Html->script(array('adm/libraries.min', 'adm/foundation.min', 'adm/app.min')) ?>
	<?php if ($this->params['controller'] == 'doacoes' && $this->params['action'] == 'admin_adicionar'): ?>
	<script type="text/javascript">
		$(document).foundation({
			abide : {
				validators : {
					testaValorDoado: function(el, required, parent) {
						var tipoDoacao   = $('#tipo-doacao')
						, valorDoacao    = $('#DoacaoValor')
						, valorDoacaoVal = parseInt( (valorDoacao.val().replace(/[^0-9]/g, ""))/100 );

						if (tipoDoacao.val() == 'M' || tipoDoacao.val() == 1) {
							if (valorDoacaoVal < valuesObject.minMonthly) {
								var textToReplace = 'O valor mínimo para doação mensal é de R$'+ valuesObject.minMonthly +',00';
								valorDoacao.next().replaceWith('<small class="error">'+textToReplace+'</small>');
								return false;
							} else{
								return true;
							};
						} else if (tipoDoacao.val() == 'U' || tipoDoacao.val() == 2){
							if (valorDoacaoVal < valuesObject.minUnique) {
								var textToReplace = 'O valor mínimo para doação única é de R$'+ valuesObject.minUnique +',00';
								valorDoacao.next().replaceWith('<small class="error">'+textToReplace+'</small>');
								return false;
							} else{
								return true;
							};
						};
					}
				}
			}
		});
	</script>
	<?php endif ?>
	<?php echo $this->fetch('script'); ?>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
