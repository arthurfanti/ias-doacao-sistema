<!DOCTYPE html>
<html class="no-js" lang="pt" >
  <head>
	<?php echo $this->Html->charset(); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<title><?= __("Doação - Instituto Ayrton Senna")?></title>

	<?php
		// echo $this->Html->meta('icon');
		echo $this->fetch('meta');

		echo $this->Html->css([
			'libraries.min',
			'app.min'
		], ['fullBase' => true]);

		echo $this->fetch('css');

		echo $this->Html->script([
			'adm/modernizr.min',
			'libraries.min',
		], ['fullBase' => true]);

	?>
	<script>
	$(function() {
		$.language = '<?= ($this->Session->check('Config.language')) ? $this->Session->read('Config.language') : 'por' ; ?>';
		$.url = '<?= $this->Html->url("/", true); ?>'
	});
	</script>
	<?php if( ($this->Session->check('Doacao.doador') && $this->Session->read('Doacao.doador') === 'PJ') || ($this->Session->check('Auth.User.Doador') && $this->Session->read('Auth.User.Doador.pessoa_id') == 2) ): ?>
	<script>$.tipoDoador = 'PJ';</script>
	<?php endif; ?>

	<?php if (PRODUCTION) : ?>
		<!-- tag principal de integração com Google Analytics -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-60922770-1', 'auto');
		ga('send', 'pageview');
		</script>

		<?php if ($this->request->params['action'] == 'inicio') : ?>
			<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
			}
			_fbq.push(['addPixelId', '1570684483180606']);
			})();
			window._fbq = window._fbq || [];
			window._fbq.push(['track', 'PixelInitialized', {}]);
			</script>
			<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1570684483180606&amp;ev=PixelInitialized" /></noscript>

			<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
			}
			_fbq.push(['addPixelId', '699443566777536']);
			})();
			window._fbq = window._fbq || [];
			window._fbq.push(['track', 'PixelInitialized', {}]);
			</script>
			<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=699443566777536&amp;ev=PixelInitialized" /></noscript>

		<?php elseif ($this->request->params['action'] == 'complemento') : ?>
			<!-- Facebook Conversion Code for Doação Concluída -->
			<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
			}
			})();
			window._fbq = window._fbq || [];
			window._fbq.push(['track', '6018147851234', {'value':'0.00','currency':'BRL'}]);
			</script>
			<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6018147851234&amp;cd[value]=0.00&amp;cd[currency]=BRL&amp;noscript=1" /></noscript>

			<!-- Google Code for Convers&atilde;o Conversion Page -->
			<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 968383705;
			var google_conversion_language = "en";
			var google_conversion_format = "3";
			var google_conversion_color = "ffffff";
			var google_conversion_label = "S8UPCL_FoAsQ2bnhzQM";
			var google_conversion_value = 0;
			var google_remarketing_only = false;
			/* ]]> */
			</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
			</script>
			<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968383705/?label=S8UPCL_FoAsQ2bnhzQM&amp;guid=ON&amp;script=0"/>
			</div>
			</noscript>

				<!-- Google Code for Agradecimento Nova 280512 Conversion Page -->
				<script type="text/javascript">
				/* <![CDATA[ */
				var google_conversion_id = 1025957246;
				var google_conversion_language = "en";
				var google_conversion_format = "3";
				var google_conversion_color = "ffffff";
				var google_conversion_label = "xfNYCOTJ_AIQ_rqb6QM";
				var google_conversion_value = 1.00;
				var google_conversion_currency = "USD";
				var google_remarketing_only = false;
				/* ]]> */
				</script>
				<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
				</script>
				<noscript>
				<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1025957246/?value=1.00&amp;currency_code=USD&amp;label=xfNYCOTJ_AIQ_rqb6QM&amp;guid=ON&amp;script=0"/>
				</div>
				</noscript>

		<?php elseif ($this->request->params['action'] == 'agradecimento') : ?>
			<?php if (!empty($doacao)) : ?>
				<!-- E-commerce tracking -->
				<script type="text/javascript">

				var _gaq = _gaq || [];
				_gaq.push(['_setAccount', 'UA-60922770-1']);
				_gaq.push(['_trackPageview']);
				_gaq.push(['_addTrans',
					'<?php echo $doacao["Doacao"]["id"] ?>',           // transaction ID - required
					'<?php echo $doacao["Doador"]["nome"] ?> <?php echo $doacao["Doador"]["sobrenome"] ?>',  // affiliation or store name
					'<?php echo $doacao["Doacao"]["valor"] ?>',          // total - required
					'0.00',           // tax
					'0.00',              // shipping
					'<?php echo $doadorEndereco["DoadorEndereco"]["cidade"] ?>',       // city
					'<?php echo $doadorEndereco["DoadorEndereco"]["estado"] ?>',     // state or province
					'<?php echo $doadorEndereco["DoadorEndereco"]["pais"] ?>'             // country
				]);

				// add item might be called for every item in the shopping cart
				// where your ecommerce engine loops through each item in the cart and
				// prints out _addItem for each
				_gaq.push(['_addItem',
					'<?php echo $doacao["Doacao"]["id"] ?>',           // transaction ID - required
					'DD44',           // SKU/code - required
					'Doacao',        // product name
					'Doacao Senna',   // category or variation
					'<?php echo $doacao["Doacao"]["valor"] ?>',          // unit price - required
					'1'               // quantity - required
				]);
				_gaq.push(['_trackTrans']); //submits transaction to the Analytics servers

				(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
				</script>
			<?php endif ?>
		<?php endif ?>
	<?php endif ?>
  </head>
<body>
	<div class="off-canvas-wrap" data-offcanvas="">
		<div class="inner-wrap"><!-- /////
			Header
			-->
			<aside class="left-off-canvas-menu">
				<ul class="off-canvas-list">
					<li><label>Instituto Ayrton Senna &middot; Doação</label></li>
					<?php if ($this->Session->check('Auth.User')): ?>
					<li><?= $this->Html->link( __('Sair'), '/logout') ?></li>
					<li><?= $this->Html->link( __('Minha Conta'), '/conta') ?></li>
					<?php else: ?>
					<li><?= $this->Html->link( __('Já sou Doador'), '/login') ?></li>
					<?php endif ?>
					<li><a href="#" data-reveal-id="modal-contato"><?= __("Fale Conosco")?></a></li>
					<li><label>Idioma / Language</label></li>
					<li>
						<select name="language" id="language">
							<option value="por">PT</option>
							<option <?= ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'eng') ? 'selected' : null ; ?> value="eng">EN</option>
						</select>
					</li>
				</ul>
			</aside>

			<div id="header" class="small-12">
				<div class="row small-collapse medium-uncollapse">
					<div class="small-6 small-push-1 medium-4 large-3 columns left">
						<div id="instituto-ayrton-senna">
							<?php echo $this->Html->link('', '/') ?>
						</div>
					</div>
					<div class="small-5 medium-2 large-3 columns left seal small-text-right">
						<img src="https://placehold.it/100x80" alt="" class="hide">
					</div>
					<div class="show-for-medium-up small-12 medium-6 columns right login">
						<?php if ($this->Session->check('Auth.User')): ?>
							<?= $this->Html->link( __('Sair'), '/logout', array('class' => 'button small info right')) ?>
							<?= $this->Html->link( __('Minha Conta'), '/conta', array('class' => 'button small info right')) ?>
						<?php else : ?>
							<?= $this->Html->link( __('Já sou Doador'), '/login', array('class' => 'button small info right')) ?>
						<?php endif; ?>
						<a class="button small success right" href="#" data-reveal-id="modal-contato"><?= __("Fale Conosco")?></a>
						<div class="small-2 right">
							<select name="language" id="language">
								<option value="por">PT</option>
								<option <?= ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'eng') ? 'selected' : null ; ?> value="eng">EN</option>
							</select>
						</div>
					</div>
					<div class="show-for-small-only small-12 columns">
						<a href="#" class="left-off-canvas-toggle button small info expand">MENU</a>
					</div>
				</div>
			</div>

			<!-- /////
			Main Content
			-->

			<?php echo $this->fetch('content'); ?>

			<!-- /////
			Footer
			-->
			<div id="footer" class="row small-collapse">
				<div id="usps">
					<div class="row" data-equalizer>
						<div class="small-text-justify small-12 medium-4 columns" data-equalizer-watch>
							<i class="sprite-site-seguro"></i><h3><?= __("Site 100% Seguro")?></h3>
							<!--<?php echo $this->Html->image('icone-certificado-seguranca-ias-doacoes.png', array('width' => '180')) ?>-->
							<!-- Begin DigiCert site seal HTML -->
							<div id="DigiCertClickID_znIIweHc" data-language="en_US">
							<a href="https://www.digicert.com/ssl-certificate.htm"><?php echo $this->Html->image('certificado-de-seguranca-digicert.png') ?></a>
							</div>
							<!-- End DigiCert site seal HTML -->
							<hr class="show-for-small-only">
						</div>
						<div class="small-text-justify small-12 medium-4 columns" data-equalizer-watch>
							<i class="politica-privacidade"></i><h3><?= __("Política Privacidade")?></h3>
							<p>
			            <a href="#"><?= __('Suas informações cadastrais e seus dados não serão comercializados, alugados ou transferidos a terceiros. Respeitamos as boas práticas para envio de comunicados, boletins e newsletters  via e-mail.') ?></a>
								<!--<a href="https://www.google.com.br/maps/place/R.+Dr.+Fernandes+Coelho,+85+-+Pinheiros,+S%C3%A3o+Paulo+-+SP,+05423-040/@-23.568457,-46.6904664,18z/data=!4m2!3m1!1s0x94ce570ac84eb87d:0xbe5ecc51abf1c55a"><?= __('Rua') ?> Dr. Fernandes Coelho, 85<br>
							Pinheiros - São Paulo - SP<br>
							05423-040 - <?= __("Brasil") ?></a>--></p>
							<hr class="show-for-small-only">
						</div>
						<div class="small-text-justify small-12 medium-4 columns" data-equalizer-watch>
							<i class="sprite-contato"></i><h3><?= __("Contato")?></h3>
							<p><a href="mailto:doacao@ias.org.br"><?= __("E-mail")?>: doacao@ias.org.br</a><br>
							<a href="tel:+551129743062"><?= __("Telefone")?>: 11 2974-3062</a></p>
						</div>
					</div>
				</div>
				<div id="credits">
					<div class="row">
						<ul>
							<li><a href="#"><?= __('Todos os direitos reservados.')?> © 2015 - Instituto Ayrton Senna</a></li>
							<li><a href="http://www.dominidesign.com.br/"><?= __("Desenvolvido por") ?> <span class="domini-digital"></span></a></li>
						</ul>
					</div>
				</div>
			</div>

			<!-- Modal contato por e-mail -->
			<?php echo $this->element('modal-contato'); ?>

			<?php echo $this->element('sql_dump'); ?>

			<?php
				echo $this->Html->script([

					'adm/foundation.min',
					'app'
				]);

				echo $this->fetch('script');
			?>
			<a class="show-for-small-only exit-off-canvas"></a>
		</div>
	</div>
		<!-- Código do Google para tag de remarketing -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 968383705;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/968383705/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
</body>
</html>