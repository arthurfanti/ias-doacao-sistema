<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $this->fetch('title'); ?></title>
<style>html, body{padding:0px; margin:0px;}</style>
</head>
<?php $url_site = 'http://dev.dominidesign.com.br/ias-doacao/app/webroot/images/email/' ?>
<body bgcolor="#f2f2f" style="display:block !important; padding:0px !important; margin:0px !important; font-family: Arial, helvetica, sans-serif !important; color:#8c9192 !important; text-decoration:none !important; position:relative;">
<div style="background:#f2f2f2; height:100%; width:100%">



<!-- Início Header -->
	<table id="header" width="680" height="110" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-bottom:0px solid #f2f2f2;">
		<tr style="padding:14px; color:#ffffff; text-decoration:none;">
        
			<td width="507"><a href="http://institutoayrtonsenna.org.br"><img src="<?php echo $url_site ?>150204_transacionais_ias_doacoes_logo_lampada.png" style="margin-left: 30px;"></a>&nbsp;</td>
            <td width="152" style="padding-top:55px"><a href="https://ias.doacaosenna.org.br" style="font-family: Arial, helvetica, sans-serif; color:#6f7073; text-decoration:none; font-size:10px"><strong>Educação do futuro, agora.</strong></a></td>
            <td width="20"></td>
		</tr>
	</table>
<!-- Final Header -->

<table id="footer" width="680" height="20" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-top:0px solid #f2f2f2;">
<tr></tr>
</table>

<!-- Início Barra Site -->
<table id="footer" width="680" height="30" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-top:0px solid #f2f2f2;">
	  <tr> 
      <td width="508" align="right"><a href="http://institutoayrtonsenna.org.br"><img src="<?php echo $url_site ?>/150204_transacionais_ias_doacoes_barra_site.png"></a></td>
	</tr>
  </table>
<!-- Final Barra Site -->

<!-- Conteúdo -->
	<table id="conteudo" width="680" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
		<tr>
			<td style="padding-left:30px; padding-right:100px;" width="680">
				<br/>
				<?php echo $this->fetch('content'); ?>
				<br/><br/><br/>
			</td>
		</tr>
  </table>

<!-- Assinatura -->
  <table id="conteudo" width="680" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
		<tr>
			<td style="padding-left:30px; padding-right:100px;" width="680">
			<?php echo __('Qualquer dúvida, entre em contato com o Instituto Ayrton Senna pelo telefone') ?>  (11) 2974-3062 <?php echo __('ou') ?> e-mail: doacao@ias.org.br
			<br/>
          </td>
		</tr>
  </table>

<!-- USPS -->
	<table id="footer" width="680" height="50" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="padding-top:30px;">
		<td width="327">
        </td>
        
	</table>

<!-- Início Footer -->
  <table id="footer" width="680" height="60" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-top:0px solid #f2f2f2;">
	  <tr style= "background:#6f7073">
			<td align="center"><table id="header2" width="680" height="110" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-bottom:0px solid #f2f2f2;">
			  <tr style="padding:14px; color:#ffffff; text-decoration:none;">
		        <td width="170"><a href="http://institutoayrtonsenna.org.br"><img src="<?php echo $url_site ?>/150204_transacionais_ias_doacoes_logo_capacete.png" style="margin-left: 30px;"></a>&nbsp;</td>
                <td width="80"></td>
                <td width="25" style="padding-top:30px"><a href= "mailto:doacao@ias.org.br"><img src="<?php echo $url_site ?>/150204_transacionais_ias_doacoes_email.png"></a></td>
                <td width="17"></td>
                <td width="20" style="padding-top:30px"><a href="https://instagram.com/institutoayrtonsenna/"><img src="<?php echo $url_site ?>/150204_transacionais_ias_doacoes_instagram.png"></a></td>
                <td width="17"></td>
                <td width="20" style="padding-top:30px"><a href="https://www.facebook.com/institutoayrtonsenna"><img src="<?php echo $url_site ?>/150204_transacionais_ias_doacoes_facebook.png"></a></td>
                <td width="17"></td>
                <td width="20" style="padding-top:30px"><a href="https://twitter.com/instayrtonsenna"><img src="<?php echo $url_site ?>/150204_transacionais_ias_doacoes_twitter.png"></a></td>
                <td width="20"></td>
                <td width="20" style="padding-top:30px"><a href="https://www.youtube.com/user/InstitutoAyrtonSenna"><img src="<?php echo $url_site ?>/150204_transacionais_ias_doacoes_youtube.png"></a></td>
                <td width="80"></td>
                
			    <td width="152" style="padding-top:38px"><a href="https://ias.doacaosenna.org.br" style="font-family: Arial, helvetica, sans-serif; color:#6f7073; text-decoration:none; font-size:10px"><strong>Educação do futuro, agora.</strong></a></td>
			    <td width="22"></td>
		      </tr>
		    </table></td>
	</tr>
  </table>

</div>
</body>
</html>