<?php echo $this->Form->create() ?>
<?php echo $this->Form->input('titulo', array('label' => 'Título')) ?>
<?php echo $this->Form->input('data_inicial', array('type' => 'text', 'label' => 'Data inicial')) ?>
<?php echo $this->Form->input('data_final', array('type' => 'text', 'label' => 'Data final')) ?>
<?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => 'Status')) ?>
<?php echo $this->Form->submit('Salvar') ?>
<?php echo $this->Form->end() ?>