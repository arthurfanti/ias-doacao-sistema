<article id="list">
	<div class="row">
		<div class="small-12 column">
			<table width="100%" id="datatable" class="cell-border hover order-column">
				<thead>
					<tr>
						<th>Mensagem</th>
						<th width="35%">Ações</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($mensagens as $item) : ?>
					<tr>
						<td><?php echo $item["Mensagem"]["mensagem"] ?></td>
						<td>
						<?php echo $this->Html->link(
							$this->Html->tag('i', ' Editar', array('class' => 'fa fa-edit')),
							array('action' => 'alterar', $item["Mensagem"]["id"]),
							array(
								'class'  => 'button tiny info radius',
								'style'  => 'margin-bottom:0; margin-right:1rem;',
								'escape' => false
							)
						);

						// echo $this->Html->link(
						// 	$this->Html->tag('i', ' Deletar', array('class' => 'fa fa-remove')),
						// 	array('action' => 'del', $item["Mensagem"]["id"]),
						// 	array(
						// 		'confirm' => 'Deseja realmente excluir este item? Lembre-se: Esta ação não poderá ser desfeita!',
						// 		'class'   => 'button tiny alert radius',
						// 		'style'   => 'margin-bottom:0; margin-right:1rem;',
						// 		'escape'  => false
						// 	)
						// )
						?>
					</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</article>
<hr>
