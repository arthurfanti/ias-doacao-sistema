<?php echo $this->Form->create() ?>
<?php echo $this->Form->hidden('id') ?>
<?php foreach ($idiomas as $idioma => $sigla) : ?>
Nome
<?php echo $this->Form->input("Mensagem.mensagem.$sigla", array('value' => $this->data["Mensagem"]["i18n_mensagem_$sigla"])) ?>
Descrição
<?php echo $this->Form->input("Mensagem.descricao.$sigla", array('value' => $this->data["Mensagem"]["i18n_descricao_$sigla"], 'type' => 'textarea')) ?>
<hr />
<?php endforeach ?>
<?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => 'Status')) ?>
<?php echo $this->Form->input('tipo', array('type' => 'select', 'label' => 'Tipo de mensagem')) ?>
<?php echo $this->Form->button('Salvar', array('type' => 'submit')) ?>
<?php echo $this->Form->end() ?>

<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
<script>
	CKEDITOR.replace('MensagemDescricaoPor');
	CKEDITOR.replace('MensagemDescricaoEng');
</script>