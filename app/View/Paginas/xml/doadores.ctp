<doadores>
<?php $doador = $data ?>
<?php //foreach ($data as $doador): ?>
	<doador>
		<codoador><?php echo $doador['Doador']['id'] ?></codoador>
		<nome><?php echo $doador['Doador']['nome'] ?></nome>
		<sobrenome><?php echo $doador['Doador']['sobrenome'] ?></sobrenome>
		<cpf><?php echo $doador['Doador']['cpf_cnpj'] ?></cpf>
		<sexo><?php echo (!isset($doador['DpjComplemento']['sexo'])) ? $doador['Doador']['sexo'] : $doador['DpjComplemento']['sexo'] ?></sexo>
		<datanascimento><?php echo $doador['Doador']['nascimento'] ?></datanascimento>
		<email1><?php echo $doador['DoadorEmail'][0]['email'] ?></email1>
		<email2><?php echo (isset($doador['DoadorEmail'][1]['email'])) ? $doador['DoadorEmail'][1]['email'] : ''; ?></email2>
		<foneresidencial><?php echo (isset($doador['DoadorTelefone'][0]['numero'])) ? $doador['DoadorTelefone'][0]['numero'] : ''; ?></foneresidencial>
		<fonecomercial><?php echo (isset($doador['DoadorTelefone'][1]['numero'])) ? $doador['DoadorTelefone'][1]['numero'] : ''; ?></fonecomercial>
		<fonecelular><?php echo (isset($doador['DoadorTelefone'][2]['numero'])) ? $doador['DoadorTelefone'][2]['numero'] : ''; ?></fonecelular>
		<endereco1><?php echo (isset($doador['DoadorEndereco'][0]['logradouro'])) ? $doador['DoadorEndereco'][0]['logradouro'] : ''; ?></endereco1>
		<numeroendereco1><?php echo (isset($doador['DoadorEndereco'][0]['numero'])) ? $doador['DoadorEndereco'][0]['numero'] : ''; ?></numeroendereco1>
		<complementoendereco1><?php echo (isset($doador['DoadorEndereco'][0]['complemento'])) ? $doador['DoadorEndereco'][0]['complemento'] : ''; ?></complementoendereco1>
		<cep1><?php echo (isset($doador['DoadorEndereco'][0]['cep'])) ? $doador['DoadorEndereco'][0]['cep'] : ''; ?></cep1>
		<bairro1><?php echo (isset($doador['DoadorEndereco'][0]['bairro'])) ? $doador['DoadorEndereco'][0]['bairro'] : ''; ?></bairro1>
		<cidade1><?php echo (isset($doador['DoadorEndereco'][0]['cidade'])) ? $doador['DoadorEndereco'][0]['cidade'] : ''; ?></cidade1>
		<estado1><?php echo (isset($doador['DoadorEndereco'][0]['estado'])) ? $doador['DoadorEndereco'][0]['estado'] : ''; ?></estado1>
		<pais1><?php echo (isset($doador['DoadorEndereco'][0]['pais'])) ? $doador['DoadorEndereco'][0]['pais'] : ''; ?></pais1>
		<endereco2><?php echo (isset($doador['DoadorEndereco'][1]['logradouro'])) ? $doador['DoadorEndereco'][1]['logradouro'] : ''; ?></endereco2>
		<numeroendereco2><?php echo (isset($doador['DoadorEndereco'][1]['numero'])) ? $doador['DoadorEndereco'][1]['numero'] : ''; ?></numeroendereco2>
		<complementoendereco2><?php echo (isset($doador['DoadorEndereco'][1]['complemento'])) ? $doador['DoadorEndereco'][1]['complemento'] : ''; ?></complementoendereco2>
		<cep2><?php echo (isset($doador['DoadorEndereco'][1]['cep'])) ? $doador['DoadorEndereco'][1]['cep'] : ''; ?></cep2>
		<bairro2><?php echo (isset($doador['DoadorEndereco'][1]['bairro'])) ? $doador['DoadorEndereco'][1]['bairro'] : ''; ?></bairro2>
		<cidade2><?php echo (isset($doador['DoadorEndereco'][1]['cidade'])) ? $doador['DoadorEndereco'][1]['cidade'] : ''; ?></cidade2>
		<estado2><?php echo (isset($doador['DoadorEndereco'][1]['estado'])) ? $doador['DoadorEndereco'][1]['estado'] : ''; ?></estado2>
		<pais2><?php echo (isset($doador['DoadorEndereco'][1]['pais'])) ? $doador['DoadorEndereco'][1]['pais'] : ''; ?></pais2>
		<comosoube><?php echo $doador['Conheceu']['id'] ?></comosoube>
		<aceitaReceberInformativo><?php echo ($doador['Doador']['recebe_email'] == 1) ? '0' : '1'; ?></aceitaReceberInformativo>
		<dpf><?php echo $doador['Pessoa']['tipo'] ?></dpf>
		<datacadastro><?php echo $doador['Doador']['created'] ?></datacadastro>
		<dtatualizacao><?php echo $doador['Doador']['modified'] ?></dtatualizacao>
		<email_amigo></email_amigo>
		<Cnome_DPJ><?php echo (isset($doador['DpjComplemento']['nome'])) ? $doador['DpjComplemento']['nome'] : ''; ?></Cnome_DPJ>
		<Cargo_Cnome_DPJ><?php echo (isset($doador['DpjComplemento']['cargo'])) ? $doador['DpjComplemento']['cargo'] : ''; ?></Cargo_Cnome_DPJ>
		<Inome_DPJ><?php echo (isset($doador['DpjComplemento']['representante'])) ? $doador['DpjComplemento']['representante'] : ''; ?></Inome_DPJ>
		<faixa_inicial_DPJ><?php echo (isset($doador['DpjComplemento']['Faixa']['faixa_inicial'])) ? str_replace('.', ',', $doador['DpjComplemento']['Faixa']['faixa_inicial']) : ''; ?></faixa_inicial_DPJ>
		<faixa_final_DPJ><?php echo (isset($doador['DpjComplemento']['Faixa']['faixa_final'])) ? str_replace('.', ',', $doador['DpjComplemento']['Faixa']['faixa_final']) : ''; ?></faixa_final_DPJ>
		<cod_nat_DPJ><?php echo (isset($doador['DpjComplemento']['Natureza']['codigo'])) ? $doador['DpjComplemento']['Natureza']['codigo'] : ''; ?></cod_nat_DPJ>
		<desc_nat_DPJ><?php echo (isset($doador['DpjComplemento']['Natureza']['descricao'])) ? $doador['DpjComplemento']['Natureza']['descricao'] : ''; ?></desc_nat_DPJ>
	</doador>
<?php //endforeach ?>
</doadores>