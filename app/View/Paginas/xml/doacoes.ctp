<doacoes>
<?php $doacao = $data ?>
<?php //foreach ($data as $doacao): ?>
	<doacao>
		<Internacional><?php echo ($doacao['Doador']['conta_brasil'] == 1) ? '1' : '2'; ?></Internacional>
		<id_doacao><?php echo $doacao['Doacao']['id'] ?></id_doacao>
		<cod_doador><?php echo $doacao['Doacao']['doador_id'] ?></cod_doador>
		<Ciclo><?php echo $doacao['Doacao']['periodo_id'] ?></Ciclo>
		<Data><?php echo $doacao['Doacao']['created'] ?></Data>
		<Valor><?php echo str_replace('.', ',', $doacao['Doacao']['valor']) ?></Valor>
		<Campanhas><?php echo (isset($doacao['Campanhas']['campanha'])) ? $doacao['Campanhas']['campanha'] : null ?></Campanhas>
		<Forma_pgto><?php echo $doacao['Pagamento']['formas_pagamento_id'] ?></Forma_pgto>
		<Dbanco><?php echo (isset($doacao['Pagamento']['FormasPagamento']['Banco']['codigo'])) ? $doacao['Pagamento']['FormasPagamento']['Banco']['codigo'] : null ?></Dbanco>
		<Dagencia><?php echo $doacao['Pagamento']['agencia'] ?></Dagencia>
		<Dconta><?php echo $doacao['Pagamento']['conta'] ?></Dconta>
		<dia_debito><?php echo (isset($doacao['Pagamento']['DiaDebito']['dia'])) ? $doacao['Pagamento']['DiaDebito']['dia'] : null ?></dia_debito>
		<Ativo><?php echo ($doacao['Doacao']['status'] == 'A') ? '1' : '0' ?></Ativo>
		<data_cancelamento><?php echo (isset($doacao['CancelamentoDoacao']['created'])) ? $doacao['CancelamentoDoacao']['created'] : null ?></data_cancelamento>
		<motivo_cancelamento></motivo_cancelamento>
		<obs_cancelamento><?php echo (isset($doacao['CancelamentoDoacao']['obs'])) ? $doacao['CancelamentoDoacao']['obs'] : null ?></obs_cancelamento>
		<data_aceite><?php echo (isset($doacao['AceiteDoacao']['created'])) ? $doacao['AceiteDoacao']['created'] : $doacao['Doacao']['created'] ?></data_aceite>
		<titulo_promo><?php echo (isset($doacao['Brinde']['Item']['Promocao']['titulo'])) ? $doacao['Brinde']['Item']['Promocao']['titulo'] : null ?></titulo_promo>
		<dt_ini_promo><?php echo (isset($doacao['Brinde']['Item']['Promocao']['data_inicial'])) ? $doacao['Brinde']['Item']['Promocao']['data_inicial'] : null ?></dt_ini_promo>
		<dt_fim_promo><?php echo (isset($doacao['Brinde']['Item']['Promocao']['data_final'])) ? $doacao['Brinde']['Item']['Promocao']['data_final'] : null ?></dt_fim_promo>
		<item_promo><?php echo (isset($doacao['Brinde']['Item']['item'])) ? $doacao['Brinde']['Item']['item'] : null ?></item_promo>
		<obs_promo><?php echo (isset($doacao['Brinde']['Item']['Promocao']['titulo'])) ? $doacao['Brinde']['Item']['Promocao']['titulo'] : null ?></obs_promo>
	</doacao>
<?php //endforeach ?>
</doacoes>