<parcelas>
<?php foreach ($data as $parcela): ?>
	<parcela>
		<id_doacao><?php echo $parcela['Parcela']['doacao_id'] ?></id_doacao>
		<Parcela><?php echo $parcela['Parcela']['parcela'] ?></Parcela>
		<msg_retorno><?php echo (isset($parcela['Parcela']['mensagem_retorno'])) ? $parcela['Parcela']['mensagem_retorno'] : null ?></msg_retorno>
		<Situacao><?php echo $parcela['Parcela']['status_id'] ?></Situacao>
		<Valor><?php echo str_replace('.', ',', $parcela['Parcela']['valor']) ?></Valor>
		<data_emissao><?php echo $parcela['Parcela']['data_geracao'] ?></data_emissao>
		<data_pagamento><?php echo (isset($parcela['Parcela']['data_pagamento'])) ? $parcela['Parcela']['data_pagamento'] : null ?></data_pagamento>
		<nosso_numero><?php echo (isset($parcela['Parcela']['nossonumero'])) ? $parcela['Parcela']['nossonumero'] : null ?></nosso_numero>
		<NSU><?php echo (!empty($parcela['Parcela']['nsu'])) ? $parcela['Parcela']['nsu'] : null ?></NSU>
		<autorizacao><?php echo (!empty($parcela['Parcela']['autorizacao'])) ? $parcela['Parcela']['autorizacao'] : null ?></autorizacao>
	</parcela>
<?php endforeach ?>
</parcelas>