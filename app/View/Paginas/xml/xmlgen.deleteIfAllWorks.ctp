<doadores>
<?php foreach ($data as $doador): ?>
	<doador>
		<codoador><?php echo $doador['Doador']['id'] ?></codoador>
		<nome><?php echo $doador['Doador']['nome'] ?></nome>
		<sobrenome><?php echo $doador['Doador']['sobrenome'] ?></sobrenome>
		<cpf><?php echo $doador['Doador']['cpf_cnpj'] ?></cpf>
		<sexo><?php echo ($doador['Doador']['sexo'] == 1) ? 'Masculino' : 'Feminino'; ?></sexo>
		<datanascimento><?php echo $doador['Doador']['nascimento'] ?></datanascimento>
		<email1><?php echo $doador['User']['email'] ?></email1>
		<email2></email2>
		<foneresidencial><?php echo (isset($doador['DoadorTelefone'][0]['numero'])) ? $doador['DoadorTelefone'][0]['numero'] : ''; ?></foneresidencial>
		<fonecomercial><?php echo (isset($doador['DoadorTelefone'][1]['numero'])) ? $doador['DoadorTelefone'][1]['numero'] : ''; ?></fonecomercial>
		<fonecelular><?php echo (isset($doador['DoadorTelefone'][2]['numero'])) ? $doador['DoadorTelefone'][2]['numero'] : ''; ?></fonecelular>
		<endereco1><?php echo (isset($doador['DoadorEndereco'][0]['logradouro'])) ? $doador['DoadorEndereco'][0]['logradouro'] : ''; ?></endereco1>
		<numeroendereco1><?php echo (isset($doador['DoadorEndereco'][0]['numero'])) ? $doador['DoadorEndereco'][0]['numero'] : ''; ?></numeroendereco1>
		<complementoendereco1><?php echo (isset($doador['DoadorEndereco'][0]['complemento'])) ? $doador['DoadorEndereco'][0]['complemento'] : ''; ?></complementoendereco1>
		<cep1><?php echo (isset($doador['DoadorEndereco'][0]['cep'])) ? $doador['DoadorEndereco'][0]['cep'] : ''; ?></cep1>
		<bairro1><?php echo (isset($doador['DoadorEndereco'][0]['bairro'])) ? $doador['DoadorEndereco'][0]['bairro'] : ''; ?></bairro1>
		<cidade1><?php echo (isset($doador['DoadorEndereco'][0]['cidade'])) ? $doador['DoadorEndereco'][0]['cidade'] : ''; ?></cidade1>
		<estado1><?php echo (isset($doador['DoadorEndereco'][0]['estado'])) ? $doador['DoadorEndereco'][0]['estado'] : ''; ?></estado1>
		<pais1><?php echo (isset($doador['DoadorEndereco'][0]['pais'])) ? $doador['DoadorEndereco'][0]['pais'] : ''; ?></pais1>
		<endereco2></endereco2>
		<numeroendereco2></numeroendereco2>
		<complementoendereco2></complementoendereco2>
		<cep2></cep2>
		<bairro2></bairro2>
		<cidade2></cidade2>
		<estado2></estado2>
		<pais2></pais2>
		<comosoube><?php echo $doador['Conheceu']['tipo'] ?></comosoube>
		<aceitaReceberInformativo><?php echo ($doador['Doador']['recebe_email'] == 1) ? 'Sim' : 'Não'; ?></aceitaReceberInformativo>
		<dpf><?php echo $doador['Pessoa']['tipo'] ?></dpf>
		<datacadastro></datacadastro>
		<dtatualizacao></dtatualizacao>
		<email_amigo></email_amigo>
		<Cnome_DPJ></Cnome_DPJ>
		<Cargo_Cnome_DPJ></Cargo_Cnome_DPJ>
		<Inome_DPJ></Inome_DPJ>
		<faixa_inicial_DPJ></faixa_inicial_DPJ>
		<faixa_final_DPJ></faixa_final_DPJ>
		<cod_nat_DPJ></cod_nat_DPJ>
		<desc_nat_DPJ></desc_nat_DPJ>
	</doador>
<?php endforeach ?>
</doadores>