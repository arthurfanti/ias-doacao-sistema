<?php echo $this->Form->create() ?>
<?php foreach ($idiomas as $idioma => $sigla) : ?>
	<?php echo $this->Form->input("Assunto.titulo.$sigla", array('label' => 'Título')) ?>
	<hr />
<?php endforeach ?>
<?php echo $this->Form->input("Assunto.destino", array('label' => 'Destino')) ?>
<?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => 'Status')) ?>
<br />
<?php echo $this->Form->button('Salvar', array('type' => 'submit')) ?>
<?php echo $this->Form->end() ?>