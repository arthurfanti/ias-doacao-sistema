<?php echo $this->Form->create() ?>
<?php echo $this->Form->hidden('id') ?>
<?php foreach ($idiomas as $idioma => $sigla) : ?>
<?php echo $this->Form->input("Assunto.titulo.$sigla", array('value' => $this->data["Assunto"]["i18n_titulo_$sigla"], 'label' => 'Título')) ?>
<hr />
<?php endforeach ?>
<?php echo $this->Form->input("Assunto.destino") ?>
<?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => 'Status')) ?>
<br />
<?php echo $this->Form->button('Salvar', array('type' => 'submit')) ?>
<?php echo $this->Form->end() ?>