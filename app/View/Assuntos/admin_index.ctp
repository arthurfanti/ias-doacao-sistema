<article id="list">
	<div class="row">
		<div class="small-12 column">
			<table width="100%" id="datatable" class="cell-border hover order-column">
				<thead>
					<tr>
						<th>Título</th>
						<th width="35%">Ações</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($assuntos as $item) : ?>
					<tr>
						<td><?php echo $item["Assunto"]["titulo"] ?></td>
						<td>
						<?php echo $this->Html->link(
							$this->Html->tag('i', ' Editar', array('class' => 'fa fa-edit')),
							array('action' => 'alterar', $item["Assunto"]["id"]),
							array(
								'class'  => 'button tiny info radius',
								'style'  => 'margin-bottom:0; margin-right:1rem;',
								'escape' => false
							)
						);
						?>
					</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</article>
<hr>
