<br/>
<h1><?php echo __('Olá') ?>,</h1>
<p>
	<span style="font-family: Arial, helvetica, sans-serif !important; color:#6f7073 !important; text-decoration:none !important; font-size:22px;">
		<?php echo __('Eu apoio a causa da educação no Brasil e sou um doador do Instituto Ayrton Senna') ?>!
	</span>
</p>
<p>
	<?php echo __('Venha você também fazer parte deste grande movimento. Vamos juntos mudar a realidade de milhares de crianças e jovens') ?>. <?php echo __('Faça sua doação clicando <a href="https://www.ias.doacaosenna.org.br">aqui</a>') ?>.
</p>
<p class="lead"><?php echo $share_message; ?></p>
<br />
<?php echo __('Conto com você') ?>!
<br />
<?php echo $nome; ?>