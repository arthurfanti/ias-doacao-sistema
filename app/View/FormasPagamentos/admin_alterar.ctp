<?php echo $this->Form->create(array('type' => 'file')) ?>
<?php echo $this->Form->hidden('id') ?>
<?php echo $this->Form->input('nome') ?>
<?php echo $this->Form->input('tipo', array('type' => 'select', 'options' => $formasPagamentos)) ?>
<?php echo $this->Form->input('mensagem_id') ?>
<?php echo $this->Form->input('logo', array('type' => 'file', 'label' => 'Logo', 'div' => 'panel')) ?>
<?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => 'Ativo')) ?>
<?php echo $this->Form->button('Salvar', array('type' => 'submit', 'class' => 'right')) ?>
<?php echo $this->Form->end() ?>
