<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="row">
	<div class="small-12 columns">
		<h2 class="text-center" style="font-size: 12rem; letter-spacing: -.5rem; color: #ed1c24; line-height: 1; margin-bottom: -1.65rem; z-index: 1; position: relative;">404</h2>
	</div>
</div>
<div class="alert-box alert">
	<div class="row">
		<div class="small-12 text-center">
			<p class="lead">
				<?php printf(
					__('A página %s solicitada não pôde ser localizada.'),
					"<strong>'{$url}'</strong>"
				); ?>
			</p>
		</div>
	</div>
</div>

<div id="valor" class="alert-box success">
	<div class="row">
		<div class="small-12 text-center revenue">
			<p class="lead"><?php echo __('Que tal transformar este erro em um grande acerto?'); ?></p>
			<a href="/valores" data-target="cadastro" name="novo" class="button small-10 medium-4 small-centered columns" type="submit">
				<?= __('Quero doar')?> <i class="sprite-quero-ajudar"></i>
			</a>
		</div>
	</div>
</div>

<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;
?>
