<?php echo $this->Form->create() ?>
<?php echo $this->Form->hidden('id') ?>
<?php echo $this->Form->input('valor', ['label' => 'Valor']) ?>
<?php //echo $this->Form->input('valor_minimo', ['label' => 'Valor']) ?>
<?php //echo $this->Form->input('texto', ['label' => 'Texto']) ?>
<?php echo $this->Form->input('tipo', array('type' => 'select', 'options' => $tiposPagamentos, 'label' => 'Tipo')) ?>
<?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => 'Ativo')) ?>
<?php echo $this->Form->input('pessoa_id', array('label' => 'Pessoa')) ?>
<?php echo $this->Form->input('divisor', array('label' => 'Divisor')) ?>
<?php foreach ($idiomas as $idioma => $sigla) : ?>
Texto antes - <?php echo $sigla ?>
<?php echo $this->Form->input("Valor.texto_antes.$sigla", ['value' => $this->data["Valor"]["i18n_texto_antes_$sigla"]]) ?>
Texto depois - <?php echo $sigla ?>
<?php echo $this->Form->input("Valor.texto_depois.$sigla", ['value' => $this->data["Valor"]["i18n_texto_depois_$sigla"]]) ?>
<hr />
<?php endforeach ?>
<?php echo $this->Form->button('Salvar', array('type' => 'submit')) ?>
<?php echo $this->Form->end() ?>