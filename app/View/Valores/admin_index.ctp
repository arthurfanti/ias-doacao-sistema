<article id="list">
	<div class="row">
		<div class="small-12 column">
			<table width="100%"class="display responsive nowrap">
				<thead>
					<tr>
						<th></th>
						<th>Valor</th>
						<th>Recorrência</th>
						<th>Tipo de Doador</th>
						<th>Mensagem / Equivalência</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th>Valor</th>
						<th>Recorrência</th>
						<th>Tipo de Doador</th>
						<th>Mensagem / Equivalência</th>
						<th>Ações</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($valores as $item) : ?>
					<tr>
						<td></td>
						<td><?php echo $item["Valor"]["valor"] ?></td>
						<td><?php echo $tiposPagamentos[$item["Valor"]["tipo"]] ?></td>
						<td><?php echo ($item['Valor']['pessoa_id'] == 1) ? 'PF' : 'PJ' ?></td>
						<td><?php echo  (isset($item['Valor']['divisor'])) ? $item['Valor']['texto_antes'] . ' ' . intval($item['Valor']['valor'] / $item['Valor']['divisor']) . ' ' . $item['Valor']['texto_depois'] : 'Divisor não definido' ?></td>
						<td>
						<?php echo $this->Html->link(
							$this->Html->tag('i', ' Editar', array('class' => 'fa fa-edit')),
							array('action' => 'alterar', $item["Valor"]["id"]),
							array(
								'class'  => 'button tiny info radius',
								'style'  => 'margin-bottom:0; margin-right:1rem;',
								'escape' => false
							)
						); ?>
					</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</article>

<?php
	echo $this->Html->scriptBlock(
		"$('table.responsive.nowrap').DataTable({
			responsive: {
				details: {
					type: 'inline',
					target: 'tr'
				}
			},
			columnDefs: [{
				className: 'control',
				orderable: false,
				targets: 0
			}],
			order: [2, 'asc'],
			language: {
				processing:     'processando dados...',
				search:         '',
				lengthMenu:     'Itens por p&aacute;gina: _MENU_',
				info:           'Exibindo itens de _START_ a _END_, num total de _TOTAL_ itens',
				infoEmpty:      'Não há itens para exibir',
				infoFiltered:   '(filtrado de _MAX_ itens no total)',
				infoPostFix:    '',
				loadingRecords: 'carregando dados...',
				zeroRecords:    'Não há itens para exibir',
				paginate: {
					first:      'Primeira',
					previous:   'Anterior',
					next:       'Seguinte',
					last:       '&uacute;ltima'
				},
				aria: {
					sortAscending:  ': habilite para classificar a coluna em ordem crescente',
					sortDescending: ': habilite para classificar a coluna em ordem decrescente'
				}
			}
		});

		$('.dataTables_filter input').attr('placeholder', 'Buscar');",
		array('inline' => false));
?>