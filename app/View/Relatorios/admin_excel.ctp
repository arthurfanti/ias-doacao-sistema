<table>
	<thead>
		<th>Doador</th>
		<th>Data</th>
		<th>Valor</th>
		<th>Tipo Doação</th>
		<th>Forma de Pagamento</th>
		<th>Status</th>
	</thead>
	<tbody>
		<?php foreach($data as $doacao): ?>
		<tr>
			<td><?php echo $doacao['Doador']['nome'] . ' ' . $doacao['Doador']['sobrenome'] ?></td>
			<td><?php echo $this->Formatacao->data($doacao['Parcela']['data_pagamento']) ?></td>
			<td><?php echo $this->Formatacao->moeda($doacao['Parcela']['valor']) ?></td>
			<td><?php echo ($doacao['Doacao']['periodo_id'] == 1) ? 'Mensal' : 'Única' ?></td>
			<td><?php echo ucwords(mb_strtolower($doacao['FormasPagamento']['nome'])) ?></td>
			<td><?php echo $doacao['Status']['situacao'];?></td>
		</tr>
		<?php endforeach;?>
	</tbody>
	<tfoot>
		<tr></tr>
		<tr></tr>
		<tr></tr>
	</tfoot>
</table>

<h3><strong>Resultados totais</strong></h3>
<table>

	<tr>
		<td>Total de Doações no período (previsto):</td><td><?php echo $this->Formatacao->moeda($valores['total_doacoes']);?></td>
	</tr>
	<tr>
		<td>Ticket médio (Total de Doação/Doadores):</td><td><?php echo $this->Formatacao->moeda($valores['ticket']);?></td>
	</tr>
	<tr>
		<td>Realizado (Doações Concluídas):</td><td style="color:#5cb85c"><?php echo $this->Formatacao->moeda($valores['doacoes_concluidas']);?></td>
	</tr>
	<tr>
		<td>Inadimplência (Doações pendentes):</td><td style="color:#f0ad4e"><?php echo $this->Formatacao->moeda($valores['total_doacoes']);?></td>
	</tr>

</table>