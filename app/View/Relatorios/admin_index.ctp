<div class="row">
	<div class="small-12 columns">
		<div class="panel">
			<span>para cadastrar manualmente uma doação, vá para a <?php echo $this->Html->link('listagem de doadores', array('controller' => 'doadores', 'action' => 'index', 'admin' => true)) ?> e, na coluna <i>ações</i>, escolha a opção <i>adicionar doação</i>.</span>
		</div>
		<?php echo $this->Form->create() ?>
			<fieldset>
				<legend>Filtros: </legend>
				<div style="position:relative">
					<div class="row">
						<!--div class="small-12 medium-6 columns">
							<select name="show_by" id="show_by">
								<option value="period">Exibir por período de tempo</option>
								<option value="day">Nas últimas 24 horas</option>
							</select>
						</div-->

						<?php if(!empty($data)): ?>
						<div class="small-12 medium-6 columns">
							<ul class="button-group">
								<li class="button tiny secondary disabled">Exportar:</li>
								<li><a href="#" class="button tiny">CSV</a></li>
								<li><a href="relatorios/excel" class="button tiny">XLS</a></li>
								<li><a href="#" class="button tiny">PDF</a></li>
							</ul>
						</div>
						<?php endif; ?>
					</div>

					<div class="row">
						<div class="small-6 medium-5 columns">
							<label for="">De:</label>
							<?php
							$valor_data_inicial = isset($this->data['Parcela']['data_inicial']) ? $this->Formatacao->data($this->data['Parcela']['data_inicial']) : null;
							$valor_data_final   = isset($this->data['Parcela']['data_final']) ? $this->Formatacao->data($this->data['Parcela']['data_final']) : null;
							?>
							<?php echo $this->Form->input('data_inicial', ['class' => 'datepicker', 'placeholder' => '00/00/0000', 'value' => $valor_data_inicial]) ?>
						</div>

						<div class="small-6 medium-5 columns">
							<label for="">Até:</label>
							<?php echo $this->Form->input('data_final', ['class' => 'datepicker', 'placeholder' => '00/00/0000', 'value' => $valor_data_final]) ?>
						</div>
					</div>
					
					<div class="row">
						<div class="small-6 medium-5 columns">
							<label for="">Tipo de doação:</label>
							<select name="tipo_doacao">
								<option value="">Todas</option>
								<?php foreach($periodos as $item):?>
									<option value="<?php echo $item['Periodo']['id'];?>" <?php if(isset($this->data['tipo_doacao']) && $this->data['tipo_doacao'] == $item['Periodo']['id']) echo 'selected';?>><?php echo $item['Periodo']['tipo'];?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class="small-6 medium-5 columns">
							<label for="">Forma de Pagamento:</label>
							<select name="formas_pagamento">
								<option value="">Todas</option>
								<?php foreach($formas as $item):?>
									<option value="<?php echo $item['FormasPagamento']['id'];?>"><?php echo $item['FormasPagamento']['nome'];?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="small-6 medium-5 columns">
							<?php
							$valor = isset($this->data['valor']) ? $this->data['valor'] : '';
							?>
							<label>Valor:</label>
							<input type="text" name="valor" value="<?php echo $valor;?>" class="form-control" placeholder="00.00">
						</div>
						
						<div class="small-6 medium-5 columns">
							<label>Tipo de Pessoa:</label>
							<select name="tipo_pessoa">
							<option value="">Todas</option>
							<?php foreach($pessoas as $item):?>
									<option value="<?php echo $item['Pessoa']['id'];?>"><?php echo $item['Pessoa']['tipo'];?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="small-6 medium-5 columns">
							<?php
							$nome = isset($this->data['Parcela']['nome']) ? $this->data['Parcela']['nome'] : '';
							?>
							<label>Nome do doador:</label>
							<?php echo $this->Form->input('nome', ['class' => 'form-control','placeholder' => 'Nome do doador', 'value' => $nome]) ?>							
						</div>
						
						<div class="small-6 medium-5 columns">
							<label>Status da transação:</label>
							<select name="status">
							<option value="">Todas</option>
							<?php foreach($status as $item):?>
									<option value="<?php echo $item['Status']['id'];?>"><?php echo $item['Status']['situacao'];?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<div class="small-12 medium-2 columns">
							<button type="submit" class="button tiny expand" style="margin-top:1.35rem">
								<i class="fa fa-filter">&nbsp;</i>Filtrar
							</button>
						</div>
					</div>

					<!-- <div style="position:absolute;width:100%;height:100%;background:white;opacity:.5;top:0"></div> -->
				</div>
			</fieldset>
		<?php echo $this->Form->end() ?>
	</div>
</div>

<article id="list">
	<div class="row">
		<div class="small-12 column">
			<table width="100%" id="datatable" class="display responsive nowrap">
				<thead>
					<tr>
						<th></th>
						<th>Doador</th>
						<th>Data</th>
						<th>Valor</th>
						<th>Tipo de Doação</th>
						<th>Forma de Pagamento</th>
						<th>Status</th>
						<th class="desktop">Doador</th>
						<th class="desktop">Data</th>
						<th class="desktop">Valor</th>
						<th class="desktop">Tipo de Doação</th>
						<th class="desktop">Forma de Pagamento</th>
						<th class="desktop">Status</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th>Doador</th>
						<th>Data</th>
						<th>Valor</th>
						<th>Tipo de Doação</th>
						<th>Forma de Pagamento</th>
						<th>Status</th>
						<th class="desktop">Doador</th>
						<th class="desktop">Data</th>
						<th class="desktop">Valor</th>
						<th class="desktop">Tipo de Doação</th>
						<th class="desktop">Forma de Pagamento</th>
						<th class="desktop">Status</th>
						<th>Ações</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($data as $doacao): ?>
						<tr>
							<td></td>
							<td><?php echo $doacao['Doador']['nome'] . ' ' . $doacao['Doador']['sobrenome'] ?></td>
							<td><?php echo $this->Formatacao->data($doacao['Parcela']['data_pagamento']) ?></td>
							<td><?php echo $this->Formatacao->moeda($doacao['Parcela']['valor']) ?></td>
							<td><?php echo ($doacao['Doacao']['periodo_id'] == 1) ? 'Mensal' : 'Única' ?></td>
							<td><?php echo ucwords(mb_strtolower($doacao['FormasPagamento']['nome'])) ?></td>
							<td><?php echo $doacao['Status']['situacao'];?></td>
							
							<td class="desktop"><?php echo $doacao['Doador']['nome'] . ' ' . $doacao['Doador']['sobrenome'] ?></td>
							<td class="desktop"><?php echo $this->Formatacao->data($doacao['Parcela']['data_pagamento']) ?></td>
							<td class="desktop"><?php echo $this->Formatacao->moeda($doacao['Parcela']['valor']) ?></td>
							<td class="desktop"><?php echo ($doacao['Doacao']['periodo_id'] == 1) ? 'Mensal' : 'Única' ?></td>
							<td class="desktop"><?php echo ucwords(mb_strtolower($doacao['FormasPagamento']['nome'])) ?></td>
							<td class="desktop"><?php echo $doacao['Status']['situacao'];?></td>
							<td>
								<?php 
								echo $this->Html->link(
									$this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o']) . ' Editar',
									['action' => 'alterar', $doacao['Doacao']['id']],
									['class' => 'button radius tiny info', 'style' => 'margin-bottom:0', 'escape' => false]
								); 
								?>
								<?php 
								echo $this->Html->link(
								$this->Html->tag('i', '', array('class' => 'fa fa-plus-square')) . ' Painel',
									array('action' => 'painel', $doacao['Doacao']['id']),
									array(
										'class' => 'button tiny radius info',
										'style' => 'margin-bottom:0',
										'escape' => false
								)); 
								?>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="small-12 column">
			
			<h3>Resultados Totais:</h3>
			
			<div class="row">
				<div class="small-6 medium-4 columns">
					<label for="">Total de Doações no período (previsto):</label>
					<input type="text" value="<?php echo $this->Formatacao->moeda($valores['total_doacoes']);?>" disabled>					
				</div>

				<div class="small-6 medium-4 columns">
					<label for="">Ticket médio (Total de Doação/Doadores):</label>
					<input type="text" value="<?php echo $this->Formatacao->moeda($valores['ticket']);?>" disabled>
				</div>

				<div class="small-6 medium-4 columns">
					<label for="">Realizado (Doações Concluídas):</label>
					<input type="text" value="<?php echo $this->Formatacao->moeda($valores['doacoes_concluidas']);?>" disabled style="color:#5cb85c">
				</div>
			</div>

			<div class="row">
				<div class="small-6 medium-4 columns">
					<label for="">Inadimplência (Doações pendentes):</label>
					<input type="text" value="<?php echo $this->Formatacao->moeda($valores['total_doacoes']);?>" disabled style="color:#f0ad4e">					
				</div>
			</div>

		</div>
	</div>

</article>

<?php
	echo $this->Html->scriptBlock(
		"$('table.responsive.nowrap').DataTable({
			responsive: {
				details: {
					type: 'inline',
					target: 'tr'
				}
			},
			columnDefs: [{
				className: 'control',
				orderable: false,
				targets: 0
			}],
			order: [2, 'asc'],
			language: {
				processing:     'processando dados...',
				search:         '',
				lengthMenu:     'Itens por p&aacute;gina: _MENU_',
				info:           'Exibindo itens de _START_ a _END_, num total de _TOTAL_ itens',
				infoEmpty:      'Não há itens para exibir',
				infoFiltered:   '(filtrado de _MAX_ itens no total)',
				infoPostFix:    '',
				loadingRecords: 'carregando dados...',
				zeroRecords:    'Não há itens para exibir',
				paginate: {
					first:      'Primeira',
					previous:   'Anterior',
					next:       'Seguinte',
					last:       '&uacute;ltima'
				},
				aria: {
					sortAscending:  ': habilite para classificar a coluna em ordem crescente',
					sortDescending: ': habilite para classificar a coluna em ordem decrescente'
				}
			}
		});

		$('.dataTables_filter input').attr('placeholder', 'Buscar');",
		array('inline' => false));
?>