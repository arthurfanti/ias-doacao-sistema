<fieldset role="filial">
	<div class="small-6 columns end">
		<?php echo $this->Form->input('DoadorEndereco.'.$index.'.cep', ['placeholder' => __('CEP'), 'class' => 'cep', 'required']); ?>
		<span class="error"><?php echo __('Preenchimento Obrigatório') ?></span>
	</div>

	<div class="row small-collapse medium-uncollapse">
		<div class="small-12 medium-7 columns">
			<?php echo $this->Form->input('DoadorEndereco.'.$index.'.logradouro', array('placeholder' => __('Logradouro') )) ?>
			<?php echo $this->Html->tag(
				'small',
				__('Preenchimento Obrigatório'),
				array('class' => 'error')
			) ?>
		</div>
		<div class="small-12 medium-2 columns">
			<?php echo $this->Form->input('DoadorEndereco.'.$index.'.numero', array('placeholder' => __('Número') , 'type' => 'text')) ?>
			<?php echo $this->Html->tag(
				'small',
				__('Preenchimento Obrigatório'),
				array('class' => 'error')
			) ?>
		</div>
		<div class="small-12 medium-3 columns">
			<?php echo $this->Form->input('DoadorEndereco.'.$index.'.complemento', array('placeholder' => __('Complemento') )) ?>
		</div>
	</div>

	<div class="row small-collapse medium-uncollapse">
		<div class="small-12 medium-4 columns">
			<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
				<select required id="estado_id" name="data[DoadorEndereco][<?php echo $index ?>][estado_id]">
					<option value="">Estado</option>
					<option data-uf="AC" value="1">Acre</option>
					<option data-uf="AL" value="2">Alagoas</option>
					<option data-uf="AP" value="3">Amapá</option>
					<option data-uf="AM" value="4">Amazonas</option>
					<option data-uf="BA" value="5">Bahia</option>
					<option data-uf="CE" value="6">Ceará</option>
					<option data-uf="DF" value="7">Distrito Federal</option>
					<option data-uf="ES" value="8">Espirito Santo</option>
					<option data-uf="GO" value="9">Goiás</option>
					<option data-uf="MA" value="10">Maranhão</option>
					<option data-uf="MT" value="11">Mato Grosso do Sul</option>
					<option data-uf="MS" value="12">Mato Grosso</option>
					<option data-uf="MG" value="13">Minas Gerais</option>
					<option data-uf="PA" value="14">Pará</option>
					<option data-uf="PB" value="15">Paraíba</option>
					<option data-uf="PR" value="16">Paraná</option>
					<option data-uf="PE" value="17">Pernambuco</option>
					<option data-uf="PI" value="18">Piauí</option>
					<option data-uf="RJ" value="19">Rio de Janeiro</option>
					<option data-uf="RN" value="20">Rio Grande do Norte</option>
					<option data-uf="RS" value="21">Rio Grande do Sul</option>
					<option data-uf="RO" value="22">Rondônia</option>
					<option data-uf="RR" value="23">Roraima</option>
					<option data-uf="SC" value="24">Santa Catarina</option>
					<option data-uf="SP" value="25">São Paulo</option>
					<option data-uf="SE" value="26">Sergipe</option>
					<option data-uf="TO" value="27">Tocantins</option>
				</select>
				<?php echo $this->Html->tag(
					'small',
					__('Preenchimento Obrigatório'),
					array('class' => 'error')
				) ?>
			<?php else: ?>
				<?php echo $this->Form->input('DoadorEndereco.'.$index.'.estado', array('type' => 'text', 'placeholder' => __('Estado'))) ?>
			<?php endif ?>
		</div>

		<div class="small-12 medium-4 columns">
			<?php if ($this->Session->check('Config.language') && $this->Session->read('Config.language') == 'por'): ?>
				<select required id="cidade_id" name="data[DoadorEndereco][<?php echo $index ?>][cidade_id]">
					<option disabled value="">Cidade</option>
				</select>
				<?php echo $this->Html->tag(
					'small',
					__('Preenchimento Obrigatório'),
					array('class' => 'error')
				) ?>
			<?php else: ?>
				<?php echo $this->Form->input('DoadorEndereco.'.$index.'.cidade', array('type' => 'text', 'placeholder' => __('Cidade'))) ?>
			<?php endif ?>
		</div>

		<div class="small-12 medium-4 columns">
			<?php echo $this->Form->input('DoadorEndereco.'.$index.'.bairro', array('placeholder' => __('Bairro') )) ?>
			<?php echo $this->Html->tag(
				'small',
				__('Preenchimento Obrigatório'),
				array('class' => 'error')
			) ?>
		</div>
		<div class="small-12 column">
			<a href="#" name="excluirFilial"><?php echo __('Excluir Filial') ?></a>
		</div>
	</div>
</fieldset>