<?php
	$subjects = $this->requestAction(
		array('plugin' => null, 'controller' => 'assuntos', 'action' => 'renderSubjects'),
		array('return')
	);
?>
	<div class="row">
		<!-- Reveal Modals begin -->
		<div id="modal-contato-resposta" class="small-8 columns reveal-modal alert-box text-center" data-reveal>
			<div class="small-12">
				<div id="success-msg" class="hide">
					<p><?= __('Sua mensagem foi enviada com sucesso, em breve nossa equipe entrará em contato com você')?>.</p>
					<small><?= __('Você também pode entrar em contato por telefone: 11 2974-3062')?>.</small>
				</div>
				<div id="error-msg" class="hide">
					<p><?= __('Ocorreu um erro no envio da sua mensagem, tente novamente mais tarde.')?>.</p>
					<small><?= __('Você também pode entrar em contato por telefone: 11 2974-3062')?>.</small>
				</div>
			</div>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<!-- Reveal Modals end -->
	</div>

	<div class="row">
		<!-- Reveal Modals begin -->
		<div id="modal-contato" class="small-8 columns reveal-modal alert-box info text-center" data-reveal>
			<form action="#" class="row" id="formFaleConosco" data-abide="ajax">
				<div class="small-12">
					<p class="lead"><?= __('Para dúvidas, sugestões, críticas ou informações, entre em contato conosco. Será um prazer ouví-lo')?>.</p>
				</div>

				<div class="small-12">
					<input type="text" name="contato-nome" value="" placeholder="<?= __('Nome') ?>" required>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>

				<div class="small-12">
					<div class="row">
						<div class="small-6 columns">
							<input type="text" name="contato-email" value="" placeholder="<?= __('Email') ?>" required pattern='email'>
					    <small class="error"><?= __('Campo Obrigatório') ?></small>
						</div>
						<div class="small-6 columns">
							<input type="tel" name="contato-telefone" value="" placeholder="<?= __('Telefone') ?>">
					    <small class="error"><?= __('Campo Obrigatório') ?></small>
						</div>
					</div>
				</div>

				<div class="small-12">
					<!-- <input type="text" name="contato-assunto" value="" placeholder="<?= __('Assunto') ?>" required> -->
					<select required name="contato-assunto" id="contatoAssunto">
						<option selected disabled value=""><?php echo __('Assunto') ?></option>
						<?php foreach ($subjects as $sbjct): ?>
							<option data-recipient="<?php echo $sbjct['Assunto']['destino'] ?>" value="<?php echo $sbjct['Assunto']['titulo'] ?>"><?php echo $sbjct['Assunto']['titulo'] ?></option>
						<?php endforeach ?>
					</select>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>

				<?php echo $this->Form->input('recipient', array('type' => 'hidden', 'id' => 'recipientEmail', 'required' => false)); ?>
				<div class="small-12">
					<textarea name="contato-mensagem" id="" cols="30" rows="10" placeholder="<?= __('Escreva sua mensagem') ?>" required></textarea>
					<small class="error"><?= __('Campo Obrigatório') ?></small>
				</div>

				<div class="small-12">
					<div class="row">
						<div class="small-6 columns">
							<input type="submit" class="button small-12" value="<?= __('Enviar') ?>">
						</div>
						<div class="small-6 columns">
							<button class="close-reveal-modal button small-12"><?= __('Cancelar') ?></button>
						</div>
					</div>
				</div>

			</form>
		</div>
		<!-- Reveal Modals end -->
	</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#contatoAssunto').change(function(event) {
			var recipient = $(this).find('option:selected').data('recipient');
			$('#recipientEmail').val(recipient);
		});

		function respostaModal($status){
			if ($status == 'success') {
				$('#modal-contato-resposta #error-msg').addClass('hide');
				$('#modal-contato-resposta #success-msg').removeClass('hide');
				$('#modal-contato-resposta').removeClass('alert').addClass('success');
			} else if($status == 'error'){
				$('#modal-contato-resposta #success-msg').addClass('hide');
				$('#modal-contato-resposta #error-msg').removeClass('hide');
				$('#modal-contato-resposta').removeClass('success').addClass('alert');
			};

			$('#modal-contato').foundation('reveal', 'close');
			closeMyModal = 1;
			$(document).bind('closed.fndtn.reveal', '[data-reveal]', function() {
				if (closeMyModal <= 1) {
					$('#modal-contato-resposta').foundation('reveal', 'open');
				  closeMyModal ++;
				};
			});
		};

		$("#formFaleConosco").on('valid', function(event) {
			event.preventDefault();
			var dados = $(this).serializeArray();

			// console.log(JSON.stringify(dados));
			// return;

			$.ajax({
				type: 'POST',
				url: '<?php echo $this->Html->url(array('controller' => 'paginas', 'action' => 'fale_conosco')) ?>',
				data: dados,
				dataType: 'JSON',
				success: function(data){
					if (data.retorno) {
						respostaModal('success');
					}
				},
				error: function(data){
					respostaModal('error');
				}
			});
		});

	});
</script>