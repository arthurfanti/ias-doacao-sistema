	<div class="row">
		<!-- Reveal Modals begin -->
		<div id="modal-indicacao" class="small-8 columns reveal-modal alert-box info text-center" data-reveal>
			<!-- <form action="#"> -->
			<?php echo $this->Form->create('Pagina', array('action' => 'mandrill', 'data-abide')) ?>
				<p class="lead"><?= __('Insira o email com quem você quer compartilhar essa ideia')?>.</p>
				<div class="row">
					<div class="small-12 columns">
						<label style="float:left; color:white" for="share-email"><?= __('Email') ?></label>
						<input required type="text" name="share-email" id="share-email">
						<span class="error"><?php echo __('Campo Obrigatório') ?></span>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label style="float:left; color:white" for="share-message"><?= __('Mensagem') ?></label>
						<textarea required name="share-message" id="share-message" cols="30" rows="10"></textarea>
						<span class="error"><?php echo __('Campo Obrigatório') ?></span>
					</div>
				</div>
				<input type="submit" class="button expand" value="<?= __('Enviar') ?>">
			</form>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<!-- Reveal Modals end -->
	</div>