	<div class="row">
		<!-- Reveal Modals begin -->
		<div id="modal-aceite" class="reveal-modal alert-box text-left" data-reveal>
			<div class="row">			
				<div class="small-12">
					<div class="row">
						<div class="small-8 columns">
							<h2><?php echo $aceite['Aceite']['titulo'] ?></h2>
						</div>
						<!-- <div class="small-3 columns options">
							<a href="./pdfs/termos_de_aceite_en.pdf" target="_blank" class="button small left pdf"></a>
							<a href="./pdfs/termos_de_aceite_en.pdf" target="_blank" class="button small left print"></a>
							<a href="./pdfs/termos_de_aceite_en.pdf" target="_blank" class="button small left download"></a>
						</div> -->
						<div class="small-1 columns">
							<a class="close-reveal-modal">&#215;</a>
						</div>
					</div>
				</div>

				<div class="small-12" id="termo">
					<?php echo $aceite['Aceite']['termo'] ?>
				</div>

			</div>
		</div>
		<!-- Reveal Modals end -->
	</div>