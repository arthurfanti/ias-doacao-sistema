<div data-alert class="alert-box alert" style="width:100%;">
	<div class="row">
		<div class="small-12 column">
			<p>
				<?php echo $message ?>
			</p>
		</div>
	</div>
	<a href="" class="close">&times;</a>
</div>