<article id="list" style="min-height:75vh">
	<div class="row">
		<div class="small-12 column">
			<table class="display responsive nowrap">
				<thead>
					<th></th>
					<th>Campanha</th>
					<th>Data Inicial</th>
					<th>Data Final</th>
					<th>Opções</th>
				</thead>
				<tfoot>
					<th></th>
					<th>Campanha</th>
					<th>Data Inicial</th>
					<th>Data Final</th>
					<th>Opções</th>
				</tfoot>
				<tbody>
					<?php foreach ($campanhas as $item) : ?>
					<tr>
						<td></td>
						<td><?php echo $item["Campanha"]["campanha"] ?></td>
						<td><?php echo $item['Campanha']['data_inicial'] ?></td>
						<td><?php echo $item['Campanha']['data_final'] ?></td>
						<td>
							<?php echo $this->Html->link(
							$this->Html->tag('i', ' Editar', array('class' => 'fa fa-edit')),
							array('action' => 'alterar', $item["Campanha"]["id"]),
							array(
								'class'  => 'button tiny info radius',
								'style'  => 'margin-bottom:0; margin-right:1rem;',
								'escape' => false
							)
						); ?>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>

		</div>
	</div>
</article>

<?php
	echo $this->Html->scriptBlock(
		"$('table.responsive.nowrap').DataTable({
			responsive: {
				details: {
					type: 'inline',
					target: 'tr'
				}
			},
			columnDefs: [{
				className: 'control',
				orderable: false,
				targets: 0
			}],
			order: [1, 'asc'],
			language: {
				processing:     'processando dados...',
				search:         '',
				lengthMenu:     'Itens por p&aacute;gina: _MENU_',
				info:           'Exibindo itens de _START_ a _END_, num total de _TOTAL_ itens',
				infoEmpty:      'Não há itens para exibir',
				infoFiltered:   '(filtrado de _MAX_ itens no total)',
				infoPostFix:    '',
				loadingRecords: 'carregando dados...',
				zeroRecords:    'Não há itens para exibir',
				paginate: {
					first:      'Primeira',
					previous:   'Anterior',
					next:       'Seguinte',
					last:       '&uacute;ltima'
				},
				aria: {
					sortAscending:  ': habilite para classificar a coluna em ordem crescente',
					sortDescending: ': habilite para classificar a coluna em ordem decrescente'
				}
			}
		});

		$('.dataTables_filter input').attr('placeholder', 'Buscar');",
		array('inline' => false));
?>