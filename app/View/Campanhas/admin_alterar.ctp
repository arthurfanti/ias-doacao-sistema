<?php echo $this->Form->create(array('type' => 'file')) ?>
<?php echo $this->Form->hidden('id') ?>
<?php echo $this->Form->input('campanha', array('label' => 'Campanha')) ?>
<?php echo $this->Form->input('data_inicial', array('type' => 'text', 'label' => 'Data inicial', 'class' => 'date')) ?>
<?php echo $this->Form->input('data_final', array('type' => 'text', 'label' => 'Data final', 'class' => 'date')) ?>
<?php echo $this->Form->input('imagem', array('type' => 'file', 'div' => 'panel')) ?>
<?php echo $this->Form->input('obs', array('label' => 'Observação')) ?>
<?php echo $this->Form->submit('Salvar') ?>
<?php echo $this->Form->end() ?>